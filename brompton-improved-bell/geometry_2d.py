import math

import unittest

tc = unittest.TestCase('__init__')


class P:
    def __init__(self, x=None, y=None, length=None, angle=None):
        if length is not None and x is not None:
            self.x = x
            self.y = math.sqrt(length * length - x * x)
        elif length is not None and y is not None:
            self.x = math.sqrt(length * length - y * y)
            self.y = y
        elif angle is not None and x is not None:
            self.x = x
            self.y = math.tan(math.radians(angle)) * x
        elif angle is not None and y is not None:
            self.x = 1 / (math.tan(math.radians(angle)) / y)
            self.y = y
        elif length is not None and angle is not None:
            self.x = math.cos(math.radians(angle)) * length
            self.y = math.sin(math.radians(angle)) * length
        else:
            self.x = x
            self.y = y
        if self.x is None or self.y is None:
            raise ('boop')

    def __str__(self):
        return  f"({self.x}, {self.y})"

    def __add__(self, other):
        return P(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return P(self.x - other.x, self.y - other.y)

    def __mul__(self, o):
        if isinstance(o, P):
            p = o
            return P(self.x * p.x, self.y * p.y)
        else:
            scalar = o
            return P(self.x * scalar, self.y * scalar)

    def __truediv__(self, scalar):
        return P(self.x / scalar, self.y / scalar)

    def distance(self, other):
        return math.sqrt(math.pow(self.x - other.x, 2) + math.pow(self.y - other.y, 2))

    def angle(self):
        if self.x == 0.0:
            return 90 if self.y > 0 else -90
        return math.degrees(math.atan(self.y / self.x))

    def length(self):
        return self.distance(P(0, 0))

    def abs(self):
        return P(abs(self.x), abs(self.y))

    def rotate(self, degrees):
        d = P(0, 0).distance(self)
        angle = self.angle()
        new_x = d * math.cos(math.radians(angle + degrees))
        new_y = d * math.sin(math.radians(angle + degrees))
        return P(new_x, new_y)

    def middle(self, other):
        return middle(self, other)

    def values(self):
        return (self.x, self.y)

    def invert_x(self):
        return P(0 - self.x, self.y)

    def invert_y(self):
        return P(self.x, 0 - self.y)

    def invert(self):
        return P(0 - self.x, 0 - self.y)


def polarCoordinate(length, angle):
    return P(math.cos(math.degrees(angle)) * length, math.sin(math.degrees(angle)) * length)


def testPoint(actual, expected):
    tc.assertAlmostEqual(actual.x, expected.x)
    tc.assertAlmostEqual(actual.y, expected.y)


testPoint(P(x=1, angle=45), P(1, 1))
testPoint(P(x=math.sqrt(3) / 2, angle=30), P(math.sqrt(3) / 2, 0.5))

testPoint(P(y=1, angle=45), P(1, 1))
testPoint(P(y=math.sqrt(3) / 2, angle=60), P(0.5, math.sqrt(3) / 2))

testPoint(P(length=math.sqrt(1 * 1 + 1 * 1), y=1), P(1, 1))
testPoint(P(length=math.sqrt(1 * 1 + 1 * 1), x=1), P(1, 1))

testPoint(P(length=math.sqrt(2 * 2 + 1 * 1), x=1), P(1, 2))
testPoint(P(length=math.sqrt(2 * 2 + 1 * 1), y=1), P(2, 1))

# testPoint(P(length=1, angle=0), P(1, 0))
testPoint(P(length=1, angle=90), P(0, 1))
testPoint(P(length=1, angle=180), P(-1, 0))
testPoint(P(length=1, angle=270), P(0, -1))

testPoint(P(0, 0) + P(2, 0), P(2, 0))
testPoint(P(1, 0).rotate(90), P(0, 1))
testPoint(P(1, 0).rotate(-90), P(0, -1))
testPoint(P(1, 0).rotate(0), P(1, 0))

testPoint(P(2, 1).rotate(0), P(2, 1))
testPoint(P(math.sqrt(2) / 2, math.sqrt(2) / 2).rotate(45), P(0, 1))
testPoint(P(math.sqrt(2) / 2, math.sqrt(2) / 2).rotate(-45), P(1, 0))

testPoint(P(math.sqrt(2) / 2, -math.sqrt(2) / 2).rotate(-45), P(0, -1))

testPoint(P(0.000000001, 6).rotate(0), P(0, 6))
testPoint(P(0, 6).rotate(0), P(0, 6))


def distance_across_circle(distance, radius):
    return math.degrees(math.acos(1 - ((distance * distance) / (2 * radius * radius))))


def distance_along_line(xy0, target_xy, dist):
    normalized = target_xy - xy0

    target_length = xy0.distance(target_xy)

    ratio = dist / target_length

    return xy0 + (normalized * ratio)


def middle(xy0, xy1):
    return distance_along_line(xy0, xy1, xy0.distance(xy1) / 2)


testPoint(distance_along_line(P(0, 0), P(10, 0), 2), P(2, 0))
testPoint(distance_along_line(P(0, 0), P(10, 10), math.sqrt(2)), P(1, 1))


def get_rectangle_xy(xy_start, rectangle_vector, rectangle_width):
    angle = rectangle_vector.angle()
    half_width = rectangle_width / 2

    top_left = xy_start + P(length=half_width, angle=angle + 90)
    bottom_left = xy_start + P(length=half_width, angle=angle - 90)

    top_right = xy_start + rectangle_vector + P(length=half_width, angle=angle + 90)
    bottom_right = xy_start + rectangle_vector + P(length=half_width, angle=angle - 90)

    return [top_left, bottom_left, bottom_right, top_right]


def find_circle(b, c, d):
    temp = c[0]**2 + c[1]**2
    bc = (b[0]**2 + b[1]**2 - temp) / 2
    cd = (temp - d[0]**2 - d[1]**2) / 2
    det = (b[0] - c[0]) * (c[1] - d[1]) - (c[0] - d[0]) * (b[1] - c[1])

    if abs(det) < 1.0e-10:
        return None

    # Center of circle
    cx = (bc*(c[1] - d[1]) - cd*(b[1] - c[1])) / det
    cy = ((b[0] - c[0]) * cd - (c[0] - d[0]) * bc) / det

    radius = ((cx - b[0])**2 + (cy - b[1])**2)**.5

    return P(cx, cy), radius


# w = 35/2
# d = 28
#
# p, r = find_circle((-35, 5), (0,28), (35, 5))
#
# print(f"{p}: {r}")