import cadquery as cq

from geometry_2d import P as P

bell = (
    cq.Workplane("XY")
        .circle(32 / 2)
        .workplane(offset=6)
        .circle(38 / 2)
        .loft(combine=True)
)

bell = bell.union((
    cq.Workplane("XY")
        .circle(38 / 2)
        .extrude(7)
        .translate((0, 0, 6))
))

bell_base_depth = 5.5

bell = bell.union((
    cq.Workplane("XZ")
        .center(-16, 0)
        .moveTo(0, 0)
        .lineTo(1, -3)
        .threePointArc((10, -3), (16, -bell_base_depth))
        # .lineTo(10, -3)
        # .lineTo(16, -bell_base_depth)
        .lineTo(16+7, -bell_base_depth)
        .lineTo(16+7, 0)
        .close()
        .extrude(14)
        .translate((0, 7.001, 0))
))

f = (
    cq.Workplane("XZ")
        .center(-16, 0)
        .moveTo(-5, 0)
        .lineTo(-5, -6)
        .lineTo(12, -7)
        # .lineTo(-2, -3)
        .lineTo(15, -10)
        .lineTo(16+7 + 9, -10)
        .lineTo(16+7 + 9, 0)
        .close()
        .extrude(25)
        .translate((0, 25/2, 0))
        .edges("|Z")
        .fillet(12)
        .edges("|X or |Y")
        .fillet(2)
)

bell = bell.union((
    cq.Workplane("XY")
        .center(32/2 - 9, 0)
        .circle(14 / 2)
        .extrude(-bell_base_depth)

))

screw_r = 4.2 / 2
bell = bell.union((
    cq.Workplane("XY")
        .center(32/2 - 9, 0)
        .circle(screw_r)
        .extrude(-10)

))

screw_chamfer_step = 2.5
screw_chamfer_step_margin = 0
bell = bell.union((
    cq.Workplane("XY")
        .center(32/2 - 9, 0)
        .circle(screw_r)
        .workplane(offset=-screw_chamfer_step - screw_chamfer_step_margin)
        .circle(screw_r + screw_chamfer_step + screw_chamfer_step_margin)
        .loft(combine=True)
        .translate((0, 0, -(10-screw_chamfer_step)))

))

b = (
    cq.Workplane("XY")
        .lineTo(0, 10)
        .lineTo(7, 9)
        .lineTo(12, 8)
        # .lineTo(12, 5)
        # .lineTo(12, 0)
        .lineTo(12, -7)
        .lineTo(0, -13)
        # .close()
        .mirrorY()
        .extrude(2.5 + 5)
        .edges("|Z")
        .fillet(5)
        .edges("<Z")
        .fillet(1)
        .edges(">Z")
        .fillet(2)
)

# b = b.union((
#     cq.Workplane("XY")
#         .circle(25 / 2)
#         .extrude(5)
#         .translate((0, 0, 2.5))
#         .edges("<Z or >Z")
#         .fillet(2)
# ))

# b = b.union((
#     cq.Workplane("XY")
#         .circle(32 / 2)
#         .extrude(5)
#         .translate((15, 0, 2.5))
#         .edges("<Z or >Z")
#         .fillet(2)
# ))

# b = b.union((
#     cq.Workplane("XY")
#         .moveTo(25.5, 35 / 2)
#         .lineTo(-9, 22.5)
#         .lineTo(-12, 5)
#         .lineTo(0, -25 / 2)
#         # .lineTo(0, 0)
#         .lineTo(10, -7.5)
#         .close()
#         .extrude(5)
#         .edges("<Z or >Z")
#         .fillet(2)
#         .translate((0, 0, 2.5))
# ))

g = (
    cq.Workplane("XY")
    .rect(10, 50)
    .extrude(4.5)
    .translate((0, -1, 2.5))
    # .edges("|Z")
    .fillet(1.5)
)

b = (
    b
        # .edges("|Z")
        # .fillet(2)
        # .edges("<Z")
        # .fillet(1)
)

stadium_cut = (
    cq.Workplane("XY")
        .rect(6.25, 14.5)
        .extrude(2)
        .edges("|Z")
        .fillet(6.2 / 2)
)

for y in [-4.0, 4.0]:
    hole = (
        cq.Workplane("XY")
            .circle(3.5 / 2)
            .extrude(10)
            .translate((0, y, 0))
    )
    hole_chamfer = (
        cq.Workplane("XY")
            .circle(6 / 2)
            .extrude(5)
            .translate((0, y, 2 + 4))
    )
    stadium_cut = stadium_cut.union(hole)
    stadium_cut = stadium_cut.union(hole_chamfer)

stadium_cut = stadium_cut.translate((-7, 0, 0))

b = b.cut(stadium_cut)

bell = bell.translate((15, 0, 7))
f = f.translate((15, 0, 7))

# f_x_offset = 0.5
f_z_offset = 3 + 3

bell = bell.rotate((0, 0, 0), (0, 0, 1), -5)
f = f.rotate((0, 0, 0), (0, 0, 1), -5)

# bell_offset = (-3, 22, f_z_offset)
bell_offset = (0, 0, f_z_offset)
bell_offset = (-15, 0, f_z_offset - 1)

bell = bell.translate(bell_offset)
f = f.translate(bell_offset)

# b = b.union(f)
top = f
b = b.union(g)

top = top.cut(bell)
b = b.cut(bell)
b = b.cut(top)

show_object(bell, name=f'bell', options={"color": (125, 45, 45)})

show_object(b, name=f'base')
show_object(top, name=f'top')

s = stadium_cut

# show_object(s, name=f'stadium')


cq.exporters.export(b, "brompton_improved_bell_base.step")