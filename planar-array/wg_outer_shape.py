from hacked_cq_cache import cq_cache

import cadquery as cq
# from wg_shapes import *
import wg_shapes
from wg_classes import *
from wg_functions import *
import wg_vars
import wg_drivers
import wg_ply


# def windows():

def edge_ply_cutout(s: WgShell, r1, ws, shell_thickness):
    w = s.w / 2

    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    angle = s.angle

    def half(ss, w0, w, d, e, sign=1):
        # h0 = -ws.z_offset - 0.5 * shell_thickness
        h0 = -ws.z_offset - ws.base_thickness - shell_thickness / 2

        if angle == r1:
            h0 += 0.25

        ss = ss.segment((-(ws.p_offset.x + ws.cutout_diameter / 2) * sign, h0),
                        (-w * sign - shell_thickness, e * sign))

        ss = ss.segment((-2 * w * sign, e * sign))

        ss = ss.segment((-2 * w0 * sign, h0))

        return ss

    s = cq.Sketch()

    s = half(s, sign=-1, w0=w, w=sf.w / 2, d=sf.d / 2, e=sf.edge_width / 2)

    s = s.hull()

    return s


@cq_cache()
def calc_woofer_flat_edge(wg_slice: WgSlice):
    return wg_shapes.wg_shell(
        radius_list=wg_slice.radius_list,
        slice_function=edge_ply_cutout,
        r1=wg_slice.radius_list[1],
        ws=wg_slice.ws,
        shell_thickness=wg_slice.shell_thickness,
    )


@cq_cache()
def calc_vents(wg_slice: WgSlice):
    ws = wg_slice.ws
    shell_thickness = wg_slice.outer_shell_thickness
    h = wg_slice.h
    # vent_h_offset = - 4 * shell_thickness
    vent_h_offset = 1

    ply_middle_with_shell = wg_shapes.calc_ply_middle_with_shell(wg_slice, shell_thickness)

    sss = (
        cq.Workplane("XY")
            .box(80, 80, wg_slice.rib_width)
    )

    front_ribs = []
    extra_r = [wg_slice.front_ribs_r[0] - 0.15, wg_slice.front_ribs_r[0] + 0.15] if wg_slice.ws.ordinal == 0 else []
    for rib_r in wg_slice.front_ribs_r + extra_r:
        front_ribs.append(move_degree(rib_r, sss))
    front_middle_slot_ribs = union_shapes_list(front_ribs)

    rear_ribs = []
    for rib_r in wg_slice.rear_ribs_r:
        rear_ribs.append(move_degree(rib_r, sss))
    rear_middle_slot_ribs = union_shapes_list(rear_ribs)

    front_vent_slots = wg_drivers.place_woofer(
        ws=ws,
        angle=ws.cbt_angle,
        shape=(
            cq.Workplane("XY")
                .center(0, TWEETER_HEIGHT / 2 if wg_slice.topmost_part else 0)
                .rect(60, h + vent_h_offset)
                .extrude(-40)
        ),
    )
    if front_middle_slot_ribs is not None:
        front_vent_slots = front_vent_slots.cut(front_middle_slot_ribs)

    rear_vent_slots = wg_drivers.place_woofer(
        ws=ws,
        angle=ws.cbt_angle,
        shape=(
            cq.Workplane("XY").workplane(offset=0)
                .center(0, TWEETER_HEIGHT / 2 if wg_slice.topmost_part else 0)
                .rect(60, h + vent_h_offset)
                .extrude(40)
        ),
    )
    rear_vent_slots = rear_vent_slots.cut(wg_shapes.calc_outer_edge_rear(wg_slice))
    if rear_middle_slot_ribs is not None:
        rear_vent_slots = rear_vent_slots.cut(rear_middle_slot_ribs)

    vents = rear_vent_slots.union(front_vent_slots)

    # vents = vents.cut(middle_slot_ribs)
    vents = vents.cut(ply_middle_with_shell)
    vents = vents.cut(wg_shapes.woofer_flat_slice(wg_slice, woofer_cutout_hole=False))

    return vents


@cq_cache()
def edge_triangle_support(angle, ws, width, z_offset):
    s = WgShell(angle=angle)

    return wg_drivers.place_woofer(
        ws=ws,
        angle=angle,
        shape=(
            cq.Workplane("XY")
                .workplane(offset=0)
                .rect(25, 1.5 * width)
                .workplane(offset=0.14 * s.w)
                .rect(5, 1.5 * width)
                .loft(combine=True)
                .translate((12.5, 0, -z_offset))
        ),
    )


@cq_cache()
def edge_triangle_supports(wg_slice: WgSlice, width, z_offset):
    wg_full = wg_shapes.wg_throat_with_verts_shape(wg_slice.radius_list, shell_vert=False)

    s1 = edge_triangle_support(wg_slice.r0, wg_slice.ws, width, z_offset)
    s2 = edge_triangle_support(wg_slice.r2, wg_slice.ws, width, z_offset)

    woof_flat = s1.union(s2).intersect(wg_full)

    return woof_flat


@cq_cache()
def wg_slice_outer_base(wg_slice: WgSlice):
    # r0, r1, r2 = radius_list[0], radius_list[1], radius_list[2]

    wg_full = wg_shapes.wg_throat_with_verts_shape(wg_slice.radius_list, shell_vert=False)
    woofer_front_box_shape = wg_shapes.woofer_front_box(wg_slice)

    wg = (
        wg_shapes.wg_throat_with_verts_shape(wg_slice.radius_list, shell=-wg_slice.outer_shell_thickness, shell_vert=False)
            .intersect(woofer_front_box_shape)
    )
    wg = wg.union(wg_shapes.woofer_flat_slice(wg_slice))

    if wg_slice.topmost_part:
        top = move_degree(wg_slice.r2, (
            cq.Workplane("XY")
                .box(50, 50, 1)
        ))
        top = top.intersect(wg_full)
        wg = wg.union(top)

    vent_slots = calc_vents(wg_slice)

    wg_rear = (
        wg_shapes.wg_throat_with_verts_shape(wg_slice.radius_list, shell=-wg_slice.shell_thickness, shell_vert=False)
            .cut(woofer_front_box_shape)
            .cut(vent_slots)
    )

    wg = wg.cut(vent_slots)
    wg = wg.union(wg_rear)

    ply_middle_with_shell = wg_shapes.calc_ply_middle_with_shell(wg_slice, wg_slice.outer_shell_thickness)

    wg = wg.union(
        wg_ply.ply_peg_supports(wg_slice, peg_fillet=0.5, inner=False)
            .cut(wg_shapes.woofer_front_box(wg_slice, extrude=2))
    )
    screw_supports = wg_ply.ply_hole_supports_inf(wg_slice, pegs=False, screws=True)
    screw_supports = screw_supports.intersect(wg_full)
    wg = wg.union(screw_supports)

    if wg_slice.screw_holes.middle_screws:
        mid_screw_supports = wg_ply.ply_hole_supports_inf(wg_slice, pegs=False, screws=False, middle_screws=True, screw_width_multi=40, screw_height_multi=3.5)
        mid_screw_supports = mid_screw_supports.intersect(wg_full)
        mid_screw_supports = mid_screw_supports.intersect(ply_middle_with_shell)
        mid_screw_supports = mid_screw_supports.intersect(woofer_front_box_shape)
        wg = wg.union(mid_screw_supports)

    front_inner_ribs = wg_ply.rib_support_inf(wg_slice, wg_slice.front_inner_ribs_r)
    front_inner_ribs = front_inner_ribs.intersect(ply_middle_with_shell)
    front_inner_ribs = front_inner_ribs.intersect(woofer_front_box_shape)
    front_inner_ribs = front_inner_ribs.intersect(wg_full)
    wg = wg.union(front_inner_ribs)

    wg = wg.cut(wg_ply.ply_peg_holes(wg_slice))
    if PlyHoles(wg_slice).outer_pegs:
        wg = wg.cut(wg_ply.ply_peg_holes(wg_slice, outer_pegs=True))
    wg = wg.cut(wg_ply.wg_screws2(wg_slice))

    if wg_slice.ws.ordinal == 0:
        wg = wg.cut(ply_bot_support_slices())

    return wg


def ply_bot_support_slices():
    return move_degree(0, (
        cq.Workplane("XY")
            .rect(50, 50)
            .extrude(PLY_EDGE_THICKNESS_SINGLE * 2)
    ))


@cq_cache()
def wg_slice_outer2(wg_slice: WgSlice):
    wg = wg_slice_outer_base(wg_slice)

    # wg = wg.cut(wg_drivers.topmost_slice_middle_woofer_vents(wg_slice))

    wg = wg.cut(wg_drivers.place_woofers(wg_slice, mirror=True, margin=True, base_margin_cutout_depth=1.0))
    wg = wg.cut(wg_shapes.top_bot_margin_cuts(wg_slice))

    woofer_flat = wg_shapes.woofer_flat_slice(wg_slice, tweet_breathe_slots=True)
    wg = wg.cut(wg_shapes.calc_ply_middle_with_shell(wg_slice).cut(woofer_flat))
    wg = wg.intersect(wg_shapes.ply_outer_edge_box(wg_slice).union(woofer_flat))

    return wg


@cq_cache()
def wg_slice_outer(wg_slice: WgSlice, right=True, left=False):
    wg = wg_slice_outer2(wg_slice)

    text_r = wg_slice.r0 + wg_slice.middle_screw_offset_r

    z = -0.8
    if wg_slice.ws.ordinal in [5, 6]:
        z += 1.0

    if right and left:
        wg = wg.mirror(mirrorPlane='YZ', union=True)
    elif right:

        t = wg_drivers.place_woofer(
            ws=wg_slice.ws,
            angle=text_r,
            shape=(
                cq.Workplane("XY")
                    .text(txt=str(f"{wg_slice.ws.ordinal} R"), fontsize=1.0, distance=2, cut=True, clean=True, halign='right')
                    .rotate((0, 0, 0), (1, 0, 0), 180)
                    .translate((wg_slice.ws.diameter / 2, 0, z))
            ),
        )
        wg = wg.cut(t)

        h_screw = list(filter(lambda x: x.rear and x.bot, PlyHoles(wg_slice).screws))[0]
        tr = wg_ply.place_wg_shape(
            angle=h_screw.angle,
            y_offset=h_screw.y_offset,
            shape=(
                cq.Workplane("XY")
                    .text(txt=str(f"{wg_slice.ws.ordinal} R"), fontsize=0.75, distance=0.25, cut=True, clean=True)
                    .rotate((0, 0, 0), (0, 0, 1), 90)
                    .rotate((0, 0, 0), (0, 0, 1), h_screw.angle)
                    .translate((1.3, 0, PLY_EDGE_THICKNESS_COMBINED / 2 - 0.1))
                    .mirror(mirrorPlane='XZ', union=False)
            ))
        wg = wg.cut(tr)

    elif left:
        t = wg_drivers.place_woofer(
            ws=wg_slice.ws,
            angle=text_r,
            shape=(
                cq.Workplane("XY")
                    .text(txt=str(f"{wg_slice.ws.ordinal} L"), fontsize=1.0, distance=2, cut=True, clean=True, halign='left')
                    .rotate((0, 0, 0), (1, 0, 0), 180)
                    .rotate((0, 0, 0), (0, 0, 1), 180)
                    .translate((wg_slice.ws.diameter / 2, 0, z))
                    .mirror(mirrorPlane='XZ', union=False)
            ),
        )
        wg = wg.cut(t)

        h_screw = list(filter(lambda x: x.rear and x.bot, PlyHoles(wg_slice).screws))[0]
        tr = wg_ply.place_wg_shape(
            angle=h_screw.angle,
            y_offset=h_screw.y_offset,
            shape=(
                cq.Workplane("XY")
                    .text(txt=str(f"{wg_slice.ws.ordinal} L"), fontsize=0.75, distance=0.25, cut=True, clean=True)
                    .rotate((0, 0, 0), (0, 0, 1), 90)
                    .rotate((0, 0, 0), (0, 0, 1), -h_screw.angle)
                    .translate((1.3, 0, PLY_EDGE_THICKNESS_COMBINED / 2 - 0.1))
            ))
        wg = wg.cut(tr)

        wg = wg.mirror(mirrorPlane='YZ', union=False)

    return wg
