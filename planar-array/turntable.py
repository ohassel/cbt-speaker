import cadquery as cq

import colors
import wg_functions

TURNTABLE_URL = 'https://www.beslagsbutiken.se/vridplatta-lazy-susan-200-350-kg.html'
TURNTABLE_PDF = 'https://www.beslagsbutiken.se/mwdownloads/download/link/id/656/'
TURNTABLE_DIAMETER = 32.96
TURNTABLE_HEIGHT = 1.45

PLY_THICKNESS = 0.4

BOTTOM_PLY_SHEETS = 3
UPPER_PLY_SHEETS = 5

TOP_PLY_THICKNESS = UPPER_PLY_SHEETS * PLY_THICKNESS
BOTTOM_PLY_THICKNESS = BOTTOM_PLY_SHEETS * PLY_THICKNESS

TOP_THICKNESS = 0.3
DEGREE_LINE_WIDTH = 0.3

TOP_DIAMETER = 50

RENDER_TOP = True

turntable = (
    cq.Workplane("XY")
        .circle(TURNTABLE_DIAMETER / 2)
        .extrude(TURNTABLE_HEIGHT)
        .translate((0, 0, BOTTOM_PLY_THICKNESS))
)

bottom = (
    cq.Workplane("XY")
        .circle(TOP_DIAMETER / 2)
        .extrude(BOTTOM_PLY_THICKNESS)
)

base = (
    cq.Workplane("XY")
        .circle(TOP_DIAMETER / 2)
        .extrude(TOP_PLY_THICKNESS)
        .translate((0, 0, BOTTOM_PLY_THICKNESS + TURNTABLE_HEIGHT))
)

top = (
    cq.Workplane("XY")
        .circle(TOP_DIAMETER / 2)
        .extrude(TOP_THICKNESS)
        .translate((0, 0, BOTTOM_PLY_THICKNESS + TURNTABLE_HEIGHT + TOP_PLY_THICKNESS))
)


def calc_degree_lines(radii, width, offset=None):
    shapes = []

    for r in radii:
        s = (
            cq.Workplane("XY")
                .rect(width, TOP_DIAMETER)
                .extrude(TOP_THICKNESS)
                .translate((0, 0, BOTTOM_PLY_THICKNESS + TURNTABLE_HEIGHT + TOP_PLY_THICKNESS))
        )

        if offset:
            xy = (TOP_DIAMETER / 2) - offset
            s = s.cut((
                cq.Workplane("XY")
                    .rect(xy * 2, xy * 2)
                    .extrude(TOP_THICKNESS)
                    .translate((0, 0, BOTTOM_PLY_THICKNESS + TURNTABLE_HEIGHT + TOP_PLY_THICKNESS))
            ))

        if r != 0:
            s = s.rotate((0, 0, 0), (0, 0, 1), r)

        shapes.append(s)

    return wg_functions.union_shapes_list(shapes)


if RENDER_TOP:

    degree_lines = calc_degree_lines(
        radii=[0, 90],
        width=DEGREE_LINE_WIDTH,
        offset=None,
    )

    degree_lines = degree_lines.union(
        calc_degree_lines(
            radii=[15, 30, 45, 60, 75, 105, 120, 135, 150, 165],
            width=DEGREE_LINE_WIDTH,
            offset=10,
        )
    )

    degree_lines = degree_lines.union(
        calc_degree_lines(
            radii=[45, 135],
            width=DEGREE_LINE_WIDTH,
            offset=15,
        )
    )

    degree_lines_small = calc_degree_lines(
        radii=[5, 10, 20, 25, 35, 40, 50, 55, 65, 70, 80, 85, 95, 100, 110, 115, 125, 130, 140, 145, 155, 160, 170, 175],
        width=DEGREE_LINE_WIDTH,
        offset=2.5,
    )

    tt = [0, 180] + list(range(15, 180, 15)) + list(range(15, -180, -15))

    shapes = []
    shapes_cut = []

    for r in tt:

        s = (
            cq.Workplane("XY")
                .text(txt=str(abs(r)), fontsize=2.5, distance=10, cut=True, clean=True)
                .rotate((0, 0, 0), (0, 0, 1), 180)
                .translate((0, TOP_DIAMETER / 2 - 5, 0))
        )
        s_cut = (
            cq.Workplane("XY")
                .box(3, 3, 10)
                .translate((0, TOP_DIAMETER / 2 - 5, 0))
        )

        if r != 0:
            s = s.rotate((0, 0, 0), (0, 0, 1), r)
            s_cut = s_cut.rotate((0, 0, 0), (0, 0, 1), r)

        shapes.append(s)
        shapes_cut.append(s_cut)

    text = wg_functions.union_shapes_list(shapes)
    text_cut = wg_functions.union_shapes_list(shapes_cut)

    text = text.intersect(top)
    degree_lines = degree_lines.cut(text_cut)

    # degree_lines = degree_lines.union(degree_lines_small)

    top = top.cut(degree_lines)
    top = top.cut(degree_lines_small)
    top = top.cut(text)

show_object(bottom, name=f'bottom', options={"color": colors.brown})
show_object(base, name=f'base', options={"color": colors.brown})
show_object(top, name=f'top', options={"color": colors.light_grey})

if RENDER_TOP:
    show_object(degree_lines, name=f'degree_lines', options={"color": colors.red})
    show_object(degree_lines_small, name=f'degree_lines_small', options={"color": colors.black})
    show_object(text, name=f'text', options={"color": colors.black})

show_object(turntable, name='turntable', options={"color": colors.light_grey, "alpha": 0.0})
# show_object(text_cut, name=f'text_cut', options={"color": colors.black})
