from geometry_2d import find_circle as find_circle
import math
from wg_vars import *


def actual_distance_across_circle(degrees, radius=CBT_RADIUS):
    return 2 * radius * math.pi * (degrees / 360)


class WgShell:
    def __init__(self, angle, w=None, d=None, edge_width=None, front=False, rear=False, flat=False, radius=None):
        if radius is None:
            radius = CBT_RADIUS
        x = angle / CBT_DEGREES

        self.angle = angle

        # print(f"f={x}")
        side = Side(angle)

        self.edge_width = edge_width or (CBT_BOT_EDGE_WIDTH - (CBT_TOP_EDGE_WIDTH_DIFF * x))

        if CBT_LINEAR_WIDTH:
            self.w = w or (CBT_BOTTOM_WIDTH - (CBT_BOT_TOP_WIDTH_DIFF * x))
        else:
            w_offset = 0
            # print(f"side_angle_xz.angle(): {side_angle_xz.angle()}")
            if front:
                self.edge_width = self.edge_width * CBT_EDGE_FORWARD_RATIO
            if rear:
                self.edge_width = self.edge_width / CBT_EDGE_FORWARD_RATIO

            if front or rear:
                w_offset = P(x=self.edge_width, angle=side_angle_xz.angle()).y
            if rear:
                w_offset = -w_offset
            # print(f"w_offset: {w_offset}")

            # self.w = w or (CBT_BOTTOM_WIDTH - (CBT_BOT_TOP_WIDTH_DIFF * x))
            self.w = side.outer_cbt_width + w_offset
        self.d = d or (CBT_BOTTOM_DEPTH - (CBT_BOT_TOP_DEPTH_DIFF * x))
        # self.edge_width = edge_width or (CBT_BOT_EDGE_WIDTH - (CBT_TOP_EDGE_WIDTH_DIFF * x))

        if flat:
            d = actual_distance_across_circle(degrees=angle, radius=radius)
            self.angle = 0
            self.center = P(0, d)
        else:
            c = P(x=radius, y=0) + P(length=-radius, angle=angle)
            self.center = P(c.x, -c.y)

    def __str__(self):
        return f"({self.angle}, {self.edge_width}, {self.w}, {self.d}, {self.center})"

    def __repr__(self):
        return self.__str__()


class WgGeometry:
    def __init__(self, s: WgShell = None, p0=None, angle=None, w=None, d=None, edge_width=None, coverage_angle=None, throat_roundover=None,
                 edge_roundover=None):

        if not angle:
            angle = s.angle

        if not throat_roundover:
            throat_roundover = CBT_THROAT_ROUNDOVER
        if not edge_roundover:
            edge_roundover = CBT_EDGE_ROUNDOVER

        if s:
            angle_from_top = abs(CBT_DEGREES - angle)
            # print(f"angle: {angle}")
            # print(f"angle_from_top: {angle_from_top}")

            if angle_from_top < DEGREES_PER_TWEETER * 1.2:
                throat_roundover = CBT_THROAT_ROUNDOVER / 2
                edge_roundover = CBT_EDGE_ROUNDOVER / 2
            elif angle_from_top < DEGREES_PER_TWEETER * 2.2:
                throat_roundover = CBT_THROAT_ROUNDOVER / 1.5
                edge_roundover = CBT_EDGE_ROUNDOVER / 1.5

        if not coverage_angle:
            coverage_angle = CBT_COVERAGE_ANGLE

        wg_angle = 90 - (coverage_angle / 2)
        throat_max_angle = 90 - wg_angle

        # b = b.moveTo()

        # b = b.rect(pt2522_x_offset, pt2522_y_offset)

        if not p0:
            p0 = P(pt2522_x_offset, pt2522_y_offset)

        throat_roundover_center = p0 + P(x=throat_roundover, y=0)

        p1 = throat_roundover_center + P(length=throat_roundover, angle=180 - throat_max_angle)
        p01 = throat_roundover_center + P(length=throat_roundover, angle=180 - throat_max_angle * 0.5)

        # p1 = pc + P(length=throat_r, angle=throat_max_angle)
        #
        step = 20

        p2 = p1 + P(y=step, angle=wg_angle)

        if not w:
            w = s.w / 2
        if not d:
            d = s.d / 2
        #
        if not edge_width:
            edge_width = s.edge_width
        e = edge_width / 2

        arc_center, arc_radius = find_circle(
            (-w, e),
            (0, d),
            (w, e)
        )

        p_step = p2
        for i in range(0, 100):

            step = step / 2

            length_to_arc_center = (p_step - arc_center).length()
            delta_to_arc_center = arc_radius - length_to_arc_center

            # print(f"p2={p2}")
            # print(f"length_to_arc_center={length_to_arc_center}")
            # print(f"delta_to_arc_center={delta_to_arc_center}")

            if delta_to_arc_center > 0:
                sign = 1
            else:
                sign = -1

            p_step = p_step + P(y=step * sign, angle=wg_angle)

        p2 = p_step

        p_edge_roundover_center = p2 + P(length=-edge_roundover, angle=wg_angle) + P(length=-edge_roundover,
                                                                                     angle=90 + wg_angle)

        step = 10
        p_step = p_edge_roundover_center
        for i in range(0, 100):

            step = step / 2

            length_to_arc_center = (p_step - arc_center).length()
            delta_to_arc_center = arc_radius - length_to_arc_center - edge_roundover

            # print(f"p2={p2}")
            # print(f"length_to_arc_center={length_to_arc_center}")
            # print(f"delta_to_arc_center={delta_to_arc_center}")

            if delta_to_arc_center > 0:
                sign = 1
            else:
                sign = -1

            p_step = p_step + P(y=step * sign, angle=wg_angle)

        self.p_edge_roundover_center = p_step
        p_edge_roundover_center = p_step
        p3 = p_edge_roundover_center + P(length=edge_roundover, angle=90 + wg_angle)
        p4 = p_edge_roundover_center + P(length=edge_roundover, angle=(p_edge_roundover_center - arc_center).angle())
        p34 = p_edge_roundover_center + P(length=edge_roundover, angle=p3.angle() + p4.angle())

        edge_before = (P(w, e) - arc_center).rotate(5) + arc_center

        self.p0 = P(p0.x, 0)
        self.p1 = P(p0.x, p0.y)
        self.p2_arc = (P(p01.x, p01.y), P(p1.x, p1.y))
        self.p3 = P(p3.x, p3.y)
        self.p4_arc = (P(p34.x, p34.y), P(p4.x, p4.y))
        self.p5_arc = (P(edge_before.x, edge_before.y), P(w, e))
        self.p6 = P(w, 0)

        self.wg_edge_middle = self.p4_arc[0]

        # print(f"wg_edge_middle: {self.wg_edge_middle}")

    def __repr__(self):
        vars_list = [self.p0, self.p1, self.p2_arc, self.p3, self.p4_arc, self.p5_arc, self.p6, self.wg_edge_middle]
        return ', '.join(map(repr, vars_list))


wg_geometry_cache = {}


def wg_geometry_cached(s: WgShell):
    # return WgGeometry(s=s)
    key = str(s)
    if key in wg_geometry_cache:
        # print(f'cache hit on {key}')
        return wg_geometry_cache[key]
    else:
        # print(f'cache miss on {key}')
        g = WgGeometry(s=s)
        wg_geometry_cache[key] = g
        return g


class Side:
    def __init__(self, angle):
        self.outer_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_FRONT_XY.length(),
                                                              upper_width=CBT_TOP_FRONT_XY.length(),
                                                              angle_cutoff=CBT_DEGREES_INCL_TOP_WOOFER))
        self.inner_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_REAR_XY.length(),
                                                              upper_width=CBT_TOP_REAR_XY.length(),
                                                              angle_cutoff=CBT_DEGREES_INCL_TOP_WOOFER))

        self.outer_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.outer_xy.x).y or 0

        self.inner_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.inner_xy.x).y or 0

        self.outer_z_ratio = 1 - (self.outer_z / side_angle_xz.y)
        self.inner_z_ratio = 1 - (self.inner_z / side_angle_xz.y)

        self.outer_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.outer_z_ratio + CBT_TOP_WIDTH
        self.inner_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.inner_z_ratio + CBT_TOP_WIDTH

    def __repr__(self):
        vars_list = [
            self.outer_xy,
            self.inner_xy,
            self.outer_z,
            self.inner_z,
            self.outer_z_ratio,
            self.inner_z_ratio,
            self.outer_cbt_width,
            self.inner_cbt_width
        ]
        return ', '.join(map(repr, vars_list))


class WgSlice:
    def __init__(self, radius_list, tweeters, ws, shell_thickness, topmost_part, outer_shell_thickness=None):
        self.radius_list = radius_list
        self.tweeters = tweeters
        self.ws = ws
        self.shell_thickness = shell_thickness
        self.topmost_part = topmost_part

        self.outer_shell_thickness = outer_shell_thickness if outer_shell_thickness else shell_thickness
        self.vert_shell_thickness = max(0.4, 0.75 * self.shell_thickness)

        self.r0, self.r1, self.r2 = radius_list[0], radius_list[1], radius_list[2]
        self.s0, self.s1, self.s2 = WgShell(angle=self.r0), WgShell(angle=self.r1), WgShell(angle=self.r2)
        self.g0, self.g1, self.g2 = wg_geometry_cached(s=self.s0), wg_geometry_cached(s=self.s1), wg_geometry_cached(
            s=self.s2)
        self.h = self.s0.center.distance(self.s2.center)

        self.woofer_ply_cutout_y = shell_thickness * 1.0

        self.r_diff = self.r1 - self.r0
        self.radius_list_margin = [self.r0 - 1, self.r0, self.r1, self.r2, self.r2 + 1]

        self.pegs_screw_offset_r = distance_across_circle(1.0, CBT_RADIUS)
        self.screws_edge_offset_r = distance_across_circle(2.25 + (2.5 / 4) * self.shell_thickness, CBT_RADIUS)
        self.middle_screw_offset_r = self.screws_edge_offset_r - self.pegs_screw_offset_r - distance_across_circle(0.75, CBT_RADIUS)
        self.woofer_offset_r = distance_across_circle(self.ws.cutout_diameter / 2 + 0.7, CBT_RADIUS)

        edge_screws_r = [
            self.r0 + self.screws_edge_offset_r,
            self.r2 - self.screws_edge_offset_r,
        ]

        self.front_inner_ribs_r = edge_screws_r.copy()
        self.front_ribs_r = edge_screws_r.copy()
        front_mrr = calc_middle_rib_offset_r(self, front=True, rear=False)
        if front_mrr is None:
            pass
        elif front_mrr == 0:
            self.front_ribs_r.append(self.r1)
        else:
            self.front_ribs_r.append(self.r1 - front_mrr)
            self.front_ribs_r.append(self.r1 + front_mrr)

        self.rear_inner_ribs_r = []
        self.rear_ribs_r = []
        rear_mrr = calc_middle_rib_offset_r(self, front=False, rear=True)
        if rear_mrr is None:
            pass
        elif rear_mrr == 0:
            self.rear_ribs_r.append(self.r1)
        else:
            self.rear_ribs_r.append(self.r1 - rear_mrr)
            self.rear_ribs_r.append(self.r1 + rear_mrr)

        self.screw_holes = PlyHoles(self)

        self.rib_width = max(1.7 * shell_thickness, Screw(WG_PLY_SCREW_GAUGE).screw_head_diameter + 0.8)

    def __repr__(self):
        return f"{self.radius_list}, {self.tweeters}, {self.ws.__repr__}, {self.shell_thickness}, {self.topmost_part}"
        # d = {key: value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(value)}
        # return d.items().__repr__()


class PlyHoles:
    def __init__(self, wg_slice: WgSlice):
        r0, r1, r2 = wg_slice.r0, wg_slice.r1, wg_slice.r2
        ws = wg_slice.ws

        pegs_edge_offset_r = distance_across_circle(1.0, CBT_RADIUS)

        if wg_slice.ws.ordinal in [7, 8]:
            pegs_edge_offset_r *= 1.5

        pegs_screw_offset_r = wg_slice.pegs_screw_offset_r

        screws_edge_offset_r = wg_slice.screws_edge_offset_r
        middle_screw_offset_r = wg_slice.middle_screw_offset_r

        woofer_offset_r = wg_slice.woofer_offset_r

        topmost_part = wg_slice.topmost_part

        outer_pegs = []
        pegs = []
        # peg_y_offset = ws.z_offset + 1.5 * wg_slice.shell_thickness
        peg_y_offset = ws.z_offset + 1.0 * wg_slice.shell_thickness + 0.25
        middle_screw_y_offset = ws.z_offset + 2.0 * wg_slice.shell_thickness + 0.5
        # peg_y_offset = ws.z_offset + 4.0 * wg_slice.shell_thickness + 0.3
        # outer_pegs.append(
        #     PlyHole(angle=r0 + screws_edge_offset_r - distance_across_circle(0.5, CBT_RADIUS), y_offset=peg_y_offset, bot=True))
        # if not topmost_part:
        #     outer_pegs.append(
        #         PlyHole(angle=r2 - screws_edge_offset_r + distance_across_circle(0.5, CBT_RADIUS), y_offset=peg_y_offset, top=True))

        pegs.extend(PlyHoleFrontAndBack(wg_slice, angle=r0 + screws_edge_offset_r + pegs_screw_offset_r, bot=True).get_ply_holes())
        pegs.extend(PlyHoleFrontAndBack(wg_slice, angle=r2 - screws_edge_offset_r - pegs_screw_offset_r, top=True).get_ply_holes())
        self.outer_pegs = outer_pegs

        # pegs.extend(
        #     PlyHoleFrontAndBack(wg_slice, angle=r1, bot=False, top=False, extra_y_offset=-0.75).get_ply_holes(
        #         front=True, rear=True))

        self.pegs = pegs

        self.screw = Screw(WG_PLY_SCREW_GAUGE)
        screw_y_offset = 0
        if self.screw.m_gauge == 'm4':
            screw_y_offset = 0.25
        screws = []
        screws.extend(PlyHoleFrontAndBack(wg_slice, angle=r0 + screws_edge_offset_r, bot=True, extra_y_offset=screw_y_offset).get_ply_holes())

        screws.extend(PlyHoleFrontAndBack(wg_slice, angle=r2 - screws_edge_offset_r, top=True, extra_y_offset=screw_y_offset,
                                          # extra_y_offset=screw_extra_y_offset,
                                          # front_extra_y_offset=0.0 if topmost_part else 0,
                                          # rear_extra_y_offset=0.0 if topmost_part else 0,
                                          ).get_ply_holes())

        self.screws = screws

        self.middle_screw = Screw('m4')
        if wg_slice.ws.ordinal in [7, 8]:
            self.middle_screws = []
        else:
            self.middle_screws = [
                PlyHole(angle=r0 + screws_edge_offset_r if wg_slice.ws.ordinal == 0 else middle_screw_offset_r, y_offset=middle_screw_y_offset),
                PlyHole(angle=r2 - middle_screw_offset_r, y_offset=middle_screw_y_offset),
            ]

    def __repr__(self):
        d = {key: value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(value)}
        return d.items().__repr__()


class PlyHole:
    def __init__(self, angle, y_offset, bot=False, top=False, front=False, rear=False):
        self.angle = angle
        self.top = top
        self.bot = bot
        self.front = front
        self.rear = rear
        self.y_offset = y_offset

    def __repr__(self):
        d = {key: value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(value)}
        return d.items().__repr__()


class PlyHoleFrontAndBack:
    def __init__(self, wg_slice, angle, bot=False, top=False, extra_y_offset=0.0, front_extra_y_offset=0.0, rear_extra_y_offset=0.0):
        self.angle = angle
        self.top = top
        self.bot = bot

        sr = WgShell(angle=angle, rear=True)
        gr = wg_geometry_cached(sr)

        sf = WgShell(angle=angle, front=True)
        gf = wg_geometry_cached(sf)

        ratio = angle / CBT_DEGREES

        y_offset = extra_y_offset + 1.8 + 1 * math.pow((1 - ratio), 3)

        front_y_offset = y_offset - 0.5
        rear_y_offset = y_offset - 0.5

        if wg_slice.ws.ordinal in [2]:
            front_y_offset += 0.25
        if wg_slice.ws.ordinal in [3, 4]:
            front_y_offset += 0.375
            rear_y_offset += 0.25
        if wg_slice.ws.ordinal in [5, 6]:
            # front_y_offset -= 1.0
            front_y_offset += 0.5
            rear_y_offset += 0.5
        # if wg_slice.ws.ordinal == 1:
        #     front_y_offset -= 0.75
        #     rear_y_offset -= 0.5

        self.front_y = gf.p4_arc[0].y - front_y_offset - 0.1 + front_extra_y_offset
        self.rear_y = 0 - gr.p4_arc[0].y + rear_y_offset + 0.1 + rear_extra_y_offset

    def get_ply_holes(self, front=True, rear=True):
        l = []
        if front:
            l.append(PlyHole(angle=self.angle, y_offset=self.front_y, bot=self.bot, top=self.top, front=True, rear=False))
        if rear:
            l.append(PlyHole(angle=self.angle, y_offset=self.rear_y, bot=self.bot, top=self.top, front=False, rear=True))
        return l

    def __repr__(self):
        d = {key: value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(value)}
        return d.items().__repr__()


class Screw:
    def __init__(self, m_gauge):
        self.m_gauge = m_gauge
        self.screwdriver_shaft_diameter = 0.8
        if m_gauge == 'm3':
            self.screw_diameter_loose = 0.37
            self.screw_head_diameter = 0.6 + 0.2
            self.heatsert_short_depth = 0.4
            self.heatsert_long_depth = 0.91
            self.heatsert_diameter = 0.4
            self.heatsert_diameter_reinforced = self.heatsert_diameter * 2
        if m_gauge == 'm4':
            self.screw_diameter_loose = 0.47
            self.screw_head_diameter = 0.7 + 0.2
            self.heatsert_short_depth = 0.5
            self.heatsert_long_depth = 1.05
            self.heatsert_diameter = 0.56
            self.heatsert_diameter_reinforced = self.heatsert_diameter * 2
        if m_gauge == 'm5':
            self.screw_diameter_loose = 0.57
            self.screw_head_diameter = 0.91 + 0.3
            self.heatsert_short_depth = 0.68
            self.heatsert_long_depth = 1.37
            self.heatsert_diameter = 0.64
            self.heatsert_diameter_reinforced = self.heatsert_diameter * 2

    def __repr__(self):
        d = {key: value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(value)}
        return d.items().__repr__()


def calc_middle_rib_offset_r(wg_slice: WgSlice, front=True, rear=False):
    h = wg_slice.h

    if front:
        return 0
    if rear:
        return None

        middle_rib_offset = middle_rib_offset = h / 6

        # if wg_slice.ws.woofer_type == 'nd140':
        #     return None
        #     # middle_rib_offset = h / 6 - 0.25 * wg_slice.shell_thickness
        # elif wg_slice.ws.woofer_type == 'nd105':
        #     return None
        #     # middle_rib_offset = wg_slice.ws.magnet_diameter / 2
        #     # middle_rib_offset = h / 6 + 0.0 * wg_slice.shell_thickness
        # elif wg_slice.ws.woofer_type == 'nd91':
        #     return None
        #     # m = 0.8
        #     # if wg_slice.ws.ordinal == 6:
        #     #     m = 1.5
        #     # middle_rib_offset = h / 6 + m * wg_slice.shell_thickness
        # elif wg_slice.ws.woofer_type in ['nd64', 'nd65']:
        #     middle_rib_offset = h / 6 + 1.2 * wg_slice.shell_thickness

        return distance_across_circle(middle_rib_offset, CBT_RADIUS)
