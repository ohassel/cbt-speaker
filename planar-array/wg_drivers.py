import cadquery as cq
from hacked_cq_cache import cq_cache

import drivers

from wg_classes import *
import wg_functions
import wg_shapes
from wg_vars import *


def place_woofer(ws, angle, shape, s=None):
    w = shape
    if s is None:
        s = WgShell(angle=angle)

    w = w.rotate((0, 0, 0), (1, 0, 0), -90)

    if ws.angle and ws.angle != 0:
        w = w.rotate((0, 0, 0), (0, 0, 1), -ws.angle)
    if ws.z_angle and ws.z_angle != 0:
        w = w.rotate((0, 0, 0), (1, 0, 0), ws.z_angle)

    w = w.translate((ws.p_offset.x, 0, 0))
    w = w.translate((0, 0, ws.p_offset.y))
    w = w.translate((0, -ws.z_offset - ws.base_thickness, 0))

    w = w.rotate((0, 0, 0), (1, 0, 0), -angle)
    w = w.translate((0, s.center.x, s.center.y))

    return w


def place_tweeter_shape(wg_slice: WgSlice, shape, tweeter_r_list=None):
    shapes = []
    if tweeter_r_list is None:
        tweeter_r_list = wg_slice.tweeters
    for angle in tweeter_r_list:
        s = shape
        s = s.rotate((0, 0, 0), (0, 0, 1), 180)
        s = s.rotate((0, 0, 0), (1, 0, 0), -90)
        shapes.append(wg_functions.move_degree(angle, s))

    b = wg_functions.union_shapes_list(shapes)

    return b


@cq_cache()
def place_tweeter_flanges(wg_slice: WgSlice, fillet_width=None, mid_flare_height=None):
    return place_tweeter_shape(wg_slice, shape=drivers.pt2522_flanges(fillet_width=fillet_width,
                                                                      mid_flare_height=mid_flare_height))


def place_tweeter_heatsert_reinforcements(wg_slice: WgSlice, mirror=False):
    screw = Screw(TWEETER_SCREW_GAUGE)

    return tweeter_holes(
        wg_slice,
        diameter=screw.heatsert_diameter_reinforced,
        extrude=-(TWEETER_SCREW_LENGTH / 2 + 0.5),
        offset=0,
        mirror=mirror
    )


def place_tweeter_screws(wg_slice: WgSlice, mirror=False):
    screw = Screw(TWEETER_SCREW_GAUGE)

    top_heatserts = tweeter_holes(
        wg_slice,
        diameter=screw.heatsert_diameter,
        extrude=-(TWEETER_SCREW_LENGTH / 2 + 0.5),
        offset=0,
        mirror=mirror
    )
    bot_screws = tweeter_holes(
        wg_slice,
        diameter=screw.screw_diameter_loose,
        extrude=(TWEETER_SCREW_LENGTH / 2 + 0.5),
        offset=0,
        mirror=mirror
    )
    bot_screw_heads = tweeter_holes(
        wg_slice,
        diameter=screw.screw_head_diameter,
        extrude=1.25,
        offset=TWEETER_SCREW_LENGTH / 2,
        mirror=mirror
    )
    bot_screw_heads = bot_screw_heads.chamfer(0.45 * screw.screw_head_diameter)
    return (
        top_heatserts
            .union(bot_screws)
            .union(bot_screw_heads)
    )


def tweeter_holes(wg_slice, diameter, extrude, offset, mirror, sideways=False, place=True):
    holes = []
    for hole in drivers.get_pt2522_holes(mirror=mirror):
        holes.append((
            cq.Workplane("XY")
                .center(hole[0], hole[1])
                .circle(diameter / 2)
                .extrude(extrude)
                .translate((0, 0, offset))
        ))
    shape = wg_functions.union_shapes_list(holes)
    if sideways:
        shape = shape.rotate((0, 0, 0), (0, 0, 1), 90)
    if place:
        shape = place_tweeter_shape(wg_slice, shape=shape)
    return shape


@cq_cache()
def place_tweeters(wg_slice: WgSlice, cut_audio_holes=False, rotate_top_down=False, margin=False, margin_rear_termina_block=True, mirror_cut=False, sideways=False):
    kwargs = {
        "cut_audio_holes": cut_audio_holes,
        # render_screw_holes_and_rivets_and_terminals:not cheap_tweeter,
        "cut_screw_holes": False if margin else True,
        # union_heat_inserts:True,
        "with_margin": margin,
        "base_xy_margin_cm": 1.0 if margin else 0,
        "platform_xy_margin_cm": 0.2 if margin else 0,
        # union_heat_insert_through_hole_distance:2.0,
        #
        # rear_screw_holes_union_inner_height:-0.9,
        # rear_screw_holes_union_outer_height:-0.6,
        # rear_screw_holes_union_diameter:1.2,
    }
    if margin and margin_rear_termina_block:
        kwargs.update({
            "terminal_block_depth": 1.5,
            "terminal_tabs_height": None,
            "terminal_tab_width": None,
            "terminal_tabs_margin": 0.2,
            "terminal_block_chamfer": 0.2,
        })
    # print(kwargs)
    d = drivers.pt2522(**kwargs)
    if rotate_top_down:
        d = d.union(d.rotate((0, 0, 0), (0, 0, 1), 180))
    if sideways:
        d = d.rotate((0, 0, 0), (0, 0, 1), 90)
    b = place_tweeter_shape(wg_slice, shape=d)

    if mirror_cut:
        b = b.cut(wg_shapes.wg_mirror_cut_shape(wg_slice.radius_list))

    return b


def topmost_slice_front_woofer_vents(wg_slice: WgSlice):
    front_vent_h = 1.0
    t = Screw('m4').heatsert_short_depth + 0.1
    vent = place_woofer(
        ws=wg_slice.ws,
        angle=wg_slice.ws.cbt_angle,
        shape=(
            cq.Workplane("XY")
                .box(30, 6, front_vent_h)
                .translate((0, 0, -t - 0.5 * front_vent_h))
        ),
    )
    return vent


def topmost_slice_rear_woofer_vents(wg_slice: WgSlice):
    vent_h = 2.75
    vent = place_woofer(
        ws=wg_slice.ws,
        angle=wg_slice.ws.cbt_angle,
        shape=(
            cq.Workplane("XY")
                .box(15, 6, vent_h)
                .translate((0, 0, vent_h / 2))
        ),
    )
    return vent


def topmost_slice_middle_woofer_vents(wg_slice: WgSlice):
    vent = place_woofer(
        ws=wg_slice.ws,
        angle=wg_slice.ws.cbt_angle,
        shape=(
            cq.Workplane("XY")
                .box(13, 6, 2.2 * wg_slice.shell_thickness)
            #     .cut((
            #     cq.Workplane("XY")
            #         .center(12.5 -1, 2.5 -1)
            #         .rect(2, 2)
            #         .circle(0.33 / 2)
            #         .extrude(2.2 * wg_slice.shell_thickness)
            #         .translate((0, 0, -1.1 * wg_slice.shell_thickness))
            # ))
        ),
    )
    return vent


@cq_cache()
def place_woofers(wg_slice: WgSlice, margin=False, mirror=False, **kwargs):
    ws = wg_slice.ws
    b = place_woofer(
        ws=ws,
        angle=ws.cbt_angle,
        shape=ws.get_woofer_margin(**kwargs) if margin else ws.get_woofer(**kwargs),
    )
    if mirror:
        b = b.mirror(mirrorPlane='YZ', union=True)

    return b


@cq_cache()
def place_woofers_cut(wg_slice: WgSlice, margin=False):
    multi = 2
    if wg_slice.ws.ordinal == 5:
        multi = 1.5
    elif wg_slice.ws.ordinal >= 6:
        multi = 1
    ws = wg_slice.ws
    return place_woofer(
        ws=ws,
        angle=ws.cbt_angle,
        shape=calc_woofer_edge_cutout(wg_slice, front_h_offset=multi * PLY_EDGE_THICKNESS_SINGLE, margin=margin),
    )


class WooferStats:
    def __init__(self,
                 ordinal,
                 p_offset=None,
                 woofer_type='nd105',
                 dual=True,
                 cbt_angle=0.0,
                 z_offset=0.0,
                 angle=0,
                 z_angle=0,
                 rotation_angle=0,
                 ):

        self.ordinal = ordinal
        self.z_offset = z_offset
        self.angle = angle
        self.cbt_angle = cbt_angle
        self.z_angle = z_angle
        self.rotation_angle = rotation_angle

        self.screw = Screw('m4')
        self.screw_hole_diameter = 0.42

        self.woofer_type = woofer_type
        self.dual = dual

        self.woofer = None
        self.woofer_margin = None

        if woofer_type == 'nd140':
            self.diameter = 13.8
            self.max_diameter = 15.45
            self.cutout_diameter = 12.0
            self.base_thickness = 0.15
            self.screw_holes_diameter = 13.8
            self.sd = 86.6
            self.surround_diameter = 11.7
            self.upper_basket = (11.8 / 2, 0.2)
            self.basket_height = 3.7
            self.magnet_diameter = 4.22
            self.magnet_height = 6.25
            self.front_slot_height = 3.5
        elif woofer_type == 'nd105':
            self.diameter = 10.5
            self.max_diameter = 12.0
            self.cutout_diameter = 9.3
            self.base_thickness = 0.1
            self.screw_holes_diameter = 10.9
            self.sd = 51.5
            self.rear_slot_sd = self.sd
            self.surround_diameter = 9.3
            self.upper_basket = (8.1 / 2, 2.15)
            self.basket_height = 3.0
            self.magnet_diameter = 4.22
            self.magnet_height = 5.7
            self.front_slot_height = 3.0
        elif woofer_type == 'nd91':
            self.diameter = 9.3
            self.max_diameter = 10.35
            self.cutout_diameter = 7.65
            self.base_thickness = 0.1
            self.screw_holes_diameter = 9.3
            self.sd = 30.4
            self.rear_slot_sd = self.sd * 1.2
            self.surround_diameter = 7.4
            self.upper_basket = (7.0 / 2, 1.7)
            self.basket_height = 2.75
            self.magnet_diameter = 4.22
            self.magnet_height = 4.49
            self.front_slot_height = 2.0
        elif woofer_type == 'nd65':
            self.diameter = 6.4
            self.max_diameter = 7.55
            self.cutout_diameter = 5.2
            self.base_thickness = 0.1
            self.screw_holes_diameter = 6.6
            self.sd = 15.6
            self.rear_slot_sd = self.sd * 1.4
            self.surround_diameter = 5.15
            self.upper_basket = (5.05 / 2, 1.4)
            self.basket_height = 1.9
            self.magnet_diameter = 3.56
            self.magnet_height = 4.3
            self.front_slot_height = 1.5
        elif woofer_type == 'nd64':
            self.diameter = 6.4
            self.max_diameter = 7.55
            self.cutout_diameter = 5.2
            self.base_thickness = 0.1
            self.screw_holes_diameter = 6.6
            self.sd = 15.6
            self.rear_slot_sd = self.sd * 1.4
            self.surround_diameter = 5.15
            self.basket_height = 1.9
            self.upper_basket = (5.05 / 2, 1.4)
            self.magnet_diameter = 3.45
            self.magnet_height = 3.45
            self.front_slot_height = 1.5

        screw_holes = []
        for r in [45, 45 + 90, 45 + 180, 45 + 270]:
            p = P(angle=r, length=self.screw_holes_diameter / 2)
            screw_holes.append(p.values())
        self.screw_holes = screw_holes

        gf = WgGeometry(WgShell(cbt_angle, front=True))
        gr = WgGeometry(WgShell(cbt_angle, rear=False))
        front_edge = gf.p5_arc[1]

        self.z_offset += front_edge.y

        edge_x = gf.p5_arc[1].x
        x_delta = abs(edge_x - gr.p5_arc[1].x)
        y_delta = gf.p5_arc[1].y + gf.p5_arc[1].y

        x_per_y = (x_delta / y_delta) * 2

        min_distance_from_edge = 0.6

        if woofer_type == 'nd140':
            min_distance_from_edge = 0
        if woofer_type == 'nd105':
            min_distance_from_edge = 0.6
        if woofer_type == 'nd91':
            min_distance_from_edge = 0.6
        if woofer_type == 'nd65':
            self.z_offset -= 1 * z_offset
            min_distance_from_edge = 0.6

        if p_offset:
            self.p_offset = p_offset
        else:
            x_from_base = abs(x_per_y * abs(z_offset) - self.diameter / 2)
            x_from_basket = abs(x_per_y * self.upper_basket[1] - self.upper_basket[0])
            x_from_magnet = abs(x_per_y * self.magnet_height - self.magnet_diameter / 2)
            min_x = min(x_from_base, x_from_basket, x_from_magnet)
            self.p_offset = P(x=edge_x - min_x - self.diameter / 2 - min_distance_from_edge, y=0)

    def __repr__(self):
        return f"{self.ordinal}"

    def get_woofer(self, **kwargs):
        if self.woofer is None:
            if self.woofer_type == 'nd140':
                self.woofer = drivers.nd140(**kwargs)
            elif self.woofer_type == 'nd105':
                self.woofer = drivers.nd105(**kwargs)
            elif self.woofer_type == 'nd91':
                self.woofer = drivers.nd91(**kwargs)
            elif self.woofer_type == 'nd65':
                self.woofer = drivers.nd65(**kwargs)
            elif self.woofer_type == 'nd64':
                self.woofer = drivers.nd64(**kwargs)
        return self.woofer

    def get_woofer_margin(self, base_margin_cutout_depth=2.0):
        if self.woofer_margin is None:
            if self.woofer_type == 'nd140':
                self.woofer_margin = drivers.nd140(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    render_screws=False,
                )
            elif self.woofer_type == 'nd105':
                self.woofer_margin = drivers.nd105(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    render_screws=False,
                )
            elif self.woofer_type == 'nd91':
                self.woofer_margin = drivers.nd91(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    render_screws=False,
                )
            elif self.woofer_type == 'nd65':
                self.woofer_margin = drivers.nd65(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    render_screws=False,
                )
            elif self.woofer_type == 'nd64':
                self.woofer_margin = drivers.nd64(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    render_screws=False,
                )
        return self.woofer_margin

    def __repr__(self):
        d = {key: value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(value)}
        return d.items().__repr__()


woofer_angle = 0
# woofer_z_offset = 0.75
# woofer_z_offset = 1.0
woofer_z_offset = -1.0

dpt = DEGREES_PER_TWEETER

woofer_stats = [
    WooferStats(ordinal=0, cbt_angle=1 * dpt, z_offset=woofer_z_offset, woofer_type='nd105'),
    WooferStats(ordinal=1, cbt_angle=3 * dpt, z_offset=woofer_z_offset, woofer_type='nd105'),
    WooferStats(ordinal=2, cbt_angle=5 * dpt, z_offset=woofer_z_offset, woofer_type='nd105'),
    WooferStats(ordinal=3, cbt_angle=7 * dpt, z_offset=woofer_z_offset, woofer_type='nd91'),
    WooferStats(ordinal=4, cbt_angle=9 * dpt, z_offset=woofer_z_offset, woofer_type='nd91'),
    WooferStats(ordinal=5, cbt_angle=11 * dpt, z_offset=woofer_z_offset, woofer_type='nd65'),
    WooferStats(ordinal=6, cbt_angle=13 * dpt, z_offset=woofer_z_offset, woofer_type='nd65'),
    WooferStats(ordinal=7, cbt_angle=15 * dpt, z_offset=woofer_z_offset, woofer_type='nd65'),
    WooferStats(ordinal=8, cbt_angle=17.5 * dpt, z_offset=woofer_z_offset, woofer_type='nd65',
                angle=woofer_angle,
                p_offset=P(0, 0), dual=False),
]


# woofer_stats = [
#     WooferStats(cbt_angle=1 * dpt, z_offset=1 * wzto + woofer_z_offset, woofer_type='nd105', angle=woofer_angle,
#                 x_offset=0.0),
#     WooferStats(cbt_angle=3 * dpt, z_offset=3 * wzto + woofer_z_offset, woofer_type='nd105', angle=woofer_angle,
#                 x_offset=0.0),
#     WooferStats(cbt_angle=5 * dpt, z_offset=5 * wzto + woofer_z_offset, woofer_type='nd105', angle=woofer_angle,
#                 x_offset=0.0),
#     WooferStats(cbt_angle=7 * dpt, z_offset=7 * wzto + woofer_z_offset, woofer_type='nd91', angle=woofer_angle,
#                 x_offset=1.0),
#     WooferStats(cbt_angle=9 * dpt, z_offset=9 * wzto + woofer_z_offset, woofer_type='nd91', angle=woofer_angle,
#                 x_offset=0.5),
#     WooferStats(cbt_angle=11 * dpt, z_offset=11 * wzto + woofer_z_offset, woofer_type='nd65', angle=woofer_angle,
#                 x_offset=0.5),
#     WooferStats(cbt_angle=13 * dpt, z_offset=13 * wzto + woofer_z_offset, woofer_type='nd65', angle=woofer_angle,
#                 x_offset=0.0),
#     WooferStats(cbt_angle=15 * dpt, z_offset=15 * wzto + woofer_z_offset, woofer_type='nd65', angle=woofer_angle,
#                 x_offset=-0.15),
#     WooferStats(cbt_angle=17 * dpt, z_offset=17 * wzto + woofer_z_offset, woofer_type='nd65', angle=woofer_angle,
#                 p_offset=P(0, 3.5), dual=False),
# ]


def calc_woofer_ply_overlap(wg_slice: WgSlice, margin=False):
    # ws = wg_slice.ws
    t = 20
    # front_shelf = wg_slice.woofer_ply_cutout_y - (1 * PRINTER_MARGIN if margin else -1 * PRINTER_MARGIN)
    chamfer_radius = wg_slice.woofer_ply_cutout_y / 3 - 0.1

    return (
        cq.Workplane("XY")
            .workplane(offset=0)
            .rect(t, 12.5 + 1.0 + (2 * PRINTER_MARGIN if margin else 0))
            # .rect(t, wg_functions.middle_value(ws.cutout_diameter + chamfer_radius, ws.diameter + 0.5))
            .extrude(-(wg_slice.woofer_ply_cutout_y + (2 * PRINTER_MARGIN if margin else 0)))
            .translate((0, 0, + 1 * PRINTER_MARGIN if margin else 0))
            # .edges("<Z")
            .chamfer(chamfer_radius)
    )


@cq_cache()
def calc_woofer_edge_cutout(wg_slice: WgSlice, union_rear=True, front_h_offset=0.0, margin=False):
    ws = wg_slice.ws
    t = 20
    # front_h = ws.magnet_height - 2.5
    front_h = ws.front_slot_height + front_h_offset
    # rear_h = ws.basket_height + 1

    front_shelf = wg_slice.shell_thickness
    chamfer_radius = front_shelf / 2 - 0.1

    max_width = ws.diameter + (2 * PRINTER_MARGIN if margin else 0)
    boop = 10.7
    max_width = boop + (2 * PRINTER_MARGIN if margin else 0)

    r = (
        cq.Workplane("XY")
            .workplane(offset=0)
            .rect(max_width, t)
            .workplane(offset=0.25)
            .rect(max_width, t)
            .workplane(offset=ws.magnet_height + (0.05 if ws.woofer_type in ['nd64', 'nd65'] else 0.25))
            .rect(max_width * 0.95 - chamfer_radius, t)
            .loft(combine=True)
            .edges(">Z and (not <Y) and (not >Y)")
            .fillet(1)
            .rotate((0, 0, 0), (0, 0, 1), 90)
    )
    fb = calc_woofer_ply_overlap(wg_slice, margin=margin)
    f = (
        cq.Workplane("XY")
            .workplane(offset=-PRINTER_MARGIN)
            .rect(t, boop + chamfer_radius + 1.5 * (PRINTER_MARGIN if margin else 0))
            .workplane(offset=front_h - 0.5)
            .rect(t, boop * 1.0 + 1.5 * (PRINTER_MARGIN if margin else 0))
            .loft(combine=True)
            .edges(">Z and (not <X) and (not >X)")
            .fillet(1)
            .rotate((0, 0, 0), (0, 1, 0), 180)
            .translate((0, 0, -front_shelf))
    )
    f = f.union(fb)
    b = f
    if union_rear:
        b = b.union(r)

    return b


def place_woofer_heatsert_reinforcements(wg_slice: WgSlice):
    ws = wg_slice.ws
    woofer_holes = []
    t = wg_slice.shell_thickness + 0.5
    for hole in ws.screw_holes:
        if wg_slice.topmost_part and hole[0] < 0:
            continue
        woofer_holes.append((
            cq.Workplane("XY")
                .center(hole[0], hole[1])
                .circle(ws.screw.heatsert_diameter_reinforced / 2)
                # .circle(1.0 * wg_slice.ws.screw_hole_diameter)
                .extrude(2 * t)
                .translate((0, 0, -1.5 * t))
        ))
    return place_woofer(
        ws=ws,
        angle=ws.cbt_angle,
        shape=wg_functions.union_shapes_list(woofer_holes),
    )
