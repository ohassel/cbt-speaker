import cadquery as cq

import wg_vars

jig = (
    cq.Workplane("XY")
        .rect(50, 30)
        .extrude(-12)
        .fillet(2)
)

desired_peg_h = 7.6
cutter_margin = 0.2
hole_h = desired_peg_h - cutter_margin

text = (
    cq.Workplane("XY")
        .text(txt=str(f"{hole_h:.1f}mm holes\n{desired_peg_h:.1f}mm pegs"), fontsize=8.0, distance=-3.0, cut=True, clean=True)
        .translate((0, 6, 0))
)

for x in [-18, -9, 0, 9, 18]:
    h = (
        cq.Workplane("XY")
            .move(x, -8)
            .circle(wg_vars.FILAMENT_PEG_DIAMETER * 10 * 1.3 / 2)
            .extrude(-hole_h)
    )
    jig = jig.cut(h)

jig = jig.cut(text)

show_object(jig, name='jig', options={})

cq.exporters.export(jig, f"filament_peg_jig.step")
