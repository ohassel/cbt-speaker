import cadquery as cq

from geometry_2d import P as P
import drivers
import wg_functions
import wg_classes
import wg_vars


# import wg_naked


def planar_mid_screw_holes():
    screw_hole_x_delta = 7.91
    screw_hole_y_delta = 11.5

    screw_hole = (
        cq.Workplane("XY")
            .circle(0.35 / 2)
            .extrude(4)
            .translate((0, 0, -2))
    )

    screw_holes = (
        screw_hole.translate((-screw_hole_x_delta / 2, -screw_hole_y_delta / 2, 0))
            .union(screw_hole.translate((screw_hole_x_delta / 2, -screw_hole_y_delta / 2, 0)))
            .union(screw_hole.translate((-screw_hole_x_delta / 2, screw_hole_y_delta / 2, 0)))
            .union(screw_hole.translate((screw_hole_x_delta / 2, screw_hole_y_delta / 2, 0)))
    )

    return screw_holes


midrange_audio_hole_height = 3.5
midrange_audio_hole_width = 4.6
midrange_platform_thickness = 0.91


def planar_mid():
    base = (
        cq.Workplane("XY")
            .box(9, 20.2, 0.3)
            .edges("|Z")
            .fillet(0.9)
    )

    platform = (
        cq.Workplane("XY")
            .box(6.7, 17.5, midrange_platform_thickness)
            .fillet(0.2)
    )

    hole = (
        cq.Workplane("XY")
            .box(midrange_audio_hole_height, midrange_audio_hole_width, 2)
            .fillet(0.25)
    )

    rib = (
        cq.Workplane("XY")
            .box(4.7, 0.55, 1.1)
            .fillet(0.25)
    )

    hole_y_delta = 5.3
    return (
        base
            .union(platform)
            .union(rib.translate((0, -hole_y_delta / 2)))
            .union(rib.translate((0, hole_y_delta / 2)))
            .cut(hole)
            .cut(hole.translate((0, -hole_y_delta, 0)))
            .cut(hole.translate((0, hole_y_delta, 0)))
            .cut(planar_mid_screw_holes())
    )


def planar_mid_wg():
    w = 9
    h = 20

    b = (
        cq.Workplane("XY")
            .threePointArc((0.05, 0.08), (7.7, 5))
            # .lineTo(7.7, 6)
            .lineTo(7.7, 0)
            .lineTo(0, 0)
            .close()
            .extrude(w)
            .translate((0, 0, -w / 2))
            .translate((midrange_audio_hole_width / 2, midrange_platform_thickness / 2, 0))
            .rotate((0, 0, 0), (0, 0, 1), 90)
            .rotate((0, 0, 0), (0, 1, 0), 90)
    )

    v = (
        cq.Workplane("XY")
            .threePointArc((0.025, 0.1), (2.8, 5))
            # .lineTo(7.7, 6)
            .lineTo(2.8, 0)
            .lineTo(0, 0)
            .close()
            .extrude(h)
            .translate((0, 0, -h / 2))
            .translate((midrange_audio_hole_height / 2, midrange_platform_thickness / 2, 0))
            .rotate((0, 0, 0), (0, 0, 1), 90)
            .rotate((0, 0, 0), (0, 1, 0), 90)
            .rotate((0, 0, 0), (0, 0, 1), 90)
    )

    b = b.mirror(mirrorPlane='XZ', union=True)
    v = v.mirror(mirrorPlane='YZ', union=True)

    wg_full = (
        b.union(v)
    )

    platform_t = 2.0
    platform = (
        cq.Workplane("XY")
            .box(9, 13.5, platform_t)
            .fillet(0.2)
            .intersect(wg_full)
    )

    platform_holes = (
        cq.Workplane("XY")
            .box(6.7, 17.5, platform_t * 2)
            # .fillet(0.2)
            .cut((
            cq.Workplane("XY")
                .box(6.7, 5.8, platform_t * 2)
        ))
            .fillet(1)
    )

    platform = (
        platform
            .cut(platform_holes)
            .cut(planar_mid_screw_holes())
    )

    out = (
        wg_full.cut(wg_full.translate((0, 0, -0.3)))
            .union(platform)
    )

    out = out.cut(planar_mid())

    return out


# tweeter_audio_hole_height = 3.4
tweeter_audio_hole_height = 1.45
tweeter_audio_hole_width = 5.6
tweeter_platform_thickness = 1.0


def planar_tweeer_wg_breathe_slots(rear_offset=0.21, middle_slot=False, holes=False):
    s = (
        cq.Workplane("XY")
            .box(9.5, 1, 9.25)
            .cut((
            cq.Workplane("XY")
                .box(3, 10, 10)

        ))
            .cut((
            cq.Workplane("XY")
                .box(10, 10, 10)
                .translate((5, 0, 0))
        ))
            .fillet(0.3)

    )

    if holes:
        hole = (
            cq.Workplane("XY")
                .circle(0.5)
                .extrude(10)
                .translate((0, 0, -5))
        )
        s = (
            hole.translate((-3, 0, 0))
                .union(hole.translate((-5, 0, 0)))
                .union(hole.translate((-4, 0, 0)).translate((0, -1.4, 0)))
        )

    s = (
        s
            .translate((0, 0, 3.25))
            .rotate((0, 0, 0), (0, 1, 0), 35)
    )

    slot = (
        s
            .rotate((0, 0, 0), (0, 0, 1), 13)
            .translate((0, -2.33 + (0.2 if holes else 0), 0))
            .mirror(mirrorPlane='XY', union=False)
        # .mirror(mirrorPlane='XZ', union=True)
        # .mirror(mirrorPlane='XY', union=True)
    )
    slot = slot.union((
        slot
            .translate((0, rear_offset, 0))
            .mirror(mirrorPlane='XY', union=False)
    ))
    if middle_slot:
        slot = slot.union((
            s
                .mirror(mirrorPlane='XY', union=True)
        ))
    slot = slot.mirror(mirrorPlane='XZ', union=True)
    return slot


def planar_tweeter_wg(
        shell_thickness=None,
        edge_breathe_slot=False,
        h_d=1.75,
        throat_roundover=0.5,
        wg_coverage_angle=120,
        wg_depth=2.0,
        edge_roundover=0.75,
):
    h = 8.5

    # h_d = 1.75
    # h_d = 2.5

    x, y = 0, 0
    x_top = 3.0
    # edge_roundover = 1.25

    g = wg_classes.WgGeometry(s=None, p0=P(0, 0),
                              angle=0.1,
                              w=50,
                              d=wg_depth,
                              edge_width=0.25,
                              coverage_angle=wg_coverage_angle,
                              throat_roundover=throat_roundover,
                              edge_roundover=edge_roundover)

    p_outer_roundover_edge = g.p_edge_roundover_center + P(edge_roundover, 0)
    v = (
        cq.Workplane("XY")
            .moveTo(0, -tweeter_platform_thickness / 2)
            .lineTo(0, 0)
            .threePointArc(g.p2_arc[0].values(), g.p2_arc[1].values())
            # .lineTo(g.p2_arc[0].x, g.p2_arc[0].y)
            # .lineTo(g.p2_arc[1].x, g.p2_arc[1].y)
            .lineTo(g.p3.x, g.p3.y)

            # .threePointArc(g.p4_arc[0].values(), g.p4_arc[1].values())
            .threePointArc(g.p4_arc[0].values(), p_outer_roundover_edge.values())
            .lineTo(p_outer_roundover_edge.x, -tweeter_platform_thickness / 2)

            # .lineTo(g.p4_arc[0].x, g.p4_arc[0].y)
            # .lineTo(g.p4_arc[1].x, g.p4_arc[1].y)

            # .lineTo(g.p4_arc[1].x, -tweeter_platform_thickness / 2)

            # .threePointArc(g.p5_arc[0].values(), g.p5_arc[1].values())
            # .lineTo(g.p6.x, g.p6.y)
            # .lineTo(g.p6.x, g.p6.y - tweeter_platform_thickness / 2)

            # .threePointArc(p_top_diagonal_edge.values(), (x_top + edge_roundover, h_d - edge_roundover))

            #     .spline([
            #     (0, 0),
            #     (0, 0.01),
            #     (x_top - 0.01, h_d),
            #     (x_top, h_d),
            #     # (x_top + edge_roundover, h_d - edge_roundover),
            #     # (x_top + edge_roundover, h_d - edge_roundover - 0.01),
            # ])
            # .lineTo(x_top, h_d)

            # .lineTo(p_top_inner_edge.x, p_top_inner_edge.y)

            # .lineTo(p_top_diagonal_edge.x, p_top_diagonal_edge.y)
            # .lineTo(x_top + edge_roundover, h_d - edge_roundover)
            # .threePointArc(p_top_diagonal_edge.values(), (x_top + edge_roundover, h_d - edge_roundover))

            .lineTo(x_top + edge_roundover, -tweeter_platform_thickness / 2)
            .lineTo(0, -tweeter_platform_thickness / 2)
            .close()
            .extrude(h)
            .translate((0, 0, -h / 2))
            .translate((tweeter_audio_hole_height / 2, tweeter_platform_thickness / 2, 0))
            .translate((x, y, 0))
            .rotate((0, 0, 0), (0, 0, 1), 90)
            .rotate((0, 0, 0), (0, 1, 0), 90)
            .rotate((0, 0, 0), (0, 0, 1), 90)
    )

    out = v

    out = out.mirror(mirrorPlane='XY', union=True)

    if shell_thickness:
        # out = out.cut(wg_full.translate((1.5 * shell, 0, shell)))
        out = (
            out
                .faces("|Y")
                .shell(shell_thickness)
        )
        # return out
        out = out.cut((
            cq.Workplane("XY")
                .box(10, 10, tweeter_platform_thickness + 2 * (p_outer_roundover_edge.y))
                .translate((-9, 0, 0))
        ))

        if edge_breathe_slot:
            out = out.cut(planar_tweeer_wg_breathe_slots())

    out = out.mirror(mirrorPlane='YZ', union=True)

    # out = out.mirror(mirrorPlane='XY', union=True)

    return out

# midrange = planar_mid()
# midrange_wg = planar_mid_wg()
# show_object(midrange, name=f'midrange', options={"color": (45, 45, 45)})
# show_object(midrange_wg, name=f'midrange_wg')
#
#
# cq.exporters.export(
#     wg_functions.scale_cm_to_mm(midrange_wg),
#     'midrange_wg.step'
# )
