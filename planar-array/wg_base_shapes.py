import cadquery as cq

from wg_classes import *
from typing import List
import geometry_2d
import wg_functions


def wg_vert_edge_holes(s=WgShell(angle=0), edge_holes_depth=0.8, edge_holes_diameter=0.3, edge_holes_gap=1.5):
    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    gf = wg_geometry_cached(sf)
    gr = wg_geometry_cached(sr)

    def calc_vert_2(s, g, y_sign=1):

        vert_edge = P(g.p5_arc[0].x, y_sign * g.p5_arc[0].y)
        vert_edge_x = vert_edge.x
        vert_edge_y = vert_edge.y
        vert_mid = P(0, y_sign * s.d / 2)

        p_center, radius = geometry_2d.find_circle((-vert_edge_x, vert_edge_y), vert_mid.values(),
                                                   (vert_edge_x, vert_edge_y))

        hole_r = geometry_2d.distance_across_circle(edge_holes_gap, radius)
        max_r = geometry_2d.distance_across_circle(vert_edge_x, radius)
        num_holes = math.floor(max_r / hole_r)

        holes = []
        for i in range(0, num_holes):
            r = i * hole_r

            if y_sign == 1:
                pp = p_center + P(angle=r + 270 + 180, length=radius - edge_holes_depth)
            else:
                pp = p_center + P(angle=r + 270, length=radius - edge_holes_depth)

            shape = (
                cq.Workplane("XY")
                    .center(pp.x, pp.y)
                    .circle(edge_holes_diameter / 2)
                    .extrude(10)
                    .translate((0, 0, -5))
                    .mirror(mirrorPlane='YZ', union=True)
            )
            # bb = bb.cut(shape)
            holes.append(shape)

        return wg_functions.union_shapes_list(holes)

    br = calc_vert_2(sr, gr, y_sign=1)
    bf = calc_vert_2(sf, gf, y_sign=-1)

    bb = br.union(bf)

    b = bb

    # g = wg_geometry_cached(s)
    #
    # b = (
    #     cq.Workplane("XY")
    #         .box(2 * g.p4_arc[1].x, s.d + 1, 3 * pt2522_h_offset)
    #         .intersect(b)
    # )

    return b


def wg_vert(s=WgShell(angle=0)):
    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    gf = wg_geometry_cached(sf)
    gr = wg_geometry_cached(sr)

    base_height = 0.15

    def calc_vert_2(s, g, y_sign=1, step_offset=0.1):
        margin = 0.01
        vert_edge = P(g.p5_arc[0].x, y_sign * g.p5_arc[0].y)
        vert_edge_x = vert_edge.x
        vert_edge_y = vert_edge.y
        vert_mid = P(0, y_sign * s.d / 2)

        bb_base = (
            cq.Workplane("XY")
                .lineTo(vert_edge_x, y_sign * 0)
                .lineTo(vert_edge_x, vert_edge_y)
                .threePointArc(vert_mid.values(), (-vert_edge_x, vert_edge_y))
                .lineTo(-vert_edge_x, y_sign * 0)
                .close()
                .extrude(base_height + 0 * margin)
                .translate((0, 0, -margin))
        )

        step_y = 1.5

        bb = (
            cq.Workplane("XY")
                .lineTo(vert_edge_x, y_sign * 0)
                .lineTo(vert_edge_x, vert_edge_y)
                .threePointArc(vert_mid.values(), (-vert_edge_x, vert_edge_y))
                .lineTo(-vert_edge_x, y_sign * 0)
                .close()
                .workplane(offset=pt2522_h_offset - base_height - step_offset)
                .lineTo(vert_edge_x, y_sign * 0)
                .lineTo(vert_edge_x, y_sign * step_y)
                # .threePointArc((0, y_sign * step_curve_y), (-g.p5_arc[0].x, y_sign * step_y))
                .lineTo(-vert_edge_x, y_sign * step_y)
                .lineTo(-vert_edge_x, y_sign * 0)
                .close()
                .workplane(offset=step_offset)
                .lineTo(vert_edge_x, y_sign * 0)
                .lineTo(vert_edge_x, y_sign * 0.5)
                .lineTo(-vert_edge_x, y_sign * 0.5)
                .lineTo(-vert_edge_x, y_sign * 0)
                .close()
                .loft(combine=True)
                .translate((0, 0, base_height - margin))
        )
        bb = bb.union(bb_base)
        return bb

    br = calc_vert_2(sr, gr, y_sign=1, step_offset=0.15)
    bf = calc_vert_2(sf, gf, y_sign=-1, step_offset=0.15)

    bb = br.union(bf)

    b = bb

    g = wg_geometry_cached(s)

    b = (
        cq.Workplane("XY")
            .box(2 * g.p4_arc[1].x, s.d + 1, 3 * pt2522_h_offset)
            .intersect(b)
    )

    return b


def calc_vert(angle, top=False, bot=False, shell=None, hole=False):
    ts = WgShell(angle=angle)

    if hole:
        wv = wg_vert_edge_holes(s=ts)
    else:
        wv = wg_vert(s=ts)

    if shell:
        if 0 > shell:
            wv = wv.cut(wv.translate((0, 0, shell)))
        else:
            wv_orig = wv
            wv = (
                wv.translate((0, 0, shell))
                    .cut(wv_orig)
            )
        # wv = wv.shell(shell)

    if top and bot:
        wv = wv.union(wv.rotate((0, 0, 0), (0, 1, 0), 180))
    elif bot or hole:
        pass
    elif top:
        wv = wv.rotate((0, 0, 0), (0, 1, 0), 180)

    wv = wv.rotate((0, 0, 0), (1, 0, 0), -angle)
    wv = wv.translate((0, ts.center.x, ts.center.y))

    return wv


def wg_2d_slice(s: WgShell):
    w = s.w / 2
    # d = s.d / 2
    #
    # e = s.edge_width / 2

    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    def half(ss, w0, w, d, e, sign=1):
        # r = r.moveTo(w * sign, 0)
        ss = ss.segment((w0 * sign, 0), (w * sign, e * sign))

        # diamond
        # ss = ss.segment((0, d * sign))
        # ss = ss.segment((-w * sign, e * sign))

        # Curved
        ss = ss.arc((0, d * sign), (-w * sign, e * sign), "ignored")

        #
        ss = ss.segment((-w * sign, e * sign), (-w0 * sign, 0))

        return ss

    s = cq.Sketch()

    s = half(s, sign=1, w0=w, w=sr.w / 2, d=sr.d / 2, e=sr.edge_width / 2)
    s = half(s, sign=-1, w0=w, w=sf.w / 2, d=sf.d / 2, e=sf.edge_width / 2)

    s = s.solve().assemble()

    return s


def wg_2d_throat(s: WgShell):
    w = s.w / 2
    # d = s.d / 2
    #
    # e = s.edge_width / 2

    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    def halff(ss, w0, g, sign=1, ):
        ss = ss.segment((g.p0.x, g.p0.y * sign), (g.p1.x, g.p1.y * sign))

        ss = ss.arc((g.p2_arc[0].x, g.p2_arc[0].y * sign), (g.p2_arc[1].x, g.p2_arc[1].y * sign), "p2")

        ss = ss.segment((g.p3.x, g.p3.y * sign))

        ss = ss.arc((g.p4_arc[0].x, g.p4_arc[0].y * sign), (g.p4_arc[1].x, g.p4_arc[1].y * sign), "p4")

        # print(g.p4_arc[1])

        ss = ss.arc((g.p5_arc[0].x, g.p5_arc[0].y * sign), (g.p5_arc[1].x, g.p5_arc[1].y * sign), "p5")

        ss = ss.segment((w0, g.p6.y * sign))

        return ss

    sk = cq.Sketch()

    sk = halff(sk, w0=w, g=wg_geometry_cached(s=sr), sign=1)
    sk = halff(sk, w0=w, g=wg_geometry_cached(s=sf), sign=-1)

    sk = sk.solve().assemble()

    return sk


def wg_shell_moved_sketch(r, slice_function=wg_2d_slice, flat=False, radius=CBT_RADIUS, **kwargs):
    s = WgShell(angle=r, flat=flat)

    p = s.center

    return (
        slice_function(s=s, **kwargs)
            .moved(
            cq.Location(
                cq.Plane.XY().rotated((-r, 0, 0)),
                cq.Vector(0, p.x, p.y)
            )
        )
    )


def wg_shell(radius_list: List[int], slice_function=wg_2d_slice, flat=False, radius=None, **kwargs):
    # step = 0.1
    # step = 1.0 / (NUM_TWEETERS)
    # step = 0.01
    # steps = int(1 / step)

    sketches = []

    # for f in [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]:
    # for i in range(1, steps + 1):

    # for t in range(0, NUM_TWEETERS + 1):
    for r in radius_list:
        # print(f"boop: {f}")
        # f = t * step
        # r = CBT_DEGREES * 0.1

        sketches.append(
            wg_shell_moved_sketch(r, slice_function, flat, radius, **kwargs)
        )

    b = (
        cq.Workplane("XY")
            .placeSketch(*sketches)
            .loft()
    )

    return b
