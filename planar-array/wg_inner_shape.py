from hacked_cq_cache import cq_cache

from wg_shapes import *
from wg_classes import *

import wg_ply
import wg_vars
import wg_drivers
import wg_functions


@cq_cache()
def wg_slice_inner_base(wg_slice: WgSlice, tweeter_wg_cuts=False):
    wg = wg_throat_with_verts_shape(wg_slice.radius_list, shell=-wg_slice.shell_thickness, shell_vert=-wg_slice.vert_shell_thickness)

    if tweeter_wg_cuts and wg_slice.ws.ordinal not in [8]:
        wg = wg.cut(wg_woofer_slots(wg_slice))

    if wg_slice.topmost_part:
        top = move_degree(wg_slice.r2, (
            cq.Workplane("XY")
                .box(50, 50, 1)
        ))
        wg_full = wg_throat_with_verts_shape(wg_slice.radius_list)
        top = top.intersect(wg_full)
        wg = wg.union(top)

    # woofer_flat = woofer_flat_slice(wg_slice, tweet_breathe_slots=True)
    # woofer_flat = woofer_flat.cut(wg_front_mid_split(wg_slice))
    # wg = wg.union(woofer_flat)

    wg = wg.union(
        wg_ply.ply_peg_supports(wg_slice, peg_fillet=0.5, inner=True)
            .cut(wg_front_mid_split(wg_slice))
    )

    screw = wg_slice.screw_holes.screw
    screw_supports = wg_ply.ply_hole_supports_inf(wg_slice, pegs=False, screws=True)
    screw_supports = screw_supports.intersect(wg_throat_with_verts_shape(wg_slice.radius_list))
    screw_supports = screw_supports.cut((
        wg_verts_shape(wg_slice.radius_list, shell=-wg_slice.vert_shell_thickness, inner=True, mid=False)
    ))
    screw_inner_cut_shell = calc_ply_middle_with_shell(wg_slice, shell_thickness=screw.heatsert_long_depth + 0.4)
    wg = wg.union(screw_supports)

    # middle_screw_supports = wg_ply.ply_hole_supports_inf(wg_slice, pegs=False, screws=False, middle_screws=True)
    # if middle_screw_supports is not None:
    #     middle_screw_supports = middle_screw_supports.cut(wg_drivers.place_woofers_cut(wg_slice))
    #     screw_inner_cut_shell_extra = calc_ply_middle_with_shell(wg_slice, shell_thickness=screw.heatsert_long_depth + 0.4)
    #     middle_screw_supports = middle_screw_supports.intersect(screw_inner_cut_shell_extra)
    #     middle_screw_supports = middle_screw_supports.intersect(woofer_front_box(wg_slice))
    #     middle_screw_supports = middle_screw_supports.cut((
    #         wg_ply.ply_holes_inf(wg_slice, pegs=False, screws=False, middle_screws=True)
    #     ))
    #     wg = wg.union(middle_screw_supports)
    #
    #     wg = wg.cut((
    #         wg_ply.ply_holes_inf(wg_slice, pegs=False, screws=False, middle_screws=True,
    #                              screw_diameter=screw.heatsert_diameter)
    #             .intersect(screw_inner_cut_shell)
    #             .chamfer(0.15)
    #     ))

    wg = wg.cut(wg_ply.ply_peg_holes(wg_slice))

    wg = wg.cut((
        wg_ply.ply_holes_inf(wg_slice, pegs=False, screws=True, middle_screws=False,
                             screw_diameter=screw.heatsert_diameter)
            .intersect(screw_inner_cut_shell)
            .chamfer(0.15)
    ))

    return wg


@cq_cache()
def wg_tweeter_flanges(wg_slice: WgSlice, fillet_width=None, mid_flare_height=None):
    throat_with_verts = wg_throat_with_verts_shape(wg_slice.radius_list)
    wg = (
        wg_drivers.place_tweeter_flanges(wg_slice, fillet_width=fillet_width, mid_flare_height=mid_flare_height)
            .intersect(wg_full_shape(wg_slice.radius_list))
            .cut(throat_with_verts)
            .cut(wg_mirror_cut_shape(wg_slice.radius_list))
    )
    return wg


@cq_cache()
def wg_woofer_slots(wg_slice: WgSlice, slot_w=0.6):
    r0, r1, r2 = wg_slice.r0, wg_slice.r1, wg_slice.r2

    # slot_w = wg_slice.shell_thickness

    rs = r1 - r0

    throat_radius_list = [r0 - rs, r0, r1, r2, r2 + rs]

    if wg_slice.ws.ordinal == 7:
        slot_w = 0.45

    throat = (
        wg_throat_shape(throat_radius_list, shell=-slot_w * 2)
            .union(wg_throat_shape(throat_radius_list, shell=slot_w * 1.5))
    )
    v_slot_w = 0.6
    verts = (
        wg_verts_shape(wg_slice.radius_list, cut_top_bot=False)
            .union(wg_verts_shape(wg_slice.radius_list, shell=-v_slot_w, cut_top_bot=False))
            .union(wg_verts_shape(wg_slice.radius_list, shell=v_slot_w / 2, cut_top_bot=False))
            .union(wg_verts_shape(wg_slice.radius_list, shell=v_slot_w, cut_top_bot=False))
    )

    b = throat.intersect(verts)

    # return verts

    shapes = []

    for r in [r0, r1, r2]:
        s = WgShell(angle=r)
        g = wg_geometry_cached(s)

        # multi = 1.75 if r == r1 else 1.5
        multi = 1.7

        l = multi * math.sqrt(math.pow(g.p3.x, 2) + math.pow(g.p3.y, 2))

        slot = (
            cq.Workplane("XY")
                .box(l, l, 10)
                .rotate((0, 0, 0), (0, 0, 1), 45)
                .cut((
                cq.Workplane("XY")
                    .box(7, 7, 10)
                    .rotate((0, 0, 0), (0, 0, 1), 45)
            ))
        )

        shapes.append(move_degree(r, slot))

    b = b.intersect(wg_functions.union_shapes_list(shapes))

    if wg_slice.ws.ordinal == 0:
        b = b.fillet(slot_w * 0.8)
    else:
        b = b.fillet(slot_w * 0.95)

    b = b.intersect(wg_throat_with_verts_shape(wg_slice.radius_list, shell=-wg_slice.shell_thickness))

    return b


@cq_cache()
def wg_slice_inner_2(wg_slice: WgSlice, tweeter_wg_cuts=False, mirror_cut=False):
    wg = wg_slice_inner_base(wg_slice, tweeter_wg_cuts=tweeter_wg_cuts)

    wg = wg.union(place_tweeter_supports(wg_slice, mirror_cut=False))

    topmost_part = wg_slice.topmost_part
    if topmost_part:
        x = wg_drivers.place_woofer(
            ws=wg_slice.ws,
            angle=wg_slice.ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .box(7.5, 7.5, 20)
            ),
        )
        x = x.intersect(wg_throat_with_verts_shape(wg_slice.radius_list))
        wg = wg.union(x)

        t = Screw('m4').heatsert_short_depth + 0.1
        w_flat_big = wg_drivers.place_woofer(
            ws=wg_slice.ws,
            angle=wg_slice.ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .box(15, 15, 2 * t)
            ),
        )
        w_hole = wg_drivers.place_woofer(
            ws=wg_slice.ws,
            angle=wg_slice.ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .circle(wg_slice.ws.cutout_diameter / 2)
                    .extrude(20)
                    .translate((0, 0, -10))
            ),
        )
        w_flat = wg_drivers.place_woofer(
            ws=wg_slice.ws,
            angle=wg_slice.ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .box(7.5, 7.5, 2 * t)
            ),
        )
        fillet_radius = 0.2
        w_flat_outer_chamfer = wg_functions.outer_fillet(w_flat_big, w_hole, fillet_radius=fillet_radius)
        if mirror_cut:
            w_flat = w_flat.cut(wg_mirror_cut_shape(wg_slice.radius_list))

        wg = wg.cut((
            wg_drivers.place_woofer(
                ws=wg_slice.ws,
                angle=wg_slice.ws.cbt_angle,
                shape=(
                    cq.Workplane("XY")
                        .box(wg_slice.ws.cutout_diameter + fillet_radius + 0.1,
                             wg_slice.ws.cutout_diameter + fillet_radius + 0.1, 2 * t)
                ),
            )
        ))
        wg = wg.union(w_flat.cut(w_flat_outer_chamfer))
        wg = wg.cut(w_hole)
        wg = wg.cut(wg_drivers.place_woofers(
            wg_slice,
            margin=True,
            base_margin_cutout_depth=10,
            screw_hole_depth=2.0
        ))

        wg = wg.cut(wg_drivers.topmost_slice_rear_woofer_vents(wg_slice))

        t = 0.3
        vent = wg_drivers.place_woofer(
            ws=wg_slice.ws,
            angle=wg_slice.ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .box(15, 5, t)
                    .translate((0, 5, wg_slice.ws.z_offset + t / 2))
                    .union((
                    cq.Workplane("XY")
                        .box(15, 5, 2 * t)
                        .translate((0, -5, t))
                ))
            ),
        )
        wg = wg.cut(vent)

        wg = wg.cut(wg_drivers.topmost_slice_front_woofer_vents(wg_slice))

    # wg = wg.cut(ply_middle_boundary().cut(wg_drivers.calc_woofer_ply_overlap(wg_slice)))
    wg = wg.cut(ply_middle_shape(wg_slice, woofer_cuts=not topmost_part, screw_peg_holes=False, margin=True))
    wg = wg.cut(ply_outer_edge_box(wg_slice))

    # wg = wg.cut(woofer_cutout(wg_slice))

    return wg


@cq_cache()
def wg_front_mid_split(wg_slice: WgSlice):
    return wg_drivers.place_tweeter_shape(
        wg_slice,
        tweeter_r_list=[wg_slice.r1],
        shape=(
            cq.Workplane("XY")
                .box(40, 40, 2 * TWEETER_SPLIT_HEIGHT)
                .translate((0, 0, 0.125))
        )
    )


@cq_cache()
def wg_rear_mid_split(wg_slice: WgSlice, offset=0.8):
    return wg_functions.move_degree(wg_slice.r1, (
        cq.Workplane("XY")
            .box(40, 40, 40)
            .translate((0, 20 + offset, 0))
    ))


@cq_cache()
def wg_mid_split_pins(wg_slice: WgSlice, front=False, intersect_supports=True):
    pins = []
    h = TWEETER_HEIGHT - 2.5
    for xy in [
        (3.25, h),
        (2.9, 0),
        (3.25, -h),
    ]:
        pins.append((
            filament_peg()
                .translate((0, 0, TWEETER_SPLIT_HEIGHT))
                .translate((xy[0], xy[1], 0))
                .translate((0, 0, -0.125 if front else 0))
                .rotate((0, 0, 0), (1, 0, 0), -90)
                .rotate((0, 0, 0), (1, 0, 0), 180 if front else 0)
        ))
    pins_shape = union_shapes_list(pins)
    moved_shape = wg_functions.move_degree(wg_slice.r1, (
        pins_shape
    ))
    if intersect_supports:
        moved_shape = moved_shape.intersect(place_tweeter_supports(wg_slice, mirror_cut=False))
    return moved_shape


def place_rear_split_supports2(wg_slice: WgSlice, shape):
    shapes = []

    for r in [
        wg_functions.middle_value(wg_slice.r0, wg_slice.r1),
        wg_functions.middle_value(wg_slice.r1, wg_slice.r2),
    ]:
        s = WgShell(r)
        g = wg_geometry_cached(s)
        offset = 1.8
        shapes.append(wg_functions.move_degree(r, (
            shape
                .rotate((0, 0, 0), (0, 1, 0), 90)
                .rotate((0, 0, 0), (0, 0, 1), -45)
                .translate((g.p2_arc[1].x + offset, g.p2_arc[1].y + offset))
        )))

    return wg_functions.union_shapes_list(shapes)


def rear_ply_peg_slide_cutout(wg_slice: WgSlice):
    holes = PlyHoles(wg_slice)

    shapes = []
    for hole in holes.pegs:
        if not hole.rear:
            continue
        offset = 2.5
        shapes.append(
            wg_ply.move_shape_to_ply(hole.angle, hole.y_offset + offset, shape=(
                cq.Workplane("XY")
                    .box(offset * 2, PLY_PEG_DIAMETER, 20)
            ))
        )

    b = wg_functions.union_shapes_list(shapes)
    b = b.union(wg_ply.ply_peg_holes(wg_slice, peg_diameter=PLY_PEG_DIAMETER, peg_length=PLY_PEG_LENGTH, chamfer=False))
    b = (
        b
            .intersect(calc_ply_middle_with_shell(wg_slice, absolute_thickness=PLY_PEG_LENGTH))
            .chamfer(0.1)
    )

    return b


@cq_cache()
def wg_slice_inner(
        wg_slice: WgSlice,
        tweeter_flanges=False,
        tweeter_wg_cuts=False,
        mirror=False,
        front=False,
        rear=False,
        rear_inner_split_bot=False,
        rear_inner_split_top=False,
):
    wg = wg_slice_inner_2(wg_slice, tweeter_wg_cuts=tweeter_wg_cuts, mirror_cut=not mirror)

    if tweeter_flanges:
        tweeter_flanges = wg_tweeter_flanges(wg_slice, fillet_width=1.5, mid_flare_height=2.5)
        wg = wg.union(tweeter_flanges)

    wg = wg.cut(wg_drivers.place_woofers(wg_slice, mirror=mirror, margin=True, base_margin_cutout_depth=0.5))

    wg = wg.cut(wg_drivers.place_tweeter_screws(wg_slice, mirror=False))
    wg = wg.cut(top_bot_margin_cuts(wg_slice))

    if rear:
        rear_split_box = wg_rear_split_box(wg_slice)
        rear_split_pegs = wg_rear_split_pins(wg_slice)

        wg = wg.cut(rear_split_pegs)
        wg = wg.cut(rear_ply_peg_slide_cutout(wg_slice))

        top = wg.intersect(rear_split_box)
        bot = wg.cut(rear_split_box)

        supports = wg_rear_split_supports(wg_slice)
        supports = supports.cut(top)

        if rear_inner_split_bot and rear_inner_split_top or (not rear_inner_split_bot and not rear_inner_split_top):
            wg = bot.union(top).union(supports)
            wg = wg.cut(wg_rear_split_support_screw_heatserts(wg_slice))
            wg = wg.cut(wg_rear_split_support_screws(wg_slice))
        if rear_inner_split_bot:
            bot = bot.union(supports)
            bot = bot.cut(wg_rear_split_support_screw_heatserts(wg_slice))
            wg = bot
        elif rear_inner_split_top:
            top = top.cut(wg_rear_split_support_screws(wg_slice))
            wg = top

    if front and not rear:
        wg = wg.cut(wg_mid_split_pins(wg_slice, front=True))
    if not front and rear:
        wg = wg.cut(wg_mid_split_pins(wg_slice))

    if mirror:
        wg = wg.mirror(mirrorPlane='YZ', union=True)

    # wg = wg.cut(wg_verts_edge_holes_shape(wg_slice.radius_list))

    if front and not rear:
        wg = wg.cut(wg_front_mid_split(wg_slice))
        wg = wg.cut(wg_rear_box_shape(wg_slice))

        t = wg_drivers.place_tweeter_shape(wg_slice, tweeter_r_list=[wg_slice.r0], shape=(
            cq.Workplane("XY")
                .text(txt=str(f"{wg_slice.ws.ordinal} F"), fontsize=1.0, distance=1.1,
                      cut=True, clean=True)
                .translate((0, 0, -0.1))
                .mirror(mirrorPlane='YZ', union=False)
                .rotate((0, 0, 0), (1, 0, 0), -90)
                .translate((0, 0, -2.2))
            # .rotate((0, 0, 0), (0, 0, 1), h_screw.angle)
        ))
        wg = wg.cut(t)

    elif not front and rear:
        wg = wg.intersect(wg_rear_mid_split(wg_slice))
        wg = wg.intersect(wg_rear_box_shape(wg_slice))

        t = wg_drivers.place_tweeter_shape(wg_slice, tweeter_r_list=[wg_slice.r0], shape=(
            cq.Workplane("XY")
                .text(txt=str(f"{wg_slice.ws.ordinal} R"), fontsize=1.0, distance=0.95 if rear_inner_split_top else 1.1, cut=True, clean=True)
                .rotate((0, 0, 0), (0, 0, 1), 180)
                .mirror(mirrorPlane='YZ', union=False)
                .rotate((0, 0, 0), (1, 0, 0), -90)
                .translate((0, 0, 3.4 if rear_inner_split_top else 2.2))
            # .rotate((0, 0, 0), (0, 0, 1), h_screw.angle)
        ))
        wg = wg.cut(t)

    wg = wg.cut(wg_tweeter_cuts(wg_slice, mirror=mirror))

    return wg


def wg_tweeter_cuts(wg_slice: WgSlice, mirror=False):
    tweeter_cuts = wg_drivers.place_tweeters(wg_slice, margin=True, mirror_cut=not mirror)
    tweeter_cuts_inverted = wg_drivers.place_tweeters(wg_slice, rotate_top_down=True, margin=True,
                                                      mirror_cut=not mirror)

    return tweeter_cuts.union(tweeter_cuts_inverted)


@cq_cache()
def wg_rear_split_box(wg_slice):
    return wg_drivers.place_tweeter_shape(
        wg_slice,
        tweeter_r_list=[wg_slice.r1],
        shape=(
            cq.Workplane("XY")
                .rect(50, 50)
                .extrude(50)
                .translate((0, 0, WG_REAR_SPLIT_HEIGHT))
        )
    )


@cq_cache()
def wg_rear_split_pins(wg_slice):
    pegs = []

    for x, y in [
        (pt2522_width / 4, 1.0),
        (pt2522_width / 4, 7.8),
        (3.85, 3.0),
        (3.75, 5.85),
    ]:
        pegs.append((
            filament_peg()
                .translate((-x, y, 0))
                .translate((0, 0, WG_REAR_SPLIT_HEIGHT))
        ))

    return wg_drivers.place_tweeter_shape(
        wg_slice,
        tweeter_r_list=[wg_slice.r1],
        shape=union_shapes_list(pegs).mirror(mirrorPlane='XZ', union=True)
    )


@cq_cache()
def wg_rear_split_supports(wg_slice):
    throat_support = wg_throat_shape(wg_slice.radius_list, shell=-WG_REAR_SPLIT_SCREW_LENGTH)
    split_screw = Screw(WG_REAR_SPLIT_SCREW_GAUGE)
    w = 3 * split_screw.heatsert_diameter
    support_box = place_rear_split_supports2(
        wg_slice,
        shape=(
            cq.Workplane("XY")
                .box(w, 10 + w, 10)
                .fillet(w * 0.45)
                .translate((0, -5, 0))
        )
    )
    # return support_box
    supp = throat_support.intersect(support_box)
    supp = supp.fillet(wg_slice.shell_thickness * 0.45)

    return supp


@cq_cache()
def wg_rear_split_support_screw_heatserts(wg_slice):
    split_screw = Screw(WG_REAR_SPLIT_SCREW_GAUGE)
    return place_rear_split_supports2(
        wg_slice,
        shape=(
            cq.Workplane("XY")
                .circle(split_screw.heatsert_diameter / 2)
                .extrude(3)
                .translate((0, 0, -1))
        )
    )


@cq_cache()
def wg_rear_split_support_screws(wg_slice):
    offset = 0.01 - 0.07
    split_screw = Screw(WG_REAR_SPLIT_SCREW_GAUGE)
    support_screw = place_rear_split_supports2(
        wg_slice,
        shape=(
            cq.Workplane("XY")
                .circle(split_screw.screw_diameter_loose / 2)
                .extrude(3)
                .translate((0, 0, -1 - offset))
        )
    )

    x = (split_screw.screw_diameter_loose - split_screw.screw_head_diameter) / 2
    support_screw_head = place_rear_split_supports2(
        wg_slice,
        shape=(
            cq.Workplane("XY")
                .workplane(offset=0)
                .circle(split_screw.screw_head_diameter / 2)
                .workplane(offset=-x)
                .circle(split_screw.screw_diameter_loose / 2)
                .loft(combine=True)
                .union((
                cq.Workplane("XY")
                    .circle(split_screw.screw_head_diameter / 2)
                    .extrude(-1)
                # .translate((0, 0, x))
            ))
                .translate((0, 0, offset))
        )
    )

    support_screw = support_screw.union(support_screw_head)
    return support_screw


@cq_cache()
def place_tweeter_supports(wg_slice: WgSlice, middle_cut=True, mirror_cut=False, intersect_throat_verts=True):
    # h = 2 * (wg_slice.ws.z_offset + wg_slice.shell_thickness) - 0.2
    h = TWEETER_SCREW_LENGTH + 1.0

    s = (
        cq.Workplane("XY")
            .box(pt2522_width + 0.5, TWEETER_HEIGHT + 0.2 + wg_slice.shell_thickness, h)
            .edges(">Z or <Z")
            .chamfer(wg_slice.shell_thickness / 2)
    )
    if middle_cut:
        s = s.cut((cq.Workplane("XY")
                   .box(5.0, TWEETER_HEIGHT + 1.0, 2 * h)
                   .cut((
            cq.Workplane("XY")
                .box(5.0, TWEETER_PLATFORM_HEIGHT, 3 * h)
        ))
                   .translate((0, 0, h))
                   ))

    b = wg_drivers.place_tweeter_shape(wg_slice, shape=s)

    if intersect_throat_verts:
        b = b.intersect(wg_throat_with_verts_shape(wg_slice.radius_list))

    if mirror_cut:
        b = b.cut(wg_mirror_cut_shape(wg_slice.radius_list))

    return b


@cq_cache()
def wg_hole_reinforcements(wg_slice: WgSlice, mirror=False, inner_rear_split_screws=True):
    # wg = wg_ply.ply_peg_holes(wg_slice, peg_diameter=PLY_PEG_DIAMETER * 1.5, peg_length=PLY_PEG_LENGTH + 0.5)

    screw_holes = wg_slice.screw_holes

    screw_length = WG_PLY_SCREW_LENGTH + 1.5

    shapes = []
    for hole in wg_slice.screw_holes.screws:
        screw = wg_ply.place_wg_shape(hole.angle, hole.y_offset, shape=(
            cq.Workplane("XY")
                .circle(screw_holes.screw.heatsert_diameter_reinforced / 2)
                .extrude(-screw_length / 2)
        ))
        screw = screw.chamfer(0.15)
        shapes.append(screw)

    wg = wg_functions.union_shapes_list(shapes)

    wg = wg.union(wg_drivers.place_tweeter_heatsert_reinforcements(wg_slice, mirror=mirror))

    if inner_rear_split_screws:
        split_screw = Screw(WG_REAR_SPLIT_SCREW_GAUGE)
        wg = wg.union(place_rear_split_supports2(
            wg_slice,
            shape=(
                cq.Workplane("XY")
                    .circle(split_screw.heatsert_diameter_reinforced / 2)
                    .extrude(20)
                    .translate((0, 0, -10))
            )
        ))

    if mirror:
        wg = wg.mirror(mirrorPlane='YZ', union=True)

    return wg


@cq_cache()
def wg_rear_box_shape(wg_slice: WgSlice):
    r0, r1, r2 = wg_slice.r0, wg_slice.r1, wg_slice.r2
    return wg_drivers.place_tweeter_shape(wg_slice, tweeter_r_list=[middle_value(r0, r1), middle_value(r1, r2)], shape=(
        cq.Workplane("XY")
            .box(50, TWEETER_HEIGHT + 0.5, 50)
            .translate((0, 0, 25))
    ))


@cq_cache()
def wg_slice_inner_tweet(wg_slice: WgSlice, tweeter_flanges, mirror=False, front=True):
    wg_full = wg_throat_with_verts_shape(wg_slice.radius_list)
    if tweeter_flanges:
        wg_full = wg_full.union(wg_tweeter_flanges(wg_slice, fillet_width=1.5, mid_flare_height=2.5))

    b = (
        place_tweeter_supports(wg_slice, mirror_cut=False, intersect_throat_verts=False)
            .cut(wg_mid_split_pins(wg_slice, front=front))
            .cut(wg_drivers.place_tweeter_screws(wg_slice, mirror=False))
            .cut(top_bot_margin_cuts(wg_slice))
            .cut(wg_tweeter_cuts(wg_slice, mirror=mirror))
            .cut(wg_rear_mid_split(wg_slice))
    )

    if front:
        t = wg_drivers.place_tweeter_shape(wg_slice, tweeter_r_list=[wg_slice.r0], shape=(
            cq.Workplane("XY")
                .text(txt=str(f"{wg_slice.ws.ordinal} F"), fontsize=0.75, distance=0.4,
                      cut=True, clean=True)
                .translate((0, 0, -0.1))
                .mirror(mirrorPlane='YZ', union=False)
                .rotate((0, 0, 0), (1, 0, 0), -90)
                .translate((0, 0, -0.6))
            # .rotate((0, 0, 0), (0, 0, 1), h_screw.angle)
        ))
        b = b.cut(t)

    if front:
        b = b.cut(wg_rear_box_shape(wg_slice))
    else:
        b = b.intersect(wg_rear_box_shape(wg_slice))

    b = (
        b
            .intersect(wg_front_mid_split(wg_slice))
            .intersect(wg_full)
    )

    return b
