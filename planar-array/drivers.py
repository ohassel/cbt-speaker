from geometry_2d import P as P
import time
# https://github.com/CadQuery/cadquery-plugins/tree/main/plugins/cq_cache
# decorate your functions that build computationally heavy shapes
from hacked_cq_cache import cq_cache
import hacked_cq_cache
import wg_functions

PLANAR_BASE_THICKNESS = 0.3
PLANAR_TOP_EDGE_OFFSET = 1.1
PLANAR_HEIGHT = 20
PLANAR_WIDTH = 6.5

MM_TO_CM = 0.1


def planar(connector_flat_tab_margin=True, show_connectors=True, extrude_screws=True, render_ridges=True,
           only_cutout=False):
    import cadquery as cq
    width = PLANAR_WIDTH
    height = PLANAR_HEIGHT
    base_thickness = PLANAR_BASE_THICKNESS

    middle_thickness = 1.1

    base = cq.Workplane("XY") \
        .box(width, height, base_thickness) \
        .edges("|Z") \
        .fillet(0.7)

    top_edge_offset = PLANAR_TOP_EDGE_OFFSET

    top = cq.Workplane("XY") \
        .workplane(offset=base_thickness / 2) \
        .rect(width - 2 * top_edge_offset, height - 2 * top_edge_offset) \
        .workplane(offset=(middle_thickness - base_thickness) / 2) \
        .rect(width - 2 * top_edge_offset - 0.4, height - 2 * top_edge_offset - 0.4) \
        .loft(combine=True) \
        .faces("+Z") \
        .fillet(0.2) \
        .mirror(mirrorPlane='XY', union=True)

    result = base.union(top)

    if show_connectors:
        connectors = cq.Workplane("XY") \
            .workplane(offset=base_thickness / 2) \
            .moveTo(0, height - 0.95 - height / 2) \
            .rect(3, 0.9) \
            .extrude(2.5 if connector_flat_tab_margin else 1)
        result = result.union(connectors)

    if render_ridges:
        ridges = cq.Workplane("XY") \
            .workplane(offset=middle_thickness / 2) \
            .moveTo(0, 5.3 / 2) \
            .rect(3.3, 0.3) \
            .extrude(0.1) \
            .faces("+Z") \
            .fillet(0.05) \
            .mirror(mirrorPlane='XY', union=True) \
            .mirror(mirrorPlane='XZ', union=True)

        result = result.union(ridges)

    if only_cutout:
        return result

    screw_hole_diameter = 0.3
    rivet_up_diameter = 0.55
    rivet_down_diameter = 0.4
    connector_screw_diameter = 0.7
    for xy_diameter in [
        # Screw holes
        (0.5, 4.25, screw_hole_diameter, screw_hole_diameter),
        (0.5, height - 4.25, screw_hole_diameter, screw_hole_diameter),

        (width - 0.5, 4.25, screw_hole_diameter, screw_hole_diameter),
        (width - 0.5, height - 4.25, screw_hole_diameter, screw_hole_diameter),

        # Rivets
        (width / 2, 0.5, rivet_up_diameter, rivet_down_diameter),

        (0.5, 1.4, rivet_up_diameter, rivet_down_diameter),
        (0.5, 7.15, rivet_up_diameter, rivet_down_diameter),
        (0.5, height - 7.15, rivet_up_diameter, rivet_down_diameter),
        (0.5, height - 1.4, rivet_up_diameter, rivet_down_diameter),

        (width - 0.5, 1.4, rivet_up_diameter, rivet_down_diameter),
        (width - 0.5, 7.15, rivet_up_diameter, rivet_down_diameter),
        (width - 0.5, height - 7.15, rivet_up_diameter, rivet_down_diameter),
        (width - 0.5, height - 1.4, rivet_up_diameter, rivet_down_diameter),

        # Connector screws
        (1.3, height - 0.9, connector_screw_diameter, connector_screw_diameter),
        (width - 1.3, height - 0.9, connector_screw_diameter, connector_screw_diameter),
    ]:
        plus = cq.Workplane("XY").moveTo(xy_diameter[0] - width / 2, xy_diameter[1] - height / 2).circle(
            xy_diameter[2] / 2).extrude(-0.7)
        minus = cq.Workplane("XY").moveTo(xy_diameter[0] - width / 2, xy_diameter[1] - height / 2).circle(
            xy_diameter[3] / 2).extrude(0.7)
        result = result.union(plus) if extrude_screws else result.cut(plus)
        result = result.union(minus) if extrude_screws else result.cut(minus)

    connector_base = cq.Workplane("XY") \
        .workplane(offset=base_thickness / 2) \
        .moveTo(0, height - 0.95 - height / 2) \
        .rect(4.8, 0.9) \
        .extrude(0.2) \
        .mirror(mirrorPlane='XY', union=True)
    result = result.union(connector_base)

    for row in [1.8, 2.75]:
        for column in [2.9, 4.1, 5.3, 6.5, 8.2, 9.4]:
            result = result.cut(
                cq.Workplane("XY") \
                    .moveTo(row - width / 2, column - height / 2) \
                    .rect(0.5, 0.5) \
                    .extrude(2) \
                    .mirror(mirrorPlane='XY', union=True) \
                    .mirror(mirrorPlane='XZ', union=True) \
                    .mirror(mirrorPlane='YZ', union=True)
            )
            for offset in [0.25, -0.25]:
                result = result.cut(
                    cq.Workplane("XY") \
                        .moveTo(row - width / 2, offset + column - height / 2) \
                        .circle(0.5 / 2) \
                        .extrude(2) \
                        .mirror(mirrorPlane='XY', union=True) \
                        .mirror(mirrorPlane='XZ', union=True) \
                        .mirror(mirrorPlane='YZ', union=True)
                )

    return result.translate((0, 0, -PLANAR_BASE_THICKNESS / 2))


def planar_bracket(board_thickness=0.4, cut_top_bottom=False, side_tab_from_edge=None, side_tab_margin=None):
    import cadquery as cq
    width = 6.5
    height = 20

    result = cq.Workplane("XY") \
        .workplane(offset=PLANAR_BASE_THICKNESS) \
        .box(width, height, board_thickness) \
        .edges("|Z") \
        .fillet(0.7)

    if side_tab_from_edge is not None:
        y_margin = 0.5
        result = result.cut(
            cq.Workplane("XY") \
                .workplane(offset=PLANAR_BASE_THICKNESS) \
                .moveTo(width / 2, height / 2 - side_tab_from_edge + y_margin) \
                .box(side_tab_margin * 2, board_thickness + y_margin * 2, board_thickness) \
                .mirror(mirrorPlane='XZ', union=True) \
                .mirror(mirrorPlane='YZ', union=True)
        )

    edge_width = 1.1
    result = result.cut(
        cq.Workplane("XY") \
            .workplane(offset=PLANAR_BASE_THICKNESS) \
            .box(width - 2 * edge_width, height - 2 * edge_width, board_thickness)
    )

    screw_hole_diameter = 0.3
    rivet_rear_diameter = 0.45

    for xy_diameter in [
        # Screw holes
        (0.5, 4.25, screw_hole_diameter),
        (0.5, height - 4.25, screw_hole_diameter),

        (width - 0.5, 4.25, screw_hole_diameter),
        (width - 0.5, height - 4.25, screw_hole_diameter),

        # Rivets
        (width / 2, 0.5, rivet_rear_diameter),

        (0.5, 1.4, rivet_rear_diameter),
        (0.5, 7.15, rivet_rear_diameter),
        (0.5, height - 7.15, rivet_rear_diameter),
        (0.5, height - 1.4, rivet_rear_diameter),

        (width - 0.5, 1.4, rivet_rear_diameter),
        (width - 0.5, 7.15, rivet_rear_diameter),
        (width - 0.5, height - 7.15, rivet_rear_diameter),
        (width - 0.5, height - 1.4, rivet_rear_diameter),
    ]:
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(xy_diameter[0] - width / 2, xy_diameter[1] - height / 2) \
                .circle(xy_diameter[2] / 2) \
                .extrude(1) \
                .mirror(mirrorPlane='XY', union=True)
        )

    # Cut slot for connector tabs
    connector_rectangle_y_offset = 0.95
    connector_tab_height = 0.9
    result = result.cut(
        cq.Workplane("XY") \
            .moveTo(0, height - connector_rectangle_y_offset - height / 2) \
            .rect(4.8, connector_tab_height) \
            .extrude(1) \
            .mirror(mirrorPlane='XY', union=True)
    )

    # boop
    if cut_top_bottom:
        result = result.intersect(
            cq.Workplane("XY") \
                .workplane(offset=PLANAR_BASE_THICKNESS) \
                .box(width, height - (side_tab_from_edge - 0.4) * 2, board_thickness)
        )

    return result.translate((0, 0, -PLANAR_BASE_THICKNESS / 2))


def cylinder(
        x,
        y,
        d,
        extrude,
        base_width,
        z_offset=0.0,
        mirror=True,
):
    import cadquery as cq
    xx = (cq.Workplane("XY")
          .moveTo(x, y)
          .circle(d / 2)
          .extrude(extrude)
          .translate((-base_width / 2, 0, 0))
          )

    if mirror:
        xx = (xx
              .mirror(mirrorPlane='YZ', union=True)
              )

    xx = (xx
          .translate((base_width / 2, 0, 0))
          .translate((0, 0, z_offset))
          )

    return xx


def box(
        x,
        y,
        w,
        h,
        extrude,
        base_width,
        z_offset=0.0,
        mirror=True,
):
    import cadquery as cq
    # return (cq.Workplane("XY")
    #     .moveTo(x, y)
    #     .box(w, h, extrude)
    #     .translate((0, 0, extrude / 2))
    #     .translate((-base_width / 2, 0, 0))
    #     .mirror(mirrorPlane='YZ', union=True)
    #     .translate((base_width / 2, 0, 0))
    #     .translate((0, 0, z_offset))
    #         )

    xx = (cq.Workplane("XY")
          .moveTo(x, y)
          .box(w, h, extrude)
          .translate((0, 0, extrude / 2))
          .translate((-base_width / 2, 0, 0))
          )

    if mirror:
        xx = (xx
              .mirror(mirrorPlane='YZ', union=True)
              )

    xx = (xx
          .translate((base_width / 2, 0, 0))
          .translate((0, 0, z_offset))
          )

    return xx


# def lt2(cut_audio_holes=True, union_heat_inserts=False, union_rear_mount_block_height=0.0,
#         union_rear_mount_block_margin=0.3):
#     import cadquery as cq
#
#     base_width = 3.8
#     base_height = 7.1
#     base_thickness = 0.2
#
#     platform_top_y_offset = 0.6
#     platform_bottom_y_offset = 0.9
#
#     platform_width = 3.2
#     platform_height = 7.1 - platform_bottom_y_offset - platform_top_y_offset
#     platform_thickness = 0.3
#
#     platform_x_offset = (base_width - platform_width) / 2
#
#     # Base
#     r = cq.Workplane("XY").center(0, 0) \
#         .moveTo(0, 0) \
#         .lineTo(base_width, 0) \
#         .lineTo(base_width, base_height) \
#         .lineTo(0, base_height) \
#         .close().extrude(base_thickness) \
#         .edges("|Z") \
#         .fillet(0.15)
#
#     # Platform
#     r = r.union(
#         cq.Workplane("XY").center(0, 0) \
#             .moveTo(0, 0) \
#             .lineTo(platform_width, 0) \
#             .lineTo(platform_width, platform_height) \
#             .lineTo(0, platform_height) \
#             .close().extrude(base_thickness + platform_thickness) \
#             .edges("|Z") \
#             .fillet(0.15) \
#             .faces("+Z") \
#             .fillet(0.15) \
#             .translate((platform_x_offset, platform_bottom_y_offset, 0))
#     )
#
#     r = r \
#         .translate((0, 0, -base_thickness / 2))
#
#     r = r.mirror(mirrorPlane='YX', union=True)
#
#     # screw_hole_diameter = 0.3
#
#     union_rear_mount_block = union_rear_mount_block_height > base_thickness
#
#     if not union_rear_mount_block:
#         r = r.cut(cylinder(x=0.15, y=0.7, d=0.3, extrude=2, base_width=base_width, z_offset=-1))
#         r = r.cut(cylinder(x=0.15, y=6.8, d=0.3, extrude=2, base_width=base_width, z_offset=-1))
#
#         r = r.cut(box(x=0.15 / 2, y=0.7, w=0.15, h=0.3, extrude=2, base_width=base_width, z_offset=-1))
#         r = r.cut(box(x=0.15 / 2, y=6.8, w=0.15, h=0.3, extrude=2, base_width=base_width, z_offset=-1))
#
#     if union_heat_inserts:
#         r = r.union(cylinder(x=0.15, y=0.7, d=0.4, extrude=-0.67 - base_thickness, base_width=base_width,
#                              z_offset=base_thickness / 2))
#         r = r.union(cylinder(x=0.15, y=6.8, d=0.4, extrude=-0.67 - base_thickness, base_width=base_width,
#                              z_offset=base_thickness / 2))
#
#     r = r.union(cylinder(x=0.7, y=0.6, d=0.4, extrude=-0.15, base_width=base_width, z_offset=-base_thickness / 2))
#     r = r.union(
#         cylinder(x=0.7, y=base_height - 0.3, d=0.4, extrude=-0.15, base_width=base_width, z_offset=-base_thickness / 2))
#
#     # r = r.union(cylinder(x=0.7, y=0.6, d=0.5, extrude=0.3, z_offset=base_thickness / 2))  # covered by terminal tabs
#     r = r.union(
#         cylinder(x=0.7, y=base_height - 0.3, d=0.5, extrude=0.3, base_width=base_width, z_offset=base_thickness / 2))
#
#     if union_rear_mount_block:  # 0.1 because floating point comparisons are unreliable
#
#         mount_margin = union_rear_mount_block_margin
#
#         r = r.union(
#             cq.Workplane("XY").center(0, 0) \
#                 .moveTo(0, 0) \
#                 .lineTo(base_width + mount_margin, 0) \
#                 .lineTo(base_width + mount_margin, base_height + mount_margin) \
#                 .lineTo(0, base_height + mount_margin) \
#                 .close().extrude(union_rear_mount_block_height) \
#                 .edges("|Z") \
#                 .fillet(0.15) \
#                 .translate((-mount_margin / 2, -mount_margin / 2, 0))
#                 .translate((0, 0, base_thickness / 2))
#         )
#     else:
#         # platform holes
#         if cut_audio_holes:
#             for xx in [0.85, 1.55]:
#                 t = base_height - 0.95
#                 b = 1.27
#                 r = r.cut(cylinder(x=xx, y=b, d=0.35, extrude=2, base_width=base_width, z_offset=-1))
#                 r = r.cut(cylinder(x=xx, y=t, d=0.35, extrude=2, base_width=base_width, z_offset=-1))
#                 r = r.cut(
#                     box(x=xx, y=t - ((t - b) / 2), w=0.35, h=t - b, extrude=2, base_width=base_width, z_offset=-1))
#
#         terminal_block_width = 2.9
#         terminal_block_height = 0.6
#         terminal_block_thickness = 0.3
#         # terminal_block_thickness = 0.8  # Including terminal tabs
#         terminal_block_x_offset = (base_width - terminal_block_width) / 2
#
#         r = r.union(
#             cq.Workplane("XY").center(0, 0) \
#                 .moveTo(0, 0) \
#                 .lineTo(terminal_block_width, 0) \
#                 .lineTo(terminal_block_width, terminal_block_height) \
#                 .lineTo(0, terminal_block_height) \
#                 .close().extrude(base_thickness / 2 + terminal_block_thickness) \
#                 .translate((terminal_block_x_offset, 0.3, 0))
#         )
#
#         r = r.union(
#             box(x=0.35 + 1.0 / 2, y=0.3 + terminal_block_height / 2, w=1.0, h=terminal_block_height, extrude=0.7,
#                 base_width=base_width, z_offset=base_thickness / 2 + terminal_block_thickness))
#
#     r = r \
#         .translate((-base_width / 2, -base_height / 2, 0))
#
#     return r


def get_pt2522_holes(mirror=False):
    base_width = 6.75
    base_height = 8.93
    # base_thickness = 0.25

    edge_screw_x = (base_width - 6.2) / 2
    edge_screw_y = (base_height - 7.9) / 2

    # inner_height = screw_inner_height

    x_offset = -base_width / 2
    y_offset = -base_height / 2

    holes = []
    for y in [edge_screw_y, base_height - edge_screw_y]:
        x = x_offset + edge_screw_x
        holes.append((x, y_offset + y))
        if mirror:
            holes.append((-x, y_offset + y))

    return holes


@cq_cache()
def pt2522_holes(
        screw_inner_height=1.0 + 0.25 / 2,
        screw_inner_diameter=0.35,

        screw_outer_height=0,
        screw_outer_diameter=0.8,

        screw_outer_angle=None,
        screw_outer_angle_height=5.0,
        screw_outer_angle_diameter=1.2,

        mirror=True,
):
    import cadquery as cq

    base_width = 6.75
    base_height = 8.93
    # base_thickness = 0.25

    edge_screw_x = (base_width - 6.2) / 2
    edge_screw_y = (base_height - 7.9) / 2

    # inner_height = screw_inner_height

    shapes = []

    for y in [edge_screw_y, base_height - edge_screw_y]:
        top = y == edge_screw_y

        if screw_inner_height:
            if screw_inner_height > 0:
                z_offset = 0.1
            else:
                z_offset = -0.1
            shapes.append(
                cylinder(x=edge_screw_x, y=y, d=screw_inner_diameter,
                         extrude=screw_inner_height + screw_outer_height + z_offset,
                         base_width=base_width,
                         z_offset=-z_offset,
                         mirror=mirror))

        if screw_outer_height is not None and screw_outer_height != 0:
            shapes.append(cylinder(x=edge_screw_x, y=y, d=screw_outer_diameter,
                                   extrude=screw_outer_height,
                                   base_width=base_width,
                                   z_offset=screw_inner_height,
                                   mirror=mirror))

        if screw_outer_angle is not None and screw_outer_angle == 0:
            shapes.append(cylinder(x=edge_screw_x, y=y, d=screw_outer_angle_diameter,
                                   extrude=screw_outer_angle_height,
                                   base_width=base_width,
                                   z_offset=screw_inner_height + screw_outer_height,
                                   mirror=mirror))
        elif screw_outer_angle is not None:
            z_offset = screw_inner_height + screw_outer_height + 0.5
            xx = (cq.Workplane("XY")
                  .circle(screw_outer_angle_diameter / 2)
                  .extrude(screw_outer_angle_height)
                  .rotate((0, 0, 0), (1, 0, 0), screw_outer_angle if top else -screw_outer_angle)
                  .translate((edge_screw_x, y, 0))
                  )

            if mirror:
                xx = (xx
                      .mirror(mirrorPlane='YZ', union=True)
                      )

            xx = (xx
                  .translate((0, 0, z_offset))
                  )

            shapes.append(xx)

    r = wg_functions.union_shapes_list(shapes)
    r = r.translate((-base_width / 2, -base_height / 2, 0))

    return r


@cq_cache()
def pt2522(cut_audio_holes=True,
           cut_screw_holes=True,
           terminal_block_depth=0.7,  # Bigger because some tabs are thicker. Minimum 0.25 + 0.1 on "flat" drivers
           terminal_block_chamfer=None,
           terminal_tabs_height=1.5,
           terminal_tab_width=0.5,
           terminal_tabs_margin=0.0,
           render_screw_holes_and_rivets_and_terminals=True,
           with_margin=False,
           base_xy_margin_cm=0.2,
           platform_xy_margin_cm=0.2,
           ):
    import cadquery as cq
    # print(f"terminal_block_depth={terminal_block_depth}")

    margin = 1 if with_margin else 0

    base_width = 6.75
    base_height = 8.93
    base_thickness = 0.25 + 0.1 * margin

    # platform_top_y_offset = 1.15
    # platform_bottom_y_offset = 1.15

    xy_margin = margin * 0.05 if margin else 0

    platform_width = 5.77 + xy_margin  # margin
    platform_height = 6.52 + xy_margin  # margin
    platform_thickness = 0.94 + 0.025  # margin
    if with_margin:
        platform_thickness += 0.1

    platform_edge_fillet = 0.15
    platform_top_fillet = 0.25

    platform_x_offset = (base_width - platform_width) / 2

    # Base
    r = (
        cq.Workplane("XY")
            .center(base_width / 2, base_height / 2)
            .box(base_width + base_xy_margin_cm, base_height + base_xy_margin_cm, base_thickness)
            .edges("|Z")
            .fillet(0.55)
    )

    # Platform
    r = r.union((
        cq.Workplane("XY")
            .center(base_width / 2, base_height / 2)
            .box(platform_width + platform_xy_margin_cm, platform_height + platform_xy_margin_cm, platform_thickness)
            # .edges("|Z")
            # .fillet(0.15)
            .faces("|Z")
            .fillet(platform_top_fillet)
    ))

    # return r

    edge_screw_x = (base_width - 6.2) / 2
    edge_screw_y = (base_height - 7.9) / 2

    if render_screw_holes_and_rivets_and_terminals:

        if cut_screw_holes:
            r = r.cut(cylinder(x=edge_screw_x, y=edge_screw_y, d=0.35, extrude=2, base_width=base_width, z_offset=-1))
            r = r.cut(cylinder(x=edge_screw_x, y=base_height - edge_screw_y, d=0.35, extrude=2, base_width=base_width,
                               z_offset=-1))

            r = r.cut(
                box(x=edge_screw_x / 2, y=edge_screw_y, w=0.28, h=0.35, extrude=2, base_width=base_width, z_offset=-1))
            r = r.cut(
                box(x=edge_screw_x / 2, y=base_height - edge_screw_y, w=0.28, h=0.35, extrude=2, base_width=base_width,
                    z_offset=-1))


        top_rivet_x = 1.125
        top_rivet_y = base_height - 0.55

        bottom_rivet_x = 1.45
        bottom_rivet_y = 0.8

        front_rivet_height = 0.3
        front_rivet_diameter = 0.7

        rear_rivet_diameter = 0.8

        r = r.union(cylinder(x=top_rivet_x, y=top_rivet_y, d=front_rivet_diameter, extrude=-front_rivet_height,
                             base_width=base_width,
                             z_offset=-base_thickness / 2))
        r = r.union(
            cylinder(x=0.3, y=base_height / 2, d=front_rivet_diameter, extrude=-front_rivet_height,
                     base_width=base_width,
                     z_offset=-base_thickness / 2))
        r = r.union(cylinder(x=bottom_rivet_x, y=bottom_rivet_y, d=front_rivet_diameter, extrude=-front_rivet_height,
                             base_width=base_width,
                             z_offset=-base_thickness / 2))

        r = r.union(cylinder(x=top_rivet_x, y=top_rivet_y, d=rear_rivet_diameter, extrude=0.5, base_width=base_width,
                             z_offset=base_thickness / 2))
        r = r.union(
            cylinder(x=0.3, y=base_height / 2, d=rear_rivet_diameter, extrude=0.5, base_width=base_width,
                     z_offset=base_thickness / 2))
        r = r.union(
            cylinder(x=bottom_rivet_x, y=bottom_rivet_y, d=rear_rivet_diameter, extrude=0.7, base_width=base_width,
                     z_offset=base_thickness / 2))

        if cut_audio_holes:
            for y_tuple in [
                # mm from top
                (16.7, 26.7),
                (28.1, 38.1),
                (39.5, 49.5),
                (50.9, 60.9),
                (62.3, 72.3),
            ]:
                y_top = y_tuple[0]
                y_bot = y_tuple[1]

                y_center = (y_bot - y_top) / 2 + y_top

                width = 4.6

                for xx in [19.3, 29]:

                    y_top_m = y_top + width / 2
                    y_bot_m = y_bot - width / 2

                    for y in [y_top_m, y_bot_m]:
                        r = r.cut(
                            cylinder(x=xx * MM_TO_CM, y=base_height - (y * MM_TO_CM), d=width * MM_TO_CM, extrude=2,
                                     base_width=base_width, z_offset=-1))

                    r = r.cut(box(x=xx * MM_TO_CM, y=base_height - y_center * MM_TO_CM, w=width * MM_TO_CM,
                                  h=(y_bot_m - y_top_m) * MM_TO_CM, extrude=2, base_width=base_width, z_offset=-1))

        #  Terminals
        terminal_block_width = 4.7 + terminal_tabs_margin  # 4.7 without margin
        terminal_block_height = 0.82 + terminal_tabs_margin / 2  # 0.82 without margin

        terminal_block_y_offset = ((base_height - platform_height) / 2) - terminal_block_height / 2

        block = box(x=base_width / 2, y=terminal_block_y_offset + terminal_tabs_margin / 2, w=terminal_block_width,
                    h=terminal_block_height + terminal_tabs_margin,
                    extrude=terminal_block_depth, base_width=base_width, z_offset=base_thickness / 2)

        if terminal_block_chamfer:
            block = (block
                     .edges(">Z")
                     .chamfer(terminal_block_chamfer)
                     )

        r = r.union(block)

        terminal_tabs_width = 2.7

        if terminal_block_depth and terminal_tabs_height:
            terminal_tab_width = terminal_tab_width + terminal_tabs_margin
            terminal_tab_height = terminal_block_height + terminal_tabs_margin

            r = r.union(
                box(x=base_width / 2 - terminal_tabs_width / 2, y=terminal_block_y_offset, w=terminal_tab_width,
                    h=terminal_tab_height,
                    extrude=terminal_tabs_height, base_width=base_width, z_offset=base_thickness / 2))

    r = r \
        .translate((-base_width / 2, -base_height / 2, 0)) \
        .mirror(mirrorPlane='YZ', union=True)

    return r


@cq_cache()
def pt2522_flanges(fillet_width=None, mid_flare_height=None, flare_depth=12, only_flare=False):
    import cadquery as cq

    if fillet_width is None:
        fillet_width = 0.75

    base_width = 6.75
    base_height = 8.93
    base_thickness = 0.25

    if fillet_width > 0.75:
        thick = True
    else:
        thick = False

    platform_top_y_offset = 1.15
    platform_bottom_y_offset = 1.15

    platform_width = 5.77  # margin
    platform_height = 6.52  # margin
    platform_thickness = fillet_width + base_thickness * 2

    platform_edge_fillet = 0.15
    platform_top_fillet = 0.27

    platform_x_offset = (base_width - platform_width) / 2

    if mid_flare_height:
        flare_w = 0.25
        flare = (
            cq.Workplane("XY")
                .lineTo(flare_w, 0)
                .lineTo(flare_w, base_thickness)
                .threePointArc((flare_w - 0.05, 1.0 + base_thickness), (0, mid_flare_height + base_thickness))
                .lineTo(0, 0)
                .close()
                # .mirrorX()
                .mirrorY()
                .extrude(flare_depth)
                .translate((0, 0, -flare_depth / 2))
                .rotate((0, 0, 0), (1, 0, 0), -90)
        )
        if only_flare:
            return flare.mirror(mirrorPlane='XY', union=True)

    a = 0
    b = 1
    if thick:
        a = -1
        b = 2

    b = (cq.Workplane("XY").center(0, 0)
         .moveTo(a * base_width, a * base_height)
         .lineTo(b * base_width, a * base_height)
         .lineTo(b * base_width, b * base_height)
         .lineTo(a * base_width, b * base_height)
         .close()
         .extrude(platform_thickness)
         )

    bb = b

    for y_tuple in [
        # mm from top
        (16.7, 26.7),
        (28.1, 38.1),
        (39.5, 49.5),
        (50.9, 60.9),
        (62.3, 72.3),
    ]:
        y_top = y_tuple[0]
        y_bot = y_tuple[1]

        y_center = (y_bot - y_top) / 2 + y_top

        width = 4.6

        for xx in [19.3, 29]:

            y_top_m = y_top + width / 2
            y_bot_m = y_bot - width / 2

            r = bb

            for y in [y_top_m, y_bot_m]:
                r = r.cut(
                    cylinder(x=xx * MM_TO_CM, y=base_height - (y * MM_TO_CM), d=width * MM_TO_CM,
                             extrude=platform_thickness * 3,
                             base_width=base_width, z_offset=-1, mirror=False))

            r = r.cut(box(x=xx * MM_TO_CM, y=base_height - y_center * MM_TO_CM, w=width * MM_TO_CM,
                          h=(y_bot_m - y_top_m) * MM_TO_CM, extrude=platform_thickness * 3, base_width=base_width,
                          z_offset=-1, mirror=False))

            r = (r
                 .faces("+Z")
                 .fillet(fillet_width)
                 )

            c = bb.cut(r)
            b = b.cut(c)

    b = (b
         .translate((-base_width / 2, -base_height / 2, 0))
         )

    b = (b.intersect(b.mirror(mirrorPlane='YZ', union=False)))

    if thick:
        b = b.intersect(cq.Workplane("XY").box(length=base_width, width=base_height, height=platform_thickness * 2))

    if mid_flare_height:
        b = b.union(flare)

    b = b.mirror(mirrorPlane='XY', union=True)

    return b


def sb65(extrude_screws=False):
    import cadquery as cq
    width = 6.4
    diameter = 8

    base_thickness = 0.3

    base_square = cq.Workplane("XY") \
        .rect(width, width) \
        .extrude(0.3)

    base_circle = cq.Workplane("XY") \
        .circle(diameter / 2) \
        .extrude(0.3)

    result = base_square.intersect(base_circle)

    # Screw holes
    for angle in [45, 135, 225, 315]:
        center = P(angle=angle, length=7.2 / 2)
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(center.x, center.y) \
                .circle(0.65 / 2) \
                .extrude(0.2)
        )
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(center.x, center.y) \
                .circle(0.33 / 2) \
                .extrude(base_thickness)
        )
        if extrude_screws:
            result = result.union(
                cq.Workplane("XY") \
                    .moveTo(center.x, center.y) \
                    .circle(0.33 / 2) \
                    .extrude(1.0) \
                    .mirror(mirrorPlane='XY', union=True)
            )

    surround = cq.Workplane("XY") \
        .circle(5.7 / 2) \
        .extrude(-0.25) \
        .cut(
        cq.Workplane("XY") \
            .circle(4.5 / 2) \
            .extrude(-0.25)
    )
    result = result.union(surround)

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=base_thickness) \
            .circle(6.0 / 2) \
            .workplane(offset=0.8) \
            .circle(5.5 / 2) \
            .loft(combine=True)
    )

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=base_thickness + 0.8) \
            .circle(5.5 / 2) \
            .workplane(offset=1.1) \
            .circle(4.6 / 2) \
            .loft(combine=True)
    )

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=3.27 + base_thickness) \
            .circle(3.8 / 2) \
            .extrude(-1.5) \
            .faces("+Z") \
            .fillet(0.3)
    )

    # Connectors
    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=0.5) \
            .rect(1.0, 5.7) \
            .extrude(2.5)
    )

    return result


def dma90(extrude_screws=False, margin=False, surround_margin=0, surround_depth=0.3):
    import cadquery as cq

    margin_int = 1 if margin else 0

    width = 9 + 0.4 * margin_int
    # diameter = 8
    base_thickness = 0.5
    base_thickness_margin = 0.1

    screw_hole_outer_diameter = 0.7
    screw_hole_inner_diameter = 0.4
    screw_holes_diameter = 10.89

    base_square = (
        cq.Workplane("XY")
            .rect(width, width)
            .extrude(base_thickness + base_thickness_margin / 2 * margin_int)
            .edges("|Z")
            .fillet(0.25)
    )
    if margin:
        base_square = base_square.translate((0, 0, - base_thickness_margin / 2))

    result = base_square

    # Screw holes
    for angle in [45, 135, 225, 315]:
        center = P(angle=angle, length=screw_holes_diameter / 2)
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(center.x, center.y) \
                .circle(screw_hole_outer_diameter / 2) \
                .extrude(0.2)
        )
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(center.x, center.y) \
                .circle(screw_hole_inner_diameter / 2) \
                .extrude(base_thickness)
        )
        if extrude_screws:
            result = result.union(
                cq.Workplane("XY") \
                    .moveTo(center.x, center.y) \
                    .circle(screw_hole_inner_diameter / 2) \
                    .extrude(1.0) \
                    .mirror(mirrorPlane='XY', union=True)
            )

    surround = cq.Workplane("XY") \
        .circle((7.8 + surround_margin) / 2) \
        .extrude(-surround_depth) \
        .cut(
        cq.Workplane("XY") \
            .circle(6.3 / 2) \
            .extrude(-surround_depth)
    )
    result = result.union(surround)

    frame = (
        cq.Workplane("YZ")
            .center(0, base_thickness)
            .moveTo(0, 0)
            .lineTo(8.4 / 2, 0)

        #     .spline([
        #     # (8.4 / 2, 0),
        #     (3.85 / 1, 0.8),
        #     (3.42 / 1, 1.7),
        #     (3.05 / 1, 2.1),
        #     (2.1 / 1, 2.55),
        # ])

            .lineTo(3.85 / 1, 0.8)
            .lineTo(3.42 / 1, 1.7)
            .lineTo(3.05 / 1, 2.1)
            .lineTo(2.1 / 1, 2.55)
            .lineTo(2.1 / 1, 2.8)
            .lineTo(0, 2.8)

            # .lineTo(3.5 / 2, -2.8)
            # .lineTo(3.5 / 2, -3.4)
            # .lineTo(0, h)
            # .lineTo(0, 0)
            .close()

            .revolve(360, (0, 1, 0), (-0, 0, 0))

            # .extrude(1)
    )

    result = result.union(frame)

    magnet = (
        cq.Workplane("XY")
            .workplane(offset=base_thickness)
            .moveTo(0, 0)
            .circle(3.52 / 2)
            .extrude(4)
            .edges(">Z")
            .fillet(0.45)
    )

    result = result.union(magnet)


    # Connectors
    for r in [+45, -45]:
        con_r = 3.1
        con_w = 1.4
        con_d = 0.8
        con_h = 4

        con = (
            cq.Workplane("XY")
                .workplane(offset=base_thickness)
                .center(con_r - con_d / 2, 0)
                .rect(con_d, con_w)
                .extrude(con_h)
                .rotate((0, 0, 0), (0, 0, 1), r)
        )
        result = result.union(con)

    return result


def sb12(extrude_screws=False, margin=False, surround_margin=0, surround_depth=0.3):
    import cadquery as cq

    margin_int = 1 if margin else 0

    width = 9 + 0.4 * margin_int
    # # diameter = 8
    # base_thickness = 0.5
    # base_thickness_margin = 0.1
    #
    # screw_hole_outer_diameter = 0.7
    # screw_hole_inner_diameter = 0.4
    # screw_holes_diameter = 10.89

    base_thickness = 0.7
    surround_diameter = 9.4
    surround_depth = 0.35

    width = 10.9 + (0.2 if surround_margin else 0.0)

    outer_radius = 12.5 + (0.2 if surround_margin else 0.0)

    p_edge = P(width/2, 0)
    p_outer_radius_offset = p_edge - P(outer_radius, 0)

    outer_circle_slice = (
        cq.Workplane("XY")
            .center(0, 0)
            .circle(outer_radius)
            .extrude(base_thickness)
            .translate((p_outer_radius_offset.x, 0, 0))
            .intersect(
            (
                cq.Workplane("XY")
                .lineTo(15, 15)
                .lineTo(15, -15)
                .close()
                .extrude(base_thickness)
            )
        )
    )

    b = outer_circle_slice

    for r in [90, 180, 270]:
        b = b.union(outer_circle_slice.rotate((0, 0, 0), (0, 0, 1), r))

    b = (
        b.edges("|Z")
            .fillet(2)
    )

    surround = (
        cq.Workplane("XY")
            .circle((surround_diameter + surround_margin) / 2)
            .extrude(surround_depth)
            .cut(
            (
                cq.Workplane("XY")
                    .circle((surround_diameter - 4 * surround_depth) / 2)
                    .extrude(surround_depth)
            )
        )
    ).translate((0, 0, base_thickness))

    b = b.union(surround)

    frame = (
        cq.Workplane("YZ")
            .center(0, 0)

            .lineTo(9.8 / 2, 0)
            .lineTo(9.8 / 2, 0.3)

            .lineTo(7.9 / 2, 1.85)
            .lineTo(6.0 / 2, 2.85)

            # Magnet
            .lineTo(7.55 / 2, 2.85)
            .lineTo(7.55 / 2, 3.35)
            .lineTo(8.05 / 2, 3.35)

            .lineTo(8.05 / 2, 5.36)
            .lineTo(7.54 / 2, 5.36)
            .lineTo(7.54 / 2, 5.56)

            .lineTo(3.46 / 2, 5.97)
            .lineTo(0, 5.97)

            .close()

            .revolve(360, (0, 1, 0), (0, 0, 0))
    )

    frame = frame.rotate((0,0,0), (1,0,0), 180)

    b = b.union(frame)

    b = b.translate((0, 0, -base_thickness))
    b = b.rotate((0, 0, 0), (1, 0, 0), 180)

    return b


def sb10(extrude_screws=False, margin=False, surround_margin=0, surround_depth=0.36):
    import cadquery as cq

    margin_int = 1 if margin else 0

    width = 7.74 + 0.3 * margin_int
    diameter = 9.55

    base_thickness = 0.3

    surround_diameter = 7.0
    surround_depth = 0.36

    base_square = (
        cq.Workplane("XY")
            .rect(width, width)
            .extrude(base_thickness)
    )

    base_circle = (
        cq.Workplane("XY")
            .circle(diameter / 2)
            .extrude(base_thickness)
    )

    b = base_square.intersect(base_circle)

    surround = (
        cq.Workplane("XY")
            .circle((surround_diameter + surround_margin) / 2)
            .extrude(surround_depth)
            .cut(
            (
                cq.Workplane("XY")
                    .circle((surround_diameter - 4 * surround_depth) / 2)
                    .extrude(surround_depth)
            )
        )
    ).translate((0, 0, base_thickness))

    b = b.union(surround)

    frame = (
        cq.Workplane("YZ")
            .center(0, 0)
            .moveTo(0, 0)

            .lineTo(6.0 / 2, 0)
            .lineTo(5.2 / 2, 1.73)

            .lineTo(4.06 / 2, 2.615)

            # Magnet
            .lineTo(5.0 / 2, 2.615)
            .lineTo(5.0 / 2, 2.915)
            .lineTo(5.5 / 2, 2.915)
            .lineTo(5.5 / 2, 3.915)
            .lineTo(5.0 / 2, 3.915)
            .lineTo(5.0 / 2, 4.065)
            .lineTo(3.8 / 2, 4.415)
            .lineTo(0, 4.415)

            .close()

            .revolve(360, (0, 1, 0), (0, 0, 0))
    )

    frame = frame.rotate((0, 0, 0), (1, 0, 0), 180)

    b = b.union(frame)

    b = b.translate((0, 0, -base_thickness))
    b = b.rotate((0, 0, 0), (1, 0, 0), 180)

    return b


def ndxx_base(width, diagonal_width, base_shape):
    base = None
    for i in range(0, 8):
        if i % 2 == 0:
            length = width
        else:
            length = diagonal_width

        pc = P(length=length / 2, angle=i * 45)

        if base:
            base = base.lineTo(pc.x, pc.y)
        else:
            base = (
                base_shape
                    .moveTo(pc.x, pc.y)
            )

    base = base.close()
    return base


@cq_cache()
def nd64(
        extrude_screws=False,
        margin=False,
        base_margin_cutout_depth=1.0,

        width=6.4,
        diagonal_width=7.7,

        base_thickness=0.1,

        surround_diameter=5.15,
        surround_depth=0.4,
        surround_margin=0,

        render_screws=True,
        screw_union=None,
        screw_holes_diameter=6.6,
        screw_hole_diameter=0.42,
        screw_hole_depth=1.0,

        basket=[
            (5.25 / 2, 0),
            (5.05 / 2, 1.4),

            (4.3 / 2, 1.4),
            (4.0 / 2, 1.9),

            (0, 1.9),
        ],
        magnet_diameter=3.45,
        magnet_length=3.45,
        magnet_fillet=0.3,
):
    import cadquery as cq

    margin_int = 1 if margin else 0

    width = width + 0.3 * margin_int
    diagonal_width = diagonal_width + 0.3 * margin_int

    base = ndxx_base(width, diagonal_width, base_shape=cq.Workplane("XY").center(0, 0))

    base = base.extrude(base_thickness + base_margin_cutout_depth * margin_int)

    if margin:
        base = base.translate((0, 0, -base_margin_cutout_depth * margin_int))

    base = (
        base.edges("|Z")
            .fillet(0.4)
    )

    b = base.intersect(base)

    surround = (
        cq.Workplane("XY")
            .circle((surround_diameter + surround_margin) / 2)
            .extrude(surround_depth)
    )
    if not margin:
        surround = surround.cut(
            (
                cq.Workplane("XY")
                    .circle((surround_diameter - 4 * surround_depth) / 2)
                    .extrude(surround_depth)
            )
        )
    surround = surround.translate((0, 0, base_thickness))


    b = b.union(surround)

    frame = (
        cq.Workplane("YZ")
            .center(0, 0)
            .moveTo(0, 0)
    )

    for t in basket:
        frame = frame.lineTo(t[0], t[1])

    frame = (
        frame
            .close()
            .revolve(360, (0, 1, 0), (0, 0, 0))
    )

    frame = frame.rotate((0,0,0), (1,0,0), 180)

    b = b.union(frame)

    magnet = (
        cq.Workplane("XY")
            .circle(magnet_diameter / 2 + 0.2 * margin_int)
            .extrude(-magnet_length - 0.2 * margin_int)
    )

    if magnet_fillet:
        magnet = (
            magnet
                .faces("|Z")
                .fillet(0.3)
        )

    b = b.union(magnet)

    # print(f"""
    # screw_holes_diameter: '{screw_holes_diameter}'
    # screw_hole_diameter: '{screw_hole_diameter}'
    # screw_hole_depth: '{screw_hole_depth}'
    # """)

    if render_screws:
        for i in range(0, 4):
            r = 45 + 90 * i
            p_hole = P(length=screw_holes_diameter / 2, angle=r)
            hole = (
                cq.Workplane("XY")
                    .center(p_hole.x, p_hole.y)
                    .circle(screw_hole_diameter / 2)
                    .extrude(screw_hole_depth + base_thickness)
            )
            if margin or screw_union is True:
                b = b.union(hole)
            else:
                b = b.cut(hole)

    b = b.translate((0, 0, -base_thickness))
    b = b.rotate((0, 0, 0), (1, 0, 0), 180)

    return b


def nd65(margin=False, **kwargs):
    return nd64(
        margin=margin,
        magnet_diameter=3.56,
        magnet_length=4.3,
        magnet_fillet=None,
        screw_holes_diameter=6.6,
        **kwargs
    )


def nd91(margin=False, **kwargs):
    return nd64(
        margin=margin,
        width=8.84,
        diagonal_width=10.35,

        base_thickness=0.1,

        surround_diameter=7.4,
        surround_depth=0.5,

        screw_holes_diameter=9.3,

        basket=[
            (7.65 / 2, 0),
            (7.0 / 2, 1.7),

            (6.35 / 2, 1.7),

            (5.87 / 2, 2.75),

            (0, 2.75),
        ],

        magnet_diameter=4.22,
        magnet_length=4.49,
        magnet_fillet=None,
        **kwargs,
    )


def nd105(margin=False, **kwargs):
    return nd64(
        margin=margin,
        width=10.5,
        diagonal_width=12.0,

        base_thickness=0.1,

        surround_diameter=9.3,
        surround_depth=0.55,

        screw_holes_diameter=10.9,

        basket=[
            (9.3 / 2, 0),
            (8.1 / 2, 2.15),

            (6.9 / 2, 2.15),

            (6.05 / 2, 3.0),

            (0, 3.0),
        ],

        magnet_diameter=4.22,
        magnet_length=5.7,
        magnet_fillet=None,
        **kwargs,
    )


def nd140(margin=False, **kwargs):
    return nd64(
        margin=margin,
        width=13.85,
        diagonal_width=15.45,

        base_thickness=0.15,

        surround_diameter=11.7,
        surround_depth=0.6,

        screw_holes_diameter=13.8,

        basket=[
            (12.0 / 2, 0),
            (11.8 / 2, 0.2),

            (10.5 / 2, 0.2),

            (9.25 / 2, 3.0),
            (7.8 / 2, 3.0),

            (7.0 / 2, 3.7),

            (0, 3.7),
        ],

        magnet_diameter=4.22,
        magnet_length=6.25,
        magnet_fillet=None,
        **kwargs,
    )


CBT_PLANAR_BRACKET_BOARD_THICKNESS = 0.3
planar_bracket_edge_margin = 0.15
# planar_bracket_side_tab_from_edge = 1.9  # 0.4 board thickness
planar_bracket_side_tab_from_edge = 1.94  # 0.3 board thickness


def show():

    start = time.time()

    # r = nd140()
    # show_object(r, name='nd140', options={"color": (125, 125, 125)})

    # p = planar(extCrude_screws=False)
    # show_object(p, name='planar', options={"color": (75, 75, 75)})
    # show_object(planar_bracket(), name='planar_bracket')
    # show_object(planar_bracket(board_thickness=0.3), name='planar_bracket')
    # pb = planar_bracket(CBT_PLANAR_BRACKET_BOARD_THICKNESS, cut_top_bottom=True,
    #                     side_tab_from_edge=planar_bracket_side_tab_from_edge,
    #                     side_tab_margin=planar_bracket_edge_margin)
    # show_object(pb, name='planar_bracket')

    r = pt2522(
        cut_audio_holes=True,
        # render_screw_holes_and_rivets_and_terminals=not cheap_tweeter,
        cut_screw_holes=False,
        with_margin=True,

        terminal_block_depth=1.5,
        terminal_tabs_height=None,
        terminal_tab_width=None,
        terminal_tabs_margin=0.3,
        terminal_block_chamfer=0.2,
    )
    show_object(r, name='pt2522', options={"color": (125, 125, 125)})

    b = pt2522_flanges(fillet_width=0.75, mid_flare_height=2.5)
    b = b.cut(r)

    show_object(b, name='pt2522_board', options={"color": (125, 125, 125)})

    # obj = p \
    #     .union(planar(show_connectors=False, extrude_screws=False).translate((10, 11, 0)).rotateAboutCenter((1, 0, 0), -90)) \
    #     .union(pb.translate((10, 0, 0))) \
    #     .union(sb.translate((10, 17, 0))) \
    #     .union(sb.translate((-2, 17, 0)).rotateAboutCenter((0, 1, 0), -90).rotateAboutCenter((1, 0, 0), 90)) \
    #     .union(sb.translate((4, 17, 0)).rotateAboutCenter((0, 1, 0), -90))
    #
    # show_object(obj.rotateAboutCenter((0, 0, 1), 90))

    # import cadquery as cq
    # workplane = obj
    # workplane = obj.intersect(
    #     cq.Workplane("XY") \
    #         .box(30, 30, 0.01)
    # ).faces("+Z")

    # show_object(workplane)
    #
    # cq.exporters.export(workplane.section(), "obj.dxf")

    end = time.time()
    elapsed = end - start
    print(f"elapsed time: '{elapsed}'")


def export(workplane, name):
    # Strip top layer
    workplane = workplane.faces("+Z")

    import cadquery as cq
    cq.exporters.export(workplane.section(), f"{name}.dxf")


# hacked_cq_cache.clear_cq_cache()

# show()
#
# export(planar_bracket(), 'planar_bracket')
