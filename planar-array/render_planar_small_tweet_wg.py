import planar_small_wg
import drivers

tweeter = drivers.pt2522()
# tweeter_wg = planar_small_wg.planar_tweeter_wg(shell_thickness=-0.29)
tweeter_wg = planar_small_wg.planar_tweeter_wg(shell_thickness=-0.3, edge_breathe_slot=True)
# tweeter_wg = planar_small_wg.planar_tweeter_wg()

show_object(tweeter, name=f'tweeter', options={"color": (45, 45, 45)})
show_object(tweeter_wg, name=f'tweeter_wg')
