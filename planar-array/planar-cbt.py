import cadquery as cq
# https://github.com/CadQuery/cadquery-plugins/tree/main/plugins/heatserts
# import heatserts
# https://github.com/CadQuery/cadquery-plugins/tree/main/plugins/cq_cache
# decorate your functions that build computationally heavy shapes
from hacked_cq_cache import *

import wg_vars
import wg_shapes
from wg_classes import *
import wg_naked
import wg_ply
import wg_inner_shape
import wg_outer_shape
import wg_drivers
import time
import wg_functions

start = time.time()

all_tweets = [
    [0],
    # [2, 3],
    # [4, 5],
    # [6, 7],
    # [8, 9],
    # [10, 11],
    # [12, 13],
    # [14, 15],
    # [16]
]


def export_with_hole_reinforcements(shape_name, shape, hole_reinforcements):
    if shape is None or hole_reinforcements is None:
        return
    shape = wg_functions.scale_cm_to_mm(shape)
    hole_reinforcements = wg_functions.scale_cm_to_mm(hole_reinforcements)
    intersected_holes = hole_reinforcements.intersect(shape)
    reinforced_shape = (
        cq.Assembly()
            .add(shape.cut(intersected_holes), name="main_shape")
            .add(intersected_holes, name="hole_reinforcements")
            .toCompound()
    )
    cq.exporters.export(reinforced_shape, shape_name)


full_ply_splices = []

RENDER_FULL_PLY = False


def render_slice(tweets, show_objects=False):

    rendered_tweeters = tweets

    radius_list = []
    for t in rendered_tweeters + [max(rendered_tweeters) + 1]:
        radius_list.append(wg_vars.DEGREES_PER_TWEETER * t)

    topmost_part = len(radius_list) == 2
    if topmost_part:
        radius_list.append(radius_list[1] + wg_vars.TOP_WOOFER_DERGREES)

    ws = wg_drivers.woofer_stats[int(rendered_tweeters[0] / 2)]

    r00 = 0
    s00 = WgShell(angle=r00)

    r0 = radius_list[0]
    s0 = WgShell(angle=r0)

    r1 = radius_list[1]
    s1 = WgShell(angle=r1)

    r2 = radius_list[2]
    s2 = WgShell(angle=r2)

    max_shell_thickness = 0.8
    min_shell_thickness = 0.5

    shell_thickness = max(max_shell_thickness * (s1.w / s00.w), min_shell_thickness)
    outer_shell_thickness = max(shell_thickness * 1.1, 1.0)

    # print(f"shell_thickness: {shell_thickness}")

    if topmost_part:
        tweeters_r = [(r1 - r0) / 2 + r0]
    else:
        tweeters_r = [(r1 - r0) / 2 + r0, (r2 - r1) / 2 + r1]

    wg_slice = WgSlice(radius_list=radius_list, tweeters=tweeters_r, ws=ws, shell_thickness=shell_thickness,
                       topmost_part=topmost_part, outer_shell_thickness=outer_shell_thickness)

    i_name = ws.ordinal

    # clear_cq_cache()

    # mirror, tweeter_flanges, tweeter_wg_cuts, render_step, split_ply = True, True, True, False, True
    mirror, tweeter_flanges, tweeter_wg_cuts, render_step, split_ply = True, True, True, False, False

    top_ruler, wg_full, wg_throat, wg_throat_with_verts, wg_inner_front, wg_inner_front_tweet, wg_inner_rear, wg_inner_rear_tweet, wg_inner_rear_tweet, wg_inner_rear_bot, wg_inner_rear_top, wg_inner_hole_reinforcements, wg_inner_no_rear_split_screws_hole_reinforcements, wg_woofer_hole_reinforcements, wg_outer_right, wg_outer_left, ply_slice, ply_slice_supports, woofers, tweeters, wg_screws = None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None
    ply_slices = []

    # top_ruler = wg_shapes.horizontal_ruler_shape(r2, width=15)
    # wg_full = wg_shapes.wg_full_shape(radius_list)
    # wg_throat = wg_shapes.wg_throat_shape(radius_list)
    # wg_throat_with_verts = wg_shapes.wg_throat_with_verts_shape(radius_list)

    # wg_inner_front = wg_inner_shape.wg_slice_inner(wg_slice, tweeter_flanges=tweeter_flanges, tweeter_wg_cuts=tweeter_wg_cuts, mirror=mirror)

    # wg_inner_front = wg_inner_shape.wg_slice_inner(wg_slice, tweeter_flanges=tweeter_flanges, tweeter_wg_cuts=tweeter_wg_cuts, mirror=mirror, front=True,
    #                                                rear=False)
    # wg_inner_rear_top = wg_inner_shape.wg_slice_inner(wg_slice, tweeter_flanges=tweeter_flanges, tweeter_wg_cuts=tweeter_wg_cuts, mirror=mirror, front=False,
    #                                                   rear=True, rear_inner_split_top=True)
    # wg_inner_rear_bot = wg_inner_shape.wg_slice_inner(wg_slice, tweeter_flanges=tweeter_flanges, tweeter_wg_cuts=tweeter_wg_cuts, mirror=mirror, front=False,
    #                                                   rear=True, rear_inner_split_bot=True)
    #
    # wg_inner_front_tweet = wg_inner_shape.wg_slice_inner_tweet(wg_slice, tweeter_flanges=tweeter_flanges, mirror=mirror, front=True)
    # wg_inner_rear_tweet = wg_inner_shape.wg_slice_inner_tweet(wg_slice, tweeter_flanges=tweeter_flanges, mirror=mirror, front=False)

    # wg_outer_right = wg_outer_shape.wg_slice_outer(wg_slice, right=True, left=False)
    # wg_outer_left = wg_outer_shape.wg_slice_outer(wg_slice, right=False, left=True)

    # wg_inner_hole_reinforcements = wg_inner_shape.wg_hole_reinforcements(wg_slice, mirror=mirror)
    # wg_inner_no_rear_split_screws_hole_reinforcements = wg_inner_shape.wg_hole_reinforcements(wg_slice, mirror=mirror, inner_rear_split_screws=False)
    # wg_woofer_hole_reinforcements = wg_drivers.place_woofer_heatsert_reinforcements(wg_slice).mirror(mirrorPlane='YZ', union=True)

    # ply_slice = wg_ply.wg_ply_middle(wg_slice, mirror=mirror, multi_splice_mounts=True, show_scale_part=False)
    # if ply_slice and RENDER_FULL_PLY:
    #     full_ply_splices.append(ply_slice)
    # ply_slice = None
    # ply_slice_supports = wg_ply.wg_multi_splice_mounts(wg_slice, cuts=False, mirror=mirror)
    # ply_slices = wg_ply.wg_ply_middles(wg_slice, mirror=mirror, split_ply=split_ply)
    # woofers = wg_drivers.place_woofers(wg_slice, mirror=mirror,
    #                                    # screw_union=True,
    #                                    # screw_hole_diameter=Screw('m4').heatsert_diameter,
    #                                    # screw_hole_depth=Screw('m4').heatsert_long_depth,
    #                                    )
    tweeters = wg_drivers.place_tweeters(wg_slice, mirror_cut=not mirror, cut_audio_holes=True, sideways=True)

    wg_inner_front, wg, wg_inner_front_tweet = wg_naked.verts(wg_slice)
    wg_inner_rear_bot = wg_naked.rear_support(wg_slice)

    # wg_inner_front_tweet = wg_functions.scale_cm_to_mm(wg_inner_front_tweet)

    wg_inner_front = wg_inner_front.cut(tweeters)

    wg_inner_front = wg_functions.scale_cm_to_mm(wg_inner_front)
    wg_inner_front_tweet = wg_functions.scale_cm_to_mm(wg_inner_front_tweet)
    wg_inner_rear_bot = wg_functions.scale_cm_to_mm(wg_inner_rear_bot)
    # wg_inner_front = (
    #     cq.Assembly()
    #         .add(wg_functions.scale_cm_to_mm(wg_inner_front), name="verts")
    #         .add(wg_functions.scale_cm_to_mm(wg), name="wg")
    #         .toCompound()
    # )

    # wg_inner_front_tweet = wg_inner_front_tweet.cut(wg_inner_front)
    #
    # cq.exporters.export(
    #     wg_inner_front_tweet,
    #     'wg_inner_front_tweet.step'
    # )

    cq.exporters.export(
        wg_inner_front,
        'wg_naked_verts.step'
    )

    cq.exporters.export(
        wg_inner_front_tweet,
        'wg_naked_tweet_supports.step'
    )

    cq.exporters.export(
        wg_inner_rear_bot,
        'wg_naked_rear_support.step'
    )

    #
    # cq.exporters.export(
    #     wg_functions.scale_cm_to_mm(wg_inner_rear_bot),
    #     'wg_naked_rear_support.step'
    # )

    # if wg_slice.ws.ordinal == 0:
    #     ply_slice_supports = (
    #         wg_shapes.wg_full_shape(radius_list)
    #             .intersect(wg_outer_shape.ply_bot_support_slices())
    #     )

    # wg_screws = wg_shapes.filament_peg()
    # wg_screws = wg_drivers.place_tweeter_screws(wg_slice, mirror=mirror)
    # wg_screws = wg_inner_shape.wg_rear_split_support_screws(wg_slice)
    # wg_outer_left = wg_inner_shape.wg_rear_split_supports(wg_slice)

    # wg_throat = wg_shapes.wg_throat_shape(radius_list, shell=-wg_slice.shell_thickness)
    # wg_inner = wg_shapes.ply_outer_edge_box(wg_slice, outer_aligned=False).intersect(wg_full)
    # wg_inner_rear = wg_shapes.calc_outer_edge_rear(wg_slice)
    # wg_screws = wg_ply.wg_screws2(wg_slice)
    # import wg_grille
    # wg_outer_left = wg_grille.grille(wg_slice)
    # wg_outer_left = wg_inner_shape.rear_ply_peg_slide_cutout(wg_slice)
    # wg_outer_left = wg_inner_shape.wg_woofer_slots(wg_slice)
    # wg_inner_front = wg_inner_shape.place_tweeter_supports(wg_slice)
    # wg_outer_left = wg_inner_shape.wg_mid_split_pins(wg_slice, front=True)
    # wg_outer_right = wg_inner_shape.wg_mid_split_pins(wg_slice)
    # wg_outer_left = wg_drivers.place_woofers_cut(wg_slice, margin=True)
    # wg_outer_right = wg_shapes.woofer_flat_slice(wg_slice, tweet_breathe_slots=True)
    # wg_outer = wg_shapes.ply_outer_edge_box(wg_slice, outer_aligned=True).intersect(wg_full)
    # ply_slice_supports = wg_ply.ply_hole_supports_shell(radius_list, shell_thickness=shell_thickness)

    if render_step:
        for t in [
            (wg_inner_front_tweet, "wg_inner_front_tweet"),
            (wg_inner_rear_tweet, "wg_inner_rear_tweet"),
        ]:
            if t is not None:
                cq.exporters.export(t[0], f"{i_name}_{t[1]}.step")
        for t in [
            (wg_inner_front, "wg_inner_front"),
            (wg_inner_rear_bot, "wg_inner_rear_bot"),
        ]:
            if t is not None:
                export_with_hole_reinforcements(
                    shape_name=f"{i_name}_{t[1]}.step",
                    shape=t[0],
                    hole_reinforcements=wg_inner_hole_reinforcements,
                )
        for t in [
            (wg_inner_rear_top, "wg_inner_rear_top"),
        ]:
            if t is not None:
                export_with_hole_reinforcements(
                    shape_name=f"{i_name}_{t[1]}.step",
                    shape=t[0],
                    hole_reinforcements=wg_inner_no_rear_split_screws_hole_reinforcements,
                )
        for t in [
            (wg_outer_left, "wg_outer_left"),
            (wg_outer_right, "wg_outer_right"),
        ]:
            if t is not None:
                export_with_hole_reinforcements(
                    shape_name=f"{i_name}_{t[1]}.step",
                    shape=t[0],
                    hole_reinforcements=wg_woofer_hole_reinforcements,
                )
        if ply_slice:
            cq.exporters.export(ply_slice, f"{i_name}_{'ply_slice'}.step")
        if ply_slice_supports:
            cq.exporters.export(ply_slice_supports, f"{i_name}_{'ply_slice_supports'}.step")
        if ply_slices:
            shape = wg_functions.union_shapes_list(ply_slices)
            # shape = shape.faces("<X")
            shape = shape.rotate((0, 0, 0), (0, 1, 0), 90)
            # show_object(shape, name=f'{i_name}_ply_slice_shape', options={"color": (255, 173, 0), "alpha": 0.75})
            # cq.exporters.export(wg_functions.scale_cm_to_mm(shape).section(), f"{i_name}_{'ply_slices'}.dxf")
            cq.exporters.export(
                wg_functions.scale_cm_to_mm(shape).section(),
                f"{i_name}_{'ply_slices'}.svg",
                opt={"projectionDir": (0, 0, 1)}
            )

    if show_objects:
        if top_ruler:
            show_object(top_ruler, name=f'{i_name}_ruler',
                        options={"color": (125, 125, 125), "alpha": 0.5})

        if wg_full:
            show_object(wg_full, name=f'{i_name}_wg_full',
                        options={"color": (45, 125, 45), "alpha": 0.75})

        if wg_throat:
            show_object(wg_throat, name=f'{i_name}_wg_throat',
                        options={"color": (45, 125, 45), "alpha": 0.75})

        if wg_throat_with_verts:
            show_object(wg_throat_with_verts, name=f'{i_name}_wg_throat',
                        options={"color": (45, 125, 45), "alpha": 0.75})

        if wg_inner_front:
            show_object(wg_inner_front, name=f'{i_name}_wg_inner_front',
                        options={"color": (125, 45, 45), "alpha": 0.0})

        if wg_inner_front_tweet:
            show_object(wg_inner_front_tweet, name=f'{i_name}_wg_inner_front_tweet',
                        options={"color": (125, 45, 45), "alpha": 0.0})

        if wg_inner_rear_bot:
            show_object(wg_inner_rear_bot, name=f'{i_name}_wg_inner_rear_bot',
                        options={"color": (125, 45, 45), "alpha": 0.0})

        if wg_inner_rear_top:
            show_object(wg_inner_rear_top, name=f'{i_name}_wg_inner_rear_top',
                        options={"color": (125, 45, 45), "alpha": 0.0})

        if wg_inner_rear_tweet:
            show_object(wg_inner_rear_tweet, name=f'{i_name}_wg_inner_rear_tweet',
                        options={"color": (125, 45, 45), "alpha": 0.0})

        if wg_inner_hole_reinforcements:
            show_object(wg_inner_hole_reinforcements, name=f'{i_name}_wg_inner_hole_reinforcements',
                        options={"color": (200, 72, 72), "alpha": 0.5})

        if wg_woofer_hole_reinforcements:
            show_object(wg_woofer_hole_reinforcements, name=f'{i_name}_wg_woofer_hole_reinforcements',
                        options={"color": (200, 72, 72), "alpha": 0.5})

        if wg_inner_no_rear_split_screws_hole_reinforcements:
            show_object(wg_inner_no_rear_split_screws_hole_reinforcements, name=f'{i_name}_wg_inner_no_rear_split_screws_hole_reinforcements',
                        options={"color": (200, 72, 72), "alpha": 0.5})

        if wg_outer_left:
            show_object(wg_outer_left, name=f'{i_name}_wg_outer_left',
                        options={"color": (125, 45, 45), "alpha": 0.0})

        if wg_outer_right:
            show_object(wg_outer_right, name=f'{i_name}_wg_outer_right',
                        options={"color": (125, 45, 45), "alpha": 0.0})

        if ply_slice:
            show_object(ply_slice, name=f'{i_name}_ply_slice', options={"color": (255, 173, 0), "alpha": 0.75})

        for i, slice in enumerate(ply_slices):
            show_object(slice, name=f'{i_name}_ply_slice_{i}', options={"color": (255, 173, 0), "alpha": 0.75})

        if ply_slice_supports:
            show_object(ply_slice_supports, name=f'{i_name}_ply_slice_supports',
                        options={"color": (45, 45, 125), "alpha": 0.5})

        if wg_screws:
            show_object(wg_screws, name=f'{i_name}_wg_screws', options={"color": (45, 45, 45)})

        if woofers:
            show_object(woofers, name=f'{i_name}_woofers', options={"color": (45, 45, 45)})

        if tweeters:
            show_object(wg_functions.scale_cm_to_mm(tweeters), name=f'{i_name}_tweeters', options={"color": (45, 45, 45)})


# if __name__ == '__main__':
#     from multiprocessing import Pool
#
#     with Pool(len(all_tweets)) as p:
#         p.map(render_slice, all_tweets)
# else:
for tweets in all_tweets:
    render_slice(tweets, show_objects=True)

    if RENDER_FULL_PLY:
        full_ply = wg_functions.union_shapes_list(full_ply_splices)
        full_ply = wg_shapes.place_ply_middle(None, full_ply, invert=True)

        full_ply = full_ply.rotate((0, 0, 0), (0, 1, 0), 90)
        # show_object(shape, name=f'{i_name}_ply_slice_shape', options={"color": (255, 173, 0), "alpha": 0.75})
        # cq.exporters.export(wg_functions.scale_cm_to_mm(shape).section(), f"{i_name}_{'ply_slices'}.dxf")
        section = wg_functions.scale_cm_to_mm(full_ply).section()
        cq.exporters.export(section, f"full_ply.dxf")
        cq.exporters.export(
            section,
            f"{'full_ply'}.svg",
            opt={"projectionDir": (0, 0, 1)}
        )

        show_object(full_ply, name=f'full_ply', options={"color": (45, 45, 45)})

end = time.time()
elapsed = end - start
print(f"elapsed time: '{elapsed}'")
