import cadquery as cq
import wg_functions
from wg_classes import WgSlice
import wg_shapes
import wg_ply


def grille(wg_slice: WgSlice):
    r0, r1, r2 = wg_slice.r0, wg_slice.r1, wg_slice.r2

    shapes = []

    lines_per_tweeter = 2
    line_r = wg_functions.DEGREES_PER_TWEETER / lines_per_tweeter

    t = 0.5

    for r_i in range(0, 6 * lines_per_tweeter):
        r = r0 - 2 * wg_functions.DEGREES_PER_TWEETER + r_i * line_r

        for r_sign in [-1, 1]:
            b = (
                cq.Workplane("XY")
                    .rect(100, 100)
                    .extrude(t)
                    .translate((0, 0, -t / 2))
                    .rotate((0, 0, 0), (0, 1, 0), r_sign * 45)
            )
            b = wg_functions.move_degree(r, b)
            shapes.append(b)

    raw_grille = wg_functions.union_shapes_list(shapes)

    # g = raw_grille

    wg_full = (
        wg_shapes.wg_full_shape(wg_slice.radius_list)
            .faces("<Z or >Z or |Z")
            .shell(-t)
    )

    g = raw_grille.intersect(wg_full)
    g = g.cut(wg_shapes.wg_throat_shape(wg_slice.radius_list))

    cq.exporters.export(g, f"grille.step")

    return g
