import cadquery as cq

from geometry_2d import P as P

DIAMETER = 50
PLY_THICKNESS = 0.9

BOTTOM_PLY_SHEETS = 1
UPPER_PLY_SHEETS = 3

TURNTABLE_URL = 'https://www.beslagsbutiken.se/vridplatta-lazy-susan-200-350-kg.html'
TURNTABLE_PDF = 'https://www.beslagsbutiken.se/mwdownloads/download/link/id/656/'
TURNTABLE_DIAMETER = 32.96
TURNTABLE_HEIGHT = 1.45

bottom = (
    cq.Workplane("XY")
        .circle(DIAMETER / 2)
        .extrude(BOTTOM_PLY_SHEETS * PLY_THICKNESS)
)

turntable = (
    cq.Workplane("XY")
        .circle(TURNTABLE_DIAMETER / 2)
        .extrude(TURNTABLE_HEIGHT)
        .translate((0, 0, BOTTOM_PLY_SHEETS))
)

top = (
    cq.Workplane("XY")
        .circle(DIAMETER / 2)
        .extrude(UPPER_PLY_SHEETS * PLY_THICKNESS)
        .translate((0, 0, BOTTOM_PLY_SHEETS + TURNTABLE_HEIGHT))
)

show_object(bottom, name='bottom', options={})
show_object(turntable, name='turntable', options={"color": (125, 125, 125), "alpha": 0.0})
show_object(top, name='top', options={})

# cq.exporters.export(b1.union(b2), "kitchen-blocks.step")
