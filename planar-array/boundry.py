import wg_vars
import wg_shapes

radius_list = []
for i in range(0, wg_vars.NUM_TWEETERS + 1):
    radius_list.append(i * wg_vars.DEGREES_PER_TWEETER)

wg_full = wg_shapes.wg_full_shape(radius_list)

show_object(wg_full, name=f'wg_full',
            options={"color": (45, 125, 45), "alpha": 0.0})
