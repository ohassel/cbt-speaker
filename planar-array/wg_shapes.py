from hacked_cq_cache import cq_cache

from wg_base_shapes import *
from wg_functions import *
from wg_classes import *
from typing import List
import wg_drivers
import wg_ply
import drivers


@cq_cache()
def wg_full_shape(radius_list: List[int]):
    return wg_shell(
        radius_list=radius_list,
        slice_function=wg_2d_slice,
    )


def horizontal_ruler_shape(angle, width):
    return move_degree(angle, (
        cq.Workplane("XY")
            .box(width, 2, 2)
    ))


@cq_cache()
def wg_throat_shape(radius_list: List[int], shell=None, inner=False):
    wg_2d_throat_shape = wg_shell(
        radius_list=radius_list,
        slice_function=wg_2d_throat,
    )

    if shell is None:
        return wg_2d_throat_shape

    wg_2d_throat_shell_shape = (
        wg_2d_throat_shape
            .faces("<Z or >Z or |Z")
            .shell(shell)
        # .mirror(mirrorPlane='YZ', union=True)
    )

    if inner:
        return wg_2d_throat_shape.cut(wg_2d_throat_shell_shape)
    else:
        return wg_2d_throat_shell_shape


@cq_cache()
def wg_mirror_cut_shape(radius_list: List[int]):
    return move_degree(radius_list[1], (
        cq.Workplane("XY")
            .box(50, 50, 50)
            .translate((-25 - 0.1, 0, 0))
    ))


@cq_cache()
def wg_verts_edge_holes_shape(radius_list: List[int]):
    r0, r1, r2 = radius_list[0], radius_list[1], radius_list[2]

    return wg_functions.union_shapes_list([
        calc_vert(r0, hole=True),
        calc_vert(r1, hole=True),
        calc_vert(r2, hole=True),
    ])


@cq_cache()
def wg_verts_shape(radius_list: List[int], shell=None, inner=False, cut_top_bot=True, bot=True, mid=True, top=True):
    # vert_shell = 0.4
    r0, r1, r2 = radius_list[0], radius_list[1], radius_list[2]

    verts = []
    if bot:
        bot_vert = calc_vert(r0, bot=True, top=False if cut_top_bot else True)
        verts.append(bot_vert)
    if mid:
        mid_vert = calc_vert(r1, top=True, bot=True)
        verts.append(mid_vert)
    if top:
        top_vert = calc_vert(r2, bot=False if cut_top_bot else True, top=True)
        verts.append(top_vert)

    mirror_cut = wg_mirror_cut_shape(radius_list)

    # return bot_vert, None, None
    if shell is None:
        return (
            union_shapes_list(verts)
                .cut(mirror_cut)
        )

    vert_shells = []
    vert_inner_shells = []
    if bot:
        bot_vert_shell = calc_vert(r0, bot=True, top=False if cut_top_bot else True, shell=shell)
        vert_shells.append(bot_vert_shell)
        bot_vert_shell_inner = bot_vert.cut(bot_vert_shell)
        vert_inner_shells.append(bot_vert_shell_inner)
    if mid:
        mid_vert_shell = calc_vert(r1, bot=True, top=True, shell=shell)
        vert_shells.append(mid_vert_shell)
        mid_vert_shell_inner = mid_vert.cut(mid_vert_shell)
        vert_inner_shells.append(mid_vert_shell_inner)
    if top:
        top_vert_shell = calc_vert(r2, bot=False if cut_top_bot else True, top=True, shell=shell)
        vert_shells.append(top_vert_shell)
        top_vert_shell_inner = top_vert.cut(top_vert_shell)
        vert_inner_shells.append(top_vert_shell_inner)

    if inner:
        return (
            union_shapes_list(vert_inner_shells)
                .cut(mirror_cut)
        )
    else:
        return (
            union_shapes_list(vert_shells)
                .cut(mirror_cut)
        )


@cq_cache()
def wg_throat_with_verts_shape(radius_list: List[int], shell=None, shell_vert=None, inner=False):
    if shell is None:
        wg = wg_throat_shape(radius_list)
        return wg.union(wg_verts_shape(radius_list))
    else:
        if shell_vert is None:
            shell_vert = shell
        elif shell_vert is False:
            shell_vert = None

        wg_outer = wg_throat_shape(radius_list, shell=shell, inner=False)

        if inner:
            wg_inner = wg_throat_shape(radius_list, shell=shell, inner=True)
            verts_inner = wg_verts_shape(radius_list, shell=shell_vert, inner=True)
            return wg_inner.union(verts_inner)
        else:
            wg_inner = wg_throat_shape(radius_list, shell=shell, inner=True)

            if shell_vert:
                verts_outer = wg_verts_shape(radius_list, shell=shell_vert, inner=False)
                verts_inner = wg_verts_shape(radius_list, shell=shell_vert, inner=True)
                return (
                    wg_outer
                        .union(verts_outer)
                        .cut(wg_inner)
                        .cut(verts_inner)
                )
            else:
                return (
                    wg_outer
                        .union(wg_verts_shape(radius_list))
                        .cut(wg_inner)
                )


@cq_cache()
def calc_ply_middle_with_shell(wg_slice: WgSlice, shell_thickness=None, absolute_thickness=None, use_new=True):
    s = WgShell(angle=0)
    wgg = wg_geometry_cached(s=s)

    ply_width = PLY_EDGE_THICKNESS_COMBINED
    ply_offset = 0.5 * PLY_EDGE_THICKNESS_SINGLE

    if wg_slice.topmost_part:
        ply_width -= 1.0 * PLY_EDGE_THICKNESS_SINGLE
        ply_offset -= 0.5 * PLY_EDGE_THICKNESS_SINGLE

    if absolute_thickness:
        width = absolute_thickness
    elif shell_thickness:
        width = 2 * shell_thickness + ply_width
    else:
        width = ply_width

    if use_new:
        return place_ply_middle(wg_slice, invert=False, shape=(
            cq.Workplane("XY")
                .box(width, 500, 500)
        ))

    return (
        cq.Workplane("XY")
            .box(width, 500, 500)
            .rotate((0, 0, 0), (0, 1, 0), PLY_INNER_EDGE_STRAIGHT_DEGREES)
            # .rotate((0, 0, 0), (0, 0, 1), PLY_INNER_EDGE_Z_ROTATION_DEGREES)
            .translate((wgg.wg_edge_middle.x + ply_offset, 0, 0))
    )


def place_ply_middle(wg_slice: WgSlice, shape, invert=False):
    s = WgShell(angle=0)
    wgg = wg_geometry_cached(s=s)

    ply_width = PLY_EDGE_THICKNESS_COMBINED
    ply_offset = 0.5 * PLY_EDGE_THICKNESS_SINGLE

    if wg_slice is not None and wg_slice.topmost_part:
        ply_width -= 1.0 * PLY_EDGE_THICKNESS_SINGLE
        ply_offset -= 0.5 * PLY_EDGE_THICKNESS_SINGLE

    x_offset = wgg.wg_edge_middle.x + ply_offset
    if invert:
        return (
            shape
                .translate((-x_offset, 0, 0))
                .rotate((0, 0, 0), (0, 1, 0), -PLY_INNER_EDGE_STRAIGHT_DEGREES)
        )
    else:
        return (
            shape
                .rotate((0, 0, 0), (0, 1, 0), PLY_INNER_EDGE_STRAIGHT_DEGREES)
                .translate((x_offset, 0, 0))
        )


@cq_cache()
def ply_middle_shape(wg_slice: WgSlice, mirror=False, woofer_cuts=True, screw_peg_holes=True, margin=False, use_new=True):
    # ply_middle = calc_ply_middle_with_shell(wg_slice, shell_thickness=PRINTER_MARGIN if margin else None)
    ply_middle = calc_ply_middle_with_shell(wg_slice, use_new=use_new)

    inters = wg_throat_with_verts_shape(wg_slice.radius_list)

    ply_middle = ply_middle.intersect(inters)

    if screw_peg_holes:
        ply_middle = ply_middle.cut(wg_ply.ply_holes_inf(wg_slice, outer_pegs=True))

    if woofer_cuts:
        if wg_slice.topmost_part:
            ply_middle = ply_middle.cut(wg_drivers.topmost_slice_front_woofer_vents(wg_slice))
            ply_middle = ply_middle.cut(wg_drivers.topmost_slice_rear_woofer_vents(wg_slice))
            # ply_middle = ply_middle.cut(wg_drivers.topmost_slice_middle_woofer_vents(wg_slice))
        else:
            edge_woofer_cut = wg_drivers.place_woofers_cut(wg_slice, margin=margin)
            # middle_woofer_cut = wg_drivers.place_woofers_cut(wg_slice, front_h_offset=0)

            # ply_middle_board = calc_ply_middle_with_shell(absolute_thickness=PLY_EDGE_THICKNESS_SINGLE)
            # edge_woofer_cut = edge_woofer_cut.cut(ply_middle_board)

            holes = wg_slice.screw_holes
            for hole in holes.screws + holes.pegs:
                boop = hole.bot and hole.front or hole.rear and hole.top
                supp = wg_ply.place_wg_shape(hole.angle, hole.y_offset, shape=(
                    cq.Workplane("XY")
                        .box(2.5 if boop else 10, 10 if boop else 2.5, 10)
                        .rotate((0, 0, 0), (0, 0, 1), 45)
                        .rotate((0, 0, 0), (0, 0, 1), -hole.angle)
                ))
                edge_woofer_cut = edge_woofer_cut.cut(supp)

            edge_woofer_cut = edge_woofer_cut.union(
                wg_drivers.place_woofer(
                    ws=wg_slice.ws,
                    angle=wg_slice.ws.cbt_angle,
                    shape=wg_drivers.calc_woofer_ply_overlap(wg_slice, margin=margin),
                )
            )

            ply_middle = ply_middle.cut(edge_woofer_cut)
            # ply_middle = ply_middle.cut(middle_woofer_cut)

            ply_middle = ply_middle.cut(woofer_cutout(wg_slice))

    if mirror:
        ply_middle = ply_middle.mirror(mirrorPlane='YZ', union=True)

    return ply_middle


@cq_cache()
def top_bot_margin_cuts(wg_slice: WgSlice):
    cut_h = PRINTER_MARGIN

    cut = move_degree(wg_slice.r0, (
        cq.Workplane("XY")
            .box(50, 50, 2 * cut_h)
    ))

    if not wg_slice.topmost_part:
        top = move_degree(wg_slice.r2, (
            cq.Workplane("XY")
                .box(50, 50, 2 * cut_h)
        ))
        cut = cut.union(top)

    return cut


@cq_cache()
def ply_outer_edge_box(wg_slice: WgSlice, outer_aligned=True, middle_aligned=False):
    s = WgShell(angle=0)
    wgg = wg_geometry_cached(s=s)

    t = PLY_EDGE_THICKNESS_COMBINED
    offset = t if outer_aligned else 0
    if middle_aligned:
        offset = t/2
    if wg_slice.topmost_part:
        t -= PLY_EDGE_THICKNESS_SINGLE
        if outer_aligned:
            t += PLY_EDGE_THICKNESS_COMBINED
        else:
            t += 1 * PLY_EDGE_THICKNESS_SINGLE

    return (
        cq.Workplane("XY")
            .box(t + 200, 500, 500)
            .translate((100, 0, 0))
            .rotate((0, 0, 0), (0, 1, 0), PLY_INNER_EDGE_STRAIGHT_DEGREES)
            # .rotate((0, 0, 0), (0, 0, 1), PLY_INNER_EDGE_Z_ROTATION_DEGREES)
            .translate((wgg.wg_edge_middle.x + PLY_EDGE_THICKNESS_SINGLE / 2, 0, 0))
            .translate((offset, 0, 0))
    )


@cq_cache()
def woofer_front_box(wg_slice: WgSlice, width=50, extrude=-50, z_offset=None):
    s = (
        cq.Workplane("XY")
            .rect(50, width)
            .extrude(extrude)
    )
    if z_offset:
        s = s.translate((0, 0, z_offset))
    return wg_drivers.place_woofer(
        ws=wg_slice.ws,
        angle=wg_slice.ws.cbt_angle,
        shape=s,
    )


@cq_cache()
def woofer_cutout(wg_slice: WgSlice, extrude=None):
    if extrude is None:
        extrude = -wg_slice.woofer_ply_cutout_y
    s = (
        cq.Workplane("XY")
            .circle(wg_slice.ws.cutout_diameter / 2)
            .extrude(extrude)
    )
    return wg_drivers.place_woofer(
        ws=wg_slice.ws,
        angle=wg_slice.ws.cbt_angle,
        shape=s,
    )


@cq_cache()
def woofer_flat(wg_slice: WgSlice, woofer_cutout_hole=True):
    s = (
        cq.Workplane("XY")
            .rect(50, 50)
    )
    if woofer_cutout_hole:
        s = s.circle(wg_slice.ws.cutout_diameter / 2)
    s = (
        s
            .extrude(-wg_slice.shell_thickness)
    )
    if woofer_cutout_hole:
        heatsert_screw = Screw(WOOFER_HOLE_M_GAUGE)
        s = (
            s
                # .fillet(0.3)
                .pushPoints(wg_slice.ws.screw_holes)
                .hole(heatsert_screw.heatsert_diameter if WOOFER_HOLE_HEATSERT else heatsert_screw.screw_diameter_loose)
                # .hole(wg_slice.ws.screw  wg_slice.ws.screw_hole_diameter)
                .cut((
                cq.Workplane("XY")
                    .circle(wg_slice.ws.cutout_diameter / 2)
                    .workplane(-wg_slice.shell_thickness / 2)
                    .circle(wg_slice.ws.cutout_diameter / 2 + wg_slice.shell_thickness / 2)
                    .loft(combine=True)
                    .translate((0, 0, -wg_slice.shell_thickness / 2))
            ))
            # .fillet(0.1)
            # .translate((0, 0, -ws.base_thickness))
        )
    return wg_drivers.place_woofer(
        ws=wg_slice.ws,
        angle=wg_slice.ws.cbt_angle,
        shape=s,
    )


def woofer_screw_holes(wg_slice: WgSlice, hole_diameter, extrude=-10):
    shapes = []
    for hole in wg_slice.ws.screw_holes:
        shapes.append((
            cq.Workplane("XY")
                .center(hole[0], hole[1])
                .circle(hole_diameter)
                .extrude(extrude)
        ))
    return wg_drivers.place_woofer(
        ws=wg_slice.ws,
        angle=wg_slice.ws.cbt_angle,
        shape=union_shapes_list(shapes),
    )


def woofer_flat_cuts(wg_slice: WgSlice, solid_h=None):
    shapes = []
    if solid_h is None:
        if wg_slice.ws.woofer_type == 'nd65':
            solid_h = 0.4
        else:
            solid_h = 0.6

    shapes.append((
        cq.Workplane("XY")
            .circle(wg_slice.ws.cutout_diameter / 2)
            .extrude(-10)
    ))

    step = 10
    cut_chamfer = (
        cq.Workplane("XY")
        .workplane(offset=-solid_h)
        .circle(wg_slice.ws.cutout_diameter / 2)
        .workplane(offset=-solid_h - step)
        .circle(wg_slice.ws.cutout_diameter / 2 + step * 2)
        .loft(combine=True)
    )

    surround_chamfer_r = 0.05
    surround_cut_chamfer = (
        cq.Workplane("XY")
        .workplane(offset=0)
        .circle(wg_slice.ws.cutout_diameter / 2 + 2 * surround_chamfer_r)
        .workplane(offset=-surround_chamfer_r)
        .circle(wg_slice.ws.cutout_diameter / 2)
        .loft(combine=True)
    )
    shapes.append(surround_cut_chamfer)

    for hole in wg_slice.ws.screw_holes:
        shapes.append((
            cq.Workplane("XY")
                .center(hole[0], hole[1])
                .circle((wg_slice.ws.screw.heatsert_diameter if WOOFER_HOLE_HEATSERT else wg_slice.ws.screw.screw_diameter_loose) / 2)
                .extrude(-10)
        ))

        cut_chamfer = cut_chamfer.cut((
            cq.Workplane("XY")
                .center(hole[0], hole[1])
                .circle(1.5 / 2)
                .extrude(-10)
        ))

    shapes.append(cut_chamfer)

    return wg_drivers.place_woofer(
        ws=wg_slice.ws,
        angle=wg_slice.ws.cbt_angle,
        shape=union_shapes_list(shapes),
    )

@cq_cache()
def calc_woofer_flat_edge(wg_slice: WgSlice, z_offset=None):
    if z_offset is None:
        z_offset = wg_slice.ws.z_offset

    def edge_ply_cutout(s: WgShell, r1, ws, shell_thickness):
        w = s.w / 2

        sf = WgShell(angle=s.angle, front=True)
        sr = WgShell(angle=s.angle, rear=True)

        angle = s.angle

        def half(ss, w0, w, d, e, sign=1):
            # h0 = -ws.z_offset - 0.5 * shell_thickness
            h0 = -z_offset - ws.base_thickness - shell_thickness / 2

            # print(f"h0: {h0}")
            # h0 = max(-shell_thickness, h0)
            if wg_slice.ws.ordinal in [5, 6, 7]:
                h0 -= shell_thickness * 1.5
            if wg_slice.ws.ordinal in [8]:
                h0 -= shell_thickness * 1.6
            #     if angle != r1:
            #         h0 += 0.25
            #
            # if angle == r1:
            #     h0 += 0.25
            h0 += 0.25


            ss = ss.segment((0, h0), (0, e * sign))

            ss = ss.segment((-2 * w * sign, e * sign))

            ss = ss.segment((-2 * w * sign, h0))

            return ss

        s = cq.Sketch()

        s = half(s, sign=-1, w0=w, w=sf.w / 2, d=sf.d / 2, e=sf.edge_width / 2)

        s = s.hull()

        return s

    return wg_shell(
        radius_list=wg_slice.radius_list_margin,
        slice_function=edge_ply_cutout,
        r1=wg_slice.radius_list[1],
        ws=wg_slice.ws,
        shell_thickness=wg_slice.shell_thickness,
    )


@cq_cache()
def calc_outer_edge_rear(wg_slice: WgSlice):
    def edge_rear_thingy(s: WgShell):
        # w = s.w / 2

        # sf = WgShell(angle=s.angle, front=True)
        sr = WgShell(angle=s.angle, rear=True)
        gr = wg_geometry_cached(sr)

        s = (
            cq.Sketch()
                .segment((0, gr.p5_arc[1].y), (20, gr.p5_arc[1].y))
                .segment((20, 20))
                .segment((0, 20))
                .hull()
        )

        return s

    shape = wg_shell(
        radius_list=wg_slice.radius_list_margin,
        slice_function=edge_rear_thingy,
    )
    # shape = shape.intersect(wg_throat_shape(wg_slice.radius_list))
    return shape


def woofer_rear_margin(wg_slice: WgSlice):
    ws = wg_slice.ws

    w = ws.diameter + 0.4
    d = ws.max_diameter + 0.4
    wd_margin = 1.5 * wg_slice.woofer_ply_cutout_y

    woofer_chamfer = cq.Workplane("XY").workplane(offset=2 * wg_slice.woofer_ply_cutout_y + 0.01)
    woofer_chamfer = drivers.ndxx_base(w + 2 * wd_margin, d + 2 * wd_margin, base_shape=woofer_chamfer)
    woofer_chamfer = woofer_chamfer.workplane(offset=-2 * wg_slice.woofer_ply_cutout_y - 0.02)
    woofer_chamfer = drivers.ndxx_base(w, d, base_shape=woofer_chamfer)
    woofer_chamfer = woofer_chamfer.loft(combine=True)
    # woofer_chamfer = woofer_chamfer.union(ws.get_woofer_margin())

    return woofer_chamfer


@cq_cache()
def woofer_flat_slice(wg_slice: WgSlice, woofer_cutout_hole=True, tweet_breathe_slots=False):
    wg_full = wg_throat_with_verts_shape(wg_slice.radius_list)

    woof_flat = calc_woofer_flat_edge(wg_slice)

    outer_edge_box = ply_outer_edge_box(wg_slice)

    ws = wg_slice.ws

    woof_flat = woof_flat.union(wg_drivers.place_woofer(
        ws=ws,
        angle=ws.cbt_angle,
        shape=(
            cq.Workplane("XY")
                .rect(40, ws.diameter + 2)
                .extrude(-0.5)
        ),
    ))

    if wg_slice.ws.woofer_type != 'nd65':
        woof_flat = woof_flat.cut(wg_drivers.place_woofer(
            ws=ws,
            angle=ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .rect(40, 40)
                    .extrude(10)
            ),
        ))
    else:
        woof_flat = woof_flat.intersect(wg_drivers.place_woofer(
            ws=ws,
            angle=ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .rect(40, ws.max_diameter + 8)
                    .workplane(offset=-2 * wg_slice.woofer_ply_cutout_y)
                    .rect(40, ws.max_diameter)
                    .loft(combine=True)
                    .translate((0, 0, 1 * wg_slice.woofer_ply_cutout_y))
            ),
        ))

        d = wg_slice.woofer_ply_cutout_y * 1.6
        if ws.ordinal == 6:
            d = wg_slice.woofer_ply_cutout_y * 1.75
        if ws.ordinal == 7:
            d = wg_slice.woofer_ply_cutout_y * 1.8
        b = wg_drivers.place_woofer(
            ws=ws,
            angle=ws.cbt_angle,
            shape=((
                cq.Workplane("XY")
                    .rect(40, 40)
                    .extrude(d)
            )),
        )
        b = b.intersect(calc_woofer_flat_edge(wg_slice, z_offset=-4))
        b = b.intersect(outer_edge_box)
        woof_flat = woof_flat.union(b)
        woof_flat = woof_flat.cut(wg_drivers.place_woofer(
            ws=ws,
            angle=ws.cbt_angle,
            shape=woofer_rear_margin(wg_slice),
        ))
        woof_flat = woof_flat.cut(wg_drivers.place_woofer(
            ws=ws,
            angle=ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .rect(40, 40)
                    .extrude(-d)
                    .translate((0, 0, -wg_slice.woofer_ply_cutout_y))
            ),
        ))

    if woofer_cutout_hole:
        woof_flat = woof_flat.cut(woofer_flat_cuts(wg_slice))

    if tweet_breathe_slots:
        b = outer_edge_box

        b = b.union((
            wg_drivers.place_woofer(
                ws=ws,
                angle=ws.cbt_angle,
                shape=(
                    cq.Workplane("XY")
                        .circle(ws.max_diameter / 2)
                        .extrude(-wg_slice.woofer_ply_cutout_y)
                        .chamfer(0.2)
                ),
            )
        ))

        b = b.union(woofer_screw_holes(wg_slice, 1.5/2, extrude=-wg_slice.woofer_ply_cutout_y))

        full_ply = ply_middle_shape(wg_slice, woofer_cuts=False, screw_peg_holes=False)
        ply_with_woofer_cut = ply_middle_shape(wg_slice, woofer_cuts=True, screw_peg_holes=False, margin=False)
        ply_inner = full_ply.cut(ply_with_woofer_cut)

        b = b.union(ply_inner)
        b = b.cut(ply_with_woofer_cut)

        woofer_mid_inner_cutout_or_outer_edge = (
            wg_drivers.place_woofers_cut(wg_slice, margin=False)
                .union(outer_edge_box)
        )
        b = b.intersect(woofer_mid_inner_cutout_or_outer_edge)

        woof_flat = woof_flat.intersect(b)

    woof_flat = woof_flat.intersect(wg_full)

    woof_flat = woof_flat.cut((
        wg_drivers.place_woofer(
            ws=ws,
            angle=ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .rect(100, 100)
                    .extrude(-5)
                    .translate((0, 0, -wg_slice.woofer_ply_cutout_y))
            ),
        )
            .cut(outer_edge_box)
    ))

    return woof_flat


@cq_cache()
def front_edge(wg_slice: WgSlice):
    wg_full = wg_throat_with_verts_shape(wg_slice.radius_list)

    woof_flat = wg_drivers.place_woofer(
        ws=wg_slice.ws,
        angle=wg_slice.ws.cbt_angle,
        shape=(
            cq.Workplane("XY")
                .rect(50, 50)
                .circle(wg_slice.ws.cutout_diameter / 2)
                .extrude(-50)
                .pushPoints(wg_slice.ws.screw_holes)
                .hole(wg_slice.ws.screw_hole_diameter)
        ),
    )
    woof_flat = woof_flat.intersect(wg_full)

    return woof_flat


def filament_peg():
    h = 0.8
    chamfer = 0.05
    b = (
        cq.Workplane("XY")
            .circle(FILAMENT_PEG_DIAMETER / 2)
            .extrude(h)
            .translate((0, 0, -h / 2))
    )
    for s in [1, -1]:
        c = (
            cq.Workplane("XY")
                .circle(FILAMENT_PEG_DIAMETER / 2 + chamfer)
                .workplane(offset=chamfer * s)
                .circle(FILAMENT_PEG_DIAMETER / 2)
                .loft(combine=True)
        )
        b = b.union(c)
    return b
