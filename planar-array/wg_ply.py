from hacked_cq_cache import cq_cache

from wg_base_shapes import *
from wg_functions import *
from wg_classes import *
import wg_shapes
import wg_functions


def move_shape_to_ply(angle, y_offset, shape):
    g = wg_geometry_cached(WgShell(angle=angle))
    return move_degree(angle, (
        shape
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .rotate((0, 0, 0), (0, 0, 1), 90)
            .translate((g.p4_arc[0].x, -y_offset, 0))
        # .rotate((0, 0, 0), (0, 1, 0), PLY_INNER_EDGE_STRAIGHT_DEGREES)
    ))


@cq_cache()
def move_hole_to_ply(angle, y_offset, diameter):
    return move_shape_to_ply(angle, y_offset, shape=(
        cq.Workplane("XY")
            .circle(diameter / 2)
            .extrude(20)
            .translate((0, 0, -10))
    ))


@cq_cache()
def move_square_to_ply(angle, y_offset, width):
    return move_rect_to_ply(angle, y_offset, width, width)


@cq_cache()
def move_rect_to_ply(angle, y_offset, width, height, rect_x_offset=0, rect_y_offset=0):
    return move_shape_to_ply(angle, y_offset, shape=(
        cq.Workplane("XY")
            .center(rect_x_offset, rect_y_offset)
            .box(width, height, 20)
    ))


@cq_cache()
def ply_holes_inf(
        wg_slice,
        pegs=True,
        outer_pegs=False,
        screws=True,
        middle_screws=True,
        peg_diameter=PLY_PEG_DIAMETER,
        screw_diameter=None,
):
    shapes = []
    if pegs:
        for hole in wg_slice.screw_holes.pegs:
            shapes.append(move_hole_to_ply(hole.angle, hole.y_offset, peg_diameter))

    if outer_pegs:
        for hole in wg_slice.screw_holes.outer_pegs:
            shapes.append(move_hole_to_ply(hole.angle, hole.y_offset, peg_diameter))

    if screws:
        if screw_diameter is None:
            sd = wg_slice.screw_holes.screw.screw_diameter_loose
        else:
            sd = screw_diameter
        for hole in wg_slice.screw_holes.screws:
            shapes.append(move_hole_to_ply(hole.angle, hole.y_offset, sd))

    if middle_screws:
        if screw_diameter is None:
            sd = wg_slice.screw_holes.middle_screw.screw_diameter_loose
        else:
            sd = screw_diameter
        for hole in wg_slice.screw_holes.middle_screws:
            shapes.append(move_hole_to_ply(hole.angle, hole.y_offset, sd))

    b = wg_functions.union_shapes_list(shapes)

    return b


@cq_cache()
def rib_support_inf(wg_slice: WgSlice, radius_r):
    fillet_radius = min(0.5, wg_slice.shell_thickness - 0.1)
    # print(radius_r)

    shapes = []
    for r in radius_r:
        shapes.append(move_rect_to_ply(
            angle=r,
            y_offset=0,
            width=50,
            height=wg_slice.rib_width,
        ).fillet(fillet_radius))

    b = wg_functions.union_shapes_list(shapes)

    return b


@cq_cache()
def ply_hole_supports_inf(wg_slice: WgSlice, pegs=True, outer_pegs=False, screws=True, middle_screws=False, screw_width_multi=8, screw_height_multi=2):
    holes = PlyHoles(wg_slice)

    fillet_radius = min(0.5, wg_slice.shell_thickness - 0.1)

    shapes = []
    if pegs:
        for hole in holes.outer_pegs if outer_pegs else holes.pegs:
            if hole.front:
                width = PLY_PEG_DIAMETER * 6
                offset = PLY_PEG_DIAMETER * 2
            elif hole.rear:
                width = PLY_PEG_DIAMETER * 6
                offset = -PLY_PEG_DIAMETER * 2
            else:
                width = PLY_PEG_DIAMETER * 3
                offset = -PLY_PEG_DIAMETER * 0.5
            shapes.append(
                move_rect_to_ply(
                    angle=hole.angle,
                    y_offset=hole.y_offset + offset,
                    width=width,
                    height=PLY_PEG_DIAMETER * 2)
                    .fillet(
                    fillet_radius))

    if screws:
        for hole in holes.screws:
            x_offset = 0
            if hole.front:
                x_offset = -3
            elif hole.rear:
                x_offset = 3
            h = move_rect_to_ply(
                angle=hole.angle,
                y_offset=hole.y_offset,
                width=screw_width_multi * wg_slice.shell_thickness,
                height=screw_height_multi * wg_slice.shell_thickness,
                rect_x_offset=x_offset * wg_slice.shell_thickness
            )
            if hole.rear:
                h = h.intersect(wg_shapes.calc_ply_middle_with_shell(wg_slice, 2.0 * wg_slice.outer_shell_thickness))
            s = h.fillet(fillet_radius)
            shapes.append(s)

    if middle_screws:
        for hole in holes.middle_screws:
            x_offset = 0
            if hole.front:
                x_offset = -3
            elif hole.rear:
                x_offset = 3
            shapes.append(move_rect_to_ply(
                angle=hole.angle,
                y_offset=hole.y_offset,
                width=screw_width_multi * wg_slice.shell_thickness,
                height=screw_height_multi * wg_slice.shell_thickness,
                rect_x_offset=x_offset * wg_slice.shell_thickness
            ).fillet(fillet_radius))

    b = wg_functions.union_shapes_list(shapes)

    return b


@cq_cache()
def ply_peg_holes(wg_slice: WgSlice, peg_diameter=None, peg_length=None, outer_pegs=False, chamfer=True):
    if peg_diameter is None:
        peg_diameter = PLY_PEG_DIAMETER
    if peg_length is None:
        peg_length = PLY_PEG_LENGTH_ONLY_OUTER if outer_pegs else PLY_PEG_LENGTH
    b = (
        ply_holes_inf(wg_slice, pegs=not outer_pegs, outer_pegs=outer_pegs, screws=False, middle_screws=False, peg_diameter=peg_diameter)
            .intersect(wg_shapes.calc_ply_middle_with_shell(wg_slice, absolute_thickness=peg_length))
    )
    if chamfer:
        b = b.chamfer(0.1)
    return b


@cq_cache()
def ply_peg_supports(
        wg_slice,
        peg_absolute_length=PLY_PEG_LENGTH + 1.2,
        outer_peg_absolute_length=PLY_PEG_LENGTH_ONLY_OUTER + 1.2,
        peg_fillet=None,
        inner=True,
):
    peg_supports = ply_hole_supports_inf(wg_slice, pegs=True, screws=False)
    ply_middle_with_peg_shell = wg_shapes.calc_ply_middle_with_shell(wg_slice, absolute_thickness=peg_absolute_length)
    peg_supports = peg_supports.intersect(ply_middle_with_peg_shell)

    if not inner and PlyHoles(wg_slice).outer_pegs:
        outer_peg_supports = ply_hole_supports_inf(wg_slice, pegs=True, outer_pegs=True, screws=False)
        ply_middle_with_outer_peg_shell = wg_shapes.calc_ply_middle_with_shell(wg_slice, absolute_thickness=outer_peg_absolute_length)
        outer_peg_supports = outer_peg_supports.intersect(ply_middle_with_outer_peg_shell)
        peg_supports = peg_supports.union(outer_peg_supports)

    if peg_fillet:
        peg_supports = peg_supports.fillet(peg_fillet)

    b = peg_supports
    ply_middle = wg_shapes.calc_ply_middle_with_shell(wg_slice)
    b = b.cut(ply_middle)

    wg_full = wg_shapes.wg_throat_with_verts_shape(wg_slice.radius_list)
    b = b.intersect(wg_full)

    return b


@cq_cache()
def wg_multi_splice_mounts(wg_slice: WgSlice, cuts=True, mirror=False):
    ply = wg_shapes.calc_ply_middle_with_shell(wg_slice)
    # middle_ply = wg_shapes.calc_ply_middle_with_shell(wg_slice, absolute_thickness=PLY_EDGE_THICKNESS_SINGLE)
    outer_edge_box = wg_shapes.ply_outer_edge_box(wg_slice, middle_aligned=True)

    offset = 0.75
    w = 3 * offset
    y_offsets = [-1, -1 - 2 * offset]
    r_offset = distance_across_circle(offset, CBT_RADIUS)

    if not cuts:
        w = w * 0.9

    shapes = []
    for r in [wg_slice.r0 - r_offset, wg_slice.r0 + r_offset, wg_slice.r2 - r_offset, wg_slice.r2 + r_offset]:
        for y_offset in y_offsets:

            x = move_rect_to_ply(
                angle=r,
                y_offset=y_offset,
                width=w,
                height=w)
            x = x.intersect(outer_edge_box)
            x = x.intersect(ply)

            screw = Screw('m4')
            h = move_hole_to_ply(
                angle=r,
                y_offset=y_offset,
                diameter=screw.screw_diameter_loose)
            if cuts:
                shapes.append(h)
            else:
                x = x.cut(h)

            shapes.append(x)

            if cuts:
                hh = move_hole_to_ply(
                    angle=r,
                    y_offset=y_offset,
                    diameter=screw.screw_head_diameter)
                screw_head_cut = wg_shapes.calc_ply_middle_with_shell(wg_slice,
                                                                      absolute_thickness=PLY_EDGE_THICKNESS_COMBINED - 1.25 * screw.screw_head_diameter)
                hh = hh.cut(screw_head_cut)
                hh = hh.chamfer(screw.screw_head_diameter * 0.48)
                shapes.append(hh)

    unioned_shapes = wg_functions.union_shapes_list(shapes)

    if mirror:
        unioned_shapes = unioned_shapes.mirror(mirrorPlane='YZ', union=True)

    return unioned_shapes


@cq_cache()
def wg_ply_middle(wg_slice: WgSlice, mirror=False, multi_splice_mounts=False, show_scale_part=False):
    ply = wg_shapes.ply_middle_shape(wg_slice, mirror=False, margin=True)

    if multi_splice_mounts:
        ply = ply.cut(wg_multi_splice_mounts(wg_slice))

    if show_scale_part and wg_slice.ws.ordinal == 0:
        import wg_drivers
        x = wg_drivers.place_woofer(
            ws=wg_slice.ws,
            angle=wg_slice.ws.cbt_angle,
            shape=(
                cq.Workplane("XY")
                    .box(5, 5, 5)
            ),
        )
        x = x.intersect(wg_shapes.calc_ply_middle_with_shell(wg_slice))
        ply = ply.union(x)

    if mirror:
        ply = ply.mirror(mirrorPlane='YZ', union=True)

    return ply


def wg_ply_middles(wg_slice: WgSlice, mirror=False, split_ply=False):
    base = wg_shapes.ply_middle_shape(wg_slice, mirror=mirror, margin=True, use_new=True)
    if not split_ply:
        return [base]

    base = (
        wg_shapes.place_ply_middle(wg_slice, base, invert=True)
            .translate((PLY_EDGE_THICKNESS_COMBINED / 2, 0, 0))
    )

    ply = []

    num_boards_list = range(0, PLY_EDGE_NUM_BOARDS)
    # num_boards_list = [0]

    overlap = 0.1

    for i in num_boards_list:
        t = PLY_EDGE_THICKNESS_SINGLE
        ply_slice = base.intersect((
            cq.Workplane("XY")
                .rect(100, 100)
                .extrude(t)
                .translate((0, 0, i * t))
                .rotate((0, 0, 0), (0, 1, 0), 90)
        ))

        ply_slice = ply_slice.translate((-i * t, 0, 0))

        # Do stuff

        ply_slice = ply_slice.union(ply_slice.translate((-t + overlap, 0, 0)))
        xx = (
            cq.Workplane("XY")
                .rect(2000, 2000)
                .extrude(overlap)
                .rotate((0, 0, 0), (0, 1, 0), 90)
            # .faces("<X")
        )
        ply_slice = ply_slice.intersect(xx)

        # return [ply_slice, xx]

        # ps = ps.faces("<X")

        ply_slice = ply_slice.translate((0, i * 25, 0))

        # Invert transformrs
        # ply_slice = ply_slice.translate((i * PLY_EDGE_THICKNESS_SINGLE, 0, 0))
        # ply_slice = ply_slice.translate((-PLY_EDGE_THICKNESS_COMBINED / 2, 0, 0))
        # ply_slice = wg_shapes.place_ply_middle(wg_slice, ply_slice, invert=False)

        ply.append(ply_slice)

    ply.append((
        cq.Workplane("XY")
            .rect(25, 25)
            .extrude(overlap)
            .rotate((0, 0, 0), (0, 1, 0), 90)
            .translate((0, -30, 0))
        # .faces("<X")
    ))

    return ply


def place_wg_shape(angle, y_offset, shape):
    g = wg_geometry_cached(WgShell(angle=0))

    s = shape

    p = P(CBT_RADIUS, 0) + P(angle=-angle, length=-(CBT_RADIUS + y_offset))

    ply_offset = 0.5 * PLY_EDGE_THICKNESS_SINGLE
    x_offset = g.wg_edge_middle.x + ply_offset
    xx = P(angle=PLY_INNER_EDGE_STRAIGHT_DEGREES, x=p.y)
    x_offset += xx.y

    s = (
        s
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .rotate((0, 0, 0), (0, 0, 1), 90)
            .translate((x_offset, p.x, p.y))
    )
    return s


def wg_screw(length, gauge, heatsert, countersunk, head_extend_extrude=0):
    screw = Screw(gauge)

    shapes = []

    screw_loose = (
        cq.Workplane("XY")
            .circle(screw.screw_diameter_loose / 2)
            .extrude(length)
            .translate((0, 0, -length / 2))
    )
    shapes.append(screw_loose)

    if heatsert:
        heatsert_length_margin = 0.2
        heatsert = (
            cq.Workplane("XY")
                .circle(screw.heatsert_diameter / 2)
                .extrude(screw.heatsert_long_depth + heatsert_length_margin * 2)
                .translate((0, 0, -length / 2 - heatsert_length_margin))
        )
        shapes.append(heatsert)

    if head_extend_extrude:
        head_extend = (
            cq.Workplane("XY")
                .circle(screw.screw_head_diameter / 2)
                .extrude(head_extend_extrude)
                .translate((0, 0, length / 2))
        )
        shapes.append(head_extend)
    if countersunk:
        head_chamfer = (
            cq.Workplane("XY")
                .circle(screw.screw_head_diameter / 2)
                .workplane(offset=-(screw.screw_head_diameter - screw.screw_diameter_loose) / 2)
                .circle(screw.screw_diameter_loose / 2)
                .loft(combine=True)
                .translate((0, 0, length / 2))
        )
        shapes.append(head_chamfer)

    return wg_functions.union_shapes_list(shapes)


def wg_screws2(wg_slice: WgSlice):
    shapes = []
    for hole in wg_slice.screw_holes.screws:
        screw = place_wg_shape(hole.angle, hole.y_offset, shape=(
            wg_screw(length=WG_PLY_SCREW_LENGTH, gauge=WG_PLY_SCREW_GAUGE, countersunk=True, heatsert=True, head_extend_extrude=10)
        ))
        shapes.append(screw)

    for hole in wg_slice.screw_holes.middle_screws:
        screw = place_wg_shape(hole.angle, hole.y_offset, shape=(
            wg_screw(length=PLY_EDGE_THICKNESS_COMBINED + 2 * wg_slice.outer_shell_thickness, gauge=WG_PLY_SCREW_GAUGE, countersunk=True, heatsert=True,
                     head_extend_extrude=10)
        ))
        shapes.append(screw)

    return wg_functions.union_shapes_list(shapes)


# Depricated, replaced by wg_screws2
def wg_screws(wg_slice: WgSlice):
    screw = wg_slice.screw_holes.screw
    countersunk_chamfer = (screw.screw_head_diameter - 0.05) / 2

    thickness = 4 + 0.2  # margin
    # if countersunk_heads:
    #     thickness -= countersunk_chamfer

    ply_middle_screw_length = wg_shapes.calc_ply_middle_with_shell(wg_slice, absolute_thickness=thickness)
    ply_middle_screw_length_chamfer = wg_shapes.calc_ply_middle_with_shell(wg_slice, absolute_thickness=thickness - (countersunk_chamfer * 2))
    ply_middle_middle_screw_length_chamfer = wg_shapes.calc_ply_middle_with_shell(wg_slice,
                                                                                  shell_thickness=wg_slice.outer_shell_thickness - (countersunk_chamfer * 1))
    # ply_middle_screw_only_threads = wg_shapes.calc_ply_middle_with_shell(wg_slice, absolute_thickness=thickness - 2 * screw.heatsert_long_depth)
    ply_middle_screw_only_threads = wg_shapes.calc_ply_middle_with_shell(wg_slice)

    heatsert = (
        ply_holes_inf(wg_slice, pegs=False, screws=True, middle_screws=False,
                      screw_diameter=screw.heatsert_diameter)
            .intersect(ply_middle_screw_length)
            .cut(ply_middle_screw_only_threads)
            .cut(wg_shapes.ply_outer_edge_box(wg_slice))
    )

    inner = (
        ply_holes_inf(wg_slice, pegs=False, screws=True, middle_screws=False)
            .intersect(ply_middle_screw_length)
    )

    screw_head_cut = (
        ply_holes_inf(wg_slice, pegs=False, screws=True, middle_screws=False,
                      screw_diameter=screw.screw_head_diameter)
            .cut(ply_middle_screw_length_chamfer)
            .intersect(wg_shapes.ply_outer_edge_box(wg_slice))
            .chamfer(countersunk_chamfer)
    )

    # middle_screws
    middle_screw_inner = (
        ply_holes_inf(wg_slice, pegs=False, screws=False, middle_screws=True)
            .intersect(ply_middle_screw_length)
    )
    middle_screw_head_cut = (
        ply_holes_inf(wg_slice, pegs=False, screws=False, middle_screws=True,
                      screw_diameter=screw.screw_head_diameter)
            .cut(ply_middle_middle_screw_length_chamfer)
            .intersect(wg_shapes.ply_outer_edge_box(wg_slice))
            .chamfer(countersunk_chamfer)
    )

    b = (
        inner
            .union(heatsert)
            .union(screw_head_cut)
            .union(middle_screw_inner)
            .union(middle_screw_head_cut)
    )

    return b
