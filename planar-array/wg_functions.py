import cadquery as cq
from wg_classes import *
import functools


def move_degree(angle, shape):
    ts = WgShell(angle=angle)

    shape = shape.rotate((0, 0, 0), (1, 0, 0), -angle)
    shape = shape.translate((0, ts.center.x, ts.center.y))

    return shape


def union_shapes_list(shape_list):
    if shape_list:
        return functools.reduce(lambda a, b: a.union(b), shape_list)
    else:
        return None


def outer_fillet(base_shape, hole_shape, fillet_radius):
    b2 = (
        hole_shape.shell(fillet_radius * 2.2)
            .intersect(base_shape)
            .fillet(fillet_radius)
    )

    b1 = (
        hole_shape.union(hole_shape.shell(fillet_radius * 1.1))
            .intersect(base_shape)
            .cut(b2)
    )

    return b1


def outer_chamfer(base_shape, hole_shape, chamfer_radius):
    b2 = (
        hole_shape.shell(chamfer_radius * 2.2)
            .intersect(base_shape)
            .chamfer(chamfer_radius)
    )

    b1 = (
        hole_shape.union(hole_shape.shell(chamfer_radius * 1.1))
            .intersect(base_shape)
            .cut(b2)
    )

    return b1


def middle_value(a, b):
    max_v = max(a, b)
    min_v = min(a, b)
    diff_v = max_v - min_v

    return min_v + diff_v / 2


def scale_cm_to_mm(workplane: cq.Workplane) -> cq.Workplane:
    return scale(workplane, 10, 10, 10)


def scale(workplane: cq.Workplane, x: float, y=None, z=None) -> cq.Workplane:
    y = y if y is not None else x
    z = z if z is not None else x
    t = cq.Matrix([
        [x, 0, 0, 0],
        [0, y, 0, 0],
        [0, 0, z, 0],
        [0, 0, 0, 1]
    ])
    return workplane.newObject([
        o.transformGeometry(t) if isinstance(o, cq.Shape) else o
        for o in workplane.objects
    ])
