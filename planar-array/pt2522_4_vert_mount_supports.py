import cadquery as cq

import wg_functions
import wg_drivers
import drivers
from wg_classes import *


tweeter_holes = wg_drivers.tweeter_holes(
    wg_slice=None,
    diameter=Screw('m3').screw_diameter_loose,
    extrude=4,
    offset=-2,
    mirror=True,
    sideways=True,
    place=False
)
tweeter_holes = tweeter_holes.rotate((0, 0, 0), (1, 0, 0), -90)

margin = False
kwargs = {
    "cut_audio_holes": False,
    # render_screw_holes_and_rivets_and_terminals:not cheap_tweeter,
    "cut_screw_holes": False if margin else True,
    # union_heat_inserts:True,
    "with_margin": margin,
    "base_xy_margin_cm": 0.1 if margin else 0,
    "platform_xy_margin_cm": 0.1 if margin else 0,
    # union_heat_insert_through_hole_distance:2.0,
    #
    # rear_screw_holes_union_inner_height:-0.9,
    # rear_screw_holes_union_outer_height:-0.6,
    # rear_screw_holes_union_diameter:1.2,
}

tweeter = drivers.pt2522(**kwargs)
tweeter = tweeter.rotate((0, 0, 0), (0, 0, 1), 90)
tweeter = tweeter.rotate((0, 0, 0), (1, 0, 0), -90)

margin = True
kwargs = {
    "cut_audio_holes": False,
    # render_screw_holes_and_rivets_and_terminals:not cheap_tweeter,
    "cut_screw_holes": False if margin else True,
    # union_heat_inserts:True,
    "with_margin": margin,
    "base_xy_margin_cm": 0.1 if margin else 0,
    "platform_xy_margin_cm": 0.1 if margin else 0,
    # union_heat_insert_through_hole_distance:2.0,
    #
    # rear_screw_holes_union_inner_height:-0.9,
    # rear_screw_holes_union_outer_height:-0.6,
    # rear_screw_holes_union_diameter:1.2,
}

tweeter_margin = drivers.pt2522(**kwargs)
tweeter_margin = tweeter_margin.rotate((0, 0, 0), (0, 0, 1), 90)
tweeter_margin = tweeter_margin.rotate((0, 0, 0), (1, 0, 0), -90)

h_part = wg_drivers.pt2522_width + 0.1

boop = (
    cq.Workplane("XY")
        .box(wg_drivers.pt2522_height, 0.8, 8)
        .chamfer(0.3)
        .intersect(
        cq.Workplane("XY")
            .box(wg_drivers.pt2522_height, 1.0, h_part)
    )
        .cut((
            cq.Workplane("XY")
                .box(wg_drivers.pt2522_height, 1.0, 8)
                .translate((0, 0.5, 0))
        ))
        .cut(
        cq.Workplane("XY")
            .box(6.52, 2.0, wg_drivers.pt2522_width + 1)
    )
        .cut(
        cq.Workplane("XY")
            .box(10, 0.35, wg_drivers.pt2522_width + 1)
    )
    #     .cut((
    #     cq.Workplane("XY")
    #         .box(10, 2.0, 3.5)
    #         .cut((
    #         cq.Workplane("XY")
    #             .box(10, 0.8, 3.5)
    #     ))
    # ))
        .cut(tweeter_holes)
        .cut(tweeter_margin)
)

boop_p = boop
tweet_p = tweeter

for i in [1, 2, 3]:

    boop = boop.union(
        boop_p.translate((0, 0, h_part * i))
    )
    tweeter = tweeter.union(
        tweet_p.translate((0, 0, h_part * i))
    )


boop = wg_functions.scale_cm_to_mm(boop)
tweeter = wg_functions.scale_cm_to_mm(tweeter)
tweeter_holes = wg_functions.scale_cm_to_mm(tweeter_holes)

cq.exporters.export(
    boop,
    '4_pt2522_vert_mounts.step'
)


show_object(boop, name=f'boop',
            options={"color": (125, 45, 45), "alpha": 0.0})

show_object(tweeter, name=f'tweeter',
            options={"color": (45, 45, 45), "alpha": 0.0})

# show_object(tweeter_holes, name=f'tweeter_holes',
#             options={"color": (45, 45, 45), "alpha": 0.0})