import cadquery as cq

# from wg_shapes import *
from wg_classes import *
import wg_functions
import wg_drivers
import planar_small_wg
import drivers

vert_w = 22
vert_d = 14

vert_support_y_offset = (vert_d - 2.5) / 2
vert_edge_screw_x_offset = (vert_w / 2 - 1.5)


def rear_support_screw_holes(wg_slice: WgSlice):
    angle = wg_functions.middle_value(wg_slice.r0, wg_slice.r1)

    h = (
        cq.Workplane("XY")
            .circle(Screw('m3').screw_diameter_loose / 2)
            .extrude(20)
            .translate((0, 0, -10))
    )

    a = h.translate((-1.3, 0, 0))
    b = h.translate((1.3, 0, 0))

    s = a.union(b)

    shape = (
        s.translate((0, vert_support_y_offset, 0))
    )

    return wg_functions.move_degree(angle, shape)


def rear_support(wg_slice: WgSlice):
    angle = wg_functions.middle_value(wg_slice.r0, wg_slice.r1)

    depth = 1.5

    h = TWEETER_HEIGHT - 0.3

    b = (
        cq.Workplane("XY")
            .box(4, depth, h)
            .cut((
            cq.Workplane("XY")
                .box(4, depth, h - 1.5)
        ))
            .chamfer(0.3)
    )

    mid_rib = (
        cq.Workplane("XY")
            .box(0.75, depth, h)
            .chamfer(0.3)
    )

    shape = (
        mid_rib
            .union(b)
            .translate((0, vert_support_y_offset, 0))
    )

    support = wg_functions.move_degree(angle, shape)

    support = support.cut(calc_vert(wg_slice.r0, bot=True, top=False))
    support = support.cut(calc_vert(wg_slice.r1, bot=False, top=True))
    support = support.cut(rear_support_screw_holes(wg_slice))

    return support


def verts(wg_slice: WgSlice):
    angle = wg_functions.middle_value(wg_slice.r0, wg_slice.r1)

    v0_full = calc_vert(wg_slice.r0, bot=True, top=False)
    v1_full = calc_vert(wg_slice.r1, bot=False, top=True)

    v0 = calc_vert(wg_slice.r0, bot=True, top=False, shell=-0.3)
    v1 = calc_vert(wg_slice.r1, bot=False, top=True, shell=-0.3)

    # tweeter_support_x = pt2522_x_offset * 4.5
    tweeter_support_y = 2.25
    tweet_support = wg_functions.move_degree(angle, (
        cq.Workplane("XY")
            .box(pt2522_height + 1, tweeter_support_y, 12)
            .cut((
            cq.Workplane("XY")
                .box(5.1, tweeter_support_y, 12)
                .translate((0, tweeter_support_y / 2, 0))
        ))
        # .chamfer(0.3)
    ))

    verts = (
        v0.intersect(v0_full)
            .union(v1.intersect(v1_full))
            .union(tweet_support.intersect(v0_full))
            .union(tweet_support.intersect(v1_full))
    )

    verts = verts.cut(rear_support_screw_holes(wg_slice))

    wg = (
        wg_drivers.place_tweeter_shape(wg_slice, tweeter_r_list=[angle], shape=(
            planar_small_wg.planar_tweeter_wg(shell_thickness=-0.25, edge_breathe_slot=True)
            # planar_small_wg.planar_tweeter_wg()
        ))
            .cut(verts)
            .cut(v0_full)
            .cut(v1_full)
    )

    # render_flanges = True
    render_flanges = False

    if render_flanges:
        verts = verts.union((
            wg_drivers.place_tweeter_shape(wg_slice, tweeter_r_list=[angle], shape=
            drivers.pt2522_flanges(fillet_width=1.0, mid_flare_height=2.5, only_flare=True)
                                           )
                .intersect((
                wg_drivers.place_tweeter_shape(wg_slice, tweeter_r_list=[angle], shape=(
                    cq.Workplane("XY")
                        .box(30, 8.5, 30)
                ))
            ))
                # .cut((
                #     wg_drivers.place_tweeter_shape(wg_slice, tweeter_r_list=[angle], shape=
                #     planar_small_wg.planar_tweeter_wg().union((
                #         planar_small_wg.planar_tweeer_wg_breathe_slots()
                #             .mirror(mirrorPlane='YZ', union=True)
                #     ))
                #                                    )
                # ))
                # wg_drivers.place_tweeter_flanges(wg_slice, fillet_width=1.5, mid_flare_height=2.5)
                #     .cut(planar_small_wg.planar_tweeter_wg())
                .cut(v0_full)
                .cut(v1_full)
        ))

    verts = verts.cut(wg_functions.move_degree(angle, (
        cq.Workplane("XY")
            .box(30, 1.0, 30)
    )))

    wg = wg.cut(wg_functions.move_degree(angle, (
        cq.Workplane("XY")
            .box(30, 1.0, 30)
    )))

    # verts = verts.cut(wg_drivers.place_tweeters(wg_slice, margin=True, margin_rear_termina_block=False))
    # verts = verts.cut(wg_drivers.place_tweeters(wg_slice, rotate_top_down=True, margin=True, margin_rear_termina_block=False))
    tweeter_holes = wg_drivers.tweeter_holes(
        wg_slice,
        diameter=Screw('m3').screw_diameter_loose,
        extrude=4,
        offset=-2,
        mirror=True,
        sideways=True,
    )

    verts = verts.cut(tweeter_holes)

    verts = verts.cut(wg_drivers.tweeter_holes(
        wg_slice,
        diameter=Screw('m3').screw_head_diameter,
        extrude=2,
        offset=0.7,
        mirror=True,
        sideways=True,
    ).chamfer(Screw('m3').screw_head_diameter*0.45)
    )

    #

    boop = (
        wg_functions.move_degree(angle, (
            cq.Workplane("XY")
                .box(wg_drivers.pt2522_height, 1.0, 8)
                .chamfer(0.3)
                .intersect(
                cq.Workplane("XY")
                    .box(wg_drivers.pt2522_height, 1.0, wg_drivers.pt2522_width - 0.1)
            )
            # .cut((
            #     cq.Workplane("XY")
            #         .box(wg_drivers.pt2522_height, 1.0, 8)
            #         .translate((0, 0.5, 0))
            # ))
                .cut(
                cq.Workplane("XY")
                    .box(6.52, 2.0, wg_drivers.pt2522_width + 1)
            )
                .cut(
                cq.Workplane("XY")
                    .box(10, 0.35, wg_drivers.pt2522_width + 1)
            )
            .cut((
                cq.Workplane("XY")
                    .box(10, 2.0, 3.5)
                    .cut((
                    cq.Workplane("XY")
                        .box(10, 0.8, 3.5)
                ))
            ))
        ))
            .cut(tweeter_holes)
            .cut(wg_drivers.place_tweeters(wg_slice, mirror_cut=False, cut_audio_holes=False, sideways=True, margin=True))
    )

    # boop = boop.chamfer(0.25)
    #
    # boop = boop.cut(verts)
    # boop = boop.cut(wg)
    # boop = boop.cut(wg_functions.move_degree(angle, (
    #     cq.Workplane("XY")
    #         .box(30, 1.0, 30)
    # )))

    return verts, wg, boop


def wg_vert():
    w = vert_w
    d = vert_d

    base_height = 0.15
    step_offset = 0.1

    base = (
        cq.Workplane("XY")
            .moveTo(-w / 2, 0)
            .threePointArc((0, d / 2), (w / 2, 0))
            .threePointArc((0, -d / 2), (-w / 2, 0))
            .close()
            .extrude(0.15)
    )

    # return base

    b = (
        cq.Workplane("XY")
            .moveTo(-w / 2, 0)
            .threePointArc((0, d / 2), (w / 2, 0))
            .threePointArc((0, -d / 2), (-w / 2, 0))
            .close()
            .workplane(offset=pt2522_h_offset - base_height)
            .rect(pt2522_x_offset * 4, pt2522_y_offset * 2)
            .loft(combine=True)
            # .extrude(0.15)
            .translate((0, 0, base_height))
    )

    return base.union(b)


def calc_vert(angle, top=False, bot=False, shell=None, hole=False):
    ts = WgShell(angle=angle)

    wv = wg_vert()

    # Cutouts  to route cables when using normal mounting
    # for x_sign in [-1, 1]:
    #     l = 1.5
    #
    #     wv = wv.cut((
    #         cq.Workplane("XY")
    #             .lineTo(-l, 0)
    #             .lineTo(0, l)
    #             .lineTo(l, 0)
    #             .close()
    #             .extrude(10)
    #             .translate((6 * x_sign, 0, 0))
    #     ))

    for x_sign, y_sign in [
        (-1, -1),
        (-1, 1),
        (1, -1),
        (1, 1),
    ]:
        wv = wv.cut((
            cq.Workplane("XY")
                .moveTo(vert_edge_screw_x_offset * x_sign, 1.0 * y_sign)
                .circle(Screw('m4').screw_diameter_loose / 2)
                .extrude(10)
        ))

    if shell:
        if 0 > shell:
            vert_shell = wv.translate((0, 0, shell))

            for s in [-1, 1]:
                vert_shell = vert_shell.cut((
                    cq.Workplane("XY")
                        .moveTo((vert_edge_screw_x_offset + 0.5) * s, 0)
                        .box(3, 4, 3)
                        .chamfer(0.5)
                ))

            wv = wv.cut(vert_shell)
        else:
            raise ('only negative shell supported')

    if top and bot:
        wv = wv.union(wv.rotate((0, 0, 0), (0, 1, 0), 180))
    elif bot or hole:
        pass
    elif top:
        wv = wv.rotate((0, 0, 0), (0, 1, 0), 180)

    wv = wv.rotate((0, 0, 0), (1, 0, 0), -angle)
    wv = wv.translate((0, ts.center.x, ts.center.y))

    return wv
