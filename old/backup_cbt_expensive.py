import math

import numpy as np
from PIL import Image


def calculate_degrees_across_radi(c, radius):
    return math.degrees(
        math.acos(1 - ((c * c) / (2 * radius * CBT_OUTER_RADIUS))))


# Speaker constants
# Unless specified constants are in meters
WIDTH_M = 1.52
HEIGHT_M = 1.52

CBT_OUTER_RADIUS = 1.4

CBT_UPPER_WIDTH = 0.12
CBT_LOWER_WIDTH = 0.26

BOARD_THICKNESS = 0.006

NUM_DRIVERS = 22

DRIVER_HOLE_RADIUS_CM = 7.2 / 2.0
DRIVER_WIDTH_CM = 6.4
DRIVER_CTC = DRIVER_WIDTH_CM / 100.0

DRIVER_DISTANCE_HOLE_TO_HOLE_CM = math.sqrt(2 * DRIVER_HOLE_RADIUS_CM * DRIVER_HOLE_RADIUS_CM)
DRIVER_HOLE_DISTANCE_TO_EDGE_CM = DRIVER_WIDTH_CM / 2.0 - math.sqrt(DRIVER_HOLE_RADIUS_CM * DRIVER_HOLE_RADIUS_CM - math.pow(DRIVER_DISTANCE_HOLE_TO_HOLE_CM / 2, 2))

DEGREES_BEYOND_LAST_DRIVER = calculate_degrees_across_radi(1.5 / 100.0, CBT_OUTER_RADIUS)

DRIVER_FRONT_CUTOUT_RADIUS = 6.5 / 2.0 / 100.0
DRIVER_FRONT_BEZZLE_THICKNESS = 0.3 / 100.0

DRIVER_CUTOUT_RADIUS = (6.4 - 1.0) / 2.0 / 100.0
DRIVER_CUTOUT_CUTOFF = 3.6 / 100.0

PIXELS_PER_METER = 3793.0

# Derived constants
DEGREES_PER_DRIVER = calculate_degrees_across_radi(DRIVER_CTC, CBT_OUTER_RADIUS)
DEGREES_PER_DRIVER_CUTOFF = calculate_degrees_across_radi(DRIVER_CUTOUT_RADIUS, CBT_OUTER_RADIUS)
DEGREES_PER_DRIVER_BEDDLE_CUTOUT = calculate_degrees_across_radi(DRIVER_FRONT_CUTOUT_RADIUS, CBT_OUTER_RADIUS)

CBT_DEGREE_CUTOFF = DEGREES_PER_DRIVER * NUM_DRIVERS + DEGREES_BEYOND_LAST_DRIVER

# Loop all drivers and calculate x and y coordinates
DRIVERS_R = []
for n in range(1, NUM_DRIVERS + 1):
    DRIVERS_R.append((n - 0.5) * DEGREES_PER_DRIVER)

DRIVERS_CROSSOVERS_R = []
for n in range(1, NUM_DRIVERS + 2):
    DRIVERS_CROSSOVERS_R.append((n - 1) * DEGREES_PER_DRIVER)

# Pixel normalized constants
WIDTH_PX = int(WIDTH_M * PIXELS_PER_METER)
HEIGHT_PX = int(HEIGHT_M * PIXELS_PER_METER)


def calculate_width(r):
    return CBT_LOWER_WIDTH - (r / CBT_DEGREE_CUTOFF) * (CBT_LOWER_WIDTH - CBT_UPPER_WIDTH)


import unittest

tc = unittest.TestCase('__init__')
tc.assertAlmostEqual(calculate_width(0), CBT_LOWER_WIDTH)
tc.assertAlmostEqual(calculate_width(CBT_DEGREE_CUTOFF), CBT_UPPER_WIDTH)


def is_in_hole(x, y, hole_x, hole_y, hole_diameter):
    hole_x_diff = abs(hole_x - x)
    hole_y_diff = abs(hole_y - y)

    hole_c_diff = math.sqrt(math.pow(hole_x_diff, 2) + math.pow(hole_y_diff, 2))

    if hole_c_diff < hole_diameter / 2.0:
        return True


def cbt_is_cutout(x_px, y_px):
    x = float(x_px) / PIXELS_PER_METER
    y = float(y_px) / PIXELS_PER_METER

    r = math.degrees(math.atan(y / x))
    c = math.sqrt(x * x + y * y)

    r_width = calculate_width(r)

    # if x == y:
    #     print(f"x = {x}, y = {y}, r = {r}, c = {c}")

    # Remove outer radius
    if CBT_OUTER_RADIUS < c:
        return True

    # Remove inner radius, more on bottom
    if c < CBT_OUTER_RADIUS - r_width:
        return True

    if CBT_OUTER_RADIUS - 0.025 < c:
        return True

    # Plug holes
    for driver_r in DRIVERS_R:
        hole_c = CBT_OUTER_RADIUS - 0.08
        hole_x = math.cos(math.radians(driver_r)) * hole_c
        hole_y = math.sin(math.radians(driver_r)) * hole_c

        if is_in_hole(x, y, hole_x, hole_y, 0.8 / 100.0):
            return True

    last_driver_relative_r = r - DRIVERS_CROSSOVERS_R[-1]
    if 0 < last_driver_relative_r and calculate_degrees_across_radi(BOARD_THICKNESS * 0.5,
                                                                    c) + calculate_degrees_across_radi(
        BOARD_THICKNESS * 0.5, c) < last_driver_relative_r:
        return True

    if CBT_OUTER_RADIUS - 0.04 < c:

        for driver_r in DRIVERS_CROSSOVERS_R:

            driver_r = driver_r + calculate_degrees_across_radi(BOARD_THICKNESS * 0.5, c)

            relative_r = abs(r - driver_r)

            if relative_r < calculate_degrees_across_radi(BOARD_THICKNESS * 0.5, c):
                return True

            if relative_r < calculate_degrees_across_radi(BOARD_THICKNESS * 1.5, c):
                return False

        return True
    return False


DRIVER_STANDOFF_OFFSET = 0.25


def driver_standoff_is_solid(x0, y0):
    x = x0 * 100.0
    y = y0 * 100.0

    if 2.0 < y:
        return False

    if 2.5 < x:
        return False

    if x + y < 1.0:
        return False

    if x + (2.0 - y) < 1.0:
        return False

    if (2.5 - x) + y < 1.0:
        return False

    if (2.5 - x) + (2 - y) < 1:
        return False

    if abs(1.25 - x) < BOARD_THICKNESS * 0.5 * 100.0 and y < 1.0 - DRIVER_STANDOFF_OFFSET:
        return False

    # Cut driver holes
    for hole_x in [1.25 + DRIVER_HOLE_DISTANCE_TO_EDGE_CM, 1.25 - DRIVER_HOLE_DISTANCE_TO_EDGE_CM]:
        hole_y = 1.0

        if is_in_hole(x, y, hole_x, hole_y, 0.2):
            return False

    return True


margin = 0.001

tc.assertEqual(driver_standoff_is_solid(0.00 + margin, 0.01), True)
# tc.assertEqual(driver_standoff_is_solid(0.01 - margin, 0.00 + 2 * margin), True)
tc.assertEqual(driver_standoff_is_solid(0.01, 0.02 - margin), True)
tc.assertEqual(driver_standoff_is_solid(0.015, 0.02 - margin), True)
# tc.assertEqual(driver_standoff_is_solid(0.015 + margin, 0.00 + 2 * margin), True)
tc.assertEqual(driver_standoff_is_solid(0.025 - margin, 0.01), True)
tc.assertEqual(driver_standoff_is_solid(0.01, 0.01), True)
tc.assertEqual(driver_standoff_is_solid(0.015, 0.01), True)

tc.assertEqual(driver_standoff_is_solid(0.00, 0.00), False)
tc.assertEqual(driver_standoff_is_solid(0.00, 0.02), False)
tc.assertEqual(driver_standoff_is_solid(0.025, 0.00), False)
tc.assertEqual(driver_standoff_is_solid(0.025, 0.02), False)

tc.assertEqual(driver_standoff_is_solid(0.004, 0.004), False)
tc.assertEqual(driver_standoff_is_solid(0.004, 0.016), False)
tc.assertEqual(driver_standoff_is_solid(0.021, 0.004), False)
tc.assertEqual(driver_standoff_is_solid(0.021, 0.016), False)

tc.assertEqual(driver_standoff_is_solid(0.0125, 0.00), False)
tc.assertEqual(driver_standoff_is_solid(0.0125, 0.005), False)

tc.assertAlmostEqual(calculate_width(CBT_DEGREE_CUTOFF), CBT_UPPER_WIDTH)


def cbt_spine_is_solid(x0, y0, width):
    x = x0 * 100.0
    y = y0 * 100.0

    r_center_x = 30
    r_center_y = width / 2.0

    y_rel_to_center = abs(r_center_y - y)
    c_r_center = math.sqrt(math.pow(abs(r_center_x - x), 2) + math.pow(y_rel_to_center, 2))

    if r_center_x < c_r_center:
        return False

    max_x = 10.0
    if max_x < x:
        return False

    if width < y:
        return False

    # if y_rel_to_center < 3.0 and x < BOARD_THICKNESS * 100.0:
    if y_rel_to_center < (DRIVER_DISTANCE_HOLE_TO_HOLE_CM / 2.0) + DRIVER_STANDOFF_OFFSET and x < BOARD_THICKNESS * 100.0:
        return False

    four_layer_boards_thickness = BOARD_THICKNESS * 100.0 / 2.0 * 4.0
    two_layer_boards_thickness = four_layer_boards_thickness / 2.0
    if y_rel_to_center < four_layer_boards_thickness and 4.0 < x:
        return False

    x_offset_edge = r_center_x - math.sqrt(math.pow(r_center_x, 2) - math.pow(width / 2.0, 2))

    edge_tubing_diameter = 0.8

    x_triangle = max_x - x_offset_edge - edge_tubing_diameter
    y_triangle = width / 2.0 - two_layer_boards_thickness
    x_rel_to_triangle = abs(x - x_offset_edge - edge_tubing_diameter)

    y_rel_to_width_edge = width / 2.0 - y_rel_to_center
    x_rel_to_edge_tubing_center = abs(x - (x_offset_edge + edge_tubing_diameter / 2.0))
    if math.sqrt(
            math.pow(y_rel_to_width_edge, 2) + math.pow(x_rel_to_edge_tubing_center, 2)) < edge_tubing_diameter / 2.0:
        return False

    if x_offset_edge + edge_tubing_diameter < x and two_layer_boards_thickness < y_rel_to_center:
        y_rel_to_triangle = y_rel_to_center - two_layer_boards_thickness
        if 1.0 < math.pow(y_rel_to_triangle / y_triangle, 1.25) + math.pow(x_rel_to_triangle / x_triangle, 1.25):
            return False

    # spine holes
    x_hole = 4.0
    y_hole = width * 0.2

    if is_in_hole(x, y_rel_to_center, x_hole, y_hole, 3.0):
        return False

    return True


def calculate_cbt_parallel(x):
    result = []
    max_y = int(1.2 * PIXELS_PER_METER)
    for y in range(1, max_y):
        if not cbt_is_cutout(x, max_y - y):
            result.append({'x': x - min_x, 'y': y})
    return result


def calculate_cbt_spine_parallel(spine_radii=None):
    # r = spine_radii

    # max_width = 0.35
    # min_width = 0.14
    #
    # width = min_width - (1 - (r / CBT_DEGREE_CUTOFF)) * (min_width - max_width)
    width = 0.24

    cbt_spine_xy = []
    for x_px in range(1, int(0.2 * PIXELS_PER_METER)):
        for y_px in range(1, int(width * PIXELS_PER_METER)):
            x = float(x_px) / PIXELS_PER_METER
            y = float(y_px) / PIXELS_PER_METER

            if cbt_spine_is_solid(x, y, width * 100.0):
                cbt_spine_xy.append({'x': x_px, 'y': y_px})

    return cbt_spine_xy


min_x = int(0.6 * PIXELS_PER_METER)
if __name__ == '__main__':

    import multiprocessing

    pool = multiprocessing.Pool(10)

    cbt_xy = []
    for l in pool.map(calculate_cbt_parallel, range(min_x, int(1.4 * PIXELS_PER_METER))):
        cbt_xy += l

    driver_standoff_xy = []

    for x_px in range(1, int(0.1 * PIXELS_PER_METER)):
        for y_px in range(1, int(0.1 * PIXELS_PER_METER)):
            x = float(x_px) / PIXELS_PER_METER
            y = float(y_px) / PIXELS_PER_METER

            if driver_standoff_is_solid(x, y):
                driver_standoff_xy.append({'x': x_px, 'y': y_px})

    cbt_spine = calculate_cbt_spine_parallel()
    calculate_cbt_spine_parallel()

    # cbt_spines = pool.map(calculate_cbt_spine_parallel, DRIVERS_CROSSOVERS_R)
    pool.close()

    edge_margin = 0.01
    edge_margin_px = int(edge_margin * PIXELS_PER_METER)

    cbt_array = np.zeros([HEIGHT_PX, WIDTH_PX, 3], dtype=np.uint8)
    # for xy in cbt_xy:
    #     for i in range(0, 4):
    #         cbt_array[HEIGHT_PX - xy['y'] + edge_margin_px, xy['x'] + int((0.72 - 0.25 * i) * PIXELS_PER_METER) + edge_margin_px] = [255, 255, 255]
    # cbt_array[xy['y'] + edge_margin_px, xy['x'] + edge_margin_px] = [255, 255, 255]

    # for xx in range(0, 8):
    #     for yy in range(0, 7):
    #         for xy in driver_standoff_xy:
    #             cbt_array[HEIGHT_PX - (xy['y'] + edge_margin_px * 3 + int(xx * 0.04 * PIXELS_PER_METER)), WIDTH_PX - (
    #                         xy['x'] + edge_margin_px * 3 + int(yy * 0.03 * PIXELS_PER_METER))] = [255, 255, 255]

    # for xy in driver_standoff_xy:
    #     cbt_array[xy['y'] + edge_margin_px, xy['x'] + edge_margin_px] = [255, 255, 255]

    # cbt_spines_xy = []
    # for xy in cbt_spines_xy:
    #     cbt_array[xy['y'] + int(0.2 * PIXELS_PER_METER), xy['x'] + edge_margin_px] = [255, 255, 255]

    x_offset = 0.0
    y_offset = 0.0
    x_offset_steps = 0
    y_offset_steps = 0

    # for spine in cbt_spines:
    for n in range(0, NUM_DRIVERS + 2):
        for xy in cbt_spine:
            cbt_array[xy['y'] + edge_margin_px * 3 + int(y_offset * PIXELS_PER_METER), xy['x'] + edge_margin_px * 3 + int(
                x_offset * PIXELS_PER_METER)] = [255, 255, 255]
        x_offset += 0.12
        x_offset_steps += 1
        # if WIDTH_M < x_offset + 0.13 or 0 < y_offset and 0.6 < x_offset + 0.13 or y_offset_steps == 3 and x_offset_steps == 3:
        if WIDTH_M < x_offset + 0.13:
            x_offset = 0.0
            y_offset += 0.26
            x_offset_steps = 0
            y_offset_steps += 1

    img = Image.fromarray(cbt_array, 'RGB')
    img.save('cbt_spine_array.png')
    img.show()
