import math

import numpy as np
from PIL import Image


def calculate_degrees_across_radi(c, radius):
    return math.degrees(
        math.acos(1 - ((c * c) / (2 * radius * CBT_OUTER_RADIUS))))


# Speaker constants
# Unless specified constants are in meters
WIDTH_M = 1.5
HEIGHT_M = 1.25

CBT_OUTER_RADIUS = 1.4

CBT_UPPER_WIDTH = 0.08
CBT_LOWER_WIDTH = 0.22

BOARD_THICKNESS = 0.006

DRIVER_CTC = 6.4 / 100.0
NUM_DRIVERS = 22

DEGREES_BEYOND_LAST_DRIVER = calculate_degrees_across_radi(1.5 / 100.0, CBT_OUTER_RADIUS)

DRIVER_FRONT_CUTOUT_RADIUS = 6.5 / 2.0 / 100.0
DRIVER_FRONT_BEZZLE_THICKNESS = 0.3 / 100.0

DRIVER_CUTOUT_RADIUS = (6.4 - 1.0) / 2.0 / 100.0
DRIVER_CUTOUT_CUTOFF = 3.6 / 100.0

PIXELS_PER_METER = 1000.0

# Derived constants
DEGREES_PER_DRIVER = calculate_degrees_across_radi(DRIVER_CTC, CBT_OUTER_RADIUS)
DEGREES_PER_DRIVER_CUTOFF = calculate_degrees_across_radi(DRIVER_CUTOUT_RADIUS, CBT_OUTER_RADIUS)
DEGREES_PER_DRIVER_BEDDLE_CUTOUT = calculate_degrees_across_radi(DRIVER_FRONT_CUTOUT_RADIUS, CBT_OUTER_RADIUS)

CBT_DEGREE_CUTOFF = DEGREES_PER_DRIVER * NUM_DRIVERS + DEGREES_BEYOND_LAST_DRIVER

# Loop all drivers and calculate x and y coordinates
DRIVERS_XY = []
for n in range(1, NUM_DRIVERS + 1):
    driver_deg = (n - 0.5) * DEGREES_PER_DRIVER

    driver_center_x = math.cos(math.radians(driver_deg)) * CBT_OUTER_RADIUS
    driver_center_y = math.sin(math.radians(driver_deg)) * CBT_OUTER_RADIUS

    DRIVERS_XY.append({'x': driver_center_x, 'y': driver_center_y, 'r': driver_deg})

DRIVERS_CROSSOVERS = []
for n in range(1, NUM_DRIVERS + 2):
    driver_deg = (n - 1) * DEGREES_PER_DRIVER

    driver_center_x = math.cos(math.radians(driver_deg)) * CBT_OUTER_RADIUS
    driver_center_y = math.sin(math.radians(driver_deg)) * CBT_OUTER_RADIUS

    DRIVERS_CROSSOVERS.append({'x': driver_center_x, 'y': driver_center_y, 'r': driver_deg})

# Pixel normalized constants
WIDTH_PX = int(WIDTH_M * PIXELS_PER_METER)
HEIGHT_PX = int(HEIGHT_M * PIXELS_PER_METER)


def calculate_width(r):
    return CBT_LOWER_WIDTH - (r / CBT_DEGREE_CUTOFF) * (CBT_LOWER_WIDTH - CBT_UPPER_WIDTH)


import unittest

tc = unittest.TestCase('__init__')
tc.assertAlmostEqual(calculate_width(0), CBT_LOWER_WIDTH)
tc.assertAlmostEqual(calculate_width(CBT_DEGREE_CUTOFF), CBT_UPPER_WIDTH)


def is_cutout(x_px, y_px):
    x = float(x_px) / PIXELS_PER_METER
    y = float(y_px) / PIXELS_PER_METER

    r = math.degrees(math.atan(y / x))
    c = math.sqrt(x * x + y * y)

    r_width = calculate_width(r)

    # if x == y:
    #     print(f"x = {x}, y = {y}, r = {r}, c = {c}")

    # Remove outer radius
    if CBT_OUTER_RADIUS < c:
        return True

    # Remove inner radius, more on bottom
    if c < CBT_OUTER_RADIUS - r_width:
        return True

    if CBT_OUTER_RADIUS - 0.025 < c:
        return True

    last_driver_relative_r = r - DRIVERS_CROSSOVERS[-1]['r']
    if 0 < last_driver_relative_r and calculate_degrees_across_radi(BOARD_THICKNESS * 2.25, c) + calculate_degrees_across_radi(BOARD_THICKNESS * 0.5, c) < last_driver_relative_r:
        return True

    if CBT_OUTER_RADIUS - 0.04 < c:

        for driver in DRIVERS_CROSSOVERS:

            driver_r = driver['r'] + calculate_degrees_across_radi(BOARD_THICKNESS * 2.25, c)

            relative_r = abs(r - driver_r)

            if relative_r < calculate_degrees_across_radi(BOARD_THICKNESS * 0.5, c):
                return True

            if relative_r < calculate_degrees_across_radi(BOARD_THICKNESS * 1.5, c):
                return False

        return True

    # Loop all drivers and cut ellipses around them
    # if not c < CBT_OUTER_RADIUS - DRIVER_CUTOUT_CUTOFF:
    #
    #     for driver in DRIVERS_XY:
    #         driver_x = driver['x']
    #         driver_y = driver['y']
    #         driver_r = driver['r']
    #
    #         relative_r = abs(r - driver_r)
    #
    #         if relative_r < DEGREES_PER_DRIVER_BEDDLE_CUTOUT:
    #             if CBT_OUTER_RADIUS - DRIVER_FRONT_BEZZLE_THICKNESS < c:
    #                 return True
    #
    #         if relative_r < DEGREES_PER_DRIVER_CUTOFF:
    #
    #             if CBT_OUTER_RADIUS - DRIVER_CUTOUT_RADIUS * 1.5 * math.pow(
    #                     (1 - (relative_r / DEGREES_PER_DRIVER_CUTOFF)), 1 / 4) < c:
    #                 return True

    return False


cbt_spine_array = np.zeros([HEIGHT_PX, WIDTH_PX, 3], dtype=np.uint8)

edge_margin = int(0.01 * PIXELS_PER_METER)
for x in range(1, WIDTH_PX - edge_margin * 2):
    for y in range(1, HEIGHT_PX - edge_margin * 2):
        if not is_cutout(x + edge_margin, HEIGHT_PX - y - edge_margin):
            cbt_spine_array[y + edge_margin, x + edge_margin] = [255, 255, 255]


img = Image.fromarray(cbt_spine_array, 'RGB')
img.save('cbt_spine_array.png')
img.show()
