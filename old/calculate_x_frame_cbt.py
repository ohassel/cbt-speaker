import math

import numpy as np
from PIL import Image

from util.geometry import is_in_hole
from util.geometry import is_in_triangle
from util.geometry import distance_across_circle

# Speaker constants
# Unless specified constants are in meters
# WIDTH_M = 0.3
# HEIGHT_M = 0.2
# WIDTH_M = 1.5
# HEIGHT_M = 1.5
WIDTH_M = 0.9
HEIGHT_M = 0.9

NUM_DRIVERS = 4

PIXELS_PER_METER = 3779.0 # 999.9 / 1000 of 1m scale in cotter
# PIXELS_PER_METER = 7559.0 # 999.9 / 1000 of 1m scale in cotter
# PIXELS_PER_METER = 1000.0
#
# Derived constants

# Pixel normalized constants
WIDTH_PX = int(WIDTH_M * PIXELS_PER_METER)
HEIGHT_PX = int(HEIGHT_M * PIXELS_PER_METER)

import unittest

tc = unittest.TestCase('__init__')

driver_holes_diameter = 7.2
driver_hole_diameter = 0.33
driver = 6.5
half_driver = driver / 2.0
baffle_width = 20.0
baffle_depth = 14.0
baffle_thickness = 0.3
edge_board_thickness = 0.3
edge_outer_board_thickness = 0.9


# board_thickness = 0.4


def baffle_is_solid(x0, y0, max_cells=NUM_DRIVERS):
    x = x0 * 100.0
    y = y0 * 100.0

    baffle_width_from_middle = 10.0
    x_middle = abs(baffle_width_from_middle - x)

    if max_cells and driver * max_cells < y:
        return False

    y = y % driver

    if half_driver < y:
        y = driver - y

    if is_in_hole(x_middle, y, 0, half_driver, 5.8):
        return False

    for r in [45, 135]:
        x_r = math.cos(math.radians(r)) * driver_holes_diameter / 2.0
        y_r = math.sin(math.radians(r)) * driver_holes_diameter / 2.0

        if is_in_hole(x_middle, y, 0 + x_r, half_driver - y_r, driver_hole_diameter):
            return False

    if y < 1:
        pass
        if baffle_width_from_middle - edge_outer_board_thickness < x_middle:
            return False
    elif y < 1.5:
        if baffle_width_from_middle - edge_outer_board_thickness - edge_board_thickness < x_middle:
            return False
    else:
        if 5 < x_middle:
            return False

    baffle_edge_boop = baffle_width_from_middle - edge_outer_board_thickness - baffle_thickness - 1.0

    if is_in_triangle(x_middle, y, baffle_width_from_middle - edge_outer_board_thickness, 1.5, baffle_edge_boop, 1.5, baffle_edge_boop, 0.6):
        return False

    if 0.6 < y:
        if 6.4 / 2 < x_middle < baffle_edge_boop:
            if not is_in_hole(x_middle, y, 0, half_driver, 6.8):
                return False

    return True


baffle_edge_thickness = 0.9
triangle_x = ((baffle_width - 2 * (0.9 + 0.3) - driver) / 2.0)
# baffle_depth = 2 * triangle_x
stud_width = 2.0 * triangle_x / 7.0

# cbt_radius = 150.0 * 2
cbt_radius = 75.0 * 2

DEGREES_PER_DRIVER = distance_across_circle(driver, cbt_radius)
# DEGREES_BEYOND_LAST_DRIVER = distance_across_circle(1.5, cbt_radius)
DEGREES_BEYOND_LAST_DRIVER = 0

CBT_DEGREE_CUTOFF = DEGREES_PER_DRIVER * NUM_DRIVERS + DEGREES_BEYOND_LAST_DRIVER

# driver_edge_r_offset = 0.9 / 2.0 + distance_across_circle(BOARD_THICKNESS, cbt_radius)
driver_edge_r_offset = 0
# driver_edge_r_offset = distance_across_circle(0.8, cbt_radius)

# Loop all drivers and calculate x and y coordinates
DRIVERS_R = []
for n in range(1, NUM_DRIVERS + 1):
    DRIVERS_R.append((n - 0.5) * DEGREES_PER_DRIVER + driver_edge_r_offset)

DRIVERS_CROSSOVERS_R = []
for n in range(1, NUM_DRIVERS + 2):
    DRIVERS_CROSSOVERS_R.append((n - 1) * DEGREES_PER_DRIVER + driver_edge_r_offset)


def closest(list, v):
    closest = list[0]
    num_driver = 0
    for i in range(0, len(list)):
        if abs(v - list[i]) < abs(v - closest):
            closest = list[i]
            num_driver = i + 1
    return closest, num_driver


def side_is_solid(x0, y0):
    x = x0 * 100.0
    y = y0 * 100.0

    r = math.degrees(math.atan(y / x))
    c = math.sqrt(x * x + y * y)

    if CBT_DEGREE_CUTOFF < r:
        return False

    if baffle_depth / 2 < abs(c - cbt_radius):
        return False

    closest_driver_crossover_r, closest_driver_number = closest(DRIVERS_CROSSOVERS_R, r)

    r_diff = abs(closest_driver_crossover_r - r)
    x_middle = abs(c - cbt_radius)

    if r_diff < distance_across_circle(2.2, c) / 2:
        if x_middle < (baffle_thickness + 0.02) / 2.0:
            return False

    if 0.75 < x_middle < baffle_depth / 2.0 - 2.0:
        if distance_across_circle(1.5, c) / 2 < r_diff or ((closest_driver_number + 1) % 2 != 0 and closest_driver_number not in [0, NUM_DRIVERS + 1]):
            return False

    return True


min_x = int(0.4 * PIXELS_PER_METER)
if __name__ == '__main__':

    picture_array = np.zeros([HEIGHT_PX, WIDTH_PX, 3], dtype=np.uint8)

    for x_px in range(1, int(0.2 * PIXELS_PER_METER)):
        for y_px in range(1, int(1.0 * PIXELS_PER_METER)):
            x = float(x_px) / PIXELS_PER_METER
            y = float(y_px) / PIXELS_PER_METER

            if baffle_is_solid(x, y):
                # driver_baffle_xy.append({'x': x_px, 'y': y_px})
                picture_array[y_px + int(0.05 * PIXELS_PER_METER), x_px + int(0.05 * PIXELS_PER_METER)] = [255, 255,
                                                                                                           255]

    for x_px in range(1, int(0.5 * PIXELS_PER_METER)):
        for y_px in range(1, int(0.8 * PIXELS_PER_METER)):
            x = float(x_px) / PIXELS_PER_METER
            y = float(y_px) / PIXELS_PER_METER

            x += 1.2
            # x += 2.8

            if side_is_solid(x, y):
                picture_array[y_px + int(0.05 * PIXELS_PER_METER), x_px + int(0.4 * PIXELS_PER_METER)] = [255, 255, 255]

    # Draw square
    for x in range(int(0.02*PIXELS_PER_METER), int(0.04*PIXELS_PER_METER)):
        for y in range(int(0.05*PIXELS_PER_METER), int(0.55*PIXELS_PER_METER)):
            picture_array[y, x] = [255, 255, 255]

    # for layer, xy_values in woofer_layer_xy_map.items():
    #     for xy in xy_values:
    #         picture_array[xy['y'] + int((0.05 if layer < 0 else 0.75) * PIXELS_PER_METER), xy['x'] + int((0.1 + ((abs(layer)-1)+0.5)) * PIXELS_PER_METER)] = [255, 255, 255]

    img = Image.fromarray(picture_array, 'RGB')
    img.save('picture_array.png')
    img.show()
