import copy

# list = [
#     1,
#     0.9934200935,
#     0.9785159447,
#     0.9557781509,
#     0.9255380593,
#     0.8879762882,
#     0.843111736,
#     0.7907649399,
#     0.7304787601,
#     0.6613542498,
#     0.5816887676
# ]

# list = [
#     1,
#     0.9852110013,
#     0.9553973618,
#     0.9100158424,
#     0.848033183,
#     0.7675139707,
#     0.6646053058,
#     0.5304288154
# ]

list = [
    1,
    0.9968962356,
    0.9906983402,
    0.9814255506,
    0.9691066474,
    0.9537798654,
    0.9354927755,
    0.9143021364,
    0.8902737189,
    0.8634821016,
    0.8340104393,
    0.8019502051,
    0.7674009065,
    0.7304697762,
    0.6912714399,
    0.6499275599,
    0.6065664579,
    0.5613227165,
    0.5143367617,
    0.4657544268,
    0.4157264998,
    0.3644082558,
    0.3119589744,
    0.2585414459
]

# groups = ['a', 'b']
# groups = ['a', 'b', 'c']
# groups = ['a', 'b', 'c', 'd']
groups = ['a', 'b', 'c', 'd', 'e', 'f']

print(list)

theoretical_best = 0
for v in list:
    theoretical_best += float(v) / len(groups)

r = {}
for g in groups:
    r[g] = []

# def get_min_group():
#     min_group = None
#     for g in groups:
#         if min_group is None or (sum(groups[g]) < sum(groups[min_group])):
#             min_group = g

values_left = {}
for v in list:
    values_left[v] = True

for g in groups:
    sum = 0

    for v in list:
        if values_left[v]:
            new_sum = sum + v
            if new_sum < 3:
                sum = new_sum
                r[g].append(v)
                values_left[v] = False

print(r)

for g, values in r.items():
    sum = 0
    for v in values:
        sum += v
    print(abs(sum - 3))

    print_values = []
    for v in values:
        print_values.append(list.index(v) + 1)
    print(print_values)
    print(values)

print('boop')

# for v in list:


# def calculate_weight_diff(group_map):
#
#     out = 0
#     for group, values in group_map.items():
#         if values:
#             for other_group, other_values in group_map.items():
#                 if group != other_group:
#                     out += abs(sum(values) - sum(other_values))
#
#             # out += abs(theoretical_best - sum(values)) + (max(values) - min(values))*5
#             # out += (max(values) - min(values))*5
#     return out
#
#
# results = []
#
# def boop(current_map, values_left):
#
#     if not values_left:
#         results.append(current_map)
#         return
#
#     v = values_left[0]
#     v_rest = values_left[1:]
#
#     for group in groups:
#         sub_map = copy.deepcopy(current_map)
#         sub_map[group] += [v]
#
#         boop(sub_map, v_rest)
#
#
# map = {}
# for group in groups:
#     map[group] = []
# boop(map, list)
#
# # print(results)
#
# print('abdullah')
#
# best_combination = results[0]
# best_value = calculate_weight_diff(results[0])
# for result in results:
#     value = calculate_weight_diff(result)
#
#     if value < best_value:
#         best_combination = result
#         best_value = value
#
# print(f"best value = {best_value}")
# print(f"best combo = {best_combination}")
