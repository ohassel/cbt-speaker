import math

import numpy as np
from PIL import Image


def calculate_degrees_across_radi(c, radius):
    return math.degrees(
        math.acos(1 - ((c * c) / (2 * radius * CBT_OUTER_RADIUS))))


# Speaker constants
# Unless specified constants are in meters
WIDTH_M = 1.52
HEIGHT_M = 1.52

CBT_OUTER_RADIUS = 1.5

CBT_UPPER_WIDTH = 0.12
CBT_LOWER_WIDTH = 0.32

BOARD_THICKNESS = 0.009

NUM_DRIVERS = 21

DRIVER_HOLE_RADIUS_CM = 7.2 / 2.0
DRIVER_WIDTH_CM = 6.4
DRIVER_CTC = DRIVER_WIDTH_CM / 100.0

DRIVER_DISTANCE_HOLE_TO_HOLE_CM = math.sqrt(2 * DRIVER_HOLE_RADIUS_CM * DRIVER_HOLE_RADIUS_CM)
DRIVER_HOLE_DISTANCE_TO_EDGE_CM = DRIVER_WIDTH_CM / 2.0 - math.sqrt(DRIVER_HOLE_RADIUS_CM * DRIVER_HOLE_RADIUS_CM - math.pow(DRIVER_DISTANCE_HOLE_TO_HOLE_CM / 2, 2))

DEGREES_BEYOND_LAST_DRIVER = calculate_degrees_across_radi(1.5 / 100.0, CBT_OUTER_RADIUS)

DRIVER_FRONT_CUTOUT_RADIUS = 6.5 / 2.0 / 100.0
# DRIVER_FRONT_BEZZLE_THICKNESS = 0.3 / 100.0

DRIVER_CUTOUT_RADIUS = 6.4 / 100.0
DRIVER_CUTOUT_CUTOFF = 3.6 / 100.0

# PIXELS_PER_METER = 3778.0 # 999.9 / 1000 of 1m scale in cotter
PIXELS_PER_METER = 1000.0

# Derived constants
DEGREES_PER_DRIVER = calculate_degrees_across_radi(DRIVER_HOLE_RADIUS_CM * 2.0 / 100.0, CBT_OUTER_RADIUS)
DEGREES_PER_DRIVER_CUTOFF = calculate_degrees_across_radi(DRIVER_CUTOUT_RADIUS, CBT_OUTER_RADIUS)
# DEGREES_PER_DRIVER_BEZZLE_CUTOUT = calculate_degrees_across_radi(DRIVER_FRONT_CUTOUT_RADIUS, CBT_OUTER_RADIUS)

CBT_DEGREE_CUTOFF = DEGREES_PER_DRIVER * NUM_DRIVERS + DEGREES_BEYOND_LAST_DRIVER

# Loop all drivers and calculate x and y coordinates
DRIVERS_R = []
for n in range(1, NUM_DRIVERS + 1):
    DRIVERS_R.append((n - 0.5) * DEGREES_PER_DRIVER)

DRIVERS_CROSSOVERS_R = []
for n in range(1, NUM_DRIVERS + 2):
    DRIVERS_CROSSOVERS_R.append((n - 1) * DEGREES_PER_DRIVER)

# Pixel normalized constants
WIDTH_PX = int(WIDTH_M * PIXELS_PER_METER)
HEIGHT_PX = int(HEIGHT_M * PIXELS_PER_METER)


def calculate_width(r):
    return CBT_LOWER_WIDTH - (r / CBT_DEGREE_CUTOFF) * (CBT_LOWER_WIDTH - CBT_UPPER_WIDTH)


import unittest

tc = unittest.TestCase('__init__')
tc.assertAlmostEqual(calculate_width(0), CBT_LOWER_WIDTH)
tc.assertAlmostEqual(calculate_width(CBT_DEGREE_CUTOFF), CBT_UPPER_WIDTH)


def is_in_hole(x, y, hole_x, hole_y, hole_diameter):
    hole_x_diff = abs(hole_x - x)
    hole_y_diff = abs(hole_y - y)

    hole_c_diff = math.sqrt(math.pow(hole_x_diff, 2) + math.pow(hole_y_diff, 2))

    if hole_c_diff < hole_diameter / 2.0:
        return True


def cbt_is_cutout(x_px, y_px):
    x = float(x_px) / PIXELS_PER_METER
    y = float(y_px) / PIXELS_PER_METER

    r = math.degrees(math.atan(y / x))
    c = math.sqrt(x * x + y * y)

    r_width = calculate_width(r)

    # if x == y:
    #     print(f"x = {x}, y = {y}, r = {r}, c = {c}")

    # Remove outer radius
    if CBT_OUTER_RADIUS < c:
        return True

    # Remove inner radius, more on bottom
    if c < CBT_OUTER_RADIUS - r_width:
        return True

    driver_edge_r_offset = calculate_degrees_across_radi(0.01, CBT_OUTER_RADIUS)
    last_driver_r_offset = DRIVERS_R[-1] + DEGREES_PER_DRIVER * 0.5 + driver_edge_r_offset * 2 + calculate_degrees_across_radi(BOARD_THICKNESS, CBT_OUTER_RADIUS)

    if last_driver_r_offset < r:
        return True

    for driver_r in DRIVERS_R:

        driver_r_offset = driver_r + driver_edge_r_offset

        r_diff = abs(r - driver_r_offset)
        r_diff_normalized = r_diff / (DEGREES_PER_DRIVER_CUTOFF / 2.0)

        if r_diff_normalized < 1.0 and CBT_OUTER_RADIUS - 0.04 * (1.0 - math.pow(r_diff_normalized, 4)) < c:
            return True

    # if CBT_OUTER_RADIUS - 0.025 < c:
    #     return True

    # Plug holes
    for driver_r in DRIVERS_R:
        hole_c = CBT_OUTER_RADIUS - 0.08
        hole_x = math.cos(math.radians(driver_r + driver_edge_r_offset)) * hole_c
        hole_y = math.sin(math.radians(driver_r + driver_edge_r_offset)) * hole_c

        if is_in_hole(x, y, hole_x, hole_y, 0.8 / 100.0):
            return True

    # last_driver_relative_r = r - DRIVERS_CROSSOVERS_R[-1]
    # if 0 < last_driver_relative_r and calculate_degrees_across_radi(BOARD_THICKNESS * 0.5,
    #                                                                 c) + calculate_degrees_across_radi(
    #     BOARD_THICKNESS * 0.5, c) < last_driver_relative_r:
    #     return True

    # if CBT_OUTER_RADIUS - 0.04 < c:
    #
    #     for driver_r in DRIVERS_CROSSOVERS_R:
    #
    #         driver_r = driver_r + calculate_degrees_across_radi(BOARD_THICKNESS * 0.5, c)
    #
    #         relative_r = abs(r - driver_r)
    #
    #         if relative_r < calculate_degrees_across_radi(BOARD_THICKNESS * 0.5, c):
    #             return True
    #
    #         if relative_r < calculate_degrees_across_radi(BOARD_THICKNESS * 1.5, c):
    #             return False
    #
    #     return True

    return False


tc.assertAlmostEqual(calculate_width(CBT_DEGREE_CUTOFF), CBT_UPPER_WIDTH)


def calculate_cbt_parallel(x):
    result = []
    max_y = int(1.3 * PIXELS_PER_METER)
    for y in range(1, max_y):
        if not cbt_is_cutout(x, max_y - y):
            result.append({'x': x - min_x, 'y': y})
    return result


min_x = int(0.4 * PIXELS_PER_METER)
if __name__ == '__main__':

    import multiprocessing

    pool = multiprocessing.Pool(10)

    cbt_xy = []
    for l in pool.map(calculate_cbt_parallel, range(min_x, int(1.55 * PIXELS_PER_METER))):
        cbt_xy += l

    pool.close()

    edge_margin = 0.01
    edge_margin_px = int(edge_margin * PIXELS_PER_METER)

    cbt_array = np.zeros([HEIGHT_PX, WIDTH_PX, 3], dtype=np.uint8)

    # # Draw square
    # for x in range(int(0.25*PIXELS_PER_METER), int(1.25*PIXELS_PER_METER)):
    #     for y in range(int(0.25*PIXELS_PER_METER), int(1.25*PIXELS_PER_METER)):
    #         cbt_array[y, x] = [255, 255, 255]

    for xy in cbt_xy:
        for i in range(0, 2):
            cbt_array[xy['y'] + int((0.15) * PIXELS_PER_METER), xy['x'] + int((0.2 - (CBT_LOWER_WIDTH + 0.04) * i) * PIXELS_PER_METER)] = [255, 255, 255]

    img = Image.fromarray(cbt_array, 'RGB')
    img.save('cbt_spine_array.png')
    img.show()
