import math

import numpy as np
from PIL import Image


def distance_across_circle(distance_across_circle, c):
    return math.degrees(math.acos(1 - ((distance_across_circle * distance_across_circle) / (2 * c * c))))


# Speaker constants
# Unless specified constants are in meters
# WIDTH_M = 0.3
# HEIGHT_M = 0.2
WIDTH_M = 1.52
HEIGHT_M = 1.52

CBT_OUTER_RADIUS = 144

CBT_UPPER_WIDTH = 8
CBT_LOWER_WIDTH = 24

SPINE_UPPER_LENGTH = 5 * 4 / 3
SPINE_LOWER_LENGTH = 15 * 4 / 3

SPINE_UPPER_WIDTH = 10.0
SPINE_LOWER_WIDTH = 30.0

BOARD_THICKNESS = 0.65
SPINES_BOARD_THICKNESS = BOARD_THICKNESS
STANDOFF_BOARD_THICKNESS = BOARD_THICKNESS

NUM_DRIVERS = 22

# DRIVER_HOLE_RADIUS_CM = 7.2 / 2.0
DRIVER_WIDTH_CM = 6.6

# DRIVER_DISTANCE_HOLE_TO_HOLE_CM = math.sqrt(2 * DRIVER_HOLE_RADIUS_CM * DRIVER_HOLE_RADIUS_CM)
# DRIVER_HOLE_DISTANCE_TO_EDGE_CM = DRIVER_WIDTH_CM / 2.0 - math.sqrt(DRIVER_HOLE_RADIUS_CM * DRIVER_HOLE_RADIUS_CM - math.pow(DRIVER_DISTANCE_HOLE_TO_HOLE_CM / 2, 2))

DEGREES_BEYOND_LAST_DRIVER = distance_across_circle(1.5, CBT_OUTER_RADIUS)

DRIVER_FRONT_CUTOUT_RADIUS = 6.5 / 2.0 / 100.0
# DRIVER_FRONT_BEZZLE_THICKNESS = 0.3 / 100.0

PIXELS_PER_METER = 3778.0*1 # 999.9 / 1000 of 1m scale in cotter
# PIXELS_PER_METER = 1000.0
# 
# Derived constants
DEGREES_PER_DRIVER = distance_across_circle(DRIVER_WIDTH_CM, CBT_OUTER_RADIUS)

CBT_DEGREE_CUTOFF = DEGREES_PER_DRIVER * NUM_DRIVERS + DEGREES_BEYOND_LAST_DRIVER

spine_thickness = distance_across_circle(SPINES_BOARD_THICKNESS, CBT_OUTER_RADIUS)
driver_edge_r_offset = spine_thickness / 2.0 + distance_across_circle(BOARD_THICKNESS, CBT_OUTER_RADIUS)

# Loop all drivers and calculate x and y coordinates
DRIVERS_R = []
for n in range(1, NUM_DRIVERS + 1):
    DRIVERS_R.append((n - 0.5) * DEGREES_PER_DRIVER + driver_edge_r_offset)

DRIVERS_CROSSOVERS_R = []
for n in range(1, NUM_DRIVERS + 2):
    DRIVERS_CROSSOVERS_R.append((n - 1) * DEGREES_PER_DRIVER + driver_edge_r_offset)

# first_driver_r_offset = DRIVERS_CROSSOVERS_R[0] + driver_edge_r_offset
# last_driver_r_offset = DRIVERS_CROSSOVERS_R[-1] + driver_edge_r_offset
first_driver_r_offset = DRIVERS_CROSSOVERS_R[0]
last_driver_r_offset = DRIVERS_CROSSOVERS_R[-1]

# Pixel normalized constants
WIDTH_PX = int(WIDTH_M * PIXELS_PER_METER)
HEIGHT_PX = int(HEIGHT_M * PIXELS_PER_METER)


def calculate_spine_length(r):
    return calculate_width(r)
    # max_degrees = last_driver_r_offset - first_driver_r_offset
    # return SPINE_UPPER_LENGTH + (SPINE_LOWER_LENGTH - SPINE_UPPER_LENGTH) * (max_degrees - r + first_driver_r_offset) / max_degrees


def calculate_spine_width(r):
    max_degrees = last_driver_r_offset - first_driver_r_offset
    return SPINE_UPPER_WIDTH + (SPINE_LOWER_WIDTH - SPINE_UPPER_WIDTH) * (max_degrees - r + first_driver_r_offset) / max_degrees


def calculate_width(r):
    return CBT_LOWER_WIDTH - (r / CBT_DEGREE_CUTOFF) * (CBT_LOWER_WIDTH - CBT_UPPER_WIDTH)


import unittest

tc = unittest.TestCase('__init__')
tc.assertAlmostEqual(calculate_width(0), CBT_LOWER_WIDTH)
tc.assertAlmostEqual(calculate_width(CBT_DEGREE_CUTOFF), CBT_UPPER_WIDTH)

tc.assertAlmostEqual(calculate_spine_width(first_driver_r_offset), SPINE_LOWER_WIDTH)
tc.assertAlmostEqual(calculate_spine_width(last_driver_r_offset), SPINE_UPPER_WIDTH)


def is_in_hole(x, y, hole_x, hole_y, hole_diameter):
    hole_x_diff = abs(hole_x - x)
    hole_y_diff = abs(hole_y - y)

    hole_c_diff = math.sqrt(math.pow(hole_x_diff, 2) + math.pow(hole_y_diff, 2))

    if hole_c_diff < hole_diameter / 2.0:
        return True


def cbt_is_cutout(x_px, y_px, middle_layer=True):
    x = float(x_px) / PIXELS_PER_METER * 100.0
    y = float(y_px) / PIXELS_PER_METER * 100.0

    r = math.degrees(math.atan(y / x))
    c = math.sqrt(x * x + y * y)

    r_width = calculate_width(r)

    # if x == y:
    #     print(f"x = {x}, y = {y}, r = {r}, c = {c}")

    # Remove outer radius
    if CBT_OUTER_RADIUS < c:
        return True

    # Remove inner radius, more on bottom
    if c < CBT_OUTER_RADIUS - r_width:
        return True

    if not middle_layer:
        if CBT_OUTER_RADIUS - 5.0 <= c and last_driver_r_offset <= r:
            return True

        if CBT_OUTER_RADIUS - 5.0 <= c and r <= first_driver_r_offset:
            return True

    # if last_driver_r_offset < r:
    # if last_driver_r_offset + spine_thickness + distance_across_circle(2 * BOARD_THICKNESS, CBT_OUTER_RADIUS) < r:
    if last_driver_r_offset + spine_thickness + distance_across_circle(BOARD_THICKNESS, CBT_OUTER_RADIUS) / 2.0 < r:
        return True

    # Remove outer radius close to driver
    if CBT_OUTER_RADIUS - 3.5 < c:

        if CBT_OUTER_RADIUS - 2.5 < c:
            return True

        for driver_r in DRIVERS_CROSSOVERS_R:

            r_diff = abs(r - driver_r)

            if not middle_layer and r_diff < distance_across_circle(SPINES_BOARD_THICKNESS, c) / 2.0:
                return True

            # if r_diff < distance_across_circle(1.8, c) / 2.0:
            if r_diff < distance_across_circle(BOARD_THICKNESS, c) / 2.0:
                return False

            c_diff = abs(abs(CBT_OUTER_RADIUS - 3.5) - c) / 3.5
            if c_diff + (r_diff + distance_across_circle(BOARD_THICKNESS, c) / 2.0) * 0.80 < 0.5:
                return False

        return True

    if not middle_layer and CBT_OUTER_RADIUS - 5 < c:
        for driver_r in DRIVERS_CROSSOVERS_R:

            r_diff = abs(r - driver_r)

            if r_diff < distance_across_circle(SPINES_BOARD_THICKNESS, c) / 2.0:
                return True

    # Plug holes
    i = -4

    for driver_r in DRIVERS_R:
        i += 1
        if i % 6 != 0:
            continue

        hole_c = CBT_OUTER_RADIUS - calculate_width(r) / 2 - 1.5
        hole_x = math.cos(math.radians(driver_r)) * hole_c
        hole_y = math.sin(math.radians(driver_r)) * hole_c

        if is_in_hole(x, y, hole_x, hole_y, 0.8):
            return True

    x_from_inner_radius = abs(abs(CBT_OUTER_RADIUS - calculate_width(0)) - x)

    if calculate_width(first_driver_r_offset) / 2 - 2 < x_from_inner_radius < calculate_width(first_driver_r_offset) / 2 and y < 2 * BOARD_THICKNESS:
        return True

    # Bottom screw cutout
    # inner_width_x_diff = abs(x - (CBT_OUTER_RADIUS - CBT_LOWER_WIDTH))
    #
    # if 3 < inner_width_x_diff < 3 + 4 and 2 < y < 2 + BOARD_THICKNESS * 2:
    #     return True
    #
    # if middle_layer:
    #     for i in range(0, 2):
    #         boop = 3 + i * 2 + 1
    #         if y <= 2 and boop - 0.3 <= inner_width_x_diff <= boop + 0.3:
    #             return True

    return False


tc.assertAlmostEqual(calculate_width(CBT_DEGREE_CUTOFF), CBT_UPPER_WIDTH)

STANDOFF_LENGTH = 1.7


def driver_standoff_is_solid(x0, y0):
    x = x0 * 100.0
    y = y0 * 100.0

    x_middle = abs(2.0 - x)

    if 2.0 < x_middle + y:
        return False

    if x_middle < SPINES_BOARD_THICKNESS / 2.0:
        if 0.5 < y < 1.1:
            return False

    if STANDOFF_LENGTH < y:
        return False

    if not (STANDOFF_LENGTH - y) + x_middle < STANDOFF_LENGTH + 0.8:
        return False

    return True


def driver_spine_is_solid(x0, y0, mini=False, r=None, inner_edge=False, outer_edge=False, solid=False, bottom=False):
    x = x0 * 100.0
    y = y0 * 100.0

    curved = True

    if mini:
        width = DRIVER_WIDTH_CM
        length = 5
        curved = False
    else:
        width = calculate_spine_width(r)
        length = calculate_spine_length(r)

    original_length = length

    standoff_thickness = STANDOFF_BOARD_THICKNESS

    curve_radius_y = length * 1.4
    curve_radius = math.sqrt(math.pow(DRIVER_WIDTH_CM / 2, 2) + curve_radius_y * curve_radius_y)
    curve_y = curve_radius_y - 0.25 - math.sqrt(curve_radius * curve_radius - math.pow(width / 2, 2))

    width_from_middle = width / 2.0
    x_middle = abs(width_from_middle - x)

    if not outer_edge:
        if y < standoff_thickness:
            if DRIVER_WIDTH_CM / 2 - STANDOFF_LENGTH < x_middle < DRIVER_WIDTH_CM / 2:
                # if inner_edge or not DRIVER_WIDTH_CM / 2 - 1.1 < x_middle < DRIVER_WIDTH_CM / 2 - 0.5:
                if not DRIVER_WIDTH_CM / 2 - 1.1 < x_middle < DRIVER_WIDTH_CM / 2 - 0.5:
                    return False

        if x_middle < 1.0 and y < 0.1:
            return False

    if bottom and outer_edge:
        length = 60

    # triangle
    max_triangle_x = width_from_middle - 0.5 - BOARD_THICKNESS * 3 / 2
    max_triangle_y = length - 1.0 - (curve_y if curved else 0.0)

    if mini:
        max_triangle_x += BOARD_THICKNESS

    y_triangle_rel_x = max_triangle_y / max_triangle_x
    triangle_c = length + BOARD_THICKNESS * 3 / 2

    if bottom and outer_edge:

        if 5 < x_middle and curve_y < y:
            boop = 180
            if not is_in_hole(x, y, boop+0.5, curve_y, 2*boop):
                return False
            if not is_in_hole(abs(width - x), y, boop+0.5, curve_y, 2*boop):
                return False
    else:
        if bottom and inner_edge:
            # triangle_c += math.sqrt(2*BOARD_THICKNESS*BOARD_THICKNESS)
            pass

        if triangle_c - y_triangle_rel_x * x_middle < y:
            return False

    if width_from_middle <= x_middle:
        return False

    if length <= y:
        return False

    if mini:
        pass
    else:
        edge_y = y - curve_y if curved else y
        edge_x_diff = abs(width_from_middle - x_middle)
        is_in_edge_hole = is_in_hole(width_from_middle - x_middle, edge_y, 0.75, 0.25, 1.0)
        if outer_edge:
            if width_from_middle - x_middle < 0.5:
                if not is_in_edge_hole:
                    return False
        else:
            if edge_x_diff < 0.5:
                return False
            if is_in_edge_hole:
                return False

        # if not inner_edge and not outer_edge:
        if (inner_edge or outer_edge) and is_in_hole(x_middle, edge_y, width / 6, original_length / 4, 0.8):
            return False

        if bottom:
            if is_in_hole(x_middle, y, 0, 50, 0.8):
                return False

            for hole_y in [6, 11, 15, 18, 21]:
                if is_in_hole(x_middle, y, 0, hole_y, 0.6):
                    return False



        if not inner_edge and not outer_edge:

            margin = 2

            parallell_triangle_c_offset = math.sqrt(2*margin*margin)
            distance_from_curve_y = math.sqrt(math.pow(x_middle, 2) + math.pow(abs(curve_radius_y - y), 2))

            if margin < y and distance_from_curve_y < curve_radius - margin and margin < x_middle:
                if x_middle < width_from_middle - margin:
                    if not triangle_c - parallell_triangle_c_offset - y_triangle_rel_x * x_middle < y:
                        return False

        if curved and DRIVER_WIDTH_CM / 2 < x_middle and y < curve_y:

            if not is_in_hole(x_middle, y, 0, curve_radius_y, 2 * curve_radius):
                return False

    # inner groove
    if not solid and not original_length < y:
        if not bottom or not original_length / 2 < y < original_length / 2 + 2:
            if x_middle < BOARD_THICKNESS / 2.0 and 2.5 < y:
                return False

            if x_middle < BOARD_THICKNESS * 3 / 2 and 5 < y:
                return False

    return True


def calculate_cbt_parallel_outer(x, middle_layer=False):
    return calculate_cbt_parallel(x, middle_layer)


def calculate_cbt_parallel_middle(x, middle_layer=True):
    return calculate_cbt_parallel(x, middle_layer)


def calculate_cbt_parallel(x, middle_layer=True):
    result = []
    max_y = int(1.3 * PIXELS_PER_METER)
    for y in range(1, max_y):
        if not cbt_is_cutout(x, max_y - y, middle_layer=middle_layer):
            result.append({'x': x - min_x, 'y': y})
    return result


def calculate_spine(mini=False, r=None, inner_edge=False, outer_edge=False, solid=False, bottom=False):
    driver_mini_spines_xy = []

    for x_px in range(1, int(0.4 * PIXELS_PER_METER)):
        for y_px in range(1, int((0.7 if bottom else 0.3) * PIXELS_PER_METER)):
            x = float(x_px) / PIXELS_PER_METER
            y = float(y_px) / PIXELS_PER_METER

            if driver_spine_is_solid(x, y, mini=mini, r=r, inner_edge=inner_edge, outer_edge=outer_edge, solid=solid, bottom=bottom):
                driver_mini_spines_xy.append({'x': x_px, 'y': y_px})

    return driver_mini_spines_xy


min_x = int(0.4 * PIXELS_PER_METER)
if __name__ == '__main__':

    import multiprocessing

    pool = multiprocessing.Pool(10)

    cbt_middle_xy = []
    for l in pool.map(calculate_cbt_parallel_middle, range(min_x, int(1.55 * PIXELS_PER_METER))):
        cbt_middle_xy += l

    cbt_outer_xy = []
    for l in pool.map(calculate_cbt_parallel_outer, range(min_x, int(1.55 * PIXELS_PER_METER))):
        cbt_outer_xy += l

    pool.close()

    driver_standoff_xy = []

    for x_px in range(1, int(0.1 * PIXELS_PER_METER)):
        for y_px in range(1, int(0.1 * PIXELS_PER_METER)):
            x = float(x_px) / PIXELS_PER_METER
            y = float(y_px) / PIXELS_PER_METER

            if driver_standoff_is_solid(x, y):
                driver_standoff_xy.append({'x': x_px, 'y': y_px})

    edge_margin = 0.01
    edge_margin_px = int(edge_margin * PIXELS_PER_METER)

    cbt_array = np.zeros([HEIGHT_PX, WIDTH_PX, 3], dtype=np.uint8)

    # # Draw square
    # for x in range(int(0.25*PIXELS_PER_METER), int(1.25*PIXELS_PER_METER)):
    #     for y in range(int(0.25*PIXELS_PER_METER), int(1.25*PIXELS_PER_METER)):
    #         cbt_array[y, x] = [255, 255, 255]

    for xy in cbt_middle_xy:
        # if int((0.99) * PIXELS_PER_METER) < xy['x'] < int((1.08) * PIXELS_PER_METER) \
        #         and int((1.01) * PIXELS_PER_METER) < xy['y'] < int((1.19) * PIXELS_PER_METER):
        cbt_array[xy['y'] + int((0.15) * PIXELS_PER_METER), xy['x'] + int(0.4 * PIXELS_PER_METER)] = [255, 255, 255]

    for xy in cbt_outer_xy:
        # if int((0.99) * PIXELS_PER_METER) < xy['x'] < int((1.08) * PIXELS_PER_METER) \
        #         and int((1.01) * PIXELS_PER_METER) < xy['y'] < int((1.19) * PIXELS_PER_METER):
        cbt_array[xy['y'] + int((0.15) * PIXELS_PER_METER), xy['x'] + int(0.05 * PIXELS_PER_METER)] = [255, 255, 255]

    for xy in driver_standoff_xy:
        cbt_array[xy['y'] + int((0.05) * PIXELS_PER_METER), xy['x'] + int((0.6) * PIXELS_PER_METER)] = [255, 255, 255]

    for xy in calculate_spine(mini=True):
        cbt_array[xy['y'] + int((0.05) * PIXELS_PER_METER), xy['x'] + int((0.7) * PIXELS_PER_METER)] = [255, 255, 255]

    for xy in calculate_spine(r=first_driver_r_offset, outer_edge=True, bottom=True, solid=True):
        cbt_array[xy['y'] + int((0.7) * PIXELS_PER_METER), xy['x'] + int((0.3) * PIXELS_PER_METER)] = [255, 255, 255]

    for xy in calculate_spine(r=first_driver_r_offset, outer_edge=True, bottom=True):
        cbt_array[xy['y'] + int((0.05) * PIXELS_PER_METER), xy['x'] + int((1.15) * PIXELS_PER_METER)] = [255, 255, 255]

    for xy in calculate_spine(r=first_driver_r_offset, inner_edge=True, bottom=True):
        cbt_array[xy['y'] + int((0.05) * PIXELS_PER_METER), xy['x'] + int((0.8) * PIXELS_PER_METER)] = [255, 255, 255]

    for xy in calculate_spine(r=last_driver_r_offset, outer_edge=True, solid=True):
        cbt_array[xy['y'] + int((0.05) * PIXELS_PER_METER), xy['x'] + int((0.4) * PIXELS_PER_METER)] = [255, 255, 255]

    for xy in calculate_spine(r=last_driver_r_offset, inner_edge=True):
        cbt_array[xy['y'] + int((0.15) * PIXELS_PER_METER), xy['x'] + int((0.55) * PIXELS_PER_METER)] = [255, 255, 255]

    tc.assertAlmostEqual(first_driver_r_offset, DRIVERS_CROSSOVERS_R[0])
    tc.assertAlmostEqual(last_driver_r_offset, DRIVERS_CROSSOVERS_R[22])

    # for i in range(0, 7):
    index = 0
    # for i in [5, 10, 14, 18]:
    # for i in [4, 8, 12, 16, 18, 20]:
    for i in [3, 6, 9, 12, 15, 18, 20]:
        # for i in range(0, 3):

        # for xy in calculate_spine(r=DRIVERS_CROSSOVERS_R[3 + i * 3]):
        for xy in calculate_spine(r=DRIVERS_CROSSOVERS_R[i]):
            cbt_array[xy['y'] + int((0.05 + index * 0.22) * PIXELS_PER_METER), xy['x'] + int((0.05) * PIXELS_PER_METER)] = [255, 255, 255]
        index += 1

    img = Image.fromarray(cbt_array, 'RGB')
    img.save('cbt_spine_array.png')
    img.show()
