import math

import numpy as np
from PIL import Image


def calculate_degrees_across_radi(c, radius):
    return math.degrees(
        math.acos(1 - ((c * c) / (2 * radius * CBT_OUTER_RADIUS))))


# Speaker constants
# Unless specified constants are in meters
WIDTH_M = 1.52
HEIGHT_M = 1.52

CBT_OUTER_RADIUS = 1.4

CBT_UPPER_WIDTH = 0.08
CBT_LOWER_WIDTH = 0.22

BOARD_THICKNESS = 0.006

DRIVER_CTC = 6.4 / 100.0
NUM_DRIVERS = 22

DEGREES_BEYOND_LAST_DRIVER = calculate_degrees_across_radi(1.5 / 100.0, CBT_OUTER_RADIUS)

DRIVER_FRONT_CUTOUT_RADIUS = 6.5 / 2.0 / 100.0
DRIVER_FRONT_BEZZLE_THICKNESS = 0.3 / 100.0

DRIVER_CUTOUT_RADIUS = (6.4 - 1.0) / 2.0 / 100.0
DRIVER_CUTOUT_CUTOFF = 3.6 / 100.0

PIXELS_PER_METER = 1000.0

# Derived constants
DEGREES_PER_DRIVER = calculate_degrees_across_radi(DRIVER_CTC, CBT_OUTER_RADIUS)
DEGREES_PER_DRIVER_CUTOFF = calculate_degrees_across_radi(DRIVER_CUTOUT_RADIUS, CBT_OUTER_RADIUS)
DEGREES_PER_DRIVER_BEDDLE_CUTOUT = calculate_degrees_across_radi(DRIVER_FRONT_CUTOUT_RADIUS, CBT_OUTER_RADIUS)

CBT_DEGREE_CUTOFF = DEGREES_PER_DRIVER * NUM_DRIVERS + DEGREES_BEYOND_LAST_DRIVER

# Loop all drivers and calculate x and y coordinates
DRIVERS_XY = []
for n in range(1, NUM_DRIVERS + 1):
    driver_deg = (n - 0.5) * DEGREES_PER_DRIVER

    driver_center_x = math.cos(math.radians(driver_deg)) * CBT_OUTER_RADIUS
    driver_center_y = math.sin(math.radians(driver_deg)) * CBT_OUTER_RADIUS

    DRIVERS_XY.append({'x': driver_center_x, 'y': driver_center_y, 'r': driver_deg})

DRIVERS_CROSSOVERS = []
for n in range(1, NUM_DRIVERS + 2):
    driver_deg = (n - 1) * DEGREES_PER_DRIVER

    driver_center_x = math.cos(math.radians(driver_deg)) * CBT_OUTER_RADIUS
    driver_center_y = math.sin(math.radians(driver_deg)) * CBT_OUTER_RADIUS

    DRIVERS_CROSSOVERS.append({'x': driver_center_x, 'y': driver_center_y, 'r': driver_deg})

# Pixel normalized constants
WIDTH_PX = int(WIDTH_M * PIXELS_PER_METER)
HEIGHT_PX = int(HEIGHT_M * PIXELS_PER_METER)


def calculate_width(r):
    return CBT_LOWER_WIDTH - (r / CBT_DEGREE_CUTOFF) * (CBT_LOWER_WIDTH - CBT_UPPER_WIDTH)


import unittest

tc = unittest.TestCase('__init__')
tc.assertAlmostEqual(calculate_width(0), CBT_LOWER_WIDTH)
tc.assertAlmostEqual(calculate_width(CBT_DEGREE_CUTOFF), CBT_UPPER_WIDTH)


def cbt_is_cutout(x_px, y_px):
    x = float(x_px) / PIXELS_PER_METER
    y = float(y_px) / PIXELS_PER_METER

    r = math.degrees(math.atan(y / x))
    c = math.sqrt(x * x + y * y)

    r_width = calculate_width(r)

    # if x == y:
    #     print(f"x = {x}, y = {y}, r = {r}, c = {c}")

    # Remove outer radius
    if CBT_OUTER_RADIUS < c:
        return True

    # Remove inner radius, more on bottom
    if c < CBT_OUTER_RADIUS - r_width:
        return True

    if CBT_OUTER_RADIUS - 0.025 < c:
        return True

    last_driver_relative_r = r - DRIVERS_CROSSOVERS[-1]['r']
    if 0 < last_driver_relative_r and calculate_degrees_across_radi(BOARD_THICKNESS * 2.25,
                                                                    c) + calculate_degrees_across_radi(
        BOARD_THICKNESS * 0.5, c) < last_driver_relative_r:
        return True

    if CBT_OUTER_RADIUS - 0.04 < c:

        for driver in DRIVERS_CROSSOVERS:

            driver_r = driver['r'] + calculate_degrees_across_radi(BOARD_THICKNESS * 2.25, c)

            relative_r = abs(r - driver_r)

            if relative_r < calculate_degrees_across_radi(BOARD_THICKNESS * 0.5, c):
                return True

            if relative_r < calculate_degrees_across_radi(BOARD_THICKNESS * 1.5, c):
                return False

        return True
    return False


def driver_standoff_is_solid(x0, y0):
    x = x0 * 100.0
    y = y0 * 100.0

    if 2.0 < y:
        return False

    if 2.5 < x:
        return False

    if x + y < 1.0:
        return False

    if x + (2.0 - y) < 1.0:
        return False

    if (2.5 - x) + y < 1.0:
        return False

    if (2.5 - x) + (2 - y) < 1:
        return False

    if abs(1.25 - x) < BOARD_THICKNESS * 0.5 * 100.0 and y < 0.75:
        return False

    return True


def cbt_spine_is_solid(x0, y0):
    x = x0 * 100.0
    y = y0 * 100.0

    if 2.0 < y:
        return False

    if 2.5 < x:
        return False

    if x + y < 1.0:
        return False

    if x + (2.0 - y) < 1.0:
        return False

    if (2.5 - x) + y < 1.0:
        return False

    if (2.5 - x) + (2 - y) < 1:
        return False

    if abs(1.25 - x) < BOARD_THICKNESS * 0.5 * 100.0 and y < 0.75:
        return False

    return True


margin = 0.001

tc.assertEqual(driver_standoff_is_solid(0.00 + margin, 0.01), True)
# tc.assertEqual(driver_standoff_is_solid(0.01 - margin, 0.00 + 2 * margin), True)
tc.assertEqual(driver_standoff_is_solid(0.01, 0.02 - margin), True)
tc.assertEqual(driver_standoff_is_solid(0.015, 0.02 - margin), True)
# tc.assertEqual(driver_standoff_is_solid(0.015 + margin, 0.00 + 2 * margin), True)
tc.assertEqual(driver_standoff_is_solid(0.025 - margin, 0.01), True)
tc.assertEqual(driver_standoff_is_solid(0.01, 0.01), True)
tc.assertEqual(driver_standoff_is_solid(0.015, 0.01), True)

tc.assertEqual(driver_standoff_is_solid(0.00, 0.00), False)
tc.assertEqual(driver_standoff_is_solid(0.00, 0.02), False)
tc.assertEqual(driver_standoff_is_solid(0.025, 0.00), False)
tc.assertEqual(driver_standoff_is_solid(0.025, 0.02), False)

tc.assertEqual(driver_standoff_is_solid(0.004, 0.004), False)
tc.assertEqual(driver_standoff_is_solid(0.004, 0.016), False)
tc.assertEqual(driver_standoff_is_solid(0.021, 0.004), False)
tc.assertEqual(driver_standoff_is_solid(0.021, 0.016), False)

tc.assertEqual(driver_standoff_is_solid(0.0125, 0.00), False)
tc.assertEqual(driver_standoff_is_solid(0.0125, 0.005), False)

tc.assertAlmostEqual(calculate_width(CBT_DEGREE_CUTOFF), CBT_UPPER_WIDTH)

cbt_xy = []

min_x = int(0.6 * PIXELS_PER_METER)
for x in range(min_x, int(1.4 * PIXELS_PER_METER)):
    max_y = int(1.2 * PIXELS_PER_METER)
    for y in range(1, max_y):
        if not cbt_is_cutout(x, max_y - y):
            cbt_xy.append({'x': x - min_x, 'y': y})
            # cbt_array[y + edge_margin_px, x + edge_margin_px] = [255, 255, 255]

# for x in range(1, WIDTH_PX - edge_margin_px * 2):
#     for y in range(1, HEIGHT_PX - edge_margin_px * 2):
#         if not cbt_is_cutout(x + edge_margin_px, HEIGHT_PX - y - edge_margin_px):
#             cbt_spine_array[y + edge_margin_px, x + edge_margin_px] = [255, 255, 255]


driver_standoff_xy = []

# for x_px in range(1, WIDTH_PX - edge_margin_px * 2):
for x_px in range(1, int(0.1 * PIXELS_PER_METER)):
    for y_px in range(1, int(0.1 * PIXELS_PER_METER)):
        x = float(x_px) / PIXELS_PER_METER
        y = float(y_px) / PIXELS_PER_METER

        if driver_standoff_is_solid(x, y):
            driver_standoff_xy.append({'x': x_px, 'y': y_px})
            # cbt_spine_array[y_px + edge_margin_px, x_px + edge_margin_px] = [255, 255, 255]

cbt_array = np.zeros([HEIGHT_PX, WIDTH_PX, 3], dtype=np.uint8)

edge_margin = 0.01
edge_margin_px = int(edge_margin * PIXELS_PER_METER)

for xy in cbt_xy:
    cbt_array[xy['y'] + edge_margin_px, xy['x'] + edge_margin_px] = [255, 255, 255]

for xy in driver_standoff_xy:
    cbt_array[xy['y'] + edge_margin_px, xy['x'] + edge_margin_px] = [255, 255, 255]

img = Image.fromarray(cbt_array, 'RGB')
img.save('cbt_spine_array.png')
img.show()
