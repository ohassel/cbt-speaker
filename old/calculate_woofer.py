import math

import numpy as np
from PIL import Image

from util.geometry import is_in_hole
from util.geometry import is_in_triangle


def distance_across_circle(distance_across_circle, c):
    return math.degrees(math.acos(1 - ((distance_across_circle * distance_across_circle) / (2 * c * c))))


# Speaker constants
# Unless specified constants are in meters
# WIDTH_M = 0.3
# HEIGHT_M = 0.2
WIDTH_M = 0.92
HEIGHT_M = 0.62

# PIXELS_PER_METER = 3778.0*1 # 999.9 / 1000 of 1m scale in cotter
# PIXELS_PER_METER = 3785.2
PIXELS_PER_METER = 8000.0
# PIXELS_PER_METER = 4000.0
# PIXELS_PER_METER = 2000.0
# PIXELS_PER_METER = 1000.0
#
# Derived constants

# Pixel normalized constants
WIDTH_PX = int(WIDTH_M * PIXELS_PER_METER)
HEIGHT_PX = int(HEIGHT_M * PIXELS_PER_METER)

import unittest

tc = unittest.TestCase('__init__')


def woofer_sheet_is_solid(x0, y0, inner=True, driver_mount_holes_diameter=0.5, driver_flange_cut=False, side_cut=True, magnet_cut=True):
    x = x0 * 100.0
    y = y0 * 100.0

    lower_width = 28.0
    # upper_width = 27.4

    half_width = lower_width / 2

    x_middle = abs(half_width - x)
    y_middle = abs(half_width - y)
    x_edge = half_width - x_middle
    y_edge = half_width - y_middle

    if y < 0:
        return False

    if lower_width / 2 < x_middle:
        return False

    if lower_width < y:
        return False

    side_cut_edge_margin = 3.1

    if side_cut and x < half_width:
        # if is_in_triangle(x, y, x1, y1, x2, y2, x3, y3)
        if is_in_triangle(half_width - x_middle, half_width - y_middle, 0, 1.8, 0, side_cut_edge_margin, 9, side_cut_edge_margin):
            return False
        if side_cut_edge_margin < y_edge:
            return False

    if y_edge < 2:
        for x_hole in [2, 4]:
            if is_in_hole(x_middle, half_width - y_middle, half_width - x_hole, 0.9, 0.4):
                return False
        for x_hole in [6]:
            if is_in_hole(x_middle, half_width - y_middle, half_width - x_hole, 0.9, 0.5):
                return False

    if magnet_cut:
        if is_in_hole(x_middle, y, 0, half_width, 18):
            return False

    if driver_flange_cut:
        if is_in_hole(x_middle, y, 0, half_width, 27):
            return False

    if inner:
        L26RO4Y_diameter = 27
        L26RO4Y_mount_holes_diameter = 25.6
        L26RO4Y_mount_holes_radius = L26RO4Y_mount_holes_diameter / 2.0
        # L26RO4Y_mount_hole_diameter = 0.5
        L26RO4Y_mount_hole_diameter = driver_mount_holes_diameter
        L26RO4Y_cutout_hole_diameter = 23.7

        if is_in_hole(x_middle, y, 0, half_width, L26RO4Y_diameter):
            if is_in_hole(x_middle, y, 0, half_width, L26RO4Y_cutout_hole_diameter):
                return False

            for r in range(0, 360, 45):
                r += 22.5
                x_r = math.cos(math.radians(r)) * L26RO4Y_mount_holes_radius
                y_r = math.sin(math.radians(r)) * L26RO4Y_mount_holes_radius

                if is_in_hole(x_middle, y, 0 + x_r, half_width + y_r, L26RO4Y_mount_hole_diameter):
                    return False

    return True


min_x = int(0.4 * PIXELS_PER_METER)
picture_array = np.zeros([HEIGHT_PX, WIDTH_PX, 3], dtype=np.uint8)

margin = 0.02
margin_px = int((margin) * PIXELS_PER_METER)


def boop(x_offset, y_offset, inner=True, driver_mount_holes_diameter=0.5, driver_flange_cut=False, side_cut=True, magnet_cut=True):
    for x_px in range(1, int(0.3 * PIXELS_PER_METER)):
        for y_px in range(1, int(0.3 * PIXELS_PER_METER)):
            x = float(x_px) / PIXELS_PER_METER
            y = float(y_px) / PIXELS_PER_METER

            if woofer_sheet_is_solid(x, y, inner, driver_mount_holes_diameter, driver_flange_cut, side_cut, magnet_cut):
                picture_array[
                    y_px + int(x_offset * PIXELS_PER_METER) + margin_px,
                    x_px + int(y_offset * PIXELS_PER_METER) + margin_px] = [255, 255, 255]


if __name__ == '__main__':
    # woofer_layer_xy_map = {}
    # for layer in [1, -1]:
    #     boop = []
    #     for x_px in range(1, int(0.3 * PIXELS_PER_METER)):
    #         for y_px in range(1, int(0.3 * PIXELS_PER_METER)):
    #             x = float(x_px) / PIXELS_PER_METER
    #             y = float(y_px) / PIXELS_PER_METER
    #
    #             if woofer_sheet_is_solid(x, 0.3 - y, layer==1):
    #                 boop.append({'x': x_px, 'y': y_px})
    #
    #     woofer_layer_xy_map[layer] = boop

    # # Draw square
    # for x in range(int(0.25*PIXELS_PER_METER), int(1.25*PIXELS_PER_METER)):
    #     for y in range(int(0.25*PIXELS_PER_METER), int(1.25*PIXELS_PER_METER)):
    #         cbt_array[y, x] = [255, 255, 255]

    # for layer, xy_values in woofer_layer_xy_map.items():
    #     for xy in xy_values:
    #         picture_array[xy['y'] + int((0.05 if layer < 0 else 0.75) * PIXELS_PER_METER), xy['x'] + int(
    #             (0.1 + ((abs(layer) - 1) + 0.5)) * PIXELS_PER_METER)] = [255, 255, 255]

    boop(0, 0, inner=True, side_cut=False, magnet_cut=False)
    boop(0, 0.3, inner=True, side_cut=True, magnet_cut=False)
    boop(0, 0.6, inner=False, driver_flange_cut=True, side_cut=False, magnet_cut=False)
    boop(0.3, 0, inner=True, driver_mount_holes_diameter=1.2, side_cut=True, magnet_cut=False)
    boop(0.3, 0.3, inner=False, side_cut=False, magnet_cut=False)
    boop(0.3, 0.6, inner=False, side_cut=False, magnet_cut=True)

    img = Image.fromarray(picture_array, 'RGB')
    img.save('picture_array.png')
    img.show()
