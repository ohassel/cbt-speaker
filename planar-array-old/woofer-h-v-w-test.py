from functools import partial

import cadquery as cq

import drivers as drivers
from geometry_2d import P as P

print('BEGIN')

all_shapes = []
all_shapes_map = {}


class Operation:
    def __init__(self, xyz=None, angle=None, mirror=None):
        self.xyz = xyz
        self.angle = angle
        self.mirror = mirror

    def __str__(self):
        return f"xyz: {self.xyz}, angle: {self.angle}, mirror: {self.mirror}"


class Shape:
    def __init__(self, name, create_shape, show_options=None):
        self.name = name
        self.show_options = show_options
        self.create_shape = create_shape
        self.shape = None
        self.operations = []
        self.visible = False
        all_shapes.append(self)
        all_shapes_map[name] = self

    def set_visible(self, visible: bool):
        self.visible = visible
        return self

    def add_operation(self, op: Operation):
        self.operations.append(op)
        return self

    def clear_operations(self):
        self.operations = []
        return self

    def rotate(self, xyz, angle):
        self.add_operation(Operation(xyz=xyz, angle=angle))
        return self

    def translate(self, xyz):
        self.add_operation(Operation(xyz=xyz))
        return self

    def mirror(self, mirror_plane):
        self.add_operation(Operation(mirror=mirror_plane))
        return self

    def get_or_create(self):
        if self.shape is None:
            print(f"create_shape '{self.name}'")
            self.shape = self.create_shape()
        return self.shape

    def cut(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().cut(other.get_or_create())
        return self

    def intersect(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().intersect(other.get_or_create())
        return self

    def union(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().union(other.get_or_create())
        return self

    def show(self):
        print(f"show: '{self.name}', visible: '{self.visible}'")
        if self.visible:
            show_object(self.get_or_create(), name=self.name, options=self.show_options or {})

    def export(self, extension="dxf"):
        print(f"export: '{self.name}', visible: '{self.visible}'")
        if self.visible:
            import cadquery as cq
            cq.exporters.export(self.get_or_create(), f"{self.name}.{extension}")

    def apply_operations(self, invert=False):
        if self.visible:

            for op in self.operations if not invert else reversed(self.operations):
                # print(op)
                if op.mirror is not None:
                    self.shape = self.get_or_create().mirror(mirrorPlane=op.mirror, union=True)
                elif op.angle is not None:
                    self.shape = self.get_or_create().rotate((0, 0, 0), op.xyz, op.angle if not invert else -op.angle)
                else:
                    self.shape = self.get_or_create().translate(
                        op.xyz if not invert else (-op.xyz[0], -op.xyz[1], -op.xyz[2]))


# cheap = False
cheap = True

frame_height = 10
shell_thickness = 0.4

(
    Shape(
        name=f"woofer_plain",
        show_options={"color": (110, 110, 110)},
        create_shape=partial(drivers.dma90,
                             )
    )
        .translate((0, 0, 4.5 + shell_thickness))
)

(
    Shape(
        name=f"h_woofer_cut_margin",
        show_options={"color": (110, 110, 110)},
        create_shape=partial(drivers.dma90,
                             extrude_screws=True,
                             margin=False,
                             )
    )
        .translate((0, 0, 4.5 + shell_thickness))
)

(
    Shape(
        name=f"v_woofer_cut_margin",
        show_options={"color": (110, 110, 110)},
        create_shape=partial(drivers.dma90,
                             extrude_screws=True,
                             margin=False,
                             surround_margin=0.1,
                             surround_depth=0.5,
                             )
    )
        .rotate((0, 0, 1), 90)
        .translate((0, 0.1, 0))
        .translate((0, 0.1, 0.25))
        .rotate((1, 0, 0), 45)
        .translate((0, 0, 5))
)

(
    Shape(
        name=f"w_woofer_cut_margin",
        show_options={"color": (110, 110, 110)},
        create_shape=partial(drivers.dma90,
                             extrude_screws=True,
                             margin=False,
                             surround_margin=0.1,
                             surround_depth=0.5,
                             )
    )
        .rotate((0, 0, 1), 180)
        .rotate((0, 1, 0), 90)
        .translate((0, 0, frame_height / 2))
    # .translate((-0.1, 0, 0))
)


def calc_angle_frame(invert=False):
    def calc_angle_frame_internal():
        return (
            cq.Workplane("XZ")
                .center(5 - shell_thickness, 0)
                .lineTo(shell_thickness, 0)
                # .lineTo(shell_thickness, shell_thickness)
                .lineTo(shell_thickness if invert else 0, shell_thickness)
                .lineTo(0, 0)
                .close()
                .extrude(5)
                .cut(
                (
                    cq.Workplane("XY")
                        .center(5 - shell_thickness, -5)
                        .lineTo(shell_thickness, 0)
                        # .lineTo(shell_thickness, shell_thickness)
                        .lineTo(0, shell_thickness)
                        # .lineTo(0, 0)
                        .close()
                        .extrude(shell_thickness)
                )
            )
                .mirror(mirrorPlane='XZ', union=True)
                .mirror(mirrorPlane='YZ', union=True)
                .mirror(mirrorPlane='XY', union=True)
        )

    b = calc_angle_frame_internal()
    b = b.union(
        calc_angle_frame_internal()
            .rotate((0, 0, 0), (0, 0, 1), 90)
    )

    return b


def calc_frame(height=10, bot_edge_inner=False, top_edge_inner=False):
    b = (
        cq.Workplane("XY")
            .rect(10, 10)
            .extrude(height)
            .faces("|Z")
            .shell(-shell_thickness)
    )

    bot_cut = (
        calc_angle_frame(not bot_edge_inner)
    )
    top_cut = (
        calc_angle_frame(not top_edge_inner)
            .translate((0, 0, height))
    )

    b = b.cut(bot_cut).cut(top_cut)

    return b


def calc_h_frame():
    b = calc_frame(10)

    h = (
        cq.Workplane("XY")
            .rect(10, 10)
            .extrude(shell_thickness)
            .faces(">Z")
            .workplane()
            .hole(7.9)
            .translate((0, 0, 4.5))
    )

    b = b.union(h)

    return b


def calc_v_frame():
    b = calc_frame(10)

    h = (
        cq.Workplane("XY")
            .rect(10-shell_thickness*2, 13.5)
            .extrude(shell_thickness)
            .faces(">Z")
            .workplane()
            # .hole(7.9)
            .hole(7.0)
            .translate((0, 0.2, 0))
            .rotate((0, 0, 0), (1, 0, 0), 45)
            .translate((0, 0, 4.75))
    )

    b = b.union(h)

    return b


def calc_w_frame():
    b = calc_frame(frame_height)

    h = (
        cq.Workplane("XY")
            .rect(frame_height, 10 - shell_thickness * 2)
            .extrude(shell_thickness)
            .faces(">Z")
            .workplane()
            # .hole(7.9)
            .hole(7.0)
            # .translate((0, 0.2, 0))
            .rotate((0, 0, 0), (0, 1, 0), 90)
            .translate((-shell_thickness, 0, 0))
            .translate((0, 0, frame_height / 2))
    )

    top = (
        cq.Workplane("XY")
            .rect(5 - shell_thickness, 10 - shell_thickness * 2)
            .extrude(shell_thickness)
            .translate((2.5 - shell_thickness / 2, 0, 0))
            .translate((0, 0, frame_height - shell_thickness))
    )

    bot = (
        cq.Workplane("XY")
            .rect(5 - shell_thickness*2, 10 - shell_thickness * 2)
            .extrude(shell_thickness)
            .translate((-2.5, 0, 0))
        # .translate((0, 0, shell_thickness))
    )

    b = b.union(h)
    b = b.union(top)
    b = b.union(bot)

    return b


Shape(
    name=f"h_frame",
    show_options={},
    create_shape=partial(calc_h_frame,
                         )
)

Shape(
    name=f"v_frame",
    show_options={},
    create_shape=partial(calc_v_frame,
                         )
)

Shape(
    name=f"w_frame",
    show_options={},
    create_shape=partial(calc_w_frame,
                         )
)

# Shape(
#     name=f"angle_frame",
#     show_options={"color": (255, 110, 110)},
#     create_shape=partial(calc_angle_frame,
#                          )
# )


print(f"shapes: {all_shapes_map.keys()}")

show_lambdas = [

    # lambda name: 'woofer_plain' in name,

    # lambda name: 'h_woofer_cut_margin' in name,
    # lambda name: 'h_frame' in name,

    # lambda name: 'v_woofer_cut_margin' in name,
    # lambda name: 'v_frame' in name,

    lambda name: 'w_woofer_cut_margin' in name,
    lambda name: 'w_frame' in name,

    # lambda name: 'angle_frame' in name,

    # lambda name: 'tweet_mount_block' in name,
    # lambda name: 'tweet_mount_splint' in name,
    # lambda name: 'pt2522' in name,

]

for name, shape in all_shapes_map.items():
    for l in show_lambdas:
        if l(name):
            print(f"show {name}")
            shape.set_visible(True)

for s in all_shapes:
    s.apply_operations()


def shapes_union(s1, s2):
    for mm in list(filter(lambda s: s1 in s.name, all_shapes)):
        for yy in list(filter(lambda s: s2 in s.name, all_shapes)):
            mm.union(yy)


def shapes_cut(s1, s2):
    for mm in list(filter(lambda s: s1 in s.name, all_shapes)):
        for yy in list(filter(lambda s: s2 in s.name, all_shapes)):
            mm.cut(yy)


def shapes_hide(s):
    ss = s
    for mm in list(filter(lambda s: ss in s.name, all_shapes)):
        all_shapes.remove(mm)


# shapes_union('wg_front', 'wg_flare')
# shapes_union('wg_back', 'wg_flare')

shapes_cut('h_frame', 'h_woofer_cut_margin')
shapes_cut('v_frame', 'v_woofer_cut_margin')
shapes_cut('w_frame', 'w_woofer_cut_margin')
# shapes_cut('tweet_mount_splint', 'tweet_mount_block_cut')

# shapes_hide('pt2522_cut_margin')
# shapes_hide('tweet_mount_block_cut')

print(all_shapes_map)

for s in all_shapes:
    s.show()
    # s.export(extension='dxf')

    # print_stl = False
    # print_stl = True
    # if print_stl and 'wg' in s.name:
    # s.export(extension='step')
