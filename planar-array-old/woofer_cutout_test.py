import cadquery as cq

import drivers

from geometry_2d import P as P

pt2522_width = 0


class WooferStats:
    def __init__(self, p_offset=None, woofer_type='nd105', dual=True, z_offset=0.0, angle=60, z_angle=0, rotation_angle=0, x_offset=0.05):

        self.z_offset = z_offset
        self.angle = angle
        self.z_angle = z_angle
        self.rotation_angle = rotation_angle

        self.screw_hole_diameter = 0.42

        self.woofer_type = woofer_type
        self.dual = dual

        self.x_offset = x_offset

        self.m4_heat_insert_diameter = 0.63

        self.woofer = None
        self.woofer_margin = None

        if woofer_type == 'nd140':
            self.diameter = 13.8
            self.max_diameter = 15.45
            self.cutout_diameter = 12.0
            self.base_thickness = 0.15
            self.screw_holes_diameter = 13.8
            self.sd = 86.6
            self.surround_diameter = 11.7
            self.basket_height = 3.7
            self.magnet_diameter = 4.22
            self.magnet_height = 6.25
        elif woofer_type == 'nd105':
            self.diameter = 10.5
            self.max_diameter = 12.0
            self.cutout_diameter = 9.3
            self.base_thickness = 0.1
            self.screw_holes_diameter = 10.85
            self.sd = 51.5
            self.rear_slot_sd = self.sd
            self.surround_diameter = 9.3
            self.basket_height = 3.0
            self.magnet_diameter = 4.22
            self.magnet_height = 5.7
        elif woofer_type == 'nd91':
            self.diameter = 9.3
            self.max_diameter = 10.35
            self.cutout_diameter = 7.65
            self.base_thickness = 0.1
            self.screw_holes_diameter = 8.84
            self.sd = 30.4
            self.rear_slot_sd = self.sd * 1.2
            self.surround_diameter = 7.4
            self.basket_height = 2.75
            self.magnet_diameter = 4.22
            self.magnet_height = 4.49
        elif woofer_type == 'nd65':
            self.diameter = 6.4
            self.max_diameter = 7.55
            self.cutout_diameter = 5.2
            self.base_thickness = 0.1
            self.screw_holes_diameter = 6.6
            self.sd = 15.6
            self.rear_slot_sd = self.sd * 1.4
            self.surround_diameter = 5.15
            self.basket_height = 1.9
            self.magnet_diameter = 3.45
            self.magnet_height = 4.3
        elif woofer_type == 'nd64':
            self.diameter = 6.4
            self.max_diameter = 7.55
            self.cutout_diameter = 5.2
            self.base_thickness = 0.1
            self.screw_holes_diameter = 6.6
            self.sd = 15.6
            self.rear_slot_sd = self.sd * 1.4
            self.surround_diameter = 5.15
            self.basket_height = 1.9
            self.magnet_diameter = 3.45
            self.magnet_height = 3.45

        self.p_offset = p_offset or P(x=self.diameter / 2 + pt2522_width / 2 + x_offset, y=0)

    def get_woofer(self):
        if self.woofer is None:
            if self.woofer_type == 'nd140':
                self.woofer = drivers.nd140()
            elif self.woofer_type == 'nd105':
                self.woofer = drivers.nd105()
            elif self.woofer_type == 'nd91':
                self.woofer = drivers.nd91()
            elif self.woofer_type == 'nd65':
                self.woofer = drivers.nd65()
            elif self.woofer_type == 'nd64':
                self.woofer = drivers.nd64()
        return self.woofer

    def get_woofer_margin(self):
        if self.woofer_margin is None:
            if self.woofer_type == 'nd140':
                self.woofer_margin = drivers.nd140(
                    margin=True,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
            elif self.woofer_type == 'nd105':
                self.woofer_margin = drivers.nd105(
                    margin=True,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
            elif self.woofer_type == 'nd91':
                self.woofer_margin = drivers.nd91(
                    margin=True,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
            elif self.woofer_type == 'nd65':
                self.woofer_margin = drivers.nd65(
                    margin=True,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
            elif self.woofer_type == 'nd64':
                self.woofer_margin = drivers.nd64(
                    margin=True,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
        return self.woofer_margin


# Angled
woofer_angle = 0
woofer_z_offset = 0.75
woofer_stats = [
    WooferStats(p_offset=None, z_offset=woofer_z_offset, woofer_type='nd140', angle=woofer_angle, x_offset=0.0),
    # WooferStats(p_offset=None, z_offset=woofer_z_offset, woofer_type='nd140', angle=woofer_angle, x_offset=0.5),
    # WooferStats(p_offset=None, z_offset=woofer_z_offset, woofer_type='nd140', angle=woofer_angle, x_offset=0.5),
    # WooferStats(p_offset=None, z_offset=woofer_z_offset, woofer_type='nd105', angle=woofer_angle),
    # WooferStats(p_offset=None, z_offset=woofer_z_offset, woofer_type='nd91', angle=woofer_angle),
    # WooferStats(p_offset=None, z_offset=woofer_z_offset, woofer_type='nd91', angle=woofer_angle),
    # WooferStats(p_offset=None, z_offset=woofer_z_offset, woofer_type='nd65', angle=woofer_angle),
    # WooferStats(p_offset=None, z_offset=0.75, woofer_type='nd65', angle=-18, rotation_angle=4, x_offset=-0.15),
    # WooferStats(p_offset=None, z_offset=1.75, woofer_type='nd65', angle=0, z_angle=0, dual=False),
]

ws = woofer_stats[0]


def calc_woofer_edge_cutout(ws, union_rear=True):
    t = 4
    front_h = ws.magnet_height - 2.5
    rear_h = ws.basket_height + 1
    front_shelf = 1.0
    # h = ws.magnet_height * 2 - 2
    r = (
        cq.Workplane("XY")
            .workplane(offset=0)
            .rect(ws.diameter + 0.5, t)
            .workplane(offset=0.25)
            .rect(ws.diameter + 0.5, t)
            .workplane(offset=ws.magnet_height + 0.25)
            .rect(ws.magnet_diameter + 1.0, t)
            .loft(combine=True)
            .edges(">Z and (not <Y) and (not >Y)")
            .fillet(1)
            .rotate((0, 0, 0), (0, 0, 1), 90)
    )
    x = ws.surround_diameter * 0.5
    fb = (
        cq.Workplane("XY")
            .workplane(offset=0)
            .rect(t, ws.surround_diameter)
            # .moveTo(-t/2, x)
            # .lineTo(t/2, x)
            # .threePointArc((ws.surround_diameter/2, 0), (t/2, -x))
            # .lineTo(-t/2, -x)
            # .threePointArc((-ws.surround_diameter/2, 0), (-t/2, x))
            # .close()
            .extrude(-front_shelf)
        # .rotate((0, 0, 0), (0, 0, 1), 90)
    )
    f = (
        cq.Workplane("XY")
            .workplane(offset=0)
            .rect(t, ws.surround_diameter)
            # .moveTo(-t/2, x)
            # .lineTo(t/2, x)
            # .threePointArc((ws.surround_diameter/2, 0), (t/2, -x))
            # .lineTo(-t/2, -x)
            # .threePointArc((-ws.surround_diameter/2, 0), (-t/2, x))
            # .close()
            .workplane(offset=front_h - 0.5)
            .rect(t, ws.surround_diameter * 0.75)
            .loft(combine=True)
            .edges(">Z and (not <X) and (not >X)")
            .fillet(2)
            # .rotate((0, 0, 0), (0, 0, 1), 90)
            .rotate((0, 0, 0), (0, 1, 0), 180)
            .translate((0, 0, -front_shelf))
    )
    f = f.union(fb)
    b = f
    if union_rear:
        b = b.union(r)
    return b


# c = (
#     cq.Workplane("XY")
#         .rect(ws.diameter, t)
#         .extrude(h/2)
#         .mirror(mirrorPlane='XY', union=True)
# )

w = ws.get_woofer()
b = calc_woofer_edge_cutout(ws)

# show_object(w, name=f'w', options={"color": (125, 125, 125)})
# show_object(b, name=f'b', options={"alpha": 0.5})

c = (
    cq.Workplane("XY")
        .box(30, 6, 30)
    .edges()
    .chamfer(1)
    .edges()
    .fillet(0.5)
)

show_object(c, name=f'c', options={"alpha": 0.5})
# show_object(c, name=f'c', options={"alpha": 0.5})

cq.exporters.export(w, "nd140.dxf")
