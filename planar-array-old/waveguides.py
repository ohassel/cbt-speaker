import cadquery as cq

from functools import partial
import drivers as drivers

import geometry_2d as geom
from geometry_2d import P as P

import numpy as np

print('BEGIN')

all_shapes = []
all_shapes_map = {}


class Operation:
    def __init__(self, xyz=None, angle=None, mirror=None):
        self.xyz = xyz
        self.angle = angle
        self.mirror = mirror

    def __str__(self):
        return f"xyz: {self.xyz}, angle: {self.angle}, mirror: {self.mirror}"


class Shape:
    def __init__(self, name, create_shape, show_options=None):
        self.name = name
        self.show_options = show_options
        self.create_shape = create_shape
        self.shape = None
        self.operations = []
        self.visible = False
        all_shapes.append(self)
        all_shapes_map[name] = self

    def set_visible(self, visible: bool):
        self.visible = visible
        return self

    def add_operation(self, op: Operation):
        self.operations.append(op)
        return self

    def clear_operations(self):
        self.operations = []
        return self

    def rotate(self, xyz, angle):
        self.add_operation(Operation(xyz=xyz, angle=angle))
        return self

    def translate(self, xyz):
        self.add_operation(Operation(xyz=xyz))
        return self

    def mirror(self, mirror_plane):
        self.add_operation(Operation(mirror=mirror_plane))
        return self

    def get_or_create(self):
        if self.shape is None:
            print(f"create_shape '{self.name}'")
            self.shape = self.create_shape()
        return self.shape

    def cut(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().cut(other.get_or_create())
        return self

    def intersect(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().intersect(other.get_or_create())
        return self

    def union(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().union(other.get_or_create())
        return self

    def show(self):
        print(f"show: '{self.name}', visible: '{self.visible}'")
        if self.visible:
            show_object(self.get_or_create(), name=self.name, options=self.show_options or {})

    def export(self, extension="dxf"):
        print(f"export: '{self.name}', visible: '{self.visible}'")
        if self.visible:
            import cadquery as cq
            cq.exporters.export(self.get_or_create(), f"{self.name}.{extension}")

    def apply_operations(self, invert=False):
        if self.visible:

            for op in self.operations if not invert else reversed(self.operations):
                # print(op)
                if op.mirror is not None:
                    self.shape = self.get_or_create().mirror(mirrorPlane=op.mirror, union=True)
                elif op.angle is not None:
                    self.shape = self.get_or_create().rotate((0, 0, 0), op.xyz, op.angle if not invert else -op.angle)
                else:
                    self.shape = self.get_or_create().translate(
                        op.xyz if not invert else (-op.xyz[0], -op.xyz[1], -op.xyz[2]))


def wg_2d_slice(x, x_offset, y_offset, throat_roundover, coverage_angle, edge_roundover, full_edge_round=True):
    w = x

    b = cq.Workplane("XY").center(0, 0)

    # x0 = P(0, 0)
    x0 = P(x=x_offset, y=y_offset)

    b = b.moveTo(x0.x, 0)
    b = b.lineTo(x0.x, x0.y)

    throat_roundover_center = x0 + P(x=throat_roundover, y=0)

    sp = []

    wg_angle = 90 - (coverage_angle / 2)

    print(f"")
    print(f"wg_angle: '{wg_angle}'")

    throat_max_angle = 90 - wg_angle
    print(f"throat_max_angle: '{throat_max_angle}'")

    end_throat_roundover = None

    for i in np.arange(0, 1, 0.01):
        i_angle = i * throat_max_angle
        p = throat_roundover_center + P(length=throat_roundover, angle=180 - i_angle)
        end_throat_roundover = p
        sp.append((p.x, p.y))
        # b = b.lineTo(p.x, p.y)

    vector_from_conical_to_roundover_center = P(length=edge_roundover, angle=180 - throat_max_angle)
    edge_roundover_x = -vector_from_conical_to_roundover_center.x + edge_roundover

    conical_end = end_throat_roundover + P(angle=wg_angle, x=w - end_throat_roundover.x - edge_roundover_x)

    print(f"conical_end: {conical_end}")

    conical_roundover_center = conical_end - vector_from_conical_to_roundover_center

    print(f"conical_roundover_center: {conical_roundover_center}")
    print(f"edge_roundover_x: {edge_roundover_x}")

    wg_depth = (conical_roundover_center.y + edge_roundover) * 2
    print(f"wg_depth: {wg_depth}")

    print(f"wg_width: {w * 2}")

    p = None

    for i in np.arange(0.0, 1, 0.01):
        i_angle = i * ((90 - throat_max_angle) + (90 if full_edge_round else 0))
        p = conical_roundover_center + P(length=edge_roundover, angle=(180 - throat_max_angle) - i_angle)

        # print(f"i_angle: {i_angle}")
        # print(f"p: {p}")

        # b = b.lineTo(p.x, p.y)
        sp.append((p.x, p.y))

    b = b.spline(sp)

    if full_edge_round:
        b = b.lineTo(w, 0)
    else:
        b = b.lineTo(w - edge_roundover_x / 2, wg_depth / 2)
        b = b.lineTo(w - edge_roundover_x / 2, 0)

    b = b.close()

    return b


def calc_wg(
        w=20,
        d=14,
        h=16,
        x_offset=3.35 / 2,
        y_offset=0.94 / 2,
        throat_roundover=3,
        coverage_angle=105,
        edge_roundover=3,
        phase_flange_height=None,
        mini_flanges=False,

        revolve_degrees=17,
        num_tweeters=None,
        degrees_per_tweeter=None,
):
    # w = (w / 2) - edge_roundover - x_offset
    # w = (w / 2) - edge_roundover
    w = (w / 2)

    b = wg_2d_slice(
        x=w,
        x_offset=x_offset,
        y_offset=y_offset,
        throat_roundover=throat_roundover,
        coverage_angle=coverage_angle,
        edge_roundover=edge_roundover,
    )

    # b = b.close()

    b = b.mirrorY()
    b = b.mirrorX()
    b = b.revolve(revolve_degrees, (1, cbt_radius, 0), (0, cbt_radius, 0))

    print('')

    # if phase_flange_height:
    #     r = cq.Workplane("XY").center(0, 0)
    #     r = r.moveTo(0.49/2, y_offset / 2)
    #
    #     r = r.spline([
    #         (0.49/2, y_offset / 2),
    #         (0.49/2, y_offset / 2 + 0.1),
    #         (0, phase_flange_height),
    #     ])
    #
    #     # r = r.lineTo(0, phase_flange_height)
    #     r = r.lineTo(0, y_offset / 2)
    #     r = r.close() \
    #         .extrude(h) \
    #         .mirror(mirrorPlane='ZY', union=True) \
    #         .mirror(mirrorPlane='ZX', union=True)
    #
    #     b = b.union(r)
    #
    # if mini_flanges:
    #     radius = 0.49/2
    #
    #     driver = cq.Workplane("XY") \
    #         .box(7, y_offset, h) \
    #         .translate((0, 0, h/2))
    #
    #     circle = cq.Workplane("XY") \
    #         .moveTo(-0.97, y_offset - radius*2) \
    #         .circle(0.49/2) \
    #         .extrude(h) \
    #         .mirror(mirrorPlane='ZY', union=True) \
    #         .mirror(mirrorPlane='ZX', union=True)
    #
    #     b = b.union(
    #         circle.cut(driver)
    #     )

    return b


def calc_flare(
        x_offset=3.35 / 2,
        y_offset=0.94 / 2,

        top=True,
        bottom=False,
):
    w = waveguide_width - waveguide_edge_roundover * 2
    d = wg_flare_depth / 2

    flare_edge_roundover = waveguide_edge_roundover / 3

    b = wg_2d_slice(
        x=(d / 2),
        x_offset=2.95,
        y_offset=y_offset,
        throat_roundover=waveguide_throat_roundover,
        coverage_angle=waveguide_flare_angle,
        edge_roundover=flare_edge_roundover,
        full_edge_round=False,
    )

    # print(f"h: {h}, wg_depth: {wg_depth}")

    b = b.mirrorX()
    b = b.extrude(w)
    b = b.translate((0 - (d / 2) + flare_edge_roundover, 0, 0))
    b = b.translate((0, 0, -w / 2))
    b = b.rotate((0, 0, 0), (0, 1, 0), 90)

    if top and bottom:
        b = b.mirror(mirrorPlane='XY', union=True)

    if not top and bottom:
        b = b.rotate((0, 0, 0), (0, 1, 0), 180)

    return b


def calc_flare(
        x_offset=3.35 / 2,
        y_offset=0.94,

        top=True,
        bottom=False,
):
    w = waveguide_width - waveguide_edge_roundover * 2
    d = wg_flare_depth / 2

    flare_edge_roundover = waveguide_flare_edge_roundover / 3

    b = wg_2d_slice(
        x=(d / 2),
        x_offset=2.95,
        y_offset=y_offset,
        throat_roundover=waveguide_throat_roundover,
        coverage_angle=waveguide_flare_angle,
        edge_roundover=flare_edge_roundover,
        full_edge_round=False,
    )

    # print(f"h: {h}, wg_depth: {wg_depth}")

    b = b.mirrorX()
    b = b.extrude(w)
    b = b.translate((0 - (d / 2) + flare_edge_roundover, 0, 0))
    b = b.translate((0, 0, -w / 2))
    b = b.rotate((0, 0, 0), (0, 1, 0), 90)

    if top and bottom:
        b = b.mirror(mirrorPlane='XY', union=True)

    if not top and bottom:
        b = b.rotate((0, 0, 0), (0, 1, 0), 180)

    return b


def calc_front_back_saw(front=True):
    base_width = 30
    base_height = 30

    margin = 1.0
    base_thickness = 0.25 + 0.05 * margin

    # Base
    b = cq.Workplane("XY").center(0, 0) \
        .moveTo(0, 0) \
        .lineTo(base_width + margin * 0.2, 0) \
        .lineTo(base_width + margin * 0.2, base_height + margin * 0.2) \
        .lineTo(0, base_height + margin * 0.2) \
        .close().extrude(15) \
        .edges("|Z") \
        .fillet(0.55) \
        .translate((-margin * 0.1, -margin * 0.1, 0))

    b = b.translate((-base_width / 2, -base_height / 2, -base_thickness / 2))

    b = b.translate((0, 0, base_thickness))

    if front:
        b = b.translate((0, 0, -base_thickness))
        b = b.rotate((0, 0, 0), (0, 1, 0), 180)

    return b


cheap = False
# cheap = True

cbt_radius = 160

wg_flare_depth = 20

waveguide_width = 24
waveguide_height = 17
waveguide_depth = 14

waveguide_throat_roundover = 2

waveguide_coverage_angle = 87.5
waveguide_edge_roundover = 1

# waveguide_coverage_angle = 60
# waveguide_edge_roundover = 2.75

waveguide_flare_edge_roundover = 1
waveguide_flare_angle = 18

num_tweeters = 1

degrees_per_tweeter = 58 / 18

# waveguide_revolve_degrees = 58
waveguide_revolve_degrees = degrees_per_tweeter * num_tweeters

waveguide_horz_edge_roundover = 2

for n in ['front', 'back']:
    wg = Shape(
        name=f"wg_{n}",
        show_options={"color": (75, 75, 200)},
        create_shape=partial(calc_wg,
                             w=waveguide_width,
                             d=waveguide_depth,
                             h=waveguide_height,

                             throat_roundover=waveguide_throat_roundover,
                             coverage_angle=waveguide_coverage_angle,
                             edge_roundover=waveguide_edge_roundover,
                             phase_flange_height=2,
                             mini_flanges=True,

                             revolve_degrees=waveguide_revolve_degrees,
                             num_tweeters=num_tweeters,
                             degrees_per_tweeter=degrees_per_tweeter,
                             )
    )

for i in range(0, num_tweeters + 1):

    wg_flare = Shape(
        name=f"wg_flare_{i}",
        show_options={"color": (75, 75, 200)},
        create_shape=partial(calc_flare,
                             top=num_tweeters > i,
                             bottom=i > 0,
                             )
    )

    if i > 0:
        angle = i * degrees_per_tweeter
        p = P(length=cbt_radius, angle=angle) - P(x=cbt_radius, y=0)
        wg_flare.rotate((1, 0, 0), -angle)

        wg_flare.translate((0, -p.x, p.y))


def setup_tweeter_angle_thingy(shape):
    for i in range(0, num_tweeters):
        s = shape
        angle = (0.5 + i) * degrees_per_tweeter
        p = P(length=cbt_radius, angle=angle) - P(x=cbt_radius, y=0)
        s.rotate((1, 0, 0), 90)
        s.rotate((0, 0, 1), 180)

        s.rotate((1, 0, 0), -angle)

        s.translate((0, -p.x, p.y))

        # print(f"pp: {(p.x, 0, p.y)}")


setup_tweeter_angle_thingy(
    shape=Shape(
        name=f"pt2522_plain_{i}",
        show_options={"color": (110, 110, 110)},
        create_shape=partial(drivers.pt2522,
                             cut_audio_holes=not cheap,
                             union_heat_inserts=False,
                             )
    )
)

for n in [True, False]:
    setup_tweeter_angle_thingy(
        Shape(
            name=f"wg_cut_{i}_{'front' if n else 'back'}",
            show_options={"color": (200, 75, 75)},
            create_shape=partial(calc_front_back_saw,
                                 front=n,
                                 )
        )
    )

setup_tweeter_angle_thingy(
    Shape(
        name='pt2522_cut_margin',
        show_options={"color": (110, 110, 110)},
        create_shape=partial(drivers.pt2522,
                             union_heat_inserts=True,
                             cut_screw_holes=False,
                             cut_audio_holes=False,
                             render_screw_holes_and_rivets_and_terminals=True,
                             with_margin=True,
                             # union_rear_mount_block_height=3.0,
                             # rear_screw_holes_union_inner_height=1.0,
                             # rear_screw_holes_union_outer_height=2.0,
                             # rear_screw_holes_union_diameter=1.0,
                             rear_screw_holes_union_inner_height=1.5,
                             rear_screw_holes_union_outer_height=0.95,
                             rear_screw_holes_union_outer_angled_height=8.0,
                             rear_screw_holes_union_outer_angled_angle=waveguide_flare_angle / 2,
                             rear_screw_holes_union_diameter=0.9,
                             terminal_block_depth=1.9,
                             terminal_tabs_height=1.9,
                             terminal_tab_width=2.0,
                             terminal_tabs_margin=0.25,
                             )
    )
)


def terminal_tab_trench(w=25, h=1.2, d=2, d0=0.2, d1=4.0):
    b = cq.Workplane("XY").center(0, 0) \
        .moveTo(0, 0) \
        .lineTo(d0 * d, 0) \
        .lineTo(d1 * d, h) \
        .lineTo(0, h) \
        .lineTo(0, 0) \
        .close().extrude(w)

    b = b.rotate((0, 0, 0), (0, 1, 0), -90)
    b = b.rotate((0, 0, 0), (0, 0, 1), 180)
    b = b.translate((-w / 2, -8.93 / 2 + h / 2 + 0.5, 1.0 * d))

    return b


setup_tweeter_angle_thingy(
    Shape(
        name='terminal_tab_trench_middle',
        show_options={"color": (110, 110, 110)},
        create_shape=partial(terminal_tab_trench, w=8)
    )
)

setup_tweeter_angle_thingy(
    Shape(
        name='terminal_tab_trench_edge',
        show_options={"color": (110, 110, 110)},
        create_shape=partial(terminal_tab_trench, w=25, d=2, h=0.6, d0=0.25, d1=1.0)
    )
)


#
# pt2522_rear_cuts = Shape(
#     name='pt2522_rear_cuts',
#     show_options={"color": (110, 110, 110)},
#     create_shape=partial(drivers.pt2522,
#                          union_heat_inserts=True,
#                          union_rear_mount_block_height=None,
#                          terminal_tabs_height=5.0,
#                          terminal_tabs_margin=0.2,
#                          rear_screw_holes_union_inner_height=5.0,
#                          rear_screw_holes_union_outer_height=2.0,
#                          )
# )
# pt2522_rear_cuts.rotate((1, 0, 0), 90)
#
# pt2522_blocker = Shape(
#     name='pt2522_blocker',
#     show_options={"color": (200, 75, 75)},
#     create_shape=partial(drivers.pt2522,
#                          union_heat_inserts=True,
#                          union_rear_mount_block_margin=0.5,
#                          union_rear_mount_block_height=7.0,
#                          union_rear_mount_block_top_diamond_height=3.0,
#                          union_rear_mount_block_bottom_diamond_height=0.0,
#                          )
# )
# pt2522_blocker.rotate((1, 0, 0), 90)
#
# pt2522_blocker_mini = Shape(
#     name='pt2522_blocker_mini',
#     show_options={"color": (200, 75, 75)},
#     create_shape=partial(drivers.pt2522,
#                          union_heat_inserts=True,
#                          union_rear_mount_block_margin=0.4,
#                          union_rear_mount_block_height=7.0,
#                          union_rear_mount_block_top_diamond_height=3.0,
#                          union_rear_mount_block_bottom_diamond_height=0.0,
#                          )
# )
# pt2522_blocker_mini.rotate((1, 0, 0), 90)


def calc_edge_cylinder_material_savers():
    b = cq.Workplane("XY") \
        .moveTo(-waveguide_width / 2, 0) \
        .circle(waveguide_depth * 0.5 * 0.65) \
        .extrude(waveguide_height) \
        .mirror(mirrorPlane='ZY', union=True) \
        .mirror(mirrorPlane='ZX', union=True)

    return b


# edge_cylinder_material_savers = Shape(
#     name='edge_cylinder_material_savers',
#     show_options={"color": (75, 200, 75)},
#     create_shape=partial(calc_edge_cylinder_material_savers)
# )
# edge_cylinder_material_savers.translate((0, 0, -waveguide_height / 2))

print(f"shapes: {all_shapes_map.keys()}")

show_lambdas = [

    # lambda name: 'edge_cylinder_material_savers' in name,

    lambda name: 'wg_front' in name,
    lambda name: 'wg_back' in name,
    lambda name: 'wg_flare' in name,

    lambda name: 'cut' in name,
    lambda name: 'pt2522' in name,
    lambda name: 'pt2522_cut_margin' in name,
    lambda name: 'terminal_tab_trench' in name,

    # lambda name: 'pt2522_plain' in name,
    # lambda name: 'pt2522_rear_cuts' in name,
    # lambda name: 'pt2522_rear_cuts' in name,
]

for name, shape in all_shapes_map.items():
    for l in show_lambdas:
        if l(name):
            print(f"show {name}")
            shape.set_visible(True)

for s in all_shapes:
    s.apply_operations()


def shapes_union(s1, s2):
    for mm in list(filter(lambda s: s1 in s.name, all_shapes)):
        for yy in list(filter(lambda s: s2 in s.name, all_shapes)):
            mm.union(yy)


def shapes_cut(s1, s2):
    for mm in list(filter(lambda s: s1 in s.name, all_shapes)):
        for yy in list(filter(lambda s: s2 in s.name, all_shapes)):
            mm.cut(yy)


def shapes_hide(s):
    ss = s
    for mm in list(filter(lambda s: ss in s.name, all_shapes)):
        all_shapes.remove(mm)


shapes_union('wg_front', 'wg_flare')
shapes_union('wg_back', 'wg_flare')

# shapes_cut('wg_horz', 'pt2522_plain')
shapes_cut('wg_front', 'pt2522')
shapes_cut('wg_back', 'pt2522')

all_shapes_map['wg_front'].cut(all_shapes_map['wg_cut_1_back'])
shapes_cut('wg_front', 'wg_cut_1_back')

shapes_cut('wg_back', 'wg_cut_1_front')
shapes_cut('wg_back', 'terminal_tab_trench')

shapes_hide('wg_flare')
shapes_hide('wg_cut')
shapes_hide('terminal_tab_trench')

# shapes_cut('wg_horz', 'lt2_blocker')
# shapes_cut('wg_horz', 'pt2522_blocker')
# shapes_cut('wg_horz', 'pt2522_flat')
#
# shapes_cut('wg_horz', 'edge_cylinder_material_savers')

print(all_shapes_map)

for s in all_shapes:
    s.show()
    # s.export(extension='dxf')

    # print_stl = False
    print_stl = True
    if print_stl and 'wg' in s.name:
        # if 'wg_front' == s.name:
        #     s.clear_operations()
        #     s.rotate((1, 0, 0), -(90 - degrees_per_tweeter/2))
        #     s.apply_operations()
        #
        # if 'wg_back' == s.name:
        #     s.clear_operations()
        #     s.rotate((1, 0, 0), +(90 + degrees_per_tweeter/2))
        #     s.apply_operations()

        s.export(extension='step')
