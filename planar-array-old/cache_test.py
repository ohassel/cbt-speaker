import cadquery as cq
import time
from hacked_cq_cache import cq_cache
from itertools import cycle


@cq_cache()
def lofting(nb_sec):
    wires = []
    radius = cycle([2, 5, 8, 6, 10])
    for i in range(nb_sec):
        if i % 2 == 0:
            Y = 1
        else:
            Y = 0
        wires.append(cq.Wire.makeCircle(next(radius), cq.Vector(0, 0, 5 * i), cq.Vector(0, Y, 1)))
    loft = cq.Solid.makeLoft(wires)
    return loft


start = time.time()

lofting(500)

end = time.time()
elapsed = end - start
print(f"elapsed time: '{elapsed}'")

# First script run :
# >>> 4500 ms
# Second script run :
# >>> 20 ms
