import cadquery as cq
# https://github.com/CadQuery/cadquery-plugins/tree/main/plugins/heatserts
# import heatserts
# https://github.com/CadQuery/cadquery-plugins/tree/main/plugins/cq_cache
# decorate your functions that build computationally heavy shapes
from hacked_cq_cache import cq_cache

import drivers

from geometry_2d import P as P
from geometry_2d import find_circle as find_circle
from geometry_2d import distance_across_circle as distance_across_circle
import math


def actual_distance_across_circle(degrees, radius):
    return 2 * radius * math.pi * (degrees / 360)


class WgShell:
    def __init__(self, angle, w=None, d=None, edge_width=None, front=False, rear=False, flat=False, radius=None):
        if radius is None:
            radius = CBT_RADIUS
        x = angle / CBT_DEGREES

        self.angle = angle

        # print(f"f={x}")
        side = Side(angle)

        self.edge_width = edge_width or (CBT_BOT_EDGE_WIDTH - (CBT_TOP_EDGE_WIDTH_DIFF * x))

        if CBT_LINEAR_WIDTH:
            self.w = w or (CBT_BOTTOM_WIDTH - (CBT_BOT_TOP_WIDTH_DIFF * x))
        else:
            w_offset = 0
            # print(f"side_angle_xz.angle(): {side_angle_xz.angle()}")
            if front:
                self.edge_width = self.edge_width * CBT_EDGE_FORWARD_RATIO
            if rear:
                self.edge_width = self.edge_width / CBT_EDGE_FORWARD_RATIO

            if front or rear:
                w_offset = P(x=self.edge_width, angle=side_angle_xz.angle()).y
            if rear:
                w_offset = -w_offset
            # print(f"w_offset: {w_offset}")

            # self.w = w or (CBT_BOTTOM_WIDTH - (CBT_BOT_TOP_WIDTH_DIFF * x))
            self.w = side.outer_cbt_width + w_offset
        self.d = d or (CBT_BOTTOM_DEPTH - (CBT_BOT_TOP_DEPTH_DIFF * x))
        # self.edge_width = edge_width or (CBT_BOT_EDGE_WIDTH - (CBT_TOP_EDGE_WIDTH_DIFF * x))

        if flat:
            d = actual_distance_across_circle(degrees=angle, radius=radius)
            self.angle = 0
            self.center = P(0, d)
        else:
            c = P(x=radius, y=0) + P(length=-radius, angle=angle)
            self.center = P(c.x, -c.y)

    def __str__(self):
        return f"({self.angle}, {self.edge_width}, {self.w}, {self.d}, {self.center})"


def wg_2d_slice(s: WgShell):
    w = s.w / 2
    # d = s.d / 2
    #
    # e = s.edge_width / 2

    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    def half(ss, w0, w, d, e, sign=1):
        # r = r.moveTo(w * sign, 0)
        ss = ss.segment((w0 * sign, 0), (w * sign, e * sign))

        # diamond
        # ss = ss.segment((0, d * sign))
        # ss = ss.segment((-w * sign, e * sign))

        # Curved
        ss = ss.arc((0, d * sign), (-w * sign, e * sign), "ignored")

        #
        ss = ss.segment((-w * sign, e * sign), (-w0 * sign, 0))

        return ss

    s = cq.Sketch()

    s = half(s, sign=1, w0=w, w=sr.w / 2, d=sr.d / 2, e=sr.edge_width / 2)
    s = half(s, sign=-1, w0=w, w=sf.w / 2, d=sf.d / 2, e=sf.edge_width / 2)

    s = s.solve().assemble()

    return s


class WgGeometry:
    def __init__(self, s: WgShell):

        throat_roundover = CBT_THROAT_ROUNDOVER
        edge_roundover = CBT_EDGE_ROUNDOVER

        wg_angle = 90 - (CBT_COVERAGE_ANGLE / 2)
        throat_max_angle = 90 - wg_angle

        # b = b.moveTo()

        # b = b.rect(pt2522_x_offset, pt2522_y_offset)

        p0 = P(pt2522_x_offset, pt2522_y_offset)

        throat_roundover_center = p0 + P(x=throat_roundover, y=0)

        p1 = throat_roundover_center + P(length=throat_roundover, angle=180 - throat_max_angle)
        p01 = throat_roundover_center + P(length=throat_roundover, angle=180 - throat_max_angle * 0.5)

        # p1 = pc + P(length=throat_r, angle=throat_max_angle)
        #
        step = 20

        p2 = p1 + P(y=step, angle=wg_angle)

        w = s.w / 2
        d = s.d / 2
        #
        e = s.edge_width / 2

        arc_center, arc_radius = find_circle(
            (-w, e),
            (0, d),
            (w, e)
        )

        p_step = p2
        for i in range(0, 100):

            step = step / 2

            length_to_arc_center = (p_step - arc_center).length()
            delta_to_arc_center = arc_radius - length_to_arc_center

            # print(f"p2={p2}")
            # print(f"length_to_arc_center={length_to_arc_center}")
            # print(f"delta_to_arc_center={delta_to_arc_center}")

            if delta_to_arc_center > 0:
                sign = 1
            else:
                sign = -1

            p_step = p_step + P(y=step * sign, angle=wg_angle)

        p2 = p_step

        p_edge_roundover_center = p2 + P(length=-edge_roundover, angle=wg_angle) + P(length=-edge_roundover,
                                                                                     angle=90 + wg_angle)

        step = 10
        p_step = p_edge_roundover_center
        for i in range(0, 100):

            step = step / 2

            length_to_arc_center = (p_step - arc_center).length()
            delta_to_arc_center = arc_radius - length_to_arc_center - edge_roundover

            # print(f"p2={p2}")
            # print(f"length_to_arc_center={length_to_arc_center}")
            # print(f"delta_to_arc_center={delta_to_arc_center}")

            if delta_to_arc_center > 0:
                sign = 1
            else:
                sign = -1

            p_step = p_step + P(y=step * sign, angle=wg_angle)

        p_edge_roundover_center = p_step
        p3 = p_edge_roundover_center + P(length=edge_roundover, angle=90 + wg_angle)
        p4 = p_edge_roundover_center + P(length=edge_roundover, angle=(p_edge_roundover_center - arc_center).angle())
        p34 = p_edge_roundover_center + P(length=edge_roundover, angle=p3.angle() + p4.angle())

        edge_before = (P(w, e) - arc_center).rotate(5) + arc_center

        self.p0 = P(p0.x, 0)
        self.p1 = P(p0.x, p0.y)
        self.p2_arc = (P(p01.x, p01.y), P(p1.x, p1.y))
        self.p3 = P(p3.x, p3.y)
        self.p4_arc = (P(p34.x, p34.y), P(p4.x, p4.y))
        self.p5_arc = (P(edge_before.x, edge_before.y), P(w, e))
        self.p6 = P(w, 0)

        self.wg_edge_middle = self.p4_arc[0]

        # print(f"wg_edge_middle: {self.wg_edge_middle}")


wg_geometry_cache = {}


def wg_geometry_cached(s: WgShell):
    # return WgGeometry(s=s)
    key = str(s)
    if key in wg_geometry_cache:
        # print(f'cache hit on {key}')
        return wg_geometry_cache[key]
    else:
        # print(f'cache miss on {key}')
        g = WgGeometry(s=s)
        wg_geometry_cache[key] = g
        return g


def edge_ply_cutout(s: WgShell):
    w = s.w / 2

    # sf = WgShell(angle=s.angle, front=True)
    # sr = WgShell(angle=s.angle, rear=True)

    p_offset_up = P(length=s.edge_width / 2, angle=90 + side_angle_xz.angle())
    p_offset_left = P(length=3, angle=side_angle_xz.angle())

    p0 = P(w - p_offset_left.x, 0 - p_offset_left.y)

    p1 = p0 + p_offset_up
    p2 = p1 + p_offset_left

    p4 = p0 - p_offset_up
    p3 = p4 + p_offset_left

    # def halff(ss, w0, g, sign=1, ):
    #
    #     p_top = g.p5_arc[1]
    #
    #     ss = ss.segment((w0-p_offset.x, -p_offset.y), (p_top.x - 4, p_top.y * sign))
    #
    #     ss = ss.segment((p_top.x, p_top.y * sign))
    #
    #     ss = ss.segment((w0, 0))
    #
    #     return ss

    sk = cq.Sketch()

    sk = sk.segment((p0.x, p0.y), (p1.x, p1.y))
    sk = sk.segment((p2.x, p2.y))
    sk = sk.segment((p3.x, p3.y))
    sk = sk.segment((p4.x, p4.y))

    sk = sk.close()

    # sk = halff(sk, w0=w, g=wg_geometry_cached(s=sr), sign=1)
    # sk = halff(sk, w0=w, g=wg_geometry_cached(s=sf), sign=-1)

    # sk = sk.close()

    sk = sk.hull()
    # sk = sk.solve().assemble()

    return sk


def wg_2d_throat(s: WgShell):
    w = s.w / 2
    # d = s.d / 2
    #
    # e = s.edge_width / 2

    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    def halff(ss, w0, g, sign=1, ):
        ss = ss.segment((g.p0.x, g.p0.y * sign), (g.p1.x, g.p1.y * sign))

        ss = ss.arc((g.p2_arc[0].x, g.p2_arc[0].y * sign), (g.p2_arc[1].x, g.p2_arc[1].y * sign), "p2")

        ss = ss.segment((g.p3.x, g.p3.y * sign))

        ss = ss.arc((g.p4_arc[0].x, g.p4_arc[0].y * sign), (g.p4_arc[1].x, g.p4_arc[1].y * sign), "p4")

        # print(g.p4_arc[1])

        ss = ss.arc((g.p5_arc[0].x, g.p5_arc[0].y * sign), (g.p5_arc[1].x, g.p5_arc[1].y * sign), "p5")

        ss = ss.segment((w0, g.p6.y * sign))

        return ss

    sk = cq.Sketch()

    sk = halff(sk, w0=w, g=wg_geometry_cached(s=sr), sign=1)
    sk = halff(sk, w0=w, g=wg_geometry_cached(s=sf), sign=-1)

    sk = sk.solve().assemble()

    return sk


pt2522_width = 6.75
pt2522_x_offset = 3.35 / 2
pt2522_y_offset = 0.94 / 2
pt2522_h_offset = 1.73

CBT_RADIUS = 150
NUM_TWEETERS = 17

cheap_calculation = True
# cheap_calculation = False
# top_woofer = False
# top_woofer = True

TWEETER_HEIGHT = 9

DEGREES_PER_TWEETER = distance_across_circle(distance=TWEETER_HEIGHT, radius=CBT_RADIUS)
# TOP_WOOFER_DERGREES = distance_across_circle(distance=8.4, radius=CBT_RADIUS)
TOP_WOOFER_DERGREES = DEGREES_PER_TWEETER
# TOP_WOOFER_WG_DEGREES = distance_across_circle(distance=7.9, radius=CBT_RADIUS)
TOP_WOOFER_WG_DEGREES = DEGREES_PER_TWEETER
TOP_WOOFER_EDGE_DERGREES = distance_across_circle(distance=0.7, radius=CBT_RADIUS)

# CBT_DEGREES = 58.45
CBT_DEGREES = DEGREES_PER_TWEETER * NUM_TWEETERS

CBT_DEGREES_INCL_TOP_WOOFER = CBT_DEGREES + TOP_WOOFER_DERGREES

print(f"CBT_DEGREES={CBT_DEGREES}")

RENDERED_TWEETERS = [0, 1]
RENDERED_TWEETERS = [2, 3]
RENDERED_TWEETERS = [4, 5]

# RENDERED_TWEETERS = [6,7]
# RENDERED_TWEETERS = [8, 9]
#
# RENDERED_TWEETERS = [10, 11]
# RENDERED_TWEETERS = [12, 13]

# RENDERED_TWEETERS = [14, 15]
RENDERED_TWEETERS = [16]
# RENDERED_TWEETERS = [14, 15, 16]
RENDERED_TWEETERS = list(range(0, NUM_TWEETERS))

# DEGREES_PER_TWEETER = CBT_DEGREES / NUM_TWEETERS

CBT_COVERAGE_ANGLE = 90

CBT_THROAT_ROUNDOVER = 2.0
CBT_EDGE_ROUNDOVER = 2.0

# CBT_TOP_FACTOR = 0.4
CBT_TOP_FACTOR = 0.382
# CBT_TOP_FACTOR = 0.35

# CBT_BOTTOM_DEPTH = 28
# CBT_BOTTOM_WIDTH = 35
# CBT_BOT_EDGE_WIDTH = 10

# CBT_BOTTOM_DEPTH = 26
# CBT_BOTTOM_WIDTH = 36

CBT_BOTTOM_DEPTH = 28
CBT_BOTTOM_WIDTH = 36
CBT_LINEAR_WIDTH = False

# PLY_EDGE_THICKNESS_SINGLE = 0.9
PLY_EDGE_THICKNESS_SINGLE = 0.65
PLY_EDGE_THICKNESS = 2 * PLY_EDGE_THICKNESS_SINGLE

PLY_INNER_EDGE_STRAIGHT_DEGREES = -3.0

# CBT_EDGE_FORWARD_RATIO = 0.6
# CBT_BOT_EDGE_WIDTH = 8

CBT_EDGE_FORWARD_RATIO = 0.6
CBT_BOT_EDGE_WIDTH = 9
# CBT_BOT_EDGE_WIDTH = CBT_BOTTOM_DEPTH * CBT_TOP_FACTOR


CBT_TOP_EDGE_WIDTH = CBT_BOT_EDGE_WIDTH * CBT_TOP_FACTOR

CBT_TOP_WIDTH = CBT_BOTTOM_WIDTH * CBT_TOP_FACTOR
CBT_TOP_DEPTH = CBT_BOTTOM_DEPTH * CBT_TOP_FACTOR

CBT_BOT_TOP_WIDTH_DIFF = CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH
CBT_BOT_TOP_DEPTH_DIFF = CBT_BOTTOM_DEPTH - CBT_TOP_DEPTH
CBT_TOP_EDGE_WIDTH_DIFF = CBT_BOT_EDGE_WIDTH - CBT_TOP_EDGE_WIDTH

PLY_EDGE_DELTA_LESS_THAN_THICKNESS = 1.25 * PLY_EDGE_THICKNESS_SINGLE


def calculate_width(angle, lower_width=CBT_BOTTOM_DEPTH, upper_width=CBT_TOP_DEPTH,
                    angle_cutoff=CBT_DEGREES_INCL_TOP_WOOFER):
    return lower_width - (angle / angle_cutoff) * (lower_width - upper_width)


def calculate_side_width(angle):
    return calculate_width(angle, CBT_BOT_EDGE_WIDTH, CBT_TOP_EDGE_WIDTH)


# CBT_BOTTOM_EDGE_OFFSET = 0
# CBT_TOP_EDGE_OFFSET = 0
CBT_BOTTOM_EDGE_OFFSET = 110
CBT_TOP_EDGE_OFFSET = 110

CBT_BOTTOM_FRONT_XY = P(CBT_RADIUS - CBT_BOTTOM_EDGE_OFFSET, 0)
CBT_BOTTOM_REAR_XY = CBT_BOTTOM_FRONT_XY - P(calculate_side_width(0), 0)
CBT_TOP_FRONT_XY = P(angle=CBT_DEGREES_INCL_TOP_WOOFER, length=CBT_RADIUS - CBT_TOP_EDGE_OFFSET)
CBT_TOP_REAR_XY = CBT_TOP_FRONT_XY - P(angle=CBT_DEGREES_INCL_TOP_WOOFER,
                                       length=calculate_side_width(CBT_DEGREES_INCL_TOP_WOOFER))

# side_angle_xz = P(CBT_BOTTOM_FRONT_XY.x - CBT_TOP_REAR_XY.x, (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) / 2)
side_angle_xz = P(
    CBT_BOTTOM_FRONT_XY.x - CBT_TOP_REAR_XY.x,
    (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) / 2
)

side_x_ratio = side_angle_xz.length() / side_angle_xz.x


class Side:
    def __init__(self, angle):
        self.outer_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_FRONT_XY.length(),
                                                              upper_width=CBT_TOP_FRONT_XY.length(),
                                                              angle_cutoff=CBT_DEGREES_INCL_TOP_WOOFER))
        self.inner_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_REAR_XY.length(),
                                                              upper_width=CBT_TOP_REAR_XY.length(),
                                                              angle_cutoff=CBT_DEGREES_INCL_TOP_WOOFER))

        self.outer_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.outer_xy.x).y or 0

        self.inner_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.inner_xy.x).y or 0

        self.outer_z_ratio = 1 - (self.outer_z / side_angle_xz.y)
        self.inner_z_ratio = 1 - (self.inner_z / side_angle_xz.y)

        self.outer_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.outer_z_ratio + CBT_TOP_WIDTH
        self.inner_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.inner_z_ratio + CBT_TOP_WIDTH


def wg_shell_moved_sketch(r, slice_function=wg_2d_slice, flat=False, radius=CBT_RADIUS, **kwargs):
    s = WgShell(angle=r, flat=flat)

    p = s.center

    return (
        slice_function(s=s, **kwargs)
            .moved(
            cq.Location(
                cq.Plane.XY().rotated((-r, 0, 0)),
                cq.Vector(0, p.x, p.y)
            )
        )
    )


def wg_shell(radius_list, slice_function=wg_2d_slice, flat=False, radius=None, **kwargs):
    # step = 0.1
    # step = 1.0 / (NUM_TWEETERS)
    # step = 0.01
    # steps = int(1 / step)

    sketches = []

    # for f in [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]:
    # for i in range(1, steps + 1):

    # for t in range(0, NUM_TWEETERS + 1):
    for r in radius_list:
        # print(f"boop: {f}")
        # f = t * step
        # r = CBT_DEGREES * 0.1

        sketches.append(
            wg_shell_moved_sketch(r, slice_function, flat, radius, **kwargs)
        )

    b = (
        cq.Workplane("XY")
            .placeSketch(*sketches)
            .loft()
    )

    return b


def wg_vert(s=WgShell(angle=0)):
    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    gf = wg_geometry_cached(sf)
    gr = wg_geometry_cached(sr)

    base_height = 0.15

    # b = (
    #     cq.Workplane("XY")
    #         .placeSketch(wg_2d_slice(s))
    #         .extrude(base_height)
    # )
    #
    # sketches = []
    #
    # sketches.append(wg_2d_slice(s))
    #
    # sketches.append(
    #     cq.Sketch().rect(pt2522_width + 3, 5.0)
    #         .moved(
    #         cq.Location(
    #             cq.Vector(0, 0, pt2522_h_offset - base_height - 0.2)
    #         )
    #     )
    # )
    #
    # sketches.append(
    #     cq.Sketch().rect(pt2522_width + 2, 1.0)
    #         .moved(
    #         cq.Location(
    #             cq.Vector(0, 0, pt2522_h_offset - base_height)
    #         )
    #     )
    # )
    #
    # r = (
    #     cq.Workplane("XY")
    #         .placeSketch(*sketches)
    #         .loft()
    # )
    #
    # b = b.union(r.translate((0, 0, base_height)))

    def calc_vert_2(s, g, y_sign=1, step_offset=0.1):
        margin = 0.01
        bb_base = (
            cq.Workplane("XY")
                .lineTo(g.p5_arc[0].x, y_sign * 0)
                .lineTo(g.p5_arc[0].x, y_sign * g.p5_arc[0].y)
                .threePointArc((0, y_sign * s.d / 2), (-g.p5_arc[0].x, y_sign * g.p5_arc[0].y))
                .lineTo(-g.p5_arc[0].x, y_sign * 0)
                .close()
                .extrude(base_height + 0 * margin)
                .translate((0, 0, -margin))
        )

        step_y = 1.5
        # step_factor = step_y / g.p5_arc[0].y
        # curve_y = (s.d / 2) - g.p5_arc[0].y
        # step_curve_y = (s.d / 2) * step_factor

        bb = (
            cq.Workplane("XY")
                .lineTo(g.p5_arc[0].x, y_sign * 0)
                .lineTo(g.p5_arc[0].x, y_sign * g.p5_arc[0].y)
                .threePointArc((0, y_sign * s.d / 2), (-g.p5_arc[0].x, y_sign * g.p5_arc[0].y))
                .lineTo(-g.p5_arc[0].x, y_sign * 0)
                .close()
                .workplane(offset=pt2522_h_offset - base_height - step_offset)
                .lineTo(g.p5_arc[0].x, y_sign * 0)
                .lineTo(g.p5_arc[0].x, y_sign * step_y)
                # .threePointArc((0, y_sign * step_curve_y), (-g.p5_arc[0].x, y_sign * step_y))
                .lineTo(-g.p5_arc[0].x, y_sign * step_y)
                .lineTo(-g.p5_arc[0].x, y_sign * 0)
                .close()
                .workplane(offset=step_offset)
                .lineTo(g.p5_arc[0].x, y_sign * 0)
                .lineTo(g.p5_arc[0].x, y_sign * 0.5)
                .lineTo(-g.p5_arc[0].x, y_sign * 0.5)
                .lineTo(-g.p5_arc[0].x, y_sign * 0)
                .close()
                .loft(combine=True)
                .translate((0, 0, base_height - margin))
        )
        bb = bb.union(bb_base)
        return bb

    br = calc_vert_2(sr, gr, y_sign=1, step_offset=0.15)
    bf = calc_vert_2(sf, gf, y_sign=-1, step_offset=0.15)

    bb = br.union(bf)
    # bb = bf

    b = bb

    # bb = bb.translate((0, 0, -2))
    #
    # b = b.union(bb)

    g = wg_geometry_cached(s)

    b = (
        cq.Workplane("XY")
            .box(2 * g.p4_arc[1].x, s.d + 1, 3 * pt2522_h_offset)
            .intersect(b)
    )

    return b


def calc_wg_verts(shell=None):
    # b = wg_vert()
    b = None

    # for t in range(1, NUM_TWEETERS + 1):
    loop_list = RENDERED_TWEETERS + [max(RENDERED_TWEETERS) + 1]
    for t in loop_list:
        tr = t * DEGREES_PER_TWEETER

        ts = WgShell(angle=tr)
        wv = wg_vert(s=ts, symmetric=True)

        # wv_before = wv
        #
        # wv = wv.union(wv.rotate((0, 0, 0), (0, 1, 0), 180))
        #
        # if t == min(loop_list):
        #     wv = wv.cut((
        #         wv_before
        #             .rotate((0, 0, 0), (0, 1, 0), 180)
        #     ))
        # elif t == max(loop_list):
        #     wv = wv.cut((
        #         wv_before
        #     ))

        if shell:
            wv = wv.shell(shell)

        #     wv_after = wv_before.rotate((0, 0, 0), (0, 1, 0), 180)
        # else:
        #     # wv = wv.mirror(mirrorPlane='XY', union=True)
        #     wv_after = wv.union(wv.rotate((0, 0, 0), (0, 1, 0), 180))

        wv = wv.rotate((0, 0, 0), (1, 0, 0), -tr)
        wv = wv.translate((0, ts.center.x, ts.center.y))

        if b:
            b = b.union(wv)
        else:
            b = wv

    return b


def tweeters(flanges=False, **kwargs):
    b = None

    for t in RENDERED_TWEETERS:
        r = (t + 0.5) * DEGREES_PER_TWEETER

        if flanges:
            w = drivers.pt2522_flanges(
                **kwargs
            )
        else:
            w = drivers.pt2522(
                **kwargs
            )

        if t % 2 == 1:
            w = w.rotate((0, 0, 0), (0, 0, 1), 180)

        s = WgShell(angle=r)

        w = w.rotate((0, 0, 0), (1, 0, 0), -90)
        w = w.rotate((0, 0, 0), (0, 1, 0), 180)

        w = w.rotate((0, 0, 0), (1, 0, 0), -r)
        w = w.translate((0, s.center.x, s.center.y))

        if b:
            b = b.union(w)
        else:
            b = w

    return b


def place_woofer(ws, angle, shape, s=None):
    w = shape
    if s is None:
        s = WgShell(angle=angle)

    w = w.rotate((0, 0, 0), (1, 0, 0), -90)

    if ws.angle and ws.angle != 0:
        w = w.rotate((0, 0, 0), (0, 0, 1), -ws.angle)
    if ws.z_angle and ws.z_angle != 0:
        w = w.rotate((0, 0, 0), (1, 0, 0), ws.z_angle)

    w = w.translate((ws.p_offset.x, 0, 0))
    w = w.translate((0, 0, ws.p_offset.y))
    w = w.translate((0, -ws.z_offset - ws.base_thickness, 0))

    w = w.rotate((0, 0, 0), (1, 0, 0), -angle)
    w = w.translate((0, s.center.x, s.center.y))

    return w


def calc_woofer_edge_cutout(ws, union_rear=True, t=6, front_h_offset=0.0):
    # front_h = ws.magnet_height - 2.5
    front_h = ws.front_slot_height + front_h_offset
    rear_h = ws.basket_height + 1
    front_shelf = 1.0
    # h = ws.magnet_height * 2 - 2
    r = (
        cq.Workplane("XY")
            .workplane(offset=0)
            .rect(ws.diameter + 0.5, t)
            .workplane(offset=0.25)
            .rect(ws.diameter + 0.5, t)
            .workplane(offset=ws.magnet_height + 0.25)
            .rect(ws.magnet_diameter + 1.0, t)
            .loft(combine=True)
            .edges(">Z and (not <Y) and (not >Y)")
            .fillet(1)
            .rotate((0, 0, 0), (0, 0, 1), 90)
    )
    x = ws.surround_diameter * 0.5
    fb = (
        cq.Workplane("XY")
            .workplane(offset=0)
            .rect(t, ws.surround_diameter)
            # .moveTo(-t/2, x)
            # .lineTo(t/2, x)
            # .threePointArc((ws.surround_diameter/2, 0), (t/2, -x))
            # .lineTo(-t/2, -x)
            # .threePointArc((-ws.surround_diameter/2, 0), (-t/2, x))
            # .close()
            .extrude(-front_shelf)
        # .rotate((0, 0, 0), (0, 0, 1), 90)
    )
    f = (
        cq.Workplane("XY")
            .workplane(offset=0)
            .rect(t, ws.surround_diameter)
            # .moveTo(-t/2, x)
            # .lineTo(t/2, x)
            # .threePointArc((ws.surround_diameter/2, 0), (t/2, -x))
            # .lineTo(-t/2, -x)
            # .threePointArc((-ws.surround_diameter/2, 0), (-t/2, x))
            # .close()
            .workplane(offset=front_h - 0.5)
            .rect(t, ws.surround_diameter * 0.75)
            .loft(combine=True)
            .edges(">Z and (not <X) and (not >X)")
            .fillet(2)
            # .rotate((0, 0, 0), (0, 0, 1), 90)
            .rotate((0, 0, 0), (0, 1, 0), 180)
            .translate((0, 0, -front_shelf))
    )
    f = f.union(fb)
    b = f
    if union_rear:
        b = b.union(r)
    return b


def get_ws_woofer(ws):
    return ws.get_woofer()


def woofers(woofer_function=get_ws_woofer, **kwargs):
    b = None

    t_list = [] + RENDERED_TWEETERS
    if top_woofer:
        t_list.append(NUM_TWEETERS)

    # print(f"t_list: {t_list}")

    for t in t_list:
        if t % 2 == 1:
            continue

        # print(f"ws_i: {ws_i}")

        ws = woofer_stats[int(t / 2)]

        # r = (t + 0.5) * DEGREES_PER_TWEETER
        r = (t + 1.0) * DEGREES_PER_TWEETER

        # w = ws.woofer
        w = woofer_function(ws, **kwargs)

        if ws.rotation_angle > 0:
            w = w.rotate((0, 0, 0), (0, 0, 1), ws.rotation_angle)

        w = w.rotate((0, 0, 0), (0, 0, 1), 180)

        w = place_woofer(ws, r, w)

        if ws.dual and RENDER_WOOFERS_MIRROR:
            w = w.mirror(mirrorPlane='YZ', union=True)

        if b:
            b = b.union(w)
        else:
            b = w

    return b


def ply_support_middle(thickness=PLY_EDGE_THICKNESS, z_offset=-1.2):
    max_r = CBT_DEGREES + TOP_WOOFER_DERGREES

    # print(f"max_r: {max_r}")

    r0 = 0
    s0 = WgShell(angle=r0)
    wgg0 = wg_geometry_cached(s=s0)

    r1 = min_r + (max_r - min_r) / 2
    s1 = WgShell(angle=r1)
    wgg1 = wg_geometry_cached(s=s1)

    r2 = max_r
    s2 = WgShell(angle=r2)
    wgg2 = wg_geometry_cached(s=s2)

    d_margin = 2

    d0 = wgg0.p4_arc[0].y * 2 - d_margin
    d1 = wgg1.p4_arc[0].y * 2 - d_margin
    d2 = wgg2.p4_arc[0].y * 2 - d_margin

    p_angle = -90 - P(wgg2.p4_arc[0].x - wgg0.p4_arc[0].x, s2.center.y).angle()

    # print(f"p_angle: {p_angle}")

    y_stretch = P(x=1, angle=-p_angle).length()
    # print(f"y_stretch: {y_stretch}")

    p0f = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS + d0 / 2), angle=r0)
    p0b = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS - d0 / 2), angle=r0)

    p1f = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS + d1 / 2), angle=r1)
    p1b = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS - d1 / 2), angle=r1)

    p2f = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS + d2 / 2), angle=r2)
    p2b = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS - d2 / 2), angle=r2)

    b = (
        cq.Workplane("XY")
            .center(0, 0)
            .lineTo(p0f.x * y_stretch, p0f.y * y_stretch)

            .threePointArc((p1f.x * y_stretch, p1f.y * y_stretch), (p2f.x * y_stretch, p2f.y * y_stretch))

            .lineTo(p2b.x * y_stretch, p2b.y * y_stretch)

            .threePointArc((p1b.x * y_stretch, p1b.y * y_stretch), (p0b.x * y_stretch, p0b.y * y_stretch))

            .close()
    )

    b = b.extrude(thickness)
    b = b.translate((0, 0, z_offset))

    b = b.rotate((0, 0, 0), (1, 0, 0), p_angle)

    return b


def ply_support_edge(
        thickness=PLY_EDGE_THICKNESS_SINGLE,
        z_offset=-1.2,
        margin=0.0,
        chamfer=None,

        min_r=0,
        max_r=(CBT_DEGREES + TOP_WOOFER_DERGREES),

        margin0=0.0,
        margin1=0.0,
        margin2=0.0,

        radius_0_margin=0.0,
        radius_1_margin=0.0,
        radius_2_margin=0.0,
):
    # max_r = CBT_DEGREES + TOP_WOOFER_DERGREES

    # print(f"min_r: {min_r}")
    # print(f"max_r: {max_r}")

    r0 = min_r
    s0 = WgShell(angle=r0)
    wgg0 = wg_geometry_cached(s=s0)

    r1 = min_r + (max_r - min_r) / 2
    s1 = WgShell(angle=r1)
    wgg1 = wg_geometry_cached(s=s1)

    r2 = max_r
    s2 = WgShell(angle=r2)
    wgg2 = wg_geometry_cached(s=s2)

    # d_margin = 2

    d0 = wgg0.p4_arc[1].y * 2 + margin + margin0
    d1 = wgg1.p4_arc[1].y * 2 + margin + margin1
    d2 = wgg2.p4_arc[1].y * 2 + margin + margin2

    ar = side_x_ratio

    p0f = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS + radius_0_margin + d0 / 2), angle=r0)
    # p0m = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS), angle=r0)
    p0b = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS + radius_0_margin - d0 / 2), angle=r0)

    p1f = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS + radius_1_margin + d1 / 2), angle=r1)
    p1b = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS + radius_1_margin - d1 / 2), angle=r1)

    p2f = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS + radius_2_margin + d2 / 2), angle=r2)
    # p2m = P(x=CBT_RADIUS, y=0) + P(length=-CBT_RADIUS, angle=r2)
    p2b = P(x=CBT_RADIUS, y=0) + P(length=-(CBT_RADIUS + radius_2_margin - d2 / 2), angle=r2)

    # x0 = wgg0.p4_arc[1].x
    # x2 = wgg2.p4_arc[1].x
    #
    # angle = P(x=x2-x0, y=p2m)

    b = (
        cq.Workplane("XY")
            .moveTo(p0f.x * ar, p0f.y)
            # .lineTo(p0f.x * ar, p0f.y)

            .threePointArc((p1f.x * ar, p1f.y), (p2f.x * ar, p2f.y))

            .lineTo(p2b.x * ar, p2b.y)

            .threePointArc((p1b.x * ar, p1b.y), (p0b.x * ar, p0b.y))

            .close()
    )

    b = b.extrude(thickness)

    if chamfer is not None:
        b = (
            b
                # .faces("<Z or >Z")
                .faces("|Z")
                .chamfer(chamfer)
        )

    b = b.translate((0, 0, z_offset))

    # b = b.rotate((0, 0, 0), (0, 1, 0), -side_angle_xz.angle())

    # b = b.rotateAboutCenter((1, 1, 0), -1)

    return b


class WooferStats:
    def __init__(self,
                 ordinal,
                 p_offset=None,
                 woofer_type='nd105',
                 dual=True,
                 cbt_angle=0.0,
                 z_offset=0.0,
                 angle=60,
                 z_angle=0,
                 rotation_angle=0,
                 x_offset=0.05
                 ):

        self.ordinal = ordinal
        self.z_offset = z_offset
        self.angle = angle
        self.cbt_angle = cbt_angle
        self.z_angle = z_angle
        self.rotation_angle = rotation_angle

        self.screw_hole_diameter = 0.42

        self.woofer_type = woofer_type
        self.dual = dual

        self.x_offset = x_offset

        self.m4_heat_insert_diameter = 0.63

        self.woofer = None
        self.woofer_margin = None

        if woofer_type == 'nd140':
            self.diameter = 13.8
            self.max_diameter = 15.45
            self.cutout_diameter = 12.0
            self.base_thickness = 0.15
            self.screw_holes_diameter = 13.8
            self.sd = 86.6
            self.surround_diameter = 11.7
            self.basket_height = 3.7
            self.magnet_diameter = 4.22
            self.magnet_height = 6.25
            self.front_slot_height = 3.5
        elif woofer_type == 'nd105':
            self.diameter = 10.5
            self.max_diameter = 12.0
            self.cutout_diameter = 9.3
            self.base_thickness = 0.1
            self.screw_holes_diameter = 10.85
            self.sd = 51.5
            self.rear_slot_sd = self.sd
            self.surround_diameter = 9.3
            self.basket_height = 3.0
            self.magnet_diameter = 4.22
            self.magnet_height = 5.7
            self.front_slot_height = 3.0
        elif woofer_type == 'nd91':
            self.diameter = 9.3
            self.max_diameter = 10.35
            self.cutout_diameter = 7.65
            self.base_thickness = 0.1
            self.screw_holes_diameter = 8.84
            self.sd = 30.4
            self.rear_slot_sd = self.sd * 1.2
            self.surround_diameter = 7.4
            self.basket_height = 2.75
            self.magnet_diameter = 4.22
            self.magnet_height = 4.49
            self.front_slot_height = 2.0
        elif woofer_type == 'nd65':
            self.diameter = 6.4
            self.max_diameter = 7.55
            self.cutout_diameter = 5.2
            self.base_thickness = 0.1
            self.screw_holes_diameter = 6.6
            self.sd = 15.6
            self.rear_slot_sd = self.sd * 1.4
            self.surround_diameter = 5.15
            self.basket_height = 1.9
            self.magnet_diameter = 3.45
            self.magnet_height = 4.3
            self.front_slot_height = 1.5
        elif woofer_type == 'nd64':
            self.diameter = 6.4
            self.max_diameter = 7.55
            self.cutout_diameter = 5.2
            self.base_thickness = 0.1
            self.screw_holes_diameter = 6.6
            self.sd = 15.6
            self.rear_slot_sd = self.sd * 1.4
            self.surround_diameter = 5.15
            self.basket_height = 1.9
            self.magnet_diameter = 3.45
            self.magnet_height = 3.45
            self.front_slot_height = 1.5

        screw_holes = []
        for r in [45, 45 + 90, 45 + 180, 45 + 270]:
            p = P(angle=r, length=self.screw_holes_diameter / 2)
            screw_holes.append(p.values())
        self.screw_holes = screw_holes

        self.p_offset = p_offset or P(x=self.diameter / 2 + pt2522_width / 2 + x_offset, y=0)

    def get_woofer(self):
        if self.woofer is None:
            if self.woofer_type == 'nd140':
                self.woofer = drivers.nd140()
            elif self.woofer_type == 'nd105':
                self.woofer = drivers.nd105()
            elif self.woofer_type == 'nd91':
                self.woofer = drivers.nd91()
            elif self.woofer_type == 'nd65':
                self.woofer = drivers.nd65()
            elif self.woofer_type == 'nd64':
                self.woofer = drivers.nd64()
        return self.woofer

    def get_woofer_margin(self):
        base_margin_cutout_depth = 2.0
        if self.woofer_margin is None:
            if self.woofer_type == 'nd140':
                self.woofer_margin = drivers.nd140(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
            elif self.woofer_type == 'nd105':
                self.woofer_margin = drivers.nd105(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
            elif self.woofer_type == 'nd91':
                self.woofer_margin = drivers.nd91(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
            elif self.woofer_type == 'nd65':
                self.woofer_margin = drivers.nd65(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
            elif self.woofer_type == 'nd64':
                self.woofer_margin = drivers.nd64(
                    margin=True,
                    base_margin_cutout_depth=base_margin_cutout_depth,
                    screw_hole_diameter=self.m4_heat_insert_diameter,
                    screw_hole_depth=1.0,
                )
        return self.woofer_margin


woofer_angle = 0
# woofer_z_offset = 0.75
woofer_z_offset = 1.0

woofer_z_top_offset = 0.0
wzto = woofer_z_top_offset / 17

dpt = DEGREES_PER_TWEETER

woofer_stats = [
    WooferStats(ordinal=0, cbt_angle=1 * dpt, z_offset=1 * wzto + woofer_z_offset, woofer_type='nd140',
                angle=woofer_angle,
                x_offset=0.75),
    WooferStats(ordinal=1, cbt_angle=3 * dpt, z_offset=3 * wzto + woofer_z_offset, woofer_type='nd140',
                angle=woofer_angle,
                x_offset=0.5),
    WooferStats(ordinal=2, cbt_angle=5 * dpt, z_offset=5 * wzto + woofer_z_offset, woofer_type='nd140',
                angle=woofer_angle,
                x_offset=0.25),
    WooferStats(ordinal=3, cbt_angle=7 * dpt, z_offset=7 * wzto + woofer_z_offset, woofer_type='nd105',
                angle=woofer_angle,
                x_offset=1.0),
    WooferStats(ordinal=4, cbt_angle=9 * dpt, z_offset=9 * wzto + woofer_z_offset, woofer_type='nd105',
                angle=woofer_angle,
                x_offset=0.5),
    WooferStats(ordinal=5, cbt_angle=11 * dpt, z_offset=11 * wzto + woofer_z_offset, woofer_type='nd91',
                angle=woofer_angle,
                x_offset=0.5),
    WooferStats(ordinal=6, cbt_angle=13 * dpt, z_offset=13 * wzto + woofer_z_offset, woofer_type='nd91',
                angle=woofer_angle,
                x_offset=0.0),
    WooferStats(ordinal=7, cbt_angle=15 * dpt, z_offset=15 * wzto + woofer_z_offset, woofer_type='nd65',
                angle=woofer_angle,
                x_offset=0.0),
    WooferStats(ordinal=8, cbt_angle=17 * dpt, z_offset=17 * wzto + woofer_z_offset, woofer_type='nd65',
                angle=woofer_angle,
                p_offset=P(0, 3.5), dual=False),
]


# woofer_stats = [
#     WooferStats(cbt_angle=1 * dpt, z_offset=1 * wzto + woofer_z_offset, woofer_type='nd105', angle=woofer_angle,
#                 x_offset=0.0),
#     WooferStats(cbt_angle=3 * dpt, z_offset=3 * wzto + woofer_z_offset, woofer_type='nd105', angle=woofer_angle,
#                 x_offset=0.0),
#     WooferStats(cbt_angle=5 * dpt, z_offset=5 * wzto + woofer_z_offset, woofer_type='nd105', angle=woofer_angle,
#                 x_offset=0.0),
#     WooferStats(cbt_angle=7 * dpt, z_offset=7 * wzto + woofer_z_offset, woofer_type='nd91', angle=woofer_angle,
#                 x_offset=1.0),
#     WooferStats(cbt_angle=9 * dpt, z_offset=9 * wzto + woofer_z_offset, woofer_type='nd91', angle=woofer_angle,
#                 x_offset=0.5),
#     WooferStats(cbt_angle=11 * dpt, z_offset=11 * wzto + woofer_z_offset, woofer_type='nd65', angle=woofer_angle,
#                 x_offset=0.5),
#     WooferStats(cbt_angle=13 * dpt, z_offset=13 * wzto + woofer_z_offset, woofer_type='nd65', angle=woofer_angle,
#                 x_offset=0.0),
#     WooferStats(cbt_angle=15 * dpt, z_offset=15 * wzto + woofer_z_offset, woofer_type='nd65', angle=woofer_angle,
#                 x_offset=-0.15),
#     WooferStats(cbt_angle=17 * dpt, z_offset=17 * wzto + woofer_z_offset, woofer_type='nd65', angle=woofer_angle,
#                 p_offset=P(0, 3.5), dual=False),
# ]


def calc_woofer_front_slots(
        ws,

        mid_platform_z=0.5,
        top_platform_z=3.0,

        wg_horz_angle=15,
        wg_vert_angle=8,

        # x_mid_offset = 1.0,
        x_top_offset=1.25,
        y_top_offset=-1.5,

        slot_sd_ratio=0.4,

        # x_top = 1.5,
        surround_depth=0.6,
):
    y_mid = ws.cutout_diameter * 0.9
    y_top = ws.cutout_diameter * 0.9

    x_top = ((ws.sd / 2) * slot_sd_ratio) / y_top
    x_mid = x_top * 1.0

    s = (
        cq.Workplane("XY")
            .circle(ws.cutout_diameter / 2)
            .extrude(surround_depth)
        # .edges(">Z")
        # .chamfer(0.5)
    )

    l = x_mid / 2

    pt = P(x=0, y=y_mid / 2 - l)

    p0 = pt + P(length=l, angle=90)
    p1 = pt + P(length=l, angle=45)
    p2 = pt + P(length=l, angle=0)

    c = (
        cq.Workplane("XY")
            # .workplane(offset=0)
            # .moveTo(0, ws.cutout_diameter / 2)
            # .threePointArc((ws.cutout_diameter / 2, 0), (0, -ws.cutout_diameter / 2))
            # .close()
            .workplane(offset=surround_depth)
            .moveTo(0, ws.cutout_diameter / 2)
            .threePointArc((ws.cutout_diameter / 2, 0), (0, -ws.cutout_diameter / 2))
            .close()
            # .circle(ws.cutout_diameter / 2)
            .workplane()
            .transformed(
            offset=cq.Vector(x_top_offset, y_top_offset, top_platform_z),
            rotate=cq.Vector(wg_horz_angle, 30, -wg_vert_angle)
        )
            .moveTo(p0.x, p0.y)
            .threePointArc((p1.x, p1.y), (p2.x, p2.y))
            .lineTo(p2.x, -p2.y)
            .threePointArc((p1.x, -p1.y), (p0.x, -p0.y))
            .threePointArc((-p1.x, -p1.y), (-p2.x, -p2.y))
            .lineTo(-p2.x, p2.y)
            .threePointArc((-p1.x, p1.y), (-p0.x, p0.y))
            .close()
            .workplane(offset=top_platform_z - mid_platform_z)
            .moveTo(p0.x, p0.y)
            .threePointArc((p1.x, p1.y), (p2.x, p2.y))
            .lineTo(p2.x, -p2.y)
            .threePointArc((p1.x, -p1.y), (p0.x, -p0.y))
            .threePointArc((-p1.x, -p1.y), (-p2.x, -p2.y))
            .lineTo(-p2.x, p2.y)
            .threePointArc((-p1.x, p1.y), (-p0.x, p0.y))
            .close()
            .loft(combine=True)
            .mirror(mirrorPlane='ZY', union=True)
    )

    s = s.union(c)

    s = s.rotate((0, 0, 0), (1, 0, 0), 180)
    # c = c.rotate((0, 0, 0), (1, 0, 0), 180)

    return s


def calc_flat_middle_plates(function):
    b = None

    for t in RENDERED_TWEETERS:
        if t % 2 == 1:
            continue

        ws = woofer_stats[int(t / 2)]

        r = (t + 1.0) * DEGREES_PER_TWEETER

        # w = flat_middle_plate(r, ws)
        w = function(r, ws)

        s = WgShell(angle=r)

        w = w.rotate((0, 0, 0), (1, 0, 0), 180)
        w = w.rotate((0, 0, 0), (1, 0, 0), -90)

        w = w.rotate((0, 0, 0), (1, 0, 0), -r)
        w = w.translate((0, s.center.x, s.center.y))

        if b:
            b = b.union(w)
        else:
            b = w

    return b


def move_degree(angle, shape):
    ts = WgShell(angle=angle)

    shape = shape.rotate((0, 0, 0), (1, 0, 0), -angle)
    shape = shape.translate((0, ts.center.x, ts.center.y))

    return shape


@cq_cache()
def calc_vert(angle, top=False, bot=False, shell=None):
    ts = WgShell(angle=angle)
    wv = wg_vert(s=ts)

    if shell:
        wv = wv.cut(wv.translate((0, 0, shell)))
        # wv = wv.shell(shell)

    if top and bot:
        wv = wv.union(wv.rotate((0, 0, 0), (0, 1, 0), 180))
    elif bot:
        pass
    elif top:
        wv = wv.rotate((0, 0, 0), (0, 1, 0), 180)

    wv = wv.rotate((0, 0, 0), (1, 0, 0), -angle)
    wv = wv.translate((0, ts.center.x, ts.center.y))

    return wv


def flat_woofer_plate(ws: WooferStats, w=30, h=30, circle_union=True, front_surround_cut=False,
                      front_screw_union=False):
    t = 1.3

    b = (
        cq.Workplane("XY")
            .center(-w / 2, -h / 2)
            .lineTo(w, 0)
            .lineTo(w, h)
            .lineTo(0, h)
            .close()
            .extrude(t)
    )

    if circle_union:
        b = b.union(
            (
                cq.Workplane("XY")
                    .center(0, 0)
                    .circle(ws.cutout_diameter / 2 + 1)
                    .extrude(5)
            )
        )

    if front_surround_cut:
        b = b.cut(
            (
                cq.Workplane("XY")
                    .center(0, 0)
                    .circle(ws.cutout_diameter / 2)
                    .extrude(5)
            )
        )

    if front_screw_union:
        for i in range(0, 4):
            r = 45 + 90 * i
            p_hole = P(length=ws.screw_holes_diameter / 2, angle=r)
            b = b.union(
                (
                    cq.Workplane("XY")
                        .center(p_hole.x, p_hole.y)
                        .circle(1.3 / 2)
                        .extrude(1.5)
                        .edges(">Z or <Z")
                        .fillet(0.2)
                )
            )

    return b


def calc_wg_pegs():
    pegs = None

    for i in range(1, 5):
        r = DEGREE_LIST[0] + (i / 5) * (DEGREE_LIST[2] - DEGREE_LIST[0])

        for sign in [1, -1]:

            s = (WgShell(angle=r, front=sign == 1, rear=sign == -1))
            wgg = wg_geometry_cached(s=s)

            peg = move_degree(r, (
                cq.Workplane("XY")
                    .center(0, 0)
                    .circle(0.6 / 2)
                    .extrude(6)
                    .translate((0, 0, -PLY_EDGE_THICKNESS / 2))
                    # .translate((0, 0, 1*sign))
                    .rotate((0, 0, 0), (0, 1, 0), -90)
                    .translate((0, 0.75 * 1 if sign == 1 else 0.1, 0))
                    .rotate((0, 0, 0), (0, 0, 1), side_angle_xz.angle())
                    .translate((wgg.p5_arc[1].x, wgg.p5_arc[1].y * sign, 0))
            ))

            if pegs is None:
                pegs = peg
            else:
                pegs = pegs.union(peg)

    return pegs


def calc_wg_ply_middle(
        bottom_part=False,
        top_part=False,
):
    ws = woofer_stats[int(RENDERED_TWEETERS[0] / 2)]

    s0 = WgShell(angle=DEGREE_LIST[0])
    # wgg0 = wg_geometry_cached(s=s0)

    s1 = WgShell(angle=DEGREE_LIST[1])
    # wgg1 = wg_geometry_cached(s=s1)

    s2 = WgShell(angle=DEGREE_LIST[2])
    # wgg2 = wg_geometry_cached(s=s2)

    # print(f"{bottom_part}, {top_part}")

    ply_z_offset = ws.z_offset + ws.base_thickness + PLY_EDGE_THICKNESS_SINGLE / 2
    h = actual_distance_across_circle(DEGREE_LIST[2] - DEGREE_LIST[0], CBT_RADIUS + ply_z_offset) - 0.025

    overlap_h = 0.0

    bottom_h_overlap = 0 if bottom_part else overlap_h
    top_h_overlap = 0 if top_part else overlap_h

    base = (
        cq.Workplane("XY")
            .center(0, 0)
            .rect(s0.w, PLY_EDGE_THICKNESS_SINGLE)
            .workplane(offset=h / 2 + bottom_h_overlap)
            .rect(s1.w, PLY_EDGE_THICKNESS_SINGLE)
            .workplane(offset=h / 2 + top_h_overlap)
            .rect(s2.w, PLY_EDGE_THICKNESS_SINGLE)
            .loft(combine=True)
            .cut((
            cq.Workplane("XY")
                .box(100, 100, 100)
                .translate((-100 / 2 - 0.1, 0, 0))
        ))
            .translate((0, 0, -h / 2 - bottom_h_overlap))
    )

    def calc_pegs(peg_w2, peg_h2, num_pegs2, peg_margin):
        pegs = (
            cq.Workplane("XY")
                .box(peg_w2 + peg_margin, PLY_EDGE_THICKNESS_SINGLE, 2 * (peg_h2 + peg_margin / 2))
        )

        p = pegs

        for i in range(1, num_pegs2):
            pegs = pegs.union((
                p.translate(((2 * i * peg_w2), 0, 0))
            ))

        return pegs

    peg_w = 0.8
    peg_h = peg_w

    peg_margin = 0.075

    if RENDER_PLY_MIDDLE_PEGS:
        # print("render middle pegs...")

        pegs_list = []

        if not bottom_part:
            pegs_list.append(True)
        if not top_part:
            pegs_list.append(False)

        for bottom_pegs in pegs_list:
            peggable_w = s0.w if bottom_pegs else s2.w
            h_offset = -h / 2 if bottom_pegs else h / 2

            tweet_w_offset = 7 / 2 + peg_w

            peggable_w_minus_tweet = peggable_w / 2 - tweet_w_offset

            num_pegs = math.floor(peggable_w_minus_tweet / (2 * peg_w))

            positive_offet = 0 if bottom_pegs else peg_w
            negative_offet = peg_w if bottom_pegs else 0

            # Positive
            base = base.union(
                calc_pegs(peg_w, peg_h, num_pegs, peg_margin=-peg_margin / 2)
                    .translate((tweet_w_offset + positive_offet, 0, h_offset))
            )
            # Negative
            base = base.cut(
                calc_pegs(peg_w, peg_h, num_pegs, peg_margin=peg_margin / 2)
                    .translate((tweet_w_offset + negative_offet, 0, h_offset))
            )

            # print("render middle pegs completed.")

    b = move_degree(DEGREE_LIST[1], (
        base.translate((0, -ply_z_offset, 0))
    ))

    ##########################

    for edge in ply_supp_edges:
        b = b.cut(edge)

    ##########################

    def calc_middle_pegs(x_margin=0.0, y_margin=0.0, z_margin=0.0, add_slot_if_single=False):

        ep_h = h - peg_h
        ep_r = distance_across_circle(ep_h, CBT_RADIUS)

        single_edge_peg_r = ep_r / 10
        single_edge_peg_y = actual_distance_across_circle(single_edge_peg_r, CBT_RADIUS)

        min_edge_peg_r = DEGREE_LIST[1] - ep_r / 2
        ep = None

        # print(f"ep_h: '{ep_h}', ep_r: '{ep_r}', d: '{d}', sr: '{sr}'")

        for i in range(0, 5):
            # for current_peg_r in [d, d+sr]:
            current_peg_r = min_edge_peg_r + (1 + 2 * i) * single_edge_peg_r
            y = -ep_h / 2 + (1 + 2 * i) * single_edge_peg_y

            # print(f"current_peg_r: '{current_peg_r}'")

            outer_offset, inner_offset = get_ply_supp_edge_outer_z_offsets(current_peg_r)

            is_single = outer_offset == inner_offset

            # print(f"edge_pegs: '{edge_pegs}'")
            offsets = []
            if outer_offset:
                offsets.append(outer_offset + PLY_EDGE_THICKNESS_SINGLE)
            if inner_offset:
                offsets.append(inner_offset - PLY_EDGE_THICKNESS_SINGLE)

            peg_y = PLY_EDGE_THICKNESS_SINGLE + y_margin
            peg_z = single_edge_peg_y + z_margin
            for x in offsets:
                p = (
                    cq.Workplane("XY")
                        .box(3 * PLY_EDGE_THICKNESS_SINGLE + x_margin, peg_y, peg_z)
                        .translate((x, 0, y))
                )

                ep = p if ep is None else ep.union(p)

            if add_slot_if_single and is_single:
                x = outer_offset
                p = (
                    cq.Workplane("XY")
                        .box(max(x_margin, 0.05), 2 * peg_y, 2 * peg_z)
                        .translate((x + PLY_EDGE_THICKNESS_SINGLE / 2, 0, y))
                )

                ep = p if ep is None else ep.cut(p)

        ep = move_degree(DEGREE_LIST[1], (
            ep.translate((0, -ply_z_offset, 0))
        ))
        return ep

    if RENDER_PLY_MIDDLE_PEGS:
        # print("render middle edge pegs...")

        b = b.union(calc_middle_pegs(x_margin=-peg_margin, add_slot_if_single=True))

        if top_part:
            max_i = sorted(ply_supp_edge_degrees.keys())[-1]
            x = ply_supp_edge_degrees[max_i]['x']
            p_cut = (
                cq.Workplane("XY")
                    .box(peg_margin, 100, 100)
                    .translate((x + PLY_EDGE_THICKNESS_SINGLE / 2, 0, 0))
            )
            p_cut = move_degree(DEGREE_LIST[1], (
                p_cut.translate((0, -ply_z_offset, 0))
            ))
            b = b.cut(p_cut)

        full_pegs = calc_middle_pegs(y_margin=peg_margin, z_margin=peg_margin)

        for i in range(0, len(ply_supp_edges)):
            ply_supp_edges[i] = ply_supp_edges[i].cut(full_pegs)

        # print("render middle edge pegs completed.")

    ###############

    for t in RENDERED_TWEETERS:
        r = (t + 0.5) * DEGREES_PER_TWEETER

        s = WgShell(angle=r, radius=CBT_RADIUS)

        w = (
            cq.Workplane("XY")
                .rect(5.5, 6.75)
                .extrude(10)
                .translate((0, 0, -5))
        )

        w = w.rotate((0, 0, 0), (1, 0, 0), -90)
        w = w.rotate((0, 0, 0), (0, 1, 0), 180)

        w = w.rotate((0, 0, 0), (1, 0, 0), -s.angle)
        w = w.translate((0, s.center.x, s.center.y))

        b = b.cut(w)

    ########################

    # print("render woofer_cut...")
    woofer_cut = place_woofer(
        ws=ws,
        angle=DEGREE_LIST[1],
        shape=((
            cq.Workplane("XY")
                .circle(ws.cutout_diameter / 2)
                .extrude(6)
                .translate((0, 0, -3))
        )),
    )

    b = b.cut(woofer_cut)
    # print("render woofer_cut completed.")

    ##################

    b = b.mirror(mirrorPlane='YZ', union=True)

    return b


def calc_wg(
        topmost_part=False
):
    ws = woofer_stats[int(RENDERED_TWEETERS[0] / 2)]

    r00 = 0
    s00 = WgShell(angle=r00)

    r0 = DEGREE_LIST[0]
    s0 = WgShell(angle=r0)
    g0 = wg_geometry_cached(s=s0)

    r1 = DEGREE_LIST[1]
    s1 = WgShell(angle=r1)
    g1 = wg_geometry_cached(s=s1)

    r2 = DEGREE_LIST[2]
    s2 = WgShell(angle=r2)
    g2 = wg_geometry_cached(s=s2)

    rt1 = r0 + (r1 - r0) / 2
    rt2 = r1 + (r2 - r1) / 2

    max_shell_thickness = 1.0
    # min_shell_thickness = 0.6

    shell_thickness = max_shell_thickness * (s1.w / s00.w)
    # shell_thickness = 0.5

    h = s0.center.distance(s2.center)

    # wg_outer_shape = wg_shell(
    #     radius_list=DEGREE_LIST,
    #     slice_function=wg_2d_slice,
    # )

    # wg_outer_shape_negative = move_degree(r1, (
    #     cq.Workplane("XY")
    #         .box(1000, 1000, 1000)
    # )).cut(wg_outer_shape)

    # return wg_outer_shape, wg_outer_shape_negative, None

    wg_full_shape = wg_shell(
        radius_list=DEGREE_LIST,
        slice_function=wg_2d_slice,
    )

    wg_full_shape_negative = move_degree(r1, (
        cq.Workplane("XY")
            .box(1000, 1000, 1000)
    )).cut(wg_full_shape)

    wg_2d_throat_shape = wg_shell(
        radius_list=DEGREE_LIST,
        slice_function=wg_2d_throat,
    )

    wg_2d_throat_shape_negative = move_degree(r1, (
        cq.Workplane("XY")
            .box(1000, 1000, 1000)
    )).cut(wg_2d_throat_shape)

    wg_2d_throat_shell_shape = (
        wg_2d_throat_shape
            .faces("<Z or >Z or |Z")
            .shell(-shell_thickness)
        # .mirror(mirrorPlane='YZ', union=True)
    )

    wg_2d_throat_shell_shape_inner = wg_2d_throat_shape.cut(wg_2d_throat_shell_shape)

    wg = wg_2d_throat_shell_shape

    print('calc_wg')
    print(f"ws.screw_holes: {ws.screw_holes}")

    woofer_flat_thickness = shell_thickness

    woofer_flat = place_woofer(
        ws=ws,
        angle=r1,
        shape=(
            cq.Workplane("XY")
                .rect(25, 25)
                .circle(ws.cutout_diameter / 2)
                .extrude(-woofer_flat_thickness)
                # .fillet(0.3)
                .pushPoints(ws.screw_holes)
                .hole(ws.screw_hole_diameter)
            # .fillet(0.1)
            # .translate((0, 0, -ws.base_thickness))
        ),
    )
    wg = wg.union(woofer_flat)

    tweet_support_h = woofer_z_offset + shell_thickness / 2

    for r in [rt1, rt2]:
        tweet_support = move_degree(r, (
            cq.Workplane("XY")
                .rect(pt2522_width, h / 2 + 0.5)
                .workplane(offset=tweet_support_h)
                .rect(pt2522_width + 4 * tweet_support_h, h / 2 + 0.5)
                .loft(combine=True)
                .translate((0, 0, 0.15))
                .rotate((0, 0, 0), (1, 0, 0), 90)
        ))
        wg = wg.union(tweet_support)

    def edge_ply_cutout(s: WgShell):
        w = s.w / 2

        sf = WgShell(angle=s.angle, front=True)
        sr = WgShell(angle=s.angle, rear=True)

        angle = s.angle

        def half(ss, w0, w, d, e, sign=1):
            # h0 = -ws.z_offset - 0.5 * shell_thickness
            h0 = -ws.z_offset - ws.base_thickness - shell_thickness / 2

            if angle == r1:
                h0 += 0.25

            ss = ss.segment((-(ws.p_offset.x + ws.cutout_diameter / 2) * sign, h0),
                            (-w * sign - shell_thickness, e * sign))

            ss = ss.segment((-2 * w * sign, e * sign))

            ss = ss.segment((-2 * w0 * sign, h0))

            return ss

        s = cq.Sketch()

        s = half(s, sign=-1, w0=w, w=sf.w / 2, d=sf.d / 2, e=sf.edge_width / 2)

        s = s.hull()

        return s

    # return wg, None, None

    ply_middle = (
        cq.Workplane("XY")
            .box(3 * PLY_EDGE_THICKNESS_SINGLE, 500, 500)
            .rotate((0, 0, 0), (0, 1, 0), PLY_INNER_EDGE_STRAIGHT_DEGREES)
            .translate((wgg.wg_edge_middle.x + PLY_EDGE_THICKNESS_SINGLE / 2, 0, 0))
    )

    ply_outer_edge_box = (
        cq.Workplane("XY")
            .box(3 * PLY_EDGE_THICKNESS_SINGLE + 200, 500, 500)
            .translate((100, 0, 0))
            .rotate((0, 0, 0), (0, 1, 0), PLY_INNER_EDGE_STRAIGHT_DEGREES)
            .translate((wgg.wg_edge_middle.x + PLY_EDGE_THICKNESS_SINGLE / 2, 0, 0))
    )

    ply_middle_with_shell = (
        cq.Workplane("XY")
            .box(2 * shell_thickness + 3 * PLY_EDGE_THICKNESS_SINGLE, 500, 500)
            .rotate((0, 0, 0), (0, 1, 0), PLY_INNER_EDGE_STRAIGHT_DEGREES)
            .translate((wgg.wg_edge_middle.x + PLY_EDGE_THICKNESS_SINGLE / 2, 0, 0))
        # .intersect(wg_2d_throat_shape)
    )

    s1r = WgShell(angle=r1, rear=True)
    g1r = wg_geometry_cached(s1r)

    s1f = WgShell(angle=r1, front=True)
    g1f = wg_geometry_cached(s1f)

    woofer_flat_edge = None
    edge_z_offset = g1f.p5_arc[1].y - ws.z_offset - shell_thickness
    print(f"edge_z_offset: {edge_z_offset}")
    if edge_z_offset > 0.3:
        woofer_flat_edge = wg_shell(
            radius_list=DEGREE_LIST,
            slice_function=edge_ply_cutout,
        )
    # return wg, woofer_flat_edge, None
    if woofer_flat_edge:
        wg = wg.union(woofer_flat_edge)

    yr = g1r.wg_edge_middle.y
    yf = g1f.wg_edge_middle.y

    r_h = P(angle=r0, length=CBT_RADIUS - yr).distance(P(angle=r2, length=CBT_RADIUS - yr))
    f_h = P(angle=r0, length=CBT_RADIUS + yf).distance(P(angle=r2, length=CBT_RADIUS + yf))

    sss = (
        cq.Workplane("XY")
            .box(80, 80, 2.0 * shell_thickness)
    )

    single_rear_support = False

    middle_slot_ribs = None
    if single_rear_support:
        middle_slot_ribs = move_degree(r1, sss)
    else:
        if ws.woofer_type == 'nd140':
            middle_rib_offset = h / 6 - 0.25 * shell_thickness
        elif ws.woofer_type == 'nd105':
            middle_rib_offset = h / 6 - 0.5 * shell_thickness
        elif ws.woofer_type == 'nd91':
            middle_rib_offset = h / 6 - 1.35 * shell_thickness
        elif ws.woofer_type == 'nd65':
            middle_rib_offset = h / 6 + 0.8 * shell_thickness

        mrr = distance_across_circle(middle_rib_offset, CBT_RADIUS)

        a = move_degree(r1 - mrr, sss)
        b = move_degree(r1 + mrr, sss)

        middle_slot_ribs = (
            a.union(b)
        )

    rear_vent_slots = place_woofer(
        ws=ws,
        angle=r1,
        shape=(
            cq.Workplane("XY").workplane(offset=0)
                .center(20, 0)
                .rect(40, h - 4 * shell_thickness)
                .workplane(offset=g1r.wg_edge_middle.y)
                .rect(40, r_h - 4 * shell_thickness)
                .workplane(offset=g1r.wg_edge_middle.y)
                .rect(40, r_h - 4 * shell_thickness)
                .loft(combine=True)
            # .translate((-20, 0, 0))
        ),
    )
    rear_vent_slots = rear_vent_slots.cut(middle_slot_ribs)
    rear_vent_slots = rear_vent_slots.cut(ply_middle_with_shell)
    wg = wg.cut(rear_vent_slots)

    front_vent_slots = place_woofer(
        ws=ws,
        angle=r1,
        shape=(
            cq.Workplane("XY").workplane(offset=-1.0 * shell_thickness)
                .center(20, 0)
                .rect(40, h - 4 * shell_thickness)
                .workplane(offset=-g1f.wg_edge_middle.y)
                .rect(40, f_h - 4 * shell_thickness)
                .workplane(offset=-g1f.wg_edge_middle.y)
                .rect(40, f_h - 4 * shell_thickness)
                .loft(combine=True)
            # .translate((-20, 0, 0))
        ),
    )
    if woofer_flat_edge:
        front_vent_slots = front_vent_slots.cut(woofer_flat_edge)
    front_vent_slots = front_vent_slots.cut(middle_slot_ribs)
    front_vent_slots = front_vent_slots.cut(ply_middle_with_shell)
    wg = wg.cut(front_vent_slots)

    # return wg, None, None

    top_bottom_supports_height = 1.5 * shell_thickness

    # wg = wg.intersect(wg_2d_throat_shape)
    wg = wg.cut(wg_2d_throat_shape_negative)

    # return wg, None, None

    # wg_raw_with_verts = wg_raw
    wg_with_verts_shape_negative = wg_2d_throat_shape_negative
    verts = None
    if RENDER_VERTS:
        vert_shell = 0.4

        bot_vert = calc_vert(r0, bot=True, top=True)
        mid_vert = calc_vert(r1, top=True, bot=True)
        top_vert = calc_vert(r2, bot=True, top=True)

        # return bot_vert, None, None

        bot_vert_shell = calc_vert(r0, bot=True, top=True, shell=-vert_shell)
        bot_vert_shell_inner = bot_vert.cut(bot_vert_shell)
        # bot_vert_shell = bot_vert_shell.cut(wg_2d_throat_shell_shape_inner)

        # return bot_vert, bot_vert_shell, bot_vert_shell_inner

        mid_vert_shell = calc_vert(r1, bot=True, top=True, shell=-vert_shell)
        mid_vert_shell_inner = mid_vert.cut(mid_vert_shell)
        # mid_vert_shell = mid_vert_shell.cut(wg_2d_throat_shell_shape_inner)

        top_vert_shell = calc_vert(r2, bot=True, top=True, shell=-vert_shell)
        top_vert_shell_inner = top_vert.cut(top_vert_shell)
        # top_vert_shell = top_vert_shell.cut(wg_2d_throat_shell_shape_inner)

        vert_shapes = (
            bot_vert
                .union(mid_vert)
                .union(top_vert)
                .cut(wg_full_shape_negative)
        )

        verts = (
            bot_vert_shell
                .union(mid_vert_shell)
                .union(top_vert_shell)
                .cut(wg_2d_throat_shell_shape_inner)
        )

        verts_inner_shell = (
            bot_vert_shell_inner
                .union(mid_vert_shell_inner)
                .union(top_vert_shell_inner)
                .cut(wg_2d_throat_shell_shape_inner)
        )

        mirror_cut = move_degree(r1, (
            cq.Workplane("XY")
                .box(50, 50, 50)
                .translate((-25 - 0.1, 0, 0))
        ))
        verts = verts.cut(mirror_cut)

        bot_cut = move_degree(r0, (
            cq.Workplane("XY")
                .box(50, 50, 50)
                .translate((0, 0, -25))
        ))
        verts = verts.cut(bot_cut)

        top_cut = move_degree(r2, (
            cq.Workplane("XY")
                .box(50, 50, 50)
                .translate((0, 0, 25))
        ))
        verts = verts.cut(top_cut)

        wg = wg.union(verts)
        wg = wg.cut(verts_inner_shell)

        wg_shape_negative = move_degree(r1, (
            cq.Workplane("XY")
                .box(1000, 1000, 1000)
        ))

        wg_shape_negative = wg_shape_negative.cut(wg_2d_throat_shape)
        wg_shape_negative = wg_shape_negative.cut(vert_shapes)

        wg_with_verts_shape_negative = wg_shape_negative

        # wg_shape_positive = wg_2d_throat_shape.union(vert_shapes)
        # wg_shape_negative = wg_outer_shape.cut(wg_shape_positive)

        # wg_shape_with_verts = (
        #     wg_outer_shape.cut(vert_shapes)
        # )

        t = 2 * 1.3

        supports = (
            move_degree(rt1, (
                cq.Workplane("XY")
                    .box(pt2522_width, t, h / 2 + 0.2)
                #     .cut((
                #     cq.Workplane("XY")
                #         .box(pt2522_x_offset * 2 + shell_thickness * 0.75, 2 * t,
                #              h / 2 - 2 * pt2522_h_offset + shell_thickness * 0.75)
                # ))
                # .translate((0, -t/2, 0))
            ))
        )
        supports = supports.union(
            move_degree(rt2, (
                cq.Workplane("XY")
                    .box(pt2522_width, t, h / 2 + 0.2)
                #     .cut((
                #     cq.Workplane("XY")
                #         .box(pt2522_x_offset * 2 + shell_thickness * 0.75, 2 * t,
                #              h / 2 - 2 * pt2522_h_offset + shell_thickness * 0.75)
                # ))
                # .translate((0, -t/2, 0))
            ))
        )

        supports = supports.cut(wg_shape_negative)
        #
        # return wg, supports, wg_shape_negative

        wg = wg.union(supports)

        # wg_raw_with_verts = wg_raw.union(verts)

    if True:
        top_bot_sups = ply_middle_with_shell

        c_h = 12

        top_bot_sups = top_bot_sups.cut(
            move_degree(r0, (
                cq.Workplane("XY")
                    .box(50, 50, c_h)
                    .translate((0, 0, top_bottom_supports_height + c_h / 2))
            ))
        )

        top_bot_sups = top_bot_sups.cut(
            move_degree(r2, (
                cq.Workplane("XY")
                    .box(50, 50, c_h)
                    .translate((0, 0, -top_bottom_supports_height - c_h / 2))
            ))
        )

        top_bot_sups = top_bot_sups.cut(wg_with_verts_shape_negative)

        wg = wg.union(top_bot_sups)

    wg = wg.cut(ply_middle)

    # return wg, None, None

    woofer_frame_margins = None
    if RENDER_WOOFER_MARGINS:
        w_margin = ws.get_woofer_margin()
        w_margin = w_margin.translate((0, 0, -0.1))
        if ws.rotation_angle > 0:
            w_margin = w_margin.rotate((0, 0, 0), (0, 0, 1), ws.rotation_angle)
        w_margin = w_margin.rotate((0, 0, 0), (0, 0, 1), 180)
        w_margin = place_woofer(ws, DEGREE_LIST[1], w_margin)

        woofer_frame_margins = w_margin

        wg = wg.cut(woofer_frame_margins)

    ##################

    if RENDER_WG_MIRROR:
        wg = wg.mirror(mirrorPlane='YZ', union=True)

    wg_middle = wg.cut(ply_outer_edge_box)
    wg_edge = wg.cut(wg_middle)

    return wg_middle, wg_edge, None


# # wg_2d = (
# #     cq.Workplane("XY")
# #         .placeSketch(wg_2d_slice(WgShell(angle=0)))
# #         .extrude(-0.1)
# # )
#
# wg_2d = (
#     wg_2d_throat(s=WgShell(angle=0))
# )
#
# show_object(wg_2d, name=f'wg_2d')

# wg_2d = wg_2d_slice(s)
# wg_2d = wg_2d.extrude(0.1)
#
# wg_throat_2d = wg_2d_throat(s)
# wg_throat_2d = wg_throat_2d.extrude(0.2)
# wg_throat_2d = wg_throat_2d.mirror(mirrorPlane='YZ', union=True)
#
# shell = wg_2d
# wg = wg_throat_2d
# wg = wg.translate((0, 0, 0.1))


class RadialCoordinate:
    def __init__(self, angle, x=0.0, y=0.0):
        self.angle = angle,
        self.x = x,
        self.y = y


woofer_stats = woofer_stats
# TWEETER_CONFIG

all_tweets = [
    # [0, 1],
    # [2, 3],
    # [4, 5],
    # [6, 7],
    # [8, 9],
    # [10, 11],
    # [12, 13],
    [14, 15],
    # [16]
]

s = WgShell(angle=0)
wgg = wg_geometry_cached(s=s)

p_edge_angle = P(x=CBT_BOT_TOP_WIDTH_DIFF, y=NUM_TWEETERS * TWEETER_HEIGHT)

# print(f"wg_edge_p4_[0]: {wgg.p4_arc[0]}")
# print(f"wg_edge_p4_[1]: {wgg.p4_arc[1]}")
# print(f"wg_edge_p5: {wgg.p5_arc[0]}")

wg_middle = wgg.p4_arc[0].x

ply_supp_mid = None
# ply_supp_mid = ply_support_middle()
#
# ply_supp_mid = ply_supp_mid.rotate((0, 0, 0), (1, 0, 0), -90)
# ply_supp_mid = ply_supp_mid.rotate((0, 0, 0), (0, 0, 1), 90)
# # ply_supp_mid = ply_supp_mid.rotate((0, 0, 0), (0, 1, 0), (-90 + p_edge_angle.angle())/2)
# ply_supp_mid = ply_supp_mid.translate((wg_middle, 0, 0))

ply_supp_edge = None


def generate_ply_supp_edge(thickness, z_offset=None, margin=0.0, chamfer=None, angle=None, **kwargs):
    b = ply_support_edge(thickness=thickness, z_offset=z_offset, margin=margin, chamfer=chamfer, **kwargs)

    # print(f'(-90 + p_edge_angle.angle())/2: {(-90 + p_edge_angle.angle())/2}')

    b = b.rotate((0, 0, 0), (1, 0, 0), -90)
    b = b.rotate((0, 0, 0), (0, 0, 1), 90)
    if angle:
        b = b.rotate((0, 0, 0), (0, 1, 0), angle)
    # b = b.rotate((0, 0, 0), (0, 1, 0), -2.5)
    b = b.translate((wgg.wg_edge_middle.x, 0, 0))

    return b


RENDER_TWEETERS = True
RENDER_TWEETER_FLANGES = True
RENDER_WOOFERS = True
RENDER_WOOFERS_MIRROR = False
RENDER_WOOFER_CUTS = True
RENDER_WOOFER_MARGINS = True

RENDER_WOOFER_FRONT_SLOTS = False
RENDER_WOOFER_REAR_SLOTS = False

RENDER_WG = True
RENDER_WG_MIRROR = False
RENDER_VERTS = True

RENDER_WG_OUTER = False
RENDER_WG_OUTER_WITH_VERTS = False

RENDER_PLY_INNER_EDGE_PLY = False
RENDER_PLY_INNER_EDGE_STRAIGHT = True
RENDER_PLY_INNER_EDGE_STRAIGHT_CHEAP = True
RENDER_PLY_MIDDLE = False
RENDER_PLY_MIDDLE_PEGS = False
RENDER_PLY_OUTER_EDGE = False

RENDER_EDGE_PLY_SLICE = True

RENDER_WG_FRONT_BACK_SPLIT = False

RENDER_WG_PRINTER_SPLIT = False

RENDER_STEP = False

wg_full = None
wg_full_throat = None
wg_full_throat_with_verts = None
wg_full_shell = None

FULL_DEGREE_LIST = []
for t in range(0, NUM_TWEETERS + 1):
    FULL_DEGREE_LIST.append(DEGREES_PER_TWEETER * t)

FULL_DEGREE_LIST = FULL_DEGREE_LIST + [max(FULL_DEGREE_LIST) + TOP_WOOFER_WG_DEGREES]

shell_thickness = 1.0

if RENDER_WG_OUTER:
    wg_full = (
        wg_shell(
            radius_list=FULL_DEGREE_LIST,
            slice_function=wg_2d_slice,
        )
    )

    wg_full_throat = wg_shell(
        radius_list=FULL_DEGREE_LIST,
        slice_function=wg_2d_throat,
    )

    # wg_full_shell = (
    #     wg_full_throat
    #         .faces("<Z or >Z or |Z")
    #         .shell(-shell_thickness)
    #         .mirror(mirrorPlane='YZ', union=True)
    # )

    wg_full_throat = wg_full_throat.mirror(mirrorPlane='YZ', union=True)

    if RENDER_WG_OUTER_WITH_VERTS:
        wg_full_throat_with_verts = wg_full_throat

        tweeterz = range(0, NUM_TWEETERS + 1)
        for i in tweeterz:
            r = i * DEGREES_PER_TWEETER

            bottom = i == 0
            top = i == tweeterz[-1]

            v = (
                calc_vert(r, bot=bottom, top=top)
            )

            wg_full_throat_with_verts = wg_full_throat_with_verts.union(v)

ply_outer_edge = None

if RENDER_PLY_OUTER_EDGE:

    t = 1 * PLY_EDGE_THICKNESS_SINGLE


    def outer_edge_function(s: WgShell, length_from_bot=None, length_from_top=None):

        sf = WgShell(angle=s.angle, front=True)
        sr = WgShell(angle=s.angle, rear=True)

        p_top_right = P(sf.w / 2, -sf.edge_width / 2)
        p_bot_right = P(sr.w / 2, sr.edge_width / 2)

        # p_step = P(-2, -0.5)
        p_step = P(angle=side_angle_xz.angle(), length=-t)

        p_up_step = p_top_right - p_bot_right

        p_top_left = p_top_right + p_step
        p_bot_left = p_bot_right + p_step

        if length_from_bot:
            p_top_left = p_bot_left + P(angle=p_up_step.angle(), length=length_from_bot)
            p_top_right = p_bot_right + P(angle=p_up_step.angle(), length=length_from_bot)

        if length_from_top:
            p_bot_left = p_top_left - P(angle=p_up_step.angle(), length=length_from_top)
            p_bot_right = p_top_right - P(angle=p_up_step.angle(), length=length_from_top)

        sk = (
            cq.Sketch()
                .segment(p_top_right.values(), p_bot_right.values())
                .segment(p_bot_left.values())
                .segment(p_top_left.values())
                .close()
        )

        sk = sk.hull()

        return sk


    # ply_outer_edge = wg_shell(
    #     radius_list=FULL_DEGREE_LIST,
    #     slice_function=outer_edge_function,
    # )

    l = 2.5

    bot = wg_shell(
        radius_list=FULL_DEGREE_LIST,
        slice_function=outer_edge_function,
        length_from_bot=l,
    )

    top = wg_shell(
        radius_list=FULL_DEGREE_LIST,
        slice_function=outer_edge_function,
        length_from_top=l,
    )

    ply_outer_edge = bot.union(top)

    for i in range(0, NUM_TWEETERS - 1, 2):

        r = DEGREES_PER_TWEETER * i

        # print(f"r: '{r}'")

        dr = distance_across_circle(l * 1.5, CBT_RADIUS)

        r0 = r - dr / 2
        r1 = r
        r2 = r + dr / 2

        if r == 0:
            r0 += dr / 2
            r1 += dr / 2
            r2 += dr / 2

        b = wg_shell(
            radius_list=[r0, r1, r2],
            slice_function=outer_edge_function,
        )

        ply_outer_edge = ply_outer_edge.union(b)

    ply_outer_edge = ply_outer_edge.mirror(mirrorPlane='YZ', union=True)

ply_supp_edge = None
ply_supp_edge_bigger = None
ply_supp_edge_slice = None
ply_supp_straight_edge_slices = []
ply_supp_edges = []
ply_supp_straight_edges = []
ply_supp_edge_degrees = {}

if True:
    z_offset = -PLY_EDGE_THICKNESS_SINGLE

    g0 = wg_geometry_cached(WgShell(angle=FULL_DEGREE_LIST[0]))
    gmax = wg_geometry_cached(WgShell(angle=FULL_DEGREE_LIST[-1]))

    edge_max_width = g0.wg_edge_middle.x - gmax.wg_edge_middle.x

    # edge_num_ply_layers = math.floor(edge_max_width / PLY_EDGE_THICKNESS_SINGLE) - 1

    edge_num_ply_layers = 11
    print(f"edge_num_ply_layers: '{edge_num_ply_layers}'")

    edge_r_list = []
    max_r = max(FULL_DEGREE_LIST)
    steps = 1000
    for i in range(0, steps):
        edge_r = 0 + (float(i) / float(steps)) * max_r
        edge_r_list.append(edge_r)

    for i in range(0, edge_num_ply_layers):

        ply_x = wgg.wg_edge_middle.x - i * PLY_EDGE_THICKNESS_SINGLE

        r_within_delta = []

        closest_r = 0

        for r in edge_r_list:
            gr = wg_geometry_cached(WgShell(angle=r))
            middle = gr.wg_edge_middle.x - PLY_EDGE_THICKNESS_SINGLE
            delta = abs(middle - ply_x)
            # print(f"i: '{i}', delta: '{delta}'")

            if closest_r > delta:
                closest_r = delta

            if delta < PLY_EDGE_DELTA_LESS_THAN_THICKNESS:
                r_within_delta.append(r)

        if not r_within_delta:
            continue

        min_r = r_within_delta[0]
        max_r = r_within_delta[-1]

        ply_supp_edge_degrees[i] = {
            "min_r": min_r,
            "max_r": max_r,
            "x": ply_x,
            "z_offset": z_offset + i * PLY_EDGE_THICKNESS_SINGLE
        }

    for i, data in ply_supp_edge_degrees.items():
        min_r = data['min_r']
        max_r = data['max_r']

        for i2 in [i + 1, i - 1]:
            if i2 in ply_supp_edge_degrees:
                pass


    def get_ply_supp_edge_outer_z_offsets(angle):
        # find outer r
        offsets = []
        outer_offset = None
        for i in ply_supp_edge_degrees.keys():
            max_r = ply_supp_edge_degrees[i]['max_r']
            if angle < max_r:
                outer_offset = ply_supp_edge_degrees[i]['x']
                break
        # find inner r
        inner_offset = None
        for i in reversed(ply_supp_edge_degrees.keys()):
            min_r = ply_supp_edge_degrees[i]['min_r']
            if min_r < angle:
                inner_offset = ply_supp_edge_degrees[i]['x']
                break

        return outer_offset, inner_offset

if RENDER_PLY_INNER_EDGE_PLY:
    # ply_margin = 0.5
    # ply_thickness = PLY_EDGE_THICKNESS
    # z_offset = -PLY_EDGE_THICKNESS_SINGLE
    # z_offset = -PLY_EDGE_THICKNESS_SINGLE/2
    # z_offset = 0
    margin = -2.5
    chamfer = None
    # chamfer = PLY_EDGE_THICKNESS_SINGLE/2

    for i, data in ply_supp_edge_degrees.items():
        if i == 0:
            continue

        min_r = data['min_r']
        max_r = data['max_r']
        z_offset = data['z_offset']

        r0 = min_r
        r1 = min_r + (max_r - min_r) / 2
        r2 = max_r

        gr0 = wg_geometry_cached(WgShell(angle=r0))
        gr1 = wg_geometry_cached(WgShell(angle=r1))
        gr2 = wg_geometry_cached(WgShell(angle=r2))

        ply_x = wgg.wg_edge_middle.x - i * PLY_EDGE_THICKNESS_SINGLE

        delta0 = abs((gr0.wg_edge_middle.x - PLY_EDGE_THICKNESS_SINGLE) - ply_x)
        delta1 = abs((gr1.wg_edge_middle.x - PLY_EDGE_THICKNESS_SINGLE) - ply_x)
        delta2 = abs((gr2.wg_edge_middle.x - PLY_EDGE_THICKNESS_SINGLE) - ply_x)

        #     print(f"""
        # i: '{i}'
        # min_r: '{min_r}'
        # max_r: '{max_r}'
        #     """.strip())

        r_margin0 = - 1.0 * PLY_EDGE_THICKNESS_SINGLE
        r_margin1 = 0.0
        r_margin2 = - 1.0 * PLY_EDGE_THICKNESS_SINGLE

        r0_ratio_of_max = r0 / max(FULL_DEGREE_LIST)
        r1_ratio_of_max = r1 / max(FULL_DEGREE_LIST)
        r2_ratio_of_max = r2 / max(FULL_DEGREE_LIST)

        max_r_add = 5.0

        # radius_add_0 = 0
        # radius_add_1 = 0
        # radius_add_2 = 0

        edge = generate_ply_supp_edge(
            thickness=1 * PLY_EDGE_THICKNESS_SINGLE,
            z_offset=z_offset,
            # margin=margin,
            chamfer=chamfer,
            min_r=min_r,
            max_r=max_r,

            margin0=5 + 1.0 * r0_ratio_of_max,
            margin1=5 + 1.0 * r1_ratio_of_max,
            margin2=5 + 1.0 * r2_ratio_of_max,

            radius_0_margin=-0.1 + r0_ratio_of_max * max_r_add,
            radius_1_margin=-0.0 + r1_ratio_of_max * max_r_add,
            radius_2_margin=-0.2 + r2_ratio_of_max * max_r_add,
        )

        if wg_full_throat_with_verts:
            edge = edge.intersect(wg_full_throat_with_verts)
        elif wg_full_throat:
            edge = edge.intersect(wg_full_throat)

        ply_supp_edges.append(edge)

if RENDER_PLY_INNER_EDGE_STRAIGHT:

    z_offset = -2 * PLY_EDGE_THICKNESS_SINGLE

    mm = 30
    # angle = -3.0
    angle = PLY_INNER_EDGE_STRAIGHT_DEGREES

    if RENDER_PLY_INNER_EDGE_STRAIGHT_CHEAP:
        e = generate_ply_supp_edge(
            thickness=3 * PLY_EDGE_THICKNESS_SINGLE,
            z_offset=z_offset + 0 * PLY_EDGE_THICKNESS_SINGLE,
            margin=mm,
            angle=angle,
        )

        if wg_full_throat_with_verts:
            e = e.intersect(wg_full_throat_with_verts)
        elif wg_full_throat:
            e = e.intersect(wg_full_throat)

        ply_supp_edge = e

    else:
        e0 = generate_ply_supp_edge(
            thickness=1 * PLY_EDGE_THICKNESS_SINGLE,
            z_offset=z_offset + 0 * PLY_EDGE_THICKNESS_SINGLE,
            margin=mm,
            angle=angle,
        )

        e1 = generate_ply_supp_edge(
            thickness=1 * PLY_EDGE_THICKNESS_SINGLE,
            z_offset=z_offset + 1 * PLY_EDGE_THICKNESS_SINGLE,
            margin=mm,
            angle=angle,
        )

        e2 = generate_ply_supp_edge(
            thickness=1 * PLY_EDGE_THICKNESS_SINGLE,
            z_offset=z_offset + 2 * PLY_EDGE_THICKNESS_SINGLE,
            margin=mm,
            angle=angle,
        )

        if wg_full_throat_with_verts:
            e0 = e0.intersect(wg_full_throat_with_verts)
            e1 = e1.intersect(wg_full_throat_with_verts)
            e2 = e2.intersect(wg_full_throat_with_verts)
        elif wg_full_throat:
            e0 = e0.intersect(wg_full_throat)
            e1 = e1.intersect(wg_full_throat)
            e2 = e2.intersect(wg_full_throat)

        ply_supp_straight_edges.append(e0)
        ply_supp_straight_edges.append(e1)
        ply_supp_straight_edges.append(e2)

for tweets in all_tweets:
    bottom_part = tweets[0] == 0
    top_woofer = len(tweets) == 1

    i_name = '_'.join(map(str, tweets))

    RENDERED_TWEETERS = tweets

    diagnoal_wg = RENDER_WG_PRINTER_SPLIT and (RENDERED_TWEETERS[0] < 12)
    split_wg = RENDER_WG_FRONT_BACK_SPLIT

    ply_edge_thickness = None
    # ply_edge_thickness = 3 * PLY_EDGE_THICKNESS_SINGLE
    # if 9 < RENDERED_TWEETERS[0]:
    #     ply_edge_thickness = 3 * PLY_EDGE_THICKNESS_SINGLE
    # if 13 < RENDERED_TWEETERS[0]:
    #     ply_edge_thickness = 2 * PLY_EDGE_THICKNESS_SINGLE

    # wv = wg_vert()
    #
    # wv = wv.cut((
    #     cq.Workplane("XY")
    #         .box(100, 100, 100)
    #     .translate((50, 0, 0))
    # ))

    DEGREE_LIST = []
    for t in RENDERED_TWEETERS + [max(RENDERED_TWEETERS) + 1]:
        DEGREE_LIST.append(DEGREES_PER_TWEETER * t)

    topmost_part = len(DEGREE_LIST) == 2
    if topmost_part:
        DEGREE_LIST.append(DEGREE_LIST[1] + TOP_WOOFER_DERGREES)

    # shell = wg_shell(
    #   radius_list=RENDERED_TWEETERS + [max(RENDERED_TWEETERS) + 1],
    #   slice_function=wg_2d_slice
    # )

    wg = None
    wg_throat = None
    wg_frame = None
    wg_edges = None
    wg_front = None
    wg_rear = None

    mid_vert = None
    top_vert = None

    ply_middle = None

    if RENDER_WG:
        wg = (
            wg_shell(
                radius_list=DEGREE_LIST,
                slice_function=wg_2d_slice,
            )
            # .mirror(mirrorPlane='YZ', union=True)
        )

        wg_throat = (
            wg_shell(
                radius_list=DEGREE_LIST,
                slice_function=wg_2d_throat,
            )
                .mirror(mirrorPlane='YZ', union=True)
        )

        wg_frame, wg_edges, wg_rear = calc_wg(
            topmost_part=topmost_part,
        )

    if RENDER_PLY_MIDDLE:
        ply_middle = calc_wg_ply_middle(bottom_part=bottom_part, top_part=top_woofer)

    # mid_vert = (
    #     calc_vert(DEGREE_LIST[1], top=True, bot=True)
    #         .cut(wg)
    # )
    #
    # top_vert = (
    #     calc_vert(DEGREE_LIST[-1], top=True)
    #         .cut(wg)
    # )

    # wv = wg_verts()
    # wv = wg_verts(shell=-0.4)
    # wv = wv.cut(wg)
    # wg = wg.union(wv)

    printer_volume = None

    tweets = None
    tweets_flanges = None
    tweets_rear_cut = None
    tweets_rear_margin = None
    woofs = None
    woofs_cutouts = None
    flats = None
    flats_rear = None

    if RENDER_TWEETERS:
        tweets = tweeters(
            cut_audio_holes=not cheap_calculation,
            # render_screw_holes_and_rivets_and_terminals=not cheap_tweeter,
            cut_screw_holes=False,
            union_heat_inserts=True,
            with_margin=True,
            # union_heat_insert_through_hole_distance=2.0,
            #
            # rear_screw_holes_union_inner_height=-0.9,
            # rear_screw_holes_union_outer_height=-0.6,
            # rear_screw_holes_union_diameter=1.2,
            terminal_block_depth=1.5,
            terminal_tabs_height=None,
            terminal_tab_width=None,
            terminal_tabs_margin=0.2,
            terminal_block_chamfer=0.2,
        )
        if RENDER_TWEETER_FLANGES:
            tweets_flanges = tweeters(flanges=True)
            if wg_throat and wg_frame:
                tweets_flanges = tweets_flanges.cut(wg_throat)
                wg_frame = wg_frame.union(tweets_flanges)

        if wg_frame:
            wg_frame = wg_frame.cut(tweets)

    if RENDER_WOOFERS:
        woofs = woofers()
        # woofs = woofs.mirror(mirrorPlane='YZ', union=True)

    if RENDER_WOOFER_CUTS and not topmost_part:
        woofs_cutouts = woofers(woofer_function=calc_woofer_edge_cutout, t=20, front_h_offset=0.0)
        if not RENDER_PLY_INNER_EDGE_STRAIGHT_CHEAP:
            woofs_cutouts_plus = woofers(woofer_function=calc_woofer_edge_cutout, t=20,
                                         front_h_offset=PLY_EDGE_THICKNESS_SINGLE)
        # woofs_cutouts = woofs_cutouts.mirror(mirrorPlane='YZ', union=True)

        if ply_supp_edge:
            ply_supp_edge = ply_supp_edge.cut(woofs_cutouts)

        for i in range(0, len(ply_supp_edges)):
            ply_supp_edges[i] = ply_supp_edges[i].cut(woofs_cutouts)

        for i in range(0, len(ply_supp_straight_edges)):
            woof_cuts = woofs_cutouts
            if not RENDER_PLY_INNER_EDGE_STRAIGHT_CHEAP:
                first_or_last = i == 0 or (i == len(ply_supp_straight_edges) - 1)
                woof_cuts = woofs_cutouts_plus if first_or_last else woofs_cutouts
            ply_supp_straight_edges[i] = ply_supp_straight_edges[i].cut(woof_cuts)

    if RENDER_EDGE_PLY_SLICE:
        if ply_supp_edge:
            ply_supp_edge_slice = ply_supp_edge

        if ply_supp_edge and wg:
            ply_supp_edge_slice = ply_supp_edge.intersect(wg)

        if wg:
            for i in range(0, len(ply_supp_straight_edges)):
                ply_supp_straight_edges[i] = ply_supp_straight_edges[i].intersect(wg)

    wg = None


    # wg = wv


    def calc_printer_volume(r, ws):
        pv = (
            cq.Workplane("XY")
                .center(0, 0)
                .box(25.6, 25.6, 25.6)
        )
        if diagnoal_wg:
            pv = pv.rotate((0, 0, 0), (0, 0, 1), 45)
        return pv


    if RENDER_WG_PRINTER_SPLIT:
        printer_volume = calc_flat_middle_plates(calc_printer_volume)

    # show_object(shell, name=f'{i_name}_shell', options={})
    # show_object(wv, name='wv', options={"color": (45, 45, 123)})

    if wg:
        show_object(wg, name=f'{i_name}_wg', options={"color": (45, 125, 45), "alpha": 0.5})
        if RENDER_STEP:
            cq.exporters.export(wg, f"{i_name}_wg.step")
    if wg_frame:
        show_object(wg_frame, name=f'{i_name}_wg_frame', options={"color": (45, 45, 125), "alpha": 0.5})
        if RENDER_STEP:
            cq.exporters.export(wg_frame, f"{i_name}_wg_frame.step")
    if wg_edges:
        show_object(wg_edges, name=f'{i_name}_wg_edges', options={"color": (45, 45, 125), "alpha": 0.5})
        if RENDER_STEP:
            cq.exporters.export(wg_edges, f"{i_name}_wg_edges.step")
    if wg_rear:
        show_object(wg_rear, name=f'{i_name}_wg_rear', options={"color": (45, 45, 125), "alpha": 0.5})
        if RENDER_STEP:
            cq.exporters.export(wg_rear, f"{i_name}_wg_rear.step")
    if top_vert:
        show_object(top_vert, name=f'{i_name}_top_vert', options={"color": (45, 45, 125), "alpha": 0.5})
    if mid_vert:
        show_object(mid_vert, name=f'{i_name}_mid_vert', options={"color": (45, 45, 125), "alpha": 0.5})
    if tweets:
        show_object(tweets, name=f'{i_name}_tweeters', options={"color": (45, 45, 45)})
    if tweets_rear_margin:
        show_object(tweets_rear_margin, name=f'{i_name}_tweeters_margin', options={"color": (45, 45, 45)})
    if tweets_rear_cut:
        show_object(tweets_rear_cut, name=f'{i_name}_tweeters_rear_cut', options={"color": (45, 45, 45)})
    if woofs:
        show_object(woofs, name=f'{i_name}_woofers', options={"color": (45, 45, 45)})
    if flats:
        show_object(flats, name=f'{i_name}_flat', options={"color": (125, 45, 45), "alpha": 0.5})
    if flats_rear:
        show_object(flats_rear, name=f'{i_name}_flat_rear', options={"color": (125, 45, 45), "alpha": 0.5})
    if printer_volume:
        show_object(printer_volume, name=f'{i_name}_printer_volume', options={"color": (45, 125, 45), "alpha": 0.5})
    if ply_middle:
        show_object(ply_middle, name=f'{i_name}_ply_middle', options={"color": (255, 173, 0), "alpha": 0.5})
    if ply_supp_edge_slice:
        show_object(ply_supp_edge_slice, name=f'{i_name}_ply_supp_edge_slice',
                    options={"color": (255, 173, 0), "alpha": 0.5})
    i = 0
    for edge in ply_supp_straight_edge_slices:
        show_object(edge, name=f'{i_name}_ply_supp_straight_edge_slice',
                    options={"color": (255, 173, 0), "alpha": 0.5})
        i = i + 1

        # cq.exporters.export(wg_frame.cut(ply_supp_edge).intersect(printer_volume), "wg_frame_intersect.step")

if wg_full:
    show_object(wg_full, name=f'wg_full', options={"color": (45, 125, 45), "alpha": 0.5})
if wg_full_throat:
    show_object(wg_full_throat, name=f'wg_full_throat', options={"color": (125, 45, 45), "alpha": 0.5})
if wg_full_throat_with_verts:
    show_object(wg_full_throat_with_verts, name=f'wg_full_throat_with_verts',
                options={"color": (125, 45, 45), "alpha": 0.5})
if wg_full_shell:
    show_object(wg_full_shell, name=f'wg_full_shell', options={"color": (45, 45, 125), "alpha": 0.5})

if ply_supp_edge and False:
    show_object(ply_supp_edge, name=f'ply_supp_edge', options={"color": (255, 173, 0), "alpha": 0.5})

if ply_outer_edge:
    show_object(ply_outer_edge, name=f'ply_outer_edge', options={"color": (255, 173, 0), "alpha": 0.5})

i = 0
for edge in ply_supp_edges:
    show_object(edge, name=f'ply_supp_edge_{i}', options={"color": (255, 173, 0), "alpha": 0.5})
    i = i + 1

i = 0
for edge in ply_supp_straight_edges:
    show_object(edge, name=f'ply_supp_straight_edges_{i}', options={"color": (255, 173, 0), "alpha": 0.5})
    i = i + 1

# if ply_supp_edge:
#     show_object(ply_supp_edge, name=f'ply_supp_edge', options={"color": (255, 173, 0), "alpha": 0.5})


# if ply_supp_mid:
#     show_object(ply_supp_mid, name='ply_supp_mid', options={"color": (255, 173, 0), "alpha": 0.5})
# if ply_supp_edge:
#     show_object(ply_supp_edge, name='ply_supp_edge', options={"color": (255, 173, 0), "alpha": 0.5})


# show_object(edge_ply_cutout(s), name='edge_ply_cutout', options={"color": (255, 173, 0), "alpha": 0.5})


print(f"""
### taper_revolve completed.
""")
