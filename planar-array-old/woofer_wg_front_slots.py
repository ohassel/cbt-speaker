import cadquery as cq

import drivers
from geometry_2d import P as P


class WooferStats:
    def __init__(self, p_offset, woofer_type='nd105', dual=True, z_offset=0.9, angle=60, rotation_angle=0):

        self.p_offset = p_offset
        self.z_offset = z_offset
        self.angle = angle
        self.rotation_angle = rotation_angle

        self.screw_hole_diameter = 0.42

        if woofer_type == 'nd140':
            self.woofer = drivers.nd140()
            self.woofer_margin = drivers.nd140(margin=True)
            self.cutout_diameter = 12.0
            self.base_thickness = 0.15
            self.screw_holes_diameter = 13.8
            self.sd = 86.6
        elif woofer_type == 'nd105':
            self.woofer = drivers.nd105()
            self.woofer_margin = drivers.nd105(margin=True)
            self.cutout_diameter = 9.3
            self.base_thickness = 0.1
            self.screw_holes_diameter = 10.85
            self.sd = 51.5
        elif woofer_type == 'nd91':
            self.woofer = drivers.nd91()
            self.woofer_margin = drivers.nd91(margin=True)
            self.cutout_diameter = 7.65
            self.base_thickness = 0.1
            self.screw_holes_diameter = 8.84
            self.sd = 30.4
        elif woofer_type == 'nd65':
            self.woofer = drivers.nd65()
            self.woofer_margin = drivers.nd65(margin=True)
            self.cutout_diameter = 5.2
            self.base_thickness = 0.1
            self.screw_holes_diameter = 6.6
            self.sd = 15.6


# Flat
# woofer_stats = [
#     WooferStats(p_offset=P(5 + 4.0, 0), woofer_type='nd105', angle=0),
#     WooferStats(p_offset=P(5 + 4.0, 0), woofer_type='nd105', angle=0),
#     WooferStats(p_offset=P(5 + 4.0, 0), woofer_type='nd105', angle=0),
#     WooferStats(p_offset=P(5 + 4.0, 0), woofer_type='nd105', angle=0),
#     WooferStats(p_offset=P(5 + 3.25, 0), woofer_type='nd91', angle=0),
#     WooferStats(p_offset=P(5 + 2.25, 0), woofer_type='nd65', angle=0),
#     WooferStats(p_offset=P(5 + 2.0, 0), woofer_type='nd65', angle=0),
#     WooferStats(p_offset=P(5 + 1.0, 0), woofer_type='nd65', angle=0, z_offset=1.2),
#     WooferStats(p_offset=P(0, 2.75), woofer_type='nd65', angle=0, dual=False, z_offset=1.2),
# ]

# Angled
woofer_angle = 65
woofer_stats = [
    WooferStats(p_offset=P(5 + 2.0, 0), z_offset=2.5, woofer_type='nd105', angle=woofer_angle),
    WooferStats(p_offset=P(5 + 2.0, 0), z_offset=2.5, woofer_type='nd105', angle=woofer_angle),
    WooferStats(p_offset=P(5 + 2.0, 0), z_offset=2.25, woofer_type='nd105', angle=woofer_angle),
    WooferStats(p_offset=P(5 + 1.5, 0), z_offset=2.25, woofer_type='nd91', angle=woofer_angle),
    WooferStats(p_offset=P(5 + 1.0, 0), z_offset=1.75, woofer_type='nd91', angle=woofer_angle),
    WooferStats(p_offset=P(5 + 0.5, 0), z_offset=2.0, woofer_type='nd65', angle=woofer_angle),
    WooferStats(p_offset=P(5 + 0.5, 0), z_offset=1.4, woofer_type='nd65', angle=woofer_angle),
    WooferStats(p_offset=P(5 + 0.9, 0), z_offset=1.5, woofer_type='nd65', angle=10, rotation_angle=5),
    WooferStats(p_offset=P(0, 2.75), woofer_type='nd65', angle=woofer_angle, dual=False, z_offset=1.2),
]


def calc_woofer_front_slots(
        ws,

        mid_platform_z=2,
        top_platform_z=3.5,

        wg_horz_angle=15,
        wg_vert_angle=5,

        # x_mid_offset = 1.0,
        x_top_offset=1.5,
        y_top_offset=-0.75,

        # x_top = 1.5,
        surround_depth=0.6,
):
    y_mid = ws.cutout_diameter * 1
    y_top = ws.cutout_diameter * 1

    x_top = ((ws.sd / 2) * 0.5) / y_top
    x_mid = x_top * 1.5

    s = (
        cq.Workplane("XY")
            .circle(ws.cutout_diameter / 2)
            .extrude(surround_depth)
        # .edges(">Z")
        # .chamfer(0.5)
    )

    l = x_mid / 2

    pt = P(x=0, y=y_mid / 2 - l)

    p0 = pt + P(length=l, angle=90)
    p1 = pt + P(length=l, angle=45)
    p2 = pt + P(length=l, angle=0)

    c = (
        cq.Workplane("XY")
            # .workplane(offset=0)
            # .moveTo(0, ws.cutout_diameter / 2)
            # .threePointArc((ws.cutout_diameter / 2, 0), (0, -ws.cutout_diameter / 2))
            # .close()
            .workplane(offset=surround_depth)
            .moveTo(0, ws.cutout_diameter / 2)
            .threePointArc((ws.cutout_diameter / 2, 0), (0, -ws.cutout_diameter / 2))
            .close()
            # .circle(ws.cutout_diameter / 2)
            .workplane()
            .transformed(
            offset=cq.Vector(x_top_offset, y_top_offset, top_platform_z),
            rotate=cq.Vector(wg_horz_angle, 30, -wg_vert_angle)
        )
            .moveTo(p0.x, p0.y)
            .threePointArc((p1.x, p1.y), (p2.x, p2.y))
            .lineTo(p2.x, -p2.y)
            .threePointArc((p1.x, -p1.y), (p0.x, -p0.y))
            .threePointArc((-p1.x, -p1.y), (-p2.x, -p2.y))
            .lineTo(-p2.x, p2.y)
            .threePointArc((-p1.x, p1.y), (-p0.x, p0.y))
            .close()
            .workplane(offset=top_platform_z - mid_platform_z)
            .moveTo(p0.x, p0.y)
            .threePointArc((p1.x, p1.y), (p2.x, p2.y))
            .lineTo(p2.x, -p2.y)
            .threePointArc((p1.x, -p1.y), (p0.x, -p0.y))
            .threePointArc((-p1.x, -p1.y), (-p2.x, -p2.y))
            .lineTo(-p2.x, p2.y)
            .threePointArc((-p1.x, p1.y), (-p0.x, p0.y))
            .close()
            .loft(combine=True)
            .mirror(mirrorPlane='ZY', union=True)
    )

    s = s.union(c)

    s = s.rotate((0, 0, 0), (1, 0, 0), 180)
    # c = c.rotate((0, 0, 0), (1, 0, 0), 180)

    return s


ws = woofer_stats[0]

w = ws.woofer

s = calc_woofer_front_slots(ws=ws)

show_object(w, name='w', options={"color": (125, 125, 125)})
show_object(s, name='s')
# show_object(c, name='c')
