from functools import partial

import cadquery as cq

import drivers as drivers
from geometry_2d import P as P

print('BEGIN')

all_shapes = []
all_shapes_map = {}


class Operation:
    def __init__(self, xyz=None, angle=None, mirror=None):
        self.xyz = xyz
        self.angle = angle
        self.mirror = mirror

    def __str__(self):
        return f"xyz: {self.xyz}, angle: {self.angle}, mirror: {self.mirror}"


class Shape:
    def __init__(self, name, create_shape, show_options=None):
        self.name = name
        self.show_options = show_options
        self.create_shape = create_shape
        self.shape = None
        self.operations = []
        self.visible = False
        all_shapes.append(self)
        all_shapes_map[name] = self

    def set_visible(self, visible: bool):
        self.visible = visible
        return self

    def add_operation(self, op: Operation):
        self.operations.append(op)
        return self

    def clear_operations(self):
        self.operations = []
        return self

    def rotate(self, xyz, angle):
        self.add_operation(Operation(xyz=xyz, angle=angle))
        return self

    def translate(self, xyz):
        self.add_operation(Operation(xyz=xyz))
        return self

    def mirror(self, mirror_plane):
        self.add_operation(Operation(mirror=mirror_plane))
        return self

    def get_or_create(self):
        if self.shape is None:
            print(f"create_shape '{self.name}'")
            self.shape = self.create_shape()
        return self.shape

    def cut(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().cut(other.get_or_create())
        return self

    def intersect(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().intersect(other.get_or_create())
        return self

    def union(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().union(other.get_or_create())
        return self

    def show(self):
        print(f"show: '{self.name}', visible: '{self.visible}'")
        if self.visible:
            show_object(self.get_or_create(), name=self.name, options=self.show_options or {})

    def export(self, extension="dxf"):
        print(f"export: '{self.name}', visible: '{self.visible}'")
        if self.visible:
            import cadquery as cq
            cq.exporters.export(self.get_or_create(), f"{self.name}.{extension}")

    def apply_operations(self, invert=False):
        if self.visible:

            for op in self.operations if not invert else reversed(self.operations):
                # print(op)
                if op.mirror is not None:
                    self.shape = self.get_or_create().mirror(mirrorPlane=op.mirror, union=True)
                elif op.angle is not None:
                    self.shape = self.get_or_create().rotate((0, 0, 0), op.xyz, op.angle if not invert else -op.angle)
                else:
                    self.shape = self.get_or_create().translate(
                        op.xyz if not invert else (-op.xyz[0], -op.xyz[1], -op.xyz[2]))


# cheap = False
cheap = True

cbt_radius = 160

num_tweeters = 3

degrees_per_tweeter = 58 / 18

# waveguide_revolve_degrees = 58
waveguide_revolve_degrees = degrees_per_tweeter * num_tweeters


def setup_tweeter_angle_thingy(shape_function, degree_offset=0.5, max_i=num_tweeters):
    for i in range(0, max_i):
        s = shape_function(i)
        angle = (degree_offset + i) * degrees_per_tweeter
        p = P(length=cbt_radius, angle=angle) - P(x=cbt_radius, y=0)
        s.rotate((1, 0, 0), 90)
        s.rotate((0, 0, 1), 180)

        s.rotate((1, 0, 0), -angle)

        s.translate((0, -p.x, p.y))

        # print(f"pp: {(p.x, 0, p.y)}")


setup_tweeter_angle_thingy(
    lambda i: Shape(
        name=f"pt2522_plain_{i}",
        show_options={"color": (110, 110, 110)},
        create_shape=partial(drivers.pt2522,
                             cut_audio_holes=not cheap,
                             union_heat_inserts=False,
                             front_mount_direction=False,
                             )
    )
)

setup_tweeter_angle_thingy(
    lambda i: Shape(
        name=f"pt2522_cut_margin_{i}",
        show_options={"color": (110, 110, 110)},
        create_shape=partial(drivers.pt2522,
                             union_heat_inserts=True,
                             front_mount_direction=False,
                             cut_screw_holes=False,
                             cut_audio_holes=False,
                             render_screw_holes_and_rivets_and_terminals=True,
                             with_margin=True,
                             terminal_tabs_margin=0.2,
                             )
    )
)


def calc_tweet_mount_block(splint_hole_union=False, bottom=False):
    outer_w = 6.75
    outer_d = 1.0

    inner_w = 1.25
    inner_d = 12

    h = 1.15 * 2

    d_offset = 0.615

    fillet_radius = 0.3

    o = (
        cq.Workplane("XY")
            .box(outer_w, h, outer_d)
            .translate((0, 0, d_offset))
            .edges(">Z and |X")
            .fillet(fillet_radius)
    )

    i = (
        cq.Workplane("XY")
            .box(inner_w, h, inner_d)
            .translate((0, 0, d_offset))
            .translate((0, 0, inner_d / 2))
            .edges("|Z")
            .fillet(fillet_radius)
    )

    if bottom:
        f = (
            cq.Workplane("XY")
                .box(outer_w*2, h/2, 3)
                .translate((0, 0, d_offset))
                .translate((0, 0, 1.25))
                .translate((0, -h/4, 0))
        )
        fh = (
            cq.Workplane("XY")
                .circle(0.5 / 2)
                .extrude(h)
                .translate((0, 0, -h / 2))
                .rotate((0, 0, 0), (1, 0, 0), 90)
                .translate((outer_w/3*2+1, 0, 2))
                .mirror(mirrorPlane='YZ', union=True)
        )
        f = f.cut(fh)
        o = o.union(f)

        f2 = (
            cq.Workplane("XY")
                .box(outer_w*2, h/2, 3)
                .translate((0, 0, d_offset))
                .translate((0, 0, 1.25))
                .translate((0, -h/4, 0))
                .translate((0, 0, 9.5))
        )
        fh2 = (
            cq.Workplane("XY")
                .circle(0.5 / 2)
                .extrude(h)
                .translate((0, 0, -h / 2))
                .rotate((0, 0, 0), (1, 0, 0), 90)
                .translate((outer_w/3*2+1, 0, 2))
                .mirror(mirrorPlane='YZ', union=True)
                .translate((0, 0, 9.5))
        )
        f2 = f2.cut(fh2)
        o = o.union(f2)

        f3 = (
            cq.Workplane("XY")
                .box(2, h/2, 11)
                .translate((0, 0, d_offset))
                .translate((0, 0, 1.5))
                .translate((0, -h/4, 0))
                .translate((0, 0, 5))
                .translate((5.75, 0, 0))
                .mirror(mirrorPlane='YZ', union=True)
        )

        f3 = f3.cut(fh)
        f3 = f3.cut(fh2)

        o = o.union(f3)

    edge_depth = inner_w * 3

    edge_hole_diameter = 0.4 + 0.02  # margin

    if not bottom:
        edge_hole = (
            cq.Workplane("XY")
                .circle(edge_hole_diameter / 2)
                .extrude(edge_depth)
                .translate((0, 0, -edge_depth / 2))
                .rotate((0, 0, 0), (0, 1, 0), 90)
                .translate((0, 0, inner_d - inner_w / 2))
        )

    splint_hole = (
        cq.Workplane("XY")
            .circle(edge_hole_diameter / 2)
            .extrude(edge_depth)
            .translate((0, 0, -edge_depth / 2))
            .rotate((0, 0, 0), (0, 1, 0), 90)
            .translate((0, 0, 0.69 * inner_d))
    )
    if splint_hole_union:
        i = i.union(splint_hole)
    else:
        i = i.cut(splint_hole)

    if not bottom:
        i = i.cut(edge_hole)

    b = o.union(i)

    return b


def calc_mount_splint(i):
    mount_block_width = 1.25

    inner_w = 1.0
    inner_d = 11

    h = 1.75

    d_offset = 0.615

    fillet_radius = 0.3

    offset = mount_block_width / 2 + inner_w / 2
    offset_sign = -1 if i % 2 == 0 else 1

    i = (
        cq.Workplane("XY")
            .box(inner_w, inner_d-1, h)
            .translate((0, 0, d_offset))
            .translate((0, 0, 0.7 * inner_d))
            .translate((offset_sign * offset, 0, 0))
            .edges("|Y")
            .fillet(fillet_radius)
    )

    edge_depth = inner_w * 2

    edge_hole_diameter = 0.5 + 0.02  # margin

    # edge_hole = (
    #     cq.Workplane("XY")
    #         .circle(edge_hole_diameter / 2)
    #         .extrude(edge_depth)
    #         .translate((0, 0, -edge_depth / 2))
    #         .rotate((0, 0, 0), (0, 1, 0), 90)
    #         .translate((0, 0, inner_d - inner_w / 2))
    # )
    #
    # i = i.cut(edge_hole)

    b = i

    return b


setup_tweeter_angle_thingy(
    lambda i: Shape(
        name=f"tweet_mount_block_{i}",
        create_shape=partial(calc_tweet_mount_block,
                             bottom=i == 0,
                             )
    ),
    degree_offset=0.0,
    max_i=num_tweeters + 1,
)

setup_tweeter_angle_thingy(
    lambda i: Shape(
        name=f"tweet_mount_block_cut_{i}",
        create_shape=partial(calc_tweet_mount_block,
                             splint_hole_union=True,
                             )
    ),
    degree_offset=0.0,
    max_i=num_tweeters + 1,
)

setup_tweeter_angle_thingy(
    lambda i: Shape(
        name=f"tweet_mount_splint_{i}",
        create_shape=partial(calc_mount_splint,
                             i,
                             )
    ),
)

print(f"shapes: {all_shapes_map.keys()}")

show_lambdas = [

    # lambda name: 'tweet_mount_block' in name,
    # lambda name: 'tweet_mount_splint' in name,
    lambda name: 'pt2522' in name,

]

for name, shape in all_shapes_map.items():
    for l in show_lambdas:
        if l(name):
            print(f"show {name}")
            shape.set_visible(True)

for s in all_shapes:
    s.apply_operations()


def shapes_union(s1, s2):
    for mm in list(filter(lambda s: s1 in s.name, all_shapes)):
        for yy in list(filter(lambda s: s2 in s.name, all_shapes)):
            mm.union(yy)


def shapes_cut(s1, s2):
    for mm in list(filter(lambda s: s1 in s.name, all_shapes)):
        for yy in list(filter(lambda s: s2 in s.name, all_shapes)):
            mm.cut(yy)


def shapes_hide(s):
    ss = s
    for mm in list(filter(lambda s: ss in s.name, all_shapes)):
        all_shapes.remove(mm)


# shapes_union('wg_front', 'wg_flare')
# shapes_union('wg_back', 'wg_flare')

shapes_cut('tweet_mount_block', 'pt2522_cut_margin')
shapes_cut('tweet_mount_splint', 'tweet_mount_block_cut')

# shapes_hide('pt2522_cut_margin')
shapes_hide('tweet_mount_block_cut')

print(all_shapes_map)

for s in all_shapes:
    s.show()
    # s.export(extension='dxf')

    # print_stl = False
    # print_stl = True
    # if print_stl and 'wg' in s.name:
    s.export(extension='step')
