import cadquery as cq

import drivers

from geometry_2d import P as P
from geometry_2d import find_circle as find_circle
from geometry_2d import distance_across_circle as distance_across_circle



class WgShell:
    def __init__(self, angle, w=None, d=None, edge_width=None, front=False, rear=False):
        x = angle / CBT_DEGREES

        self.angle = angle

        # print(f"f={x}")
        side = Side(angle)

        self.edge_width = edge_width or (CBT_BOT_EDGE_WIDTH - (CBT_TOP_EDGE_WIDTH_DIFF * x))

        w_offset = 0
        # print(f"side_angle_xz.angle(): {side_angle_xz.angle()}")
        if front or rear:
            w_offset = P(x=self.edge_width, angle=side_angle_xz.angle()).y
        if rear:
            w_offset = -w_offset
        # print(f"w_offset: {w_offset}")

        # self.w = w or (CBT_BOTTOM_WIDTH - (CBT_BOT_TOP_WIDTH_DIFF * x))
        self.w = side.outer_cbt_width + w_offset
        self.d = d or (CBT_BOTTOM_DEPTH - (CBT_BOT_TOP_DEPTH_DIFF * x))
        # self.edge_width = edge_width or (CBT_BOT_EDGE_WIDTH - (CBT_TOP_EDGE_WIDTH_DIFF * x))

        c = P(x=CBT_RADIUS, y=0) + P(length=-CBT_RADIUS, angle=angle)
        self.center = P(c.x, -c.y)

    def __str__(self):
        return  f"({self.angle}, {self.edge_width}, {self.w}, {self.d}, {self.center})"


def wg_2d_slice(s: WgShell):

    w = s.w / 2
    # d = s.d / 2
    #
    # e = s.edge_width / 2

    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    def half(ss, w0, w, d, e, sign=1):
        # r = r.moveTo(w * sign, 0)
        ss = ss.segment((w0 * sign, 0), (w * sign, e * sign))

        # diamond
        # ss = ss.segment((0, d * sign))
        # ss = ss.segment((-w * sign, e * sign))

        # Curved
        ss = ss.arc((0, d * sign), (-w * sign, e * sign), "ignored")

        #
        ss = ss.segment((-w * sign, e * sign), (-w0 * sign, 0))

        return ss

    s = cq.Sketch()

    s = half(s, sign=1, w0=w, w=sr.w / 2, d=sr.d / 2, e=sr.edge_width / 2)
    s = half(s, sign=-1, w0=w, w=sf.w / 2, d=sf.d / 2, e=sf.edge_width / 2)

    s = s.solve().assemble()

    return s


class WgGeometry:
    def __init__(self, s: WgShell):

        throat_roundover = CBT_THROAT_ROUNDOVER
        edge_roundover = CBT_EDGE_ROUNDOVER

        wg_angle = 90 - (CBT_COVERAGE_ANGLE / 2)
        throat_max_angle = 90 - wg_angle

        # b = b.moveTo()

        # b = b.rect(pt2522_x_offset, pt2522_y_offset)

        p0 = P(pt2522_x_offset, pt2522_y_offset)

        throat_roundover_center = p0 + P(x=throat_roundover, y=0)

        p1 = throat_roundover_center + P(length=throat_roundover, angle=180 - throat_max_angle)
        p01 = throat_roundover_center + P(length=throat_roundover, angle=180 - throat_max_angle * 0.5)

        # p1 = pc + P(length=throat_r, angle=throat_max_angle)
        #
        step = 20

        p2 = p1 + P(y=step, angle=wg_angle)

        w = s.w / 2
        d = s.d / 2
        #
        e = s.edge_width / 2

        arc_center, arc_radius = find_circle(
            (-w, e),
            (0, d),
            (w, e)
        )

        p_step = p2
        for i in range(0, 100):

            step = step / 2

            length_to_arc_center = (p_step - arc_center).length()
            delta_to_arc_center = arc_radius - length_to_arc_center

            # print(f"p2={p2}")
            # print(f"length_to_arc_center={length_to_arc_center}")
            # print(f"delta_to_arc_center={delta_to_arc_center}")

            if delta_to_arc_center > 0:
                sign = 1
            else:
                sign = -1

            p_step = p_step + P(y=step * sign, angle=wg_angle)

        p2 = p_step

        p_edge_roundover_center = p2 + P(length=-edge_roundover, angle=wg_angle) + P(length=-edge_roundover,
                                                                                     angle=90 + wg_angle)

        step = 10
        p_step = p_edge_roundover_center
        for i in range(0, 100):

            step = step / 2

            length_to_arc_center = (p_step - arc_center).length()
            delta_to_arc_center = arc_radius - length_to_arc_center - edge_roundover

            # print(f"p2={p2}")
            # print(f"length_to_arc_center={length_to_arc_center}")
            # print(f"delta_to_arc_center={delta_to_arc_center}")

            if delta_to_arc_center > 0:
                sign = 1
            else:
                sign = -1

            p_step = p_step + P(y=step * sign, angle=wg_angle)

        p_edge_roundover_center = p_step
        p3 = p_edge_roundover_center + P(length=edge_roundover, angle=90 + wg_angle)
        p4 = p_edge_roundover_center + P(length=edge_roundover, angle=(p_edge_roundover_center - arc_center).angle())
        p34 = p_edge_roundover_center + P(length=edge_roundover, angle=p3.angle() + p4.angle())

        edge_before = (P(w, e) - arc_center).rotate(5) + arc_center

        self.w = w
        self.e = e
        self.edge_roundover = edge_roundover
        self.arc_center = arc_center
        self.wg_angle = wg_angle
        self.p_edge_roundover_center = p_edge_roundover_center

        self.p0 = P(p0.x, 0)
        self.p1 = p0
        self.p2_arc = (P(p01.x, p01.y), P(p1.x, p1.y))
        self.p3 = p3
        self.p4_arc = (P(p34.x, p34.y), P(p4.x, p4.y))
        self.p5_arc = (P(edge_before.x, edge_before.y), P(w, e))
        self.p6 = P(w, 0)

    def calc_y(self, x, edge_offset=0.0):
        if self.p2_arc[1].x < x <= self.p3.x:
            x0 = self.p2_arc[1].x
            y0 = self.p2_arc[1].y
            xd = x-x0
            r = 90 + self.wg_angle
            y_offset = P(length=edge_offset, angle=r).y * 2
            return y0 + P(x=xd, angle=self.wg_angle).y - y_offset
        if self.p3.x < x <= self.p4_arc[1].x:
            test_r = 45
            step = 90
            for i in range(0, 100):
                edge_before = self.p_edge_roundover_center + P(length=self.edge_roundover-edge_offset, angle=test_r)
                # print(f"test_r: {test_r}")
                if edge_before.x > x:
                    test_r = test_r + step
                else:
                    test_r = test_r - step
                step = step/2
            return edge_before.y
        if self.p4_arc[1].x < x:
            w = self.w
            e = self.e
            arc_center = self.arc_center
            r = P(w, e).angle()
            l = P(w, e).length()
            test_r = 45
            step = 90
            for i in range(0, 100):
                edge_before = (P(length=l-edge_offset, angle=r) - arc_center).rotate(test_r) + arc_center
                # print(f"test_r: {test_r}")
                if edge_before.x > x:
                    test_r = test_r + step
                else:
                    test_r = test_r - step
                step = step/2
            return edge_before.y
        return 2


def wg_2d_throat(s: WgShell):

    w = s.w / 2
    # d = s.d / 2
    #
    # e = s.edge_width / 2

    sf = WgShell(angle=s.angle, front=True)
    sr = WgShell(angle=s.angle, rear=True)

    def halff(ss, w0, g, sign=1, ):
        ss = ss.segment((g.p0.x, g.p0.y * sign), (g.p1.x, g.p1.y * sign))

        ss = ss.arc((g.p2_arc[0].x, g.p2_arc[0].y * sign), (g.p2_arc[1].x, g.p2_arc[1].y * sign), "p2")

        ss = ss.segment((g.p3.x, g.p3.y * sign))

        ss = ss.arc((g.p4_arc[0].x, g.p4_arc[0].y * sign), (g.p4_arc[1].x, g.p4_arc[1].y * sign), "p4")

        # print(g.p4_arc[1])

        ss = ss.arc((g.p5_arc[0].x, g.p5_arc[0].y * sign), (g.p5_arc[1].x, g.p5_arc[1].y * sign), "p5")

        ss = ss.segment((w0, g.p6.y * sign))

        return ss

    sk = cq.Sketch()

    sk = halff(sk, w0=w, g=WgGeometry(s=sr), sign=1)
    sk = halff(sk, w0=w, g=WgGeometry(s=sf), sign=-1)

    sk = sk.solve().assemble()

    return sk


pt2522_x_offset = 3.35 / 2
pt2522_y_offset = 0.94 / 2
pt2522_h_offset = 1.65


CBT_RADIUS = 150
NUM_TWEETERS = 17

cheap_calculation = True
# top_woofer = False
# top_woofer = True

TWEETER_HEIGHT = 9

DEGREES_PER_TWEETER = distance_across_circle(distance=TWEETER_HEIGHT, radius=CBT_RADIUS)
TOP_WOOFER_DERGREES = distance_across_circle(distance=6, radius=CBT_RADIUS)

# CBT_DEGREES = 58.45
CBT_DEGREES = DEGREES_PER_TWEETER * NUM_TWEETERS

CBT_DEGREES_INCL_TOP_WOOFER = CBT_DEGREES + TOP_WOOFER_DERGREES

print(f"CBT_DEGREES={CBT_DEGREES}")

RENDERED_TWEETERS = [0,1]
RENDERED_TWEETERS = [2,3]
RENDERED_TWEETERS = [4,5]

# RENDERED_TWEETERS = [6,7]
# RENDERED_TWEETERS = [8, 9]
#
# RENDERED_TWEETERS = [10, 11]
# RENDERED_TWEETERS = [12, 13]

# RENDERED_TWEETERS = [14, 15]
RENDERED_TWEETERS = [16]
# RENDERED_TWEETERS = [14, 15, 16]
RENDERED_TWEETERS = list(range(0, NUM_TWEETERS))

# DEGREES_PER_TWEETER = CBT_DEGREES / NUM_TWEETERS

CBT_COVERAGE_ANGLE = 90

CBT_THROAT_ROUNDOVER = 2.0
CBT_EDGE_ROUNDOVER = 2.0

# CBT_TOP_FACTOR = 0.4
CBT_TOP_FACTOR = 0.382
# CBT_TOP_FACTOR = 0.35

# CBT_BOTTOM_DEPTH = 28
# CBT_BOTTOM_WIDTH = 35
# CBT_BOT_EDGE_WIDTH = 10

# CBT_BOTTOM_DEPTH = 26
# CBT_BOTTOM_WIDTH = 36

CBT_BOTTOM_DEPTH = 25
CBT_BOTTOM_WIDTH = 33

PLY_EDGE_THICKNESS_SINGLE = 0.65
PLY_EDGE_THICKNESS = 4*PLY_EDGE_THICKNESS_SINGLE

# CBT_BOT_EDGE_WIDTH = 12
CBT_BOT_EDGE_WIDTH = CBT_BOTTOM_DEPTH * CBT_TOP_FACTOR


CBT_TOP_EDGE_WIDTH = CBT_BOT_EDGE_WIDTH * CBT_TOP_FACTOR

CBT_TOP_WIDTH = CBT_BOTTOM_WIDTH * CBT_TOP_FACTOR
CBT_TOP_DEPTH = CBT_BOTTOM_DEPTH * CBT_TOP_FACTOR

CBT_BOT_TOP_WIDTH_DIFF = CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH
CBT_BOT_TOP_DEPTH_DIFF = CBT_BOTTOM_DEPTH - CBT_TOP_DEPTH
CBT_TOP_EDGE_WIDTH_DIFF = CBT_BOT_EDGE_WIDTH - CBT_TOP_EDGE_WIDTH


def calculate_width(angle, lower_width=CBT_BOTTOM_DEPTH, upper_width=CBT_TOP_DEPTH, angle_cutoff=CBT_DEGREES_INCL_TOP_WOOFER):
    return lower_width - (angle / angle_cutoff) * (lower_width - upper_width)


def calculate_side_width(angle):
    return calculate_width(angle, CBT_BOT_EDGE_WIDTH, CBT_TOP_EDGE_WIDTH)


CBT_BOTTOM_EDGE_OFFSET = 0
CBT_TOP_EDGE_OFFSET = 0

CBT_BOTTOM_FRONT_XY = P(CBT_RADIUS - CBT_BOTTOM_EDGE_OFFSET, 0)
CBT_BOTTOM_REAR_XY = CBT_BOTTOM_FRONT_XY - P(calculate_side_width(0), 0)
CBT_TOP_FRONT_XY = P(angle=CBT_DEGREES_INCL_TOP_WOOFER, length=CBT_RADIUS - CBT_TOP_EDGE_OFFSET)
CBT_TOP_REAR_XY = CBT_TOP_FRONT_XY - P(angle=CBT_DEGREES_INCL_TOP_WOOFER, length=calculate_side_width(CBT_DEGREES_INCL_TOP_WOOFER))

# side_angle_xz = P(CBT_BOTTOM_FRONT_XY.x - CBT_TOP_REAR_XY.x, (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) / 2)
side_angle_xz = P(
    CBT_BOTTOM_FRONT_XY.x - CBT_TOP_REAR_XY.x,
    (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) / 2
)

side_x_ratio = side_angle_xz.length() / side_angle_xz.x


class Side:
    def __init__(self, angle):
        self.outer_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_FRONT_XY.length(),
                                                              upper_width=CBT_TOP_FRONT_XY.length(),
                                                              angle_cutoff=CBT_DEGREES_INCL_TOP_WOOFER))
        self.inner_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_REAR_XY.length(),
                                                              upper_width=CBT_TOP_REAR_XY.length(),
                                                              angle_cutoff=CBT_DEGREES_INCL_TOP_WOOFER))

        self.outer_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.outer_xy.x).y or 0

        self.inner_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.inner_xy.x).y or 0

        self.outer_z_ratio = 1 - (self.outer_z / side_angle_xz.y)
        self.inner_z_ratio = 1 - (self.inner_z / side_angle_xz.y)

        self.outer_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.outer_z_ratio + CBT_TOP_WIDTH
        self.inner_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.inner_z_ratio + CBT_TOP_WIDTH

angle = 0

t = wg_2d_throat(WgShell(angle=angle))

def calc_peg_points(angle, x_step = 2.0, x_edge_offset = 0.75, edge_offset=0.0):

    sf = WgShell(angle=angle, front=True)
    sr = WgShell(angle=angle, rear=True)

    gr=WgGeometry(s=sr)
    gf=WgGeometry(s=sf)

    points = []

    for front in [True, False]:
        g = gf if front else gr
        xmax = g.w - x_edge_offset
        x_list = [xmax]
        for i in range(0, 20):
            x = xmax - i * x_step
            if x > (pt2522_x_offset + 1):
                x_list.append(x)
        for x in x_list:
            y = g.calc_y(x=x, edge_offset=edge_offset)
            points.append(P(x, -y if front else y))

    return points


peg_radius = 0.6/2

pointz = calc_peg_points(
    angle=angle,
    x_step=2.0,
    x_edge_offset=0.75,
    edge_offset=peg_radius + 0.3,
)

pegs = None
for p in pointz:
    c = (
        cq.Workplane("XY")
            .moveTo(p.x, p.y)
            .circle(peg_radius)
            .extrude(1)
    )
    if pegs is None:
        pegs = c
    else:
        pegs = pegs.union(c)



show_object(t, name=f't', options={"color": (125, 45, 45)})
show_object(pegs, name=f'pegs', options={"color": (45, 125, 45)})