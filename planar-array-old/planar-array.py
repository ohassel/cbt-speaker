from geometry_2d import P as P
import drivers as drivers

import cadquery as cq
from functools import partial
import math

print('BEGIN')


class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


ARRAY_HEIGHT_OFFSET = 20
NUM_PLANAR_DRIVERS = 1
PLANAR_DRIVER_H = drivers.PLANAR_HEIGHT + 0.05

ARRAY_EDGE_THICKNESS = 0.4 * 3
ARRAY_PIPE_XY = 2.5
ARRAY_CORNER_CHAMFER = 1.5
ARRAY_WIDTH = 22
ARRAY_DEPTH = 14
ARRAY_EDGE_WIDTH = ARRAY_DEPTH - 2 * ARRAY_CORNER_CHAMFER
ARRAY_HEIGHT = ARRAY_HEIGHT_OFFSET + NUM_PLANAR_DRIVERS * PLANAR_DRIVER_H

all_shapes = []
all_shapes_map = {}


class Operation:
    def __init__(self, xyz, angle=None):
        self.xyz = xyz
        self.angle = angle

    def __str__(self):
        return f"xyz: {self.xyz}, angle: {self.angle}"


class Shape:
    def __init__(self, name, create_shape, show_options=None):
        self.name = name
        self.show_options = show_options
        self.create_shape = create_shape
        self.shape = None
        self.operations = []
        self.visible = False
        all_shapes.append(self)
        all_shapes_map[name] = self

    def set_visible(self, visible: bool):
        self.visible = visible
        return self

    def add_operation(self, op: Operation):
        self.operations.append(op)
        return self

    def rotate(self, xyz, angle):
        self.add_operation(Operation(xyz=xyz, angle=angle))
        return self

    def translate(self, xyz):
        self.add_operation(Operation(xyz=xyz))
        return self

    def get_or_create(self):
        if self.shape is None:
            print(f"create_shape '{self.name}'")
            self.shape = self.create_shape()
        return self.shape

    def cut(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().cut(other.get_or_create())
        return self

    def union(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().union(other.get_or_create())
        return self

    def show(self):
        print(f"show: '{self.name}', visible: '{self.visible}'")
        if self.visible:
            show_object(self.get_or_create(), name=self.name, options=self.show_options or {})


    def apply_operations(self, invert=False):
        if self.visible:

            for op in self.operations if not invert else reversed(self.operations):
                # print(op)
                if op.angle is not None:
                    self.shape = self.get_or_create().rotate((0, 0, 0), op.xyz, op.angle if not invert else -op.angle)
                else:
                    self.shape = self.get_or_create().translate(
                        op.xyz if not invert else (-op.xyz[0], -op.xyz[1], -op.xyz[2]))


def side_steel_square_pipe(xy_cm=ARRAY_PIPE_XY, h_cm=ARRAY_HEIGHT):
    return cq.Workplane("XY") \
        .workplane(offset=h_cm / 2) \
        .box(xy_cm, xy_cm, h_cm)


for i in range(0, 2):
    s = Shape(name=f'edge_pipe_{i}', show_options={"color": (105, 105, 105)},
              create_shape=partial(side_steel_square_pipe)
              )
    s.translate(((-1 if i == 1 else 1) * (ARRAY_WIDTH / 2 - ARRAY_PIPE_XY / 2), 0, 0))


def calc_cover():
    base = cq.Workplane("XY").center(0, 0) \
        .moveTo(0, 0) \
        .lineTo(ARRAY_WIDTH / 2, 0) \
        .lineTo(ARRAY_WIDTH / 2, ARRAY_PIPE_XY / 2)

    base = base.threePointArc((-ARRAY_WIDTH / 4, 4), (-ARRAY_WIDTH / 2, ARRAY_PIPE_XY / 2))
    # base = base.lineTo(0, 4)
    base = base.lineTo(0, 0)

    base = base.close() \
        .extrude(ARRAY_HEIGHT - ARRAY_HEIGHT_OFFSET) \
        .mirror(mirrorPlane='ZY', union=True) \
        .mirror(mirrorPlane='ZX', union=True)

    return base


cover = Shape(name='cover', show_options={"color": (101, 67, 33)}, create_shape=partial(calc_cover))
cover.translate((0, 0, ARRAY_HEIGHT_OFFSET))


def calc_waveguide():
    base = cq.Workplane("XY").center(0, 0) \
        .moveTo(0, 0) \
        .lineTo(ARRAY_WIDTH / 2, 0) \
        .lineTo(ARRAY_WIDTH / 2, ARRAY_PIPE_XY / 2)

    planar_edge = 1.9
    planar_thickness = drivers.PLANAR_TOP_EDGE_OFFSET / 2
    step = (ARRAY_WIDTH / 2) - planar_edge

    # base = base.spline([
    #     (5, r.side_middle.y + CBT_BASE_DEPTH - 13.5),
    #     (4, r.side_middle.y + CBT_BASE_DEPTH - 12.0),
    #     (3, r.side_middle.y + CBT_BASE_DEPTH - 11.5),
    # ])

    # base = base.threePointArc((-step/2, 4), (-step, ARRAY_PIPE_XY / 4))
    base = base.threePointArc((5, 3), (planar_edge, planar_thickness))
    # base = base.threePointArc((-ARRAY_WIDTH / 4, 4), (-ARRAY_WIDTH / 2, ARRAY_PIPE_XY / 2))

    # base = base.lineTo(planar_edge, 1)
    base = base.lineTo(planar_edge, 0)

    base = base.close().extrude(ARRAY_HEIGHT - ARRAY_HEIGHT_OFFSET)

    base = base.mirror(mirrorPlane='ZY', union=True)
    base = base.mirror(mirrorPlane='ZX', union=True)

    return base


wg = Shape(name='waveguide', show_options={"color": (101, 67, 33)}, create_shape=partial(calc_waveguide))
wg.translate((0, 0, ARRAY_HEIGHT_OFFSET))


def calc_planar():
    return drivers.planar(only_cutout=True)


for i in range(0, NUM_PLANAR_DRIVERS):
    s = Shape(name=f'planar_{i}', show_options={"color": (75, 75, 75)},
              create_shape=partial(calc_planar)
              )
    # s.translate((0, 0, drivers.PLANAR_BASE_THICKNESS / 2))
    s.rotate((1, 0, 0), -90)
    s.translate((0, 0, PLANAR_DRIVER_H / 2))
    s.translate((0, 0, ARRAY_HEIGHT_OFFSET))

    s.translate((0, 0, PLANAR_DRIVER_H * i))
    # s.translate((0, -ARRAY_PIPE_XY/2, 0))


def calc_woofer():
    return drivers.sb65()


for i in range(0, NUM_PLANAR_DRIVERS):
    s = Shape(name=f'woofer_{i}', show_options={"color": (75, 75, 75)},
              create_shape=partial(calc_woofer)
              )
    # s.rotate((1, 0, 0), -90)

    # s.rotate((0, 0, 1), 90)
    # s.translate((0, 4, 0))
    # s.translate((5, 0, 0))
    # s.translate((0, 0, 6.4 / 2))

    # s.rotate((0, 1, 0), -90)
    # s.translate((7.5, 0, 0))

    s.rotate((0, 1, 0), 90)
    s.translate((6, 0, 0))

    # s.rotate((0, 1, 0), 180)
    # s.rotate((1, 0, 0), 90)
    # s.translate((6.5, 0, 0))
    # s.translate((0, -0.15, 0))

    s.translate((0, 0, PLANAR_DRIVER_H))
    s.translate((0, 0, ARRAY_HEIGHT_OFFSET))
    # s.translate((0, -ARRAY_PIPE_XY / 2, 0))

    s.translate((0, 0, PLANAR_DRIVER_H * i))


def calc_x_cover():
    w = 11
    d = 7.5

    # b = b.spline([
    #     (5, r.side_middle.y + CBT_BASE_DEPTH - 13.5),
    #     (4, r.side_middle.y + CBT_BASE_DEPTH - 12.0),
    #     (3, r.side_middle.y + CBT_BASE_DEPTH - 11.5),
    # ])

    b = cq.Workplane("XY").center(0, 0)
    b = b.moveTo(0, 0)
    b = b.lineTo(w, 0)
    b = b.lineTo(w, d - 1.5)
    b = b.lineTo(w - 1.5, d)
    b = b.lineTo(0, d)

    # b = b.threePointArc((-ARRAY_WIDTH / 4, 4), (-ARRAY_WIDTH / 2, ARRAY_PIPE_XY / 2))
    # b = b.lineTo(0, 4)
    b = b.lineTo(0, 0)

    b = b.close() \
        .extrude(ARRAY_HEIGHT - ARRAY_HEIGHT_OFFSET) \
        .mirror(mirrorPlane='ZY', union=True) \
        .mirror(mirrorPlane='ZX', union=True)

    return b


x_cover = Shape(name='x_cover', show_options={"color": (101, 67, 33)}, create_shape=partial(calc_x_cover))
x_cover.translate((0, 0, ARRAY_HEIGHT_OFFSET))


# x_cover.translate((50, 0, 0))


def calc_edge():
    b = cq.Workplane("XY").center(0, 0).moveTo(0, 0)
    b = b.lineTo(ARRAY_EDGE_WIDTH, 0)
    b = b.lineTo(ARRAY_EDGE_WIDTH, ARRAY_HEIGHT)
    b = b.lineTo(0, ARRAY_HEIGHT)
    # b.lineTo(0, 0)

    b = b.close()
    b = b.extrude(ARRAY_EDGE_THICKNESS)

    # b.translate((-ARRAY_WIDTH / 2, -ARRAY_HEIGHT))

    return b


for sign in [1, -1]:
    edge = Shape(name=f'edge_ply_{sign}', show_options={"color": (101, 67, 33)},
                 create_shape=partial(calc_edge))

    edge.rotate((0, 1, 0), 90)
    edge.rotate((1, 0, 0), 90)
    edge.translate((-ARRAY_EDGE_THICKNESS / 2, -ARRAY_EDGE_WIDTH / 2, 0))
    edge.translate((sign * (ARRAY_WIDTH / 2 - 0.5 * ARRAY_EDGE_THICKNESS), 0, 0))


def calc_x_waveguide(lattice=False):
    planar_edge = 1.9
    planar_thickness = drivers.PLANAR_TOP_EDGE_OFFSET / 2

    wg_offset = planar_edge

    w = ARRAY_WIDTH / 2
    d = ARRAY_DEPTH / 2
    chamfer = ARRAY_CORNER_CHAMFER

    b = cq.Workplane("XY").center(0, 0).moveTo(0, 0)

    xy = []

    for i in range(0, 100):
        xy.append((w, (float(i) / 100) * (d - chamfer)))

    # xy = xy + [
    #     (w, d - chamfer),
    #     (w - chamfer, d),
    #     # (w - chamfer * 2, d),
    #     # (w - chamfer * 3, d - chamfer),
    # ]

    # for i in range(2, 25):
    #     ii = float(i) / 25
    #     xy.append((2 * wg_offset + ii*7, 0 + ii*d))

    xy = xy + [
        # (wg_offset + 1, planar_thickness + 1.5),
        (wg_offset, planar_thickness),
        (wg_offset, 0),
    ]

    b = b.spline(xy)

    # b = b.lineTo(w, 0)
    # b = b.lineTo(w, d-1.5)
    # b = b.lineTo(w-1.5, d)
    # b = b.lineTo(wg_offset, d)

    # b = b.threePointArc((-ARRAY_WIDTH / 4, 4), (-ARRAY_WIDTH / 2, ARRAY_PIPE_XY / 2))
    # b = b.lineTo(0, 4)

    # b = b.spline([
    #     (w-1.5, d),
    #     (wg_offset+0.25, planar_thickness+0.25),
    #     (wg_offset, planar_thickness),
    # ])

    # b = b.lineTo(wg_offset, planar_thickness)
    # b = b.lineTo(wg_offset, 0)

    b = b.close()
    b = b.extrude(ARRAY_HEIGHT - ARRAY_HEIGHT_OFFSET)

    if lattice:
        cell_length = 1.5
        dl = calc_diamond_cubic_packing(
            length=cell_length,
            line_d=cell_length / 7.5,
            node_d=cell_length / 5,
            x=4,
            y=3,
            z=1,
        )

        # dl = dl.translate((planar_edge, 0, 0))

        b = b.intersect(dl)
        # b = b.union(dl)

    b = b.mirror(mirrorPlane='ZY', union=True)
    b = b.mirror(mirrorPlane='ZX', union=True)

    return b


wg = Shape(name='x_waveguide', show_options={"color": (101, 67, 33), "alpha": 0.5},
           create_shape=partial(calc_x_waveguide, lattice=False))
wg.translate((0, 0, ARRAY_HEIGHT_OFFSET))

wgl = Shape(name='x_waveguide_lattice', show_options={"color": (101, 67, 33)},
            create_shape=partial(calc_x_waveguide, lattice=True))
wgl.translate((0, 0, ARRAY_HEIGHT_OFFSET))


def sphere_at(xyz: tuple, diameter):
    b = cq.Workplane("XY").center(0, 0).moveTo(0, 0)
    b = b.sphere(diameter)
    b = b.translate(xyz)

    return b


def calc_diamond_cell_component(length=1.0, line_d=0.1, node_d=0.2, cheap=False):
    l = length / 2

    line_l = math.sqrt(l * l + l * l + l * l)

    b = None

    if not cheap:

        b = sphere_at(xyz=(0, 0, 0), diameter=node_d)

        for xyz in [
            (l, l, -l),
            (-l, l, l),
            (l, -l, l),
            (-l, -l, -l),
        ]:
            b = b.union(
                sphere_at(xyz=xyz, diameter=node_d)
            )

    for t in [
        ((1, 1, 0), 55),
        ((-1, 1, 0), 55 + 180),
        ((1, -1, 0), 55 + 180),
        ((-1, -1, 0), 55),
    ]:
        next = cq.Workplane("XY").center(0, 0).moveTo(0, 0)
        if cheap:
            next = next.rect(line_d, line_d).extrude(line_l)
        else:
            next = next.circle(line_d).extrude(line_l)
        next = next.rotate((0, 0, 0), t[0], t[1])
        if b is None:
            b = next
        else:
            b = b.union(next)

    return b


def calc_diamond_cell(length=1.0, line_d=0.1, node_d=0.2, cheap=False):
    b = calc_diamond_cell_component(length=length, line_d=line_d, node_d=node_d, cheap=cheap) \
        .translate((-length / 2, -length / 2, -length / 2))

    b = b.union(
        calc_diamond_cell_component(length=length, line_d=line_d, node_d=node_d, cheap=cheap) \
            .translate((+length / 2, +length / 2, -length / 2))
    )

    b = b.union(
        calc_diamond_cell_component(length=length, line_d=line_d, node_d=node_d, cheap=cheap) \
            .translate((-length / 2, +length / 2, +length / 2))
    )

    b = b.union(
        calc_diamond_cell_component(length=length, line_d=line_d, node_d=node_d, cheap=cheap) \
            .translate((+length / 2, -length / 2, +length / 2))
    )

    # b = b.translate((0, 0, -l))

    return b


def calc_diamond_cubic_packing(length=1.0, line_d=0.1, node_d=0.15, x=2, y=2, z=2, cheap=True):
    b = None

    for xx in range(0, x):
        for yy in range(0, y):
            for zz in range(0, z):
                nn = calc_diamond_cell(length=length, line_d=line_d, node_d=node_d, cheap=cheap)
                nn = nn.translate((length + 2 * xx * length, length + 2 * yy * length, length + 2 * zz * length))

                if b is None:
                    b = nn
                else:
                    b = b.union(nn)

    return b


diamond_lattice = Shape(name='diamond_cubic_packing', show_options={"color": (101, 67, 33)},
                        create_shape=partial(calc_diamond_cubic_packing))

show_lambdas = [
    lambda name: 'planar' in name,
    lambda name: 'woofer' in name,
    # lambda name: 'cover' == name,
    # lambda name: 'waveguide' == name,
    # lambda name: 'x_cover' == name,
    lambda name: 'edge_ply' in name,
    lambda name: 'x_waveguide' == name,
    # lambda name: 'x_waveguide_lattice' == name,
    # lambda name: 'edge_pipe' in name,
    # lambda name: 'diamond_cubic_packing' in name,
]

for name, shape in all_shapes_map.items():
    for l in show_lambdas:
        if l(name):
            shape.set_visible(True)

for s in all_shapes:
    s.apply_operations()


def shapes_cut(s1, s2):
    for mm in list(filter(lambda s: s1 in s.name, all_shapes)):
        for yy in list(filter(lambda s: s2 in s.name, all_shapes)):
            mm.cut(yy)


shapes_cut('x_waveguide', 'edge_ply')
shapes_cut('waveguide', 'edge_pipe')
shapes_cut('cover', 'edge_pipe')
shapes_cut('base', 'y_middle')

print(all_shapes_map)

for s in all_shapes:
    s.show()

# show_object(plate_amp, name='plate_amp', options={"color": (125, 125, 125)})
