import math

import numpy as np
from PIL import Image

import util.geometry_2d as geom
from util.geometry_2d import P as P

# Speaker constants
# Unless specified constants are in meters
# WIDTH_M = 0.3
# HEIGHT_M = 0.2
WIDTH_M = 200
HEIGHT_M = 200

# PIXELS_PER_METER = 3778.0*1 # 999.9 / 1000 of 1m scale in cotter
# PIXELS_PER_METER = 3785.2
# PIXELS_PER_METER = 8000.0
# PIXELS_PER_METER = 4000.0
# PIXELS_PER_METER = 2000.0
PIXELS_PER_METER = 1000.0
#
# Derived constants
PIXELS_PER_CENTIMETER = 1000 / 100

# Pixel normalized constants
WIDTH_PX = int(WIDTH_M * PIXELS_PER_CENTIMETER)
HEIGHT_PX = int(HEIGHT_M * PIXELS_PER_CENTIMETER)

CBT_OUTER_RADIUS = 144

CBT_UPPER_WIDTH = 8
CBT_LOWER_WIDTH = 24

# Planar midrange
NUM_PLANARS = 7
PLANAR_HEIGHT_CM = 20

planar_r_offset = 0

DEGREES_PER_PLANAR = geom.distance_across_circle(PLANAR_HEIGHT_CM, CBT_OUTER_RADIUS)

# Loop all drivers and calculate x and y coordinates
PLANAR_DRIVERS_R = []
for n in range(1, NUM_PLANARS + 1):
    PLANAR_DRIVERS_R.append((n - 0.5) * DEGREES_PER_PLANAR + planar_r_offset)

PLANAR_DRIVERS_CROSSOVERS_R = []
for n in range(1, NUM_PLANARS + 2):
    PLANAR_DRIVERS_CROSSOVERS_R.append((n - 1) * DEGREES_PER_PLANAR + planar_r_offset)

CBT_DEGREE_CUTOFF = DEGREES_PER_PLANAR * 7.25


def calculate_width(angle, lower_width=CBT_LOWER_WIDTH, upper_width=CBT_UPPER_WIDTH, angle_cutoff=CBT_DEGREE_CUTOFF):
    return lower_width - (angle / angle_cutoff) * (lower_width - upper_width)


# cm

# RIB_EDGE_ANGLE = 22.5
# RIB_SIDE_WIDTH = RIB_WIDTH / 3
RIB_SIDE_WIDTH = 3

CBT_OUTER_BOARD_THICKNESS = 0.65
CBT_INNER_BOARD_THICKNESS = CBT_OUTER_BOARD_THICKNESS

CBT_RIB_INNER_BOARD_DISTANCE = CBT_OUTER_BOARD_THICKNESS * 4

CBT_REAR_SLOT_CUTOUT = 2

SB65_SQUARE_WIDTH = 6.4
SB65_CUTOUT_DIAMETER = 5.7
SB65_SCREW_HOLES_DIAMETER = 7.2
SB65_SCREW_HOLE_DIAMETER = 0.33

CBT_TOP_WIDTH = 10
CBT_BOTTOM_WIDTH = 30
CBT_BOTTOM_EDGE_OFFSET = 3
CBT_TOP_EDGE_OFFSET = 1.2

CBT_BOTTOM_FRONT_XY = P(CBT_OUTER_RADIUS - CBT_BOTTOM_EDGE_OFFSET, 0)
CBT_BOTTOM_REAR_XY = CBT_BOTTOM_FRONT_XY - P(RIB_SIDE_WIDTH, 0)
CBT_TOP_FRONT_XY = P(angle=CBT_DEGREE_CUTOFF, length=CBT_OUTER_RADIUS - CBT_TOP_EDGE_OFFSET)
CBT_TOP_REAR_XY = CBT_TOP_FRONT_XY - P(angle=CBT_DEGREE_CUTOFF, length=RIB_SIDE_WIDTH)

# cbt_x_length = CBT_BOTTOM_FRONT_XY.x - CBT_TOP_FRONT_XY.x
side_angle_xz = P(CBT_BOTTOM_FRONT_XY.x - CBT_TOP_REAR_XY.x, (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) / 2)
pass


def calculate_cbt_slice(angle):
    inner_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_REAR_XY.length(),
                                                     upper_width=CBT_TOP_REAR_XY.length(),
                                                     angle_cutoff=CBT_DEGREE_CUTOFF))
    outer_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_FRONT_XY.length(),
                                                     upper_width=CBT_TOP_FRONT_XY.length(),
                                                     angle_cutoff=CBT_DEGREE_CUTOFF))

    inner_z = P(angle=side_angle_xz.angle(), length=CBT_BOTTOM_FRONT_XY.x - inner_xy.x).y
    outer_z = P(angle=side_angle_xz.angle(), length=CBT_BOTTOM_FRONT_XY.x - outer_xy.x).y

    outer_rib_offset = calculate_width(angle=angle, lower_width=CBT_BOTTOM_EDGE_OFFSET,
                                       upper_width=CBT_TOP_EDGE_OFFSET,
                                       angle_cutoff=CBT_DEGREE_CUTOFF)

    outer_rib_offset_angle = 1 - math.degrees(math.asin((outer_z - inner_z) / RIB_SIDE_WIDTH))

    inner_x_to_front_ratio = ((inner_xy.x - side_angle_xz.x) / (CBT_BOTTOM_FRONT_XY.x - side_angle_xz.x))
    outer_x_to_front_ratio = ((outer_xy.x - side_angle_xz.x) / (CBT_BOTTOM_FRONT_XY.x - side_angle_xz.x))

    inner_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * inner_x_to_front_ratio + CBT_TOP_WIDTH
    outer_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * outer_x_to_front_ratio + CBT_TOP_WIDTH

    return {
        "inner_xy": inner_xy,
        "inner_z": inner_z,
        "inner_cbt_width": inner_cbt_width,
        "outer_xy": outer_xy,
        "outer_z": outer_z,
        "outer_cbt_width": outer_cbt_width,
        "outer_rib_offset": outer_rib_offset,
        "outer_rib_offset_angle": outer_rib_offset_angle,
    }


def get_sorted_closest(value, input_list):
    out = sorted(input_list, key=lambda x: abs(x - value))
    return out


x = get_sorted_closest(30, PLANAR_DRIVERS_CROSSOVERS_R)

boop = 0

def cbt_side(xy):
    r = xy.angle()
    c = xy.length()

    radius = calculate_cbt_slice(r)

    # Remove outer radius
    if not radius["inner_xy"].length() < c < radius["outer_xy"].length():
        return False

    if CBT_DEGREE_CUTOFF < r:
        return False

    closest_planar_crossover_angle = get_sorted_closest(r, PLANAR_DRIVERS_CROSSOVERS_R)[0]
    # for angle in [get_sorted_closest(r, PLANAR_DRIVERS_CROSSOVERS_R)[0]]:
    # for angle in PLANAR_DRIVERS_CROSSOVERS_R:
    boop = calculate_cbt_slice(closest_planar_crossover_angle)

    vector = boop["inner_xy"] - boop["outer_xy"]

    offset = 0.5 * CBT_RIB_INNER_BOARD_DISTANCE + 0.5 * CBT_INNER_BOARD_THICKNESS

    for v in [-90, +90]:
        if geom.is_in_rectangle(xy, boop["outer_xy"] + P(angle=vector.angle() + v, length=offset) + vector / 3, vector / 3, CBT_INNER_BOARD_THICKNESS):
            return False

    # for test in [
    #     CBT_BOTTOM_FRONT_XY,
    #     CBT_BOTTOM_REAR_XY,
    #     CBT_TOP_FRONT_XY,
    #     CBT_TOP_REAR_XY,
    # ]:
    #     if geom.is_in_hole(xy, test, 2):
    #         return False

    return True


def cbt_rib(xy_both, angle=None, num_woofers=2):
    radius = calculate_cbt_slice(angle)

    # RIB_DEPTH = 22
    # RIB_TOP_CURVE_OFFSET = RIB_WIDTH / 10

    width_from_middle = radius["outer_cbt_width"] / 2

    top_left_corner = P(width_from_middle, 0)
    xy = (top_left_corner - xy_both).abs()

    # if RIB_DEPTH < xy_both.y:
    #     return False

    if width_from_middle < xy.x:
        return False

    # Cut woofers
    if num_woofers > 0:
        woofer_center = P(0, 4) if num_woofers == 1 else P(SB65_SQUARE_WIDTH / 2 + 0.1, 4)
        if geom.is_in_hole(xy,
                           woofer_center,
                           SB65_CUTOUT_DIAMETER):
            return False

        woofer_holes = list(map(lambda angle: woofer_center + P(angle=angle, length=SB65_SCREW_HOLES_DIAMETER / 2),
                                [45, 135, 225, 315]))
        for woofer_hole_angle in woofer_holes:
            if geom.is_in_hole(xy,
                               woofer_hole_angle,
                               SB65_SCREW_HOLE_DIAMETER):
                return False

    # Top triangle
    # boop = 1 / (math.tan(math.radians(RIB_EDGE_ANGLE)) / 2)
    planar_width_from_middle = 3.25
    top_side_edge = P(width_from_middle, radius["outer_rib_offset"])
    top_edge_of_planar = P(planar_width_from_middle, 0)
    if geom.is_in_triangle(xy,
                           top_side_edge,
                           top_edge_of_planar,
                           top_left_corner):
        # if not geom.is_in_radius(xy,
        #                          top_side_edge,
        #                          top_edge_of_planar,
        #                          P(width_from_middle - ((width_from_middle - planar_width_from_middle) / 1.75),
        #                            RIB_TOP_CURVE_OFFSET / 4)):
        return False

    side_middle_vector = P(0, RIB_SIDE_WIDTH).rotate(radius["outer_rib_offset_angle"])
    side_middle = top_side_edge + side_middle_vector

    if side_middle.x < xy.x and side_middle.y < xy.y:
        return False

    if geom.is_in_triangle(xy,
                           top_side_edge,
                           side_middle,
                           side_middle + P(10, 0)):
        return False

    # Add cutouts for side board
    side_board_peg = side_middle_vector / 3
    if geom.is_in_rectangle(xy,
                            top_side_edge,
                            side_board_peg,
                            CBT_OUTER_BOARD_THICKNESS * 2):
        return False

    # Cut away towards the top where we else leave a super thin spike
    if geom.is_in_rectangle(xy,
                            top_side_edge,
                            P(0, 0) - side_board_peg,
                            CBT_OUTER_BOARD_THICKNESS * 2):
        return False

    if geom.is_in_rectangle(xy,
                            top_side_edge + side_board_peg * 2,
                            side_board_peg,
                            CBT_OUTER_BOARD_THICKNESS * 2):
        return False

    bottom = P(0, side_middle.x + side_middle.y)
    if bottom.y < xy.y:
        return False

    if geom.is_in_triangle(xy,
                           side_middle,
                           side_middle + P(0, side_middle.x),
                           bottom):
        return False

    rear_cutout_y = bottom.y - 10

    if geom.is_in_rectangle(xy,
                            bottom,
                            P(0, -rear_cutout_y - CBT_OUTER_BOARD_THICKNESS),
                            CBT_OUTER_BOARD_THICKNESS):
        return False

    return True


def cbt_spine(xy):
    r = xy.angle()
    c = P(0, 0).distance(xy)

    r_width = calculate_width(r)

    # Remove outer radius
    if CBT_OUTER_RADIUS < c:
        return False

    # Remove inner radius, more on bottom
    if c < CBT_OUTER_RADIUS - r_width:
        return False

    if CBT_DEGREE_CUTOFF < r:
        return False

    return True


picture_array = np.zeros([HEIGHT_PX, WIDTH_PX, 3], dtype=np.uint8)


def get_array_y(x_px, max_y_px, function):
    out_array = np.zeros([HEIGHT_PX], dtype=np.bool8)
    for y_px in range(1, max_y_px):
        x = float(x_px) / PIXELS_PER_CENTIMETER
        y = float(y_px) / PIXELS_PER_CENTIMETER

        if (function(P(x, y))):
            out_array[y_px] = [True]

    return {"x": x_px, "out_array": out_array}


import multiprocessing
from joblib import Parallel, delayed
from tqdm import tqdm

num_cores = multiprocessing.cpu_count()


def paint(offset_m, width_m, height_m, function):
    input = range(1, int(width_m * PIXELS_PER_CENTIMETER))

    processed_list = []

    parallel = True
    if parallel:
        input = tqdm(input)
        processed_list = Parallel(n_jobs=num_cores)(
            delayed(get_array_y)(x_px, int(height_m * PIXELS_PER_CENTIMETER), function) for x_px in input)
    else:
        for x_px in input:
            processed_list.append(get_array_y(x_px, int(height_m * PIXELS_PER_CENTIMETER), function))

    for list in processed_list:
        x = list["x"]
        array = list["out_array"]
        for y in range(0, len(array)):
            if array[y]:
                picture_array[
                    y + int(offset_m.y * PIXELS_PER_CENTIMETER),
                    x + int(offset_m.x * PIXELS_PER_CENTIMETER)
                ] = [255, 255, 255]


if __name__ == '__main__':
    # Draw lines
    # paint(0.1, 0.1, 0.4, 0.4, lambda x, y: (x*100 + y*100) % 2 == 0)

    # boop(0, 1, 1, 0, math.sqrt(2)/2, math.sqrt(2)/2)

    paint(P(60, 10), 140, 140, lambda xy: cbt_spine(xy + P(50, 0)))
    paint(P(80, 30), 140, 140, lambda xy: cbt_side(xy + P(50, 0)))

    offset = P(5, 5)
    for n in [
        {"angle": PLANAR_DRIVERS_CROSSOVERS_R[0], "num_woofers": [2]},
        {"angle": PLANAR_DRIVERS_CROSSOVERS_R[1], "num_woofers": [2]},
        {"angle": PLANAR_DRIVERS_CROSSOVERS_R[2], "num_woofers": [2]},
        {"angle": PLANAR_DRIVERS_CROSSOVERS_R[3], "num_woofers": [1, 2]},
        {"angle": PLANAR_DRIVERS_CROSSOVERS_R[4], "num_woofers": [1, 2]},
        {"angle": PLANAR_DRIVERS_CROSSOVERS_R[5], "num_woofers": [1]},
        {"angle": PLANAR_DRIVERS_CROSSOVERS_R[6], "num_woofers": [1]},
        {"angle": PLANAR_DRIVERS_CROSSOVERS_R[7], "num_woofers": [0, 1]},
    ]:
        angle = n["angle"]
        radius = calculate_cbt_slice(angle)
        width = radius["outer_cbt_width"]
        height = width * 0.75

        x_offset = P(0, 0)
        for num_woofers in n["num_woofers"]:
            paint(offset + x_offset, width, height, lambda xy: cbt_rib(xy,
                                                                       angle=angle,
                                                                       num_woofers=num_woofers
                                                                       ))
            x_offset += P(width + 5, 0)

        offset += P(0, height + 4)

    img = Image.fromarray(picture_array, 'RGB')
    img.save('picture_array.png')
    img.show()
