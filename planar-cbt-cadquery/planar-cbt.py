import geometry_2d as geom
from geometry_2d import P as P
import drivers as drivers
import math

CBT_OUTER_BOARD_THICKNESS = 0.8
CBT_INNER_BOARD_THICKNESS = 0.4
CBT_PLANAR_BRACKET_BOARD_THICKNESS = 0.3
planar_bracket_edge_margin = 0.15
# planar_bracket_side_tab_from_edge = 1.9  # 0.4 board thickness
planar_bracket_side_tab_from_edge = 1.94  # 0.3 board thickness

CBT_SPINE_BOARD_THICKNESS = CBT_OUTER_BOARD_THICKNESS

CBT_OUTER_RADIUS = 144

CBT_UPPER_DEPTH = 8
CBT_LOWER_DEPTH = 24

# Planar midrange
NUM_PLANARS = 7
PLANAR_HEIGHT_MARGIN_CM = 0.05
PLANAR_HEIGHT_CM = 20 + PLANAR_HEIGHT_MARGIN_CM  # add margin

CBT_TOP_WIDTH = 10
CBT_BOTTOM_WIDTH = 30
CBT_BOTTOM_EDGE_OFFSET = 4
CBT_TOP_EDGE_OFFSET = 2.5

# RIB_SIDE_WIDTH = 3
RIB_SIDE_WIDTH_TOP = 2
RIB_SIDE_WIDTH_BOTTOM = 3

SB65_SQUARE_WIDTH = 6.4
SB65_CUTOUT_DIAMETER = 5.7
SB65_SCREW_HOLES_DIAMETER = 7.2
SB65_SCREW_HOLE_DIAMETER = 0.33

WOOFER_SLOT_DEPTH = SB65_SQUARE_WIDTH + 1.7
WOOFER_SLOT_DISTANCE_TO_DRIVER_MARGIN = 1.0
WOOFER_SLOT_HEIGHT = 3

slot_offset = geom.distance_across_circle(WOOFER_SLOT_HEIGHT + 2 * CBT_INNER_BOARD_THICKNESS, CBT_OUTER_RADIUS) / 2
planar_r_offset = slot_offset / 2 + geom.distance_across_circle(CBT_INNER_BOARD_THICKNESS * 3, CBT_OUTER_RADIUS)
woofer_r_offset = geom.distance_across_circle(CBT_INNER_BOARD_THICKNESS, CBT_OUTER_RADIUS)

DEGREES_PER_PLANAR = geom.distance_across_circle(PLANAR_HEIGHT_CM, CBT_OUTER_RADIUS)
CBT_DEGREE_CUTOFF = DEGREES_PER_PLANAR * NUM_PLANARS + 2 * planar_r_offset + woofer_r_offset

# Loop all drivers and calculate x and y coordinates
PLANARS_R = []
for n in range(1, NUM_PLANARS + 1):
    PLANARS_R.append((n - 0.5) * DEGREES_PER_PLANAR + planar_r_offset + woofer_r_offset)

WOOFERS_R = []
SPINES_R = []
SPINES_R.append(geom.distance_across_circle(CBT_INNER_BOARD_THICKNESS, CBT_OUTER_RADIUS) / 2)
for n in range(1, NUM_PLANARS + 2):
    center = (n - 1) * DEGREES_PER_PLANAR + planar_r_offset + woofer_r_offset
    WOOFERS_R.append(center)
    SPINES_R.append(center - slot_offset)
    SPINES_R.append(center + slot_offset)


def calculate_width(angle, lower_width=CBT_LOWER_DEPTH, upper_width=CBT_UPPER_DEPTH, angle_cutoff=CBT_DEGREE_CUTOFF):
    return lower_width - (angle / angle_cutoff) * (lower_width - upper_width)


def calculate_side_width(angle):
    return calculate_width(angle, RIB_SIDE_WIDTH_BOTTOM, RIB_SIDE_WIDTH_TOP)


CBT_BOTTOM_FRONT_XY = P(CBT_OUTER_RADIUS - CBT_BOTTOM_EDGE_OFFSET, 0)
CBT_BOTTOM_REAR_XY = CBT_BOTTOM_FRONT_XY - P(calculate_side_width(0), 0)
CBT_TOP_FRONT_XY = P(angle=CBT_DEGREE_CUTOFF, length=CBT_OUTER_RADIUS - CBT_TOP_EDGE_OFFSET)
CBT_TOP_REAR_XY = CBT_TOP_FRONT_XY - P(angle=CBT_DEGREE_CUTOFF, length=calculate_side_width(CBT_DEGREE_CUTOFF))

# side_angle_xz = P(CBT_BOTTOM_FRONT_XY.x - CBT_TOP_REAR_XY.x, (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) / 2)
side_angle_xz = P(
    CBT_BOTTOM_FRONT_XY.x - CBT_TOP_REAR_XY.x,
    (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) / 2
)


#######################################
#######################################
def calculate_spine():
    import cadquery as cq

    spine = cq.Workplane("XY")
    spine = spine.center(-CBT_OUTER_RADIUS, 0)

    curoff_angle = CBT_DEGREE_CUTOFF + 0.02
    angles = list(range(0, int(curoff_angle))) + [curoff_angle]

    spine = spine.spline(
        list(map(
            lambda angle: P(angle=angle, length=CBT_OUTER_RADIUS - WOOFER_SLOT_DEPTH).values()
            , angles))
    )

    spine = spine.polarLineTo(distance=CBT_OUTER_RADIUS - Rib(curoff_angle).bottom.y - 0.1, angle=curoff_angle)

    spine = spine.spline(
        list(map(
            lambda angle: P(angle=angle, length=CBT_OUTER_RADIUS - Rib(angle).bottom.y - 0.1).values()
            , reversed(angles)))
    )
    result = spine.close().extrude(CBT_SPINE_BOARD_THICKNESS)

    for angle in (SPINES_R + [SPINES_R[-1] + geom.distance_across_circle(0.2, CBT_OUTER_RADIUS)]):
        r = Rib(angle)
        r.bottom.y - r.spine_depth
        rect = geom.get_rectangle_xy(
            P(length=CBT_OUTER_RADIUS, angle=angle),
            P(length=r.bottom.y - r.spine_depth, angle=angle + 180),
            CBT_INNER_BOARD_THICKNESS
        )
        result = result.cut(
            cq.Workplane("XY") \
                .center(-CBT_OUTER_RADIUS, 0) \
                .moveTo(rect[0].x, rect[0].y) \
                .lineTo(rect[1].x, rect[1].y) \
                .lineTo(rect[2].x, rect[2].y) \
                .lineTo(rect[3].x, rect[3].y) \
                .lineTo(rect[0].x, rect[0].y) \
                .close()
                .extrude(CBT_SPINE_BOARD_THICKNESS)
        )

    # Cut mount pegs
    for angle in WOOFERS_R:
        if angle == WOOFERS_R[-1]:
            continue
        r = Rib(angle)
        peg = P(length=CBT_OUTER_RADIUS - r.woofer_slot_y - ((r.bottom.y - r.woofer_slot_y) / 2), angle=angle)
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(peg.x - CBT_OUTER_RADIUS, peg.y) \
                .circle(0.6 / 2) \
                .extrude(CBT_SPINE_BOARD_THICKNESS)
        )

    return result


class Rib:
    def __init__(self, angle):
        side = Side(angle=angle)

        self.outer_rib_offset = calculate_width(angle=angle, lower_width=CBT_BOTTOM_EDGE_OFFSET,
                                                upper_width=CBT_TOP_EDGE_OFFSET,
                                                angle_cutoff=CBT_DEGREE_CUTOFF)

        self.edge_of_planar = P(6.5 / 2, 0)
        self.top_side_edge = P(side.outer_cbt_width / 2, self.outer_rib_offset)

        self.side_middle_vector = P(length=calculate_side_width(angle),
                                    x=0 - (side.outer_cbt_width - side.inner_cbt_width) / 2)

        # self.side_middle_vector = P(0, RIB_SIDE_WIDTH).rotate(side_angle_xz.angle())
        self.side_middle = self.top_side_edge + self.side_middle_vector

        bottom_spike = P(0, self.side_middle.y + P(angle=45, x=self.side_middle.x).y)
        self.bottom_board_width = bottom_spike - P(angle=-45,
                                                   length=-math.sqrt(2 * math.pow(CBT_SPINE_BOARD_THICKNESS / 2, 2)))
        self.bottom = P(0, self.bottom_board_width.y)

        self.spine_depth = (self.bottom.y - WOOFER_SLOT_DEPTH) * 0.7

        self.woofer_slot_y = WOOFER_SLOT_DEPTH - CBT_INNER_BOARD_THICKNESS / 2
        woofer_slot_c = CBT_OUTER_RADIUS - self.woofer_slot_y
        self.woofer_slot_height = (P(angle=0 + slot_offset, length=woofer_slot_c) - \
                                   P(angle=0 - slot_offset, length=woofer_slot_c)).length() - CBT_INNER_BOARD_THICKNESS
        self.woofer_slot_width = 2 * (self.bottom.y - WOOFER_SLOT_DEPTH) + 1.0

        self.slot_tabs = [
            {
                "center": P(self.woofer_slot_width / 4, self.woofer_slot_y + CBT_INNER_BOARD_THICKNESS),
                "angle": 0,
            },
            {
                "center": P(-self.woofer_slot_width / 4, self.woofer_slot_y + CBT_INNER_BOARD_THICKNESS),
                "angle": 0,
            },
        ]

        self.cable_holes = [
            P(max(self.woofer_slot_width / 7, 1.5), self.woofer_slot_y + (self.bottom.y - self.woofer_slot_y) / 3),
            P(-max(self.woofer_slot_width / 7, 1.5), self.woofer_slot_y + (self.bottom.y - self.woofer_slot_y) / 3),
        ]

        # print(f"rib( angle: '{angle}', woofer_slot_height: '{self.woofer_slot_height}' )")

        single_woofer = P(0, WOOFER_SLOT_DEPTH - SB65_SQUARE_WIDTH / 2 - WOOFER_SLOT_DISTANCE_TO_DRIVER_MARGIN)
        dual_woofers_x_offset = P(SB65_SQUARE_WIDTH / 2 + 0.1, 0)
        self.woofers_xy = {
            1: [single_woofer],
            2: [
                single_woofer + dual_woofers_x_offset,
                single_woofer - dual_woofers_x_offset
            ],
            3: [
                single_woofer + dual_woofers_x_offset * 2 + P(0, 0.5),
                single_woofer + P(0, 0.5),
                single_woofer - dual_woofers_x_offset * 2 + P(0, 0.5)
            ],
            4: [
                single_woofer + dual_woofers_x_offset * 3,
                single_woofer + dual_woofers_x_offset,
                single_woofer - dual_woofers_x_offset,
                single_woofer - dual_woofers_x_offset * 3
            ]
        }


def calculate_rib(
        angle,
        num_woofers=0,
        edge_tab=True,
        planar_baffle_cut=True,
        woofer_slot_tabs=True,
        cable_holes=True,
        bottom=False):
    r = Rib(angle=angle)

    import cadquery as cq
    rib = cq.Workplane("XY").center(0, 0)
    # rib = rib.rotateAboutCenter((CBT_OUTER_RADIUS, 0 ,0), angleDegrees=180)

    if planar_baffle_cut:

        # Fix platform of planar since the cut didn't work when exporting
        rib = rib.lineTo(0, 0.4)
        rib = rib.lineTo(r.edge_of_planar.x - drivers.PLANAR_TOP_EDGE_OFFSET - 0.3, 0.4)
        rib = rib.lineTo(r.edge_of_planar.x - drivers.PLANAR_TOP_EDGE_OFFSET - 0.15, 0.3)

        rib = rib.lineTo(r.edge_of_planar.x - drivers.PLANAR_TOP_EDGE_OFFSET, r.edge_of_planar.y)
        rib = rib.lineTo(r.edge_of_planar.x - drivers.PLANAR_TOP_EDGE_OFFSET,
                         r.edge_of_planar.y + CBT_PLANAR_BRACKET_BOARD_THICKNESS)
        rib = rib.lineTo(r.edge_of_planar.x - planar_bracket_edge_margin,
                         r.edge_of_planar.y + CBT_PLANAR_BRACKET_BOARD_THICKNESS)
        rib = rib.lineTo(r.edge_of_planar.x - planar_bracket_edge_margin, r.edge_of_planar.y)
        rib = rib.lineTo(r.edge_of_planar.x, r.edge_of_planar.y)
    else:
        rib = rib.moveTo(0, - CBT_PLANAR_BRACKET_BOARD_THICKNESS)
        rib = rib.lineTo(r.edge_of_planar.x, r.edge_of_planar.y - CBT_PLANAR_BRACKET_BOARD_THICKNESS)
        rib = rib.lineTo(r.edge_of_planar.x - planar_bracket_edge_margin,
                         r.edge_of_planar.y - CBT_PLANAR_BRACKET_BOARD_THICKNESS)

    # rib = rib.lineTo(r.top_side_edge.x, r.top_side_edge.y)
    a = P(abs(r.edge_of_planar.x - r.top_side_edge.x), abs(r.edge_of_planar.y - r.top_side_edge.y)).angle()
    a = min(a, 30)

    # rib = rib.threePointArc((r.top_side_edge - (P(0.1, 0).rotate(40))).values(), r.top_side_edge.values())
    rib = rib.threePointArc((r.top_side_edge - (P(0.1, 0).rotate(a * 2))).values(), r.top_side_edge.values())

    margin = 0.05
    edge_board_thickness = CBT_OUTER_BOARD_THICKNESS + margin
    edge_inner_board_thickness = CBT_INNER_BOARD_THICKNESS + margin
    rib = rib.polarLine(distance=0 - edge_board_thickness, angle=r.side_middle_vector.angle() + 90)
    if edge_tab:
        rib = rib.polarLine(distance=0 - r.side_middle_vector.length() / 3, angle=r.side_middle_vector.angle())
        rib = rib.polarLine(distance=0 - edge_inner_board_thickness - 0.05, angle=r.side_middle_vector.angle() - 90)
        rib = rib.polarLine(distance=0 - r.side_middle_vector.length() / 3, angle=r.side_middle_vector.angle())
        rib = rib.polarLine(distance=0 - edge_inner_board_thickness - 0.05, angle=r.side_middle_vector.angle() + 90)
        rib = rib.polarLine(distance=0 - r.side_middle_vector.length() / 3,
                            angle=r.side_middle_vector.angle())
    else:
        rib = rib.polarLine(distance=0 - r.side_middle_vector.length(), angle=r.side_middle_vector.angle())
    # rib = rib.polarLine(distance=0 - edge_board_thickness, angle=r.side_middle_vector.angle() - 90)

    rib = rib.lineTo(r.side_middle.x, r.side_middle.y)
    rib = rib.lineTo(r.bottom_board_width.x, r.bottom_board_width.y)
    rib = rib.lineTo(r.bottom_board_width.x, r.bottom_board_width.y - r.spine_depth)
    rib = rib.line(-r.bottom_board_width.x, 0)

    result = rib.close() \
        .extrude(CBT_INNER_BOARD_THICKNESS) \
        .mirror(mirrorPlane='ZY', union=True)

    # Cut woofers
    if num_woofers > 0:
        for woofer in r.woofers_xy[num_woofers]:
            result = result.cut(
                cq.Workplane("XY")
                    .moveTo(woofer.x, woofer.y)
                    .circle(SB65_CUTOUT_DIAMETER / 2)
                    .extrude(CBT_INNER_BOARD_THICKNESS)
            )

            woofer_holes = list(map(lambda angle: woofer + P(angle=angle, length=SB65_SCREW_HOLES_DIAMETER / 2),
                                    [45, 135, 225, 315]))
            for woofer_hole_angle in woofer_holes:
                result = result.cut(
                    cq.Workplane("XY")
                        .moveTo(woofer_hole_angle.x, woofer_hole_angle.y)
                        .circle(SB65_SCREW_HOLE_DIAMETER / 2)
                        .extrude(CBT_INNER_BOARD_THICKNESS)
                )

    # Cut tabs
    if woofer_slot_tabs:
        for tab in r.slot_tabs:
            t = SlotTab()
            result = result.cut(
                cq.Workplane("XY") \
                    .center(0, 0) \
                    # .moveTo(tab["center"].x, tab["center"].y) \
                    .rect(t.outer_width, CBT_INNER_BOARD_THICKNESS) \
                    .extrude(CBT_INNER_BOARD_THICKNESS) \
                    .rotate((0, 0, 0), (0, 0, 1), 0 + tab["angle"]) \
                    .translate((tab["center"].x, tab["center"].y, 0))
            )

    # cut cable holes
    if cable_holes:
        for hole in r.cable_holes:
            result = result.cut(
                cq.Workplane("XY") \
                    .moveTo(hole.x, hole.y) \
                    .circle(min(1, abs(hole.x / 3))) \
                    .extrude(CBT_INNER_BOARD_THICKNESS)
            )

    # if bottom, cut holes to mount to base
    if bottom:
        for hole in [
            P(10, 5),
            P(-10, 5),
            P(2.5, 16),
            P(-2.5, 16),
        ]:
            result = result.cut(
                cq.Workplane("XY") \
                    .moveTo(hole.x, hole.y) \
                    .circle(0.3) \
                    .extrude(CBT_INNER_BOARD_THICKNESS)
            )

    return result


class Side:
    def __init__(self, angle):
        self.outer_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_FRONT_XY.length(),
                                                              upper_width=CBT_TOP_FRONT_XY.length(),
                                                              angle_cutoff=CBT_DEGREE_CUTOFF))
        self.inner_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_REAR_XY.length(),
                                                              upper_width=CBT_TOP_REAR_XY.length(),
                                                              angle_cutoff=CBT_DEGREE_CUTOFF))

        self.outer_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.outer_xy.x).y or 0

        self.inner_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.inner_xy.x).y or 0

        self.outer_z_ratio = 1 - (self.outer_z / side_angle_xz.y)
        self.inner_z_ratio = 1 - (self.inner_z / side_angle_xz.y)

        self.outer_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.outer_z_ratio + CBT_TOP_WIDTH
        self.inner_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.inner_z_ratio + CBT_TOP_WIDTH


def calculate_edge(layers=2):
    angles = list(range(0, int(CBT_DEGREE_CUTOFF))) + [CBT_DEGREE_CUTOFF]

    #     Lengthen x-axis since we have it at an angle
    ratio = side_angle_xz.length() / side_angle_xz.x

    move_to_center = lambda t: (t[0] - CBT_BOTTOM_FRONT_XY.x, t[1])

    transform_x = lambda t: (ratio * t[0], t[1])
    # transform_x = lambda t: t

    outer = list(map(
        lambda angle: transform_x(move_to_center((P(angle=angle, length=Side(angle).outer_xy.length())).values())),
        angles
    ))

    inner = list(map(
        lambda angle: transform_x(move_to_center((P(angle=angle, length=Side(angle).inner_xy.length())).values())),
        reversed(angles)
    ))

    import cadquery as cq

    spine = cq.Workplane("XY")
    # spine = spine.center(-CBT_OUTER_RADIUS + CBT_BOTTOM_EDGE_OFFSET, 0)
    spine = spine.center(0, 0)

    spine = spine.spline(outer)
    spine = spine.lineTo(inner[0][0], inner[0][1])
    spine = spine.spline(inner)

    return spine.close() \
        .extrude(layers * CBT_INNER_BOARD_THICKNESS)


def calculate_slot_rear(angle):
    import cadquery as cq

    r = Rib(angle)

    return cq.Workplane("XY") \
        .rect(r.woofer_slot_width, r.woofer_slot_height) \
        .extrude(CBT_INNER_BOARD_THICKNESS)


def calculate_slot_edge():
    import cadquery as cq
    return cq.Workplane("XY") \
        .rect(30, Rib(0).woofer_slot_height + CBT_INNER_BOARD_THICKNESS * 2) \
        .extrude(CBT_INNER_BOARD_THICKNESS)


class SlotTab:
    def __init__(self):
        self.inner_height = (Rib(0).woofer_slot_height)
        self.outer_height = self.inner_height + CBT_INNER_BOARD_THICKNESS * 2

        self.inner_width = 2.5
        self.outer_width = self.inner_width / 2


def calculate_slot_tab():
    import cadquery as cq

    s = SlotTab()
    height = s.outer_height + CBT_INNER_BOARD_THICKNESS * 2
    return cq.Workplane("XY") \
        .box(s.outer_width, height, CBT_INNER_BOARD_THICKNESS) \
        .union(
        cq.Workplane("XY") \
            .box(s.inner_width, CBT_INNER_BOARD_THICKNESS, CBT_INNER_BOARD_THICKNESS) \
            .translate((0, height / 2 - CBT_INNER_BOARD_THICKNESS / 2, 0))
    ) \
        .translate((0, 0, CBT_INNER_BOARD_THICKNESS / 2))


def calculate_square(length=10):
    import cadquery as cq
    return cq.Workplane("XY") \
        .rect(length, length) \
        .extrude(CBT_INNER_BOARD_THICKNESS)


def do_operations(shape, oprations, invert=False):
    out = shape

    for op in oprations if not invert else reversed(oprations):
        if op["type"] == "rotate":
            out = out.rotate((0, 0, 0), op["xyz"], op["angle"] if not invert else -op["angle"])
        elif op["type"] == "translate":
            out = out.translate(op["xyz"] if not invert else (-op["xyz"][0], -op["xyz"][1], -op["xyz"][2]))

    return out


def render_cbt(
        render_spine=True,
        render_edge=True,
        render_ribs=True,
        render_slots=True,
        render_tabs=True,
        render_planars=True,
        render_planar_brackets=True,
        render_woofers=True,
        render_2d_projection=False):
    shapes_2d = [(0, calculate_square(length=10))]  # (<rough width in cm>, <shape>)

    spine = None
    if render_spine:
        spine = calculate_spine()
        if render_2d_projection:
            shapes_2d.append((50, spine))

        spine = spine.translate((0, 0, -CBT_SPINE_BOARD_THICKNESS / 2))
        spine = spine.rotate((0, 0, 0), (0, 0, 1), 90)
        spine = spine.rotate((0, 0, 0), (0, 1, 0), 90)
        spine = spine.rotate((0, 0, 0), (0, 0, 1), 180)

    edge_operations = [
        {"type": "translate", "xyz": (0, 0, -CBT_INNER_BOARD_THICKNESS)},
        {"type": "rotate", "xyz": (1, 0, 0), "angle": 90},
        {"type": "rotate", "xyz": (0, 0, 1), "angle": -90},
        {"type": "translate", "xyz": (0, -0.15, 0)},
        {"type": "rotate", "xyz": (0, 0, CBT_BOTTOM_EDGE_OFFSET / 2), "angle": side_angle_xz.angle()},
        {"type": "translate", "xyz": (Rib(0).top_side_edge.x - CBT_OUTER_BOARD_THICKNESS, Rib(0).top_side_edge.y, 0)},
    ]
    inner_edge = None
    if render_edge:
        inner_edge = calculate_edge(layers=1)
        inner_edge = do_operations(inner_edge, edge_operations)

    # if render_2d_projection:
    #     shapes_2d.append((10, calculate_slot_edge().rotateAboutCenter((0, 0, 1), -90)))

    rear_slots = []
    if render_slots:
        for i in range(0, len(WOOFERS_R)):
            angle = WOOFERS_R[i]
            outer_radius = P(length=CBT_OUTER_RADIUS, angle=angle)

            slot_rear = calculate_slot_rear(angle=angle)
            if render_2d_projection:
                shapes_2d.append((4, slot_rear.rotateAboutCenter((0, 0, 1), -90)))

            slot_rear = slot_rear.rotate((0, 0, 0), (1, 0, 0), -90)
            slot_rear = slot_rear.translate((0, Rib(angle).woofer_slot_y - CBT_INNER_BOARD_THICKNESS / 2, 0))
            slot_rear = slot_rear.rotate((0, 0, 0), (-1, 0, 0), angle)
            slot_rear = slot_rear.translate((0, CBT_OUTER_RADIUS - outer_radius.x, outer_radius.y))
            rear_slots.append(slot_rear)

    tabs = None
    if render_tabs:
        for i in range(0, len(WOOFERS_R)):
            angle = WOOFERS_R[i]
            if angle in [WOOFERS_R[-1], WOOFERS_R[-2]]:
                continue
            outer_radius = P(length=CBT_OUTER_RADIUS, angle=angle)
            r = Rib(angle=angle)
            for t in r.slot_tabs:
                tab = calculate_slot_tab()

                tab = tab.translate((0, 0, -CBT_INNER_BOARD_THICKNESS / 2))
                tab = tab.rotate((0, 0, 0), (1, 0, 0), 90)
                tab = tab.translate((t["center"].x, t["center"].y, 0))
                tab = tab.rotate((0, 0, 0), (-1, 0, 0), angle)
                tab = tab.translate((0, CBT_OUTER_RADIUS - outer_radius.x, outer_radius.y))

                tabs = tab if tabs is None else tabs.union(tab)

    planars = None
    planar_brackets = None
    for i in range(0, len(PLANARS_R)):
        angle = PLANARS_R[i]
        outer_radius = P(length=CBT_OUTER_RADIUS, angle=angle)

        operations = []
        operations.append({"type": "translate", "xyz": (0, 0, 0.2)})
        operations.append({"type": "rotate", "xyz": (1, 0, 0), "angle": -90})
        if i in [0, 2, 4]:
            operations.append({"type": "rotate", "xyz": (0, 1, 0), "angle": 180})
        operations.append({"type": "rotate", "xyz": (-1, 0, 0), "angle": angle})
        operations.append({"type": "translate", "xyz": (0, CBT_OUTER_RADIUS - outer_radius.x, outer_radius.y)})

        if render_planars:
            p = drivers.planar()
            p = do_operations(p, operations)
            planars = p if planars is None else planars.union(p)

        if render_planar_brackets:
            p = drivers.planar_bracket(CBT_PLANAR_BRACKET_BOARD_THICKNESS, cut_top_bottom=True,
                                       side_tab_from_edge=planar_bracket_side_tab_from_edge,
                                       side_tab_margin=planar_bracket_edge_margin)
            p = do_operations(p, operations)
            planar_brackets = p if planar_brackets is None else planar_brackets.union(p)

    woofers = None
    ribs = []
    for n in [
        {"angle": SPINES_R[0], "num_woofers": 0, "edge_tab": False, "planar_baffle_cut": False, "bottom": True},

        {"angle": SPINES_R[1], "num_woofers": 0, "planar_baffle_cut": False, "bottom": True},
        {"angle": SPINES_R[2], "num_woofers": 2, "woofer_facing": "up"},

        {"angle": SPINES_R[3], "num_woofers": 2, "woofer_facing": "down"},
        {"angle": SPINES_R[4], "num_woofers": 2, "woofer_facing": "up"},

        {"angle": SPINES_R[5], "num_woofers": 2, "woofer_facing": "down"},
        {"angle": SPINES_R[6], "num_woofers": 2, "woofer_facing": "up"},

        {"angle": SPINES_R[7], "num_woofers": 2, "woofer_facing": "down"},
        {"angle": SPINES_R[8], "num_woofers": 1, "woofer_facing": "up"},

        {"angle": SPINES_R[9], "num_woofers": 2, "woofer_facing": "down"},
        {"angle": SPINES_R[10], "num_woofers": 2, "woofer_facing": "up"},

        {"angle": SPINES_R[11], "num_woofers": 1, "woofer_facing": "down"},
        {"angle": SPINES_R[12], "num_woofers": 1, "woofer_facing": "up"},

        {"angle": SPINES_R[13], "num_woofers": 1, "woofer_facing": "down"},
        {"angle": SPINES_R[14], "num_woofers": 1, "woofer_facing": "up"},

        {"angle": SPINES_R[15], "num_woofers": 1, "woofer_facing": "down", "woofer_slot_tabs": False,
         "cable_holes": False},
        {"angle": SPINES_R[16], "num_woofers": 0, "planar_baffle_cut": False, "woofer_slot_tabs": False,
         "cable_holes": False},
        {"angle": SPINES_R[16] + geom.distance_across_circle(0.2, CBT_OUTER_RADIUS), "only_cut": True,
         "cable_holes": False},
    ]:
        if render_ribs:
            outer_radius = P(length=CBT_OUTER_RADIUS, angle=n["angle"])
            rib = calculate_rib(angle=n["angle"],
                                num_woofers=n["num_woofers"] if "num_woofers" in n else 0,
                                edge_tab=n["edge_tab"] if "edge_tab" in n else True,
                                planar_baffle_cut=n["planar_baffle_cut"] if "planar_baffle_cut" in n else True,
                                woofer_slot_tabs=n["woofer_slot_tabs"] if "woofer_slot_tabs" in n else True,
                                cable_holes=n["cable_holes"] if "cable_holes" in n else True,
                                bottom=n["bottom"] if "bottom" in n else False,
                                )

            only_cut = "only_cut" in n and n["only_cut"]
            if render_2d_projection and not only_cut:
                shapes_2d.append((32, rib))

            rib = rib.translate((0, 0, -CBT_INNER_BOARD_THICKNESS / 2))
            rib = rib.rotate((0, 0, 0), (1, 0, 0), -n["angle"])
            rib = rib.translate((0, CBT_OUTER_RADIUS - outer_radius.x, outer_radius.y))

            if render_edge:
                inner_edge = inner_edge.cut(rib)

            if only_cut:
                continue

            ribs.append(rib)

        if render_woofers:
            if n["num_woofers"] > 0:
                r = Rib(angle=n["angle"])
                face_up = n["woofer_facing"] == "up"
                rib_woofers = None

                for woofer in r.woofers_xy[n["num_woofers"]]:
                    w = drivers.sb65()
                    w = w.rotate((0, 0, 0), (0, 0, 1), 90)
                    w = w.translate((woofer.x, woofer.y, 0))

                    rib_woofers = w if rib_woofers is None else rib_woofers.union(w)

                rib_woofers = rib_woofers.translate((0, 0, CBT_INNER_BOARD_THICKNESS / 2))

                if not face_up:
                    rib_woofers = rib_woofers.rotate((0, 0, 0), (0, 1, 0), 180)

                rib_woofers = rib_woofers.rotate((0, 0, 0), (1, 0, 0), -n["angle"])

                rib_woofers = rib_woofers.translate((0, CBT_OUTER_RADIUS - outer_radius.x, outer_radius.y))

                woofers = rib_woofers if woofers is None else woofers.union(rib_woofers)

    if render_edge and render_2d_projection:
        shapes_2d.append((50, do_operations(inner_edge, edge_operations, invert=True)))

    if render_tabs and render_2d_projection:
        shapes_2d.append((5, calculate_slot_tab()))

    options = {}
    # options = {"alpha":0.5, "color": (64, 164, 223)}

    if render_2d_projection:
        offset = 0
        sum = None
        for shape in shapes_2d:
            offset += shape[0]
            s = shape[1]
            s = s.translate((offset, 0, 0))
            sum = s if sum is None else sum.union(s)

        sum = sum.faces("-Z")
        show_object(sum)
        import cadquery as cq
        cq.exporters.export(sum.section(), f"planar-cbt.dxf")
    else:

        if render_spine:
            show_object(spine, name='spine', options=options)
        if render_edge:
            show_object(inner_edge.mirror(mirrorPlane='YZ', union=True), name='inner_edge', options=options)

        for i in range(0, len(ribs)):
            show_object(ribs[i], name=f"rib{i}", options=options)

        for i in range(0, len(rear_slots)):
            show_object(rear_slots[i], name=f"rear_slot{i}", options=options)

        if render_tabs:
            show_object(tabs, name=f"tabs", options=options)

        if planar_brackets is not None:
            show_object(planar_brackets, name='planar_brackets', options=options)

        if planars is not None:
            show_object(planars, name='planars', options={"color": (75, 75, 75)})
        if woofers is not None:
            show_object(woofers, name='woofers', options={"color": (125, 125, 125)})

    # Strip top layer
    # result = result.faces("+Z")

    # r.exportSvg('test.svg', view_vector = (-1.75,1.1,5))
    # cq.exporters.export(result.section(),'result.dxf')
    # import cadquery as cq
    # cq.exporters.export(spine, 'result.dxf')


def export(workplane, name):
    # Strip top layer
    workplane = workplane.faces("-Z")

    import cadquery as cq
    cq.exporters.export(workplane.section(), f"{name}.dxf")

    # In inkscape:
    # 1. scale to correct size
    # 2. CTRL+A all lines (select and transform objects)
    # 3. CTRL+A all lines (edit paths by nodes) and select "Join selected nodes"


# render_cbt()

render_cbt(
    render_spine=True,
    render_ribs=True,
    render_edge=True,
    render_slots=False,
    render_tabs=True,
    render_planars=True,
    render_planar_brackets=True,
    render_woofers=True,
    render_2d_projection=False
)

# spine = calculate_spine()
# rib = calculate_rib(angle=SPINES_R[2],
#                     num_woofers=3,
#                     edge_tab=True,
#                     planar_baffle_cut=True,
#                     bottom=False)
# rib = rib.rotate((0, 0, 0), (0, 0, 1), angleDegrees=45)
# rib = rib.translate((15.5, -15.5, 0))
#
# slot = calculate_slot_rear(angle=0)
#
# edge = calculate_edge(layers=1)
#
# tab = calculate_slot_tab()
#
# pb = drivers.planar_bracket(CBT_PLANAR_BRACKET_BOARD_THICKNESS, cut_top_bottom=True,
#                             side_tab_from_edge=planar_bracket_side_tab_from_edge,
#                             side_tab_margin=planar_bracket_edge_margin)

# Side(0)
# Side(15)
# Side(50)
# Rib(0)
# Rib(50)

# pb = drivers.planar_bracket(CBT_PLANAR_BRACKET_BOARD_THICKNESS, cut_top_bottom=True, side_tab_from_edge=planar_bracket_side_tab_from_edge, side_tab_margin=planar_bracket_edge_margin)
# export(pb, 'planar_bracket')
