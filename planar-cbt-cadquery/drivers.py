from geometry_2d import P as P

PLANAR_BASE_THICKNESS = 0.3
PLANAR_TOP_EDGE_OFFSET = 1.1


def planar(connector_flat_tab_margin=True, show_connectors=True, extrude_screws=True, render_ridges=True, only_cutout=False):
    import cadquery as cq
    width = 6.5
    height = 20
    base_thickness = PLANAR_BASE_THICKNESS

    middle_thickness = 1.1

    base = cq.Workplane("XY") \
        .box(width, height, base_thickness) \
        .edges("|Z") \
        .fillet(0.7)

    top_edge_offset = PLANAR_TOP_EDGE_OFFSET

    top = cq.Workplane("XY") \
        .workplane(offset=base_thickness / 2) \
        .rect(width - 2 * top_edge_offset, height - 2 * top_edge_offset) \
        .workplane(offset=(middle_thickness - base_thickness) / 2) \
        .rect(width - 2 * top_edge_offset - 0.4, height - 2 * top_edge_offset - 0.4) \
        .loft(combine=True) \
        .faces("+Z") \
        .fillet(0.2) \
        .mirror(mirrorPlane='XY', union=True)

    result = base.union(top)

    if only_cutout:
        return result

    screw_hole_diameter = 0.3
    rivet_up_diameter = 0.55
    rivet_down_diameter = 0.4
    connector_screw_diameter = 0.7
    for xy_diameter in [
        # Screw holes
        (0.5, 4.25, screw_hole_diameter, screw_hole_diameter),
        (0.5, height - 4.25, screw_hole_diameter, screw_hole_diameter),

        (width - 0.5, 4.25, screw_hole_diameter, screw_hole_diameter),
        (width - 0.5, height - 4.25, screw_hole_diameter, screw_hole_diameter),

        # Rivets
        (width / 2, 0.5, rivet_up_diameter, rivet_down_diameter),

        (0.5, 1.4, rivet_up_diameter, rivet_down_diameter),
        (0.5, 7.15, rivet_up_diameter, rivet_down_diameter),
        (0.5, height - 7.15, rivet_up_diameter, rivet_down_diameter),
        (0.5, height - 1.4, rivet_up_diameter, rivet_down_diameter),

        (width - 0.5, 1.4, rivet_up_diameter, rivet_down_diameter),
        (width - 0.5, 7.15, rivet_up_diameter, rivet_down_diameter),
        (width - 0.5, height - 7.15, rivet_up_diameter, rivet_down_diameter),
        (width - 0.5, height - 1.4, rivet_up_diameter, rivet_down_diameter),

        # Connector screws
        (1.3, height - 0.9, connector_screw_diameter, connector_screw_diameter),
        (width - 1.3, height - 0.9, connector_screw_diameter, connector_screw_diameter),
    ]:
        plus = cq.Workplane("XY").moveTo(xy_diameter[0] - width / 2, xy_diameter[1] - height / 2).circle(
            xy_diameter[2] / 2).extrude(-0.7)
        minus = cq.Workplane("XY").moveTo(xy_diameter[0] - width / 2, xy_diameter[1] - height / 2).circle(
            xy_diameter[3] / 2).extrude(0.7)
        result = result.union(plus) if extrude_screws else result.cut(plus)
        result = result.union(minus) if extrude_screws else result.cut(minus)

    connector_base = cq.Workplane("XY") \
        .workplane(offset=base_thickness / 2) \
        .moveTo(0, height - 0.95 - height / 2) \
        .rect(4.8, 0.9) \
        .extrude(0.2) \
        .mirror(mirrorPlane='XY', union=True)
    result = result.union(connector_base)

    if show_connectors:
        connectors = cq.Workplane("XY") \
            .workplane(offset=base_thickness / 2) \
            .moveTo(0, height - 0.95 - height / 2) \
            .rect(3, 0.9) \
            .extrude(2.5 if connector_flat_tab_margin else 1)
        result = result.union(connectors)

    if render_ridges:
        ridges = cq.Workplane("XY") \
            .workplane(offset=middle_thickness / 2) \
            .moveTo(0, 5.3 / 2) \
            .rect(3.3, 0.3) \
            .extrude(0.1) \
            .faces("+Z") \
            .fillet(0.05) \
            .mirror(mirrorPlane='XY', union=True) \
            .mirror(mirrorPlane='XZ', union=True)

        result = result.union(ridges)

    for row in [1.8, 2.75]:
        for column in [2.9, 4.1, 5.3, 6.5, 8.2, 9.4]:
            result = result.cut(
                cq.Workplane("XY") \
                    .moveTo(row - width / 2, column - height / 2) \
                    .rect(0.5, 0.5) \
                    .extrude(2) \
                    .mirror(mirrorPlane='XY', union=True) \
                    .mirror(mirrorPlane='XZ', union=True) \
                    .mirror(mirrorPlane='YZ', union=True)
            )
            for offset in [0.25, -0.25]:
                result = result.cut(
                    cq.Workplane("XY") \
                        .moveTo(row - width / 2, offset + column - height / 2) \
                        .circle(0.5 / 2) \
                        .extrude(2) \
                        .mirror(mirrorPlane='XY', union=True) \
                        .mirror(mirrorPlane='XZ', union=True) \
                        .mirror(mirrorPlane='YZ', union=True)
                )

    return result.translate((0, 0, -PLANAR_BASE_THICKNESS / 2))


def planar_bracket(board_thickness=0.4, cut_top_bottom=False, side_tab_from_edge=None, side_tab_margin=None):
    import cadquery as cq
    width = 6.5
    height = 20

    result = cq.Workplane("XY") \
        .workplane(offset=PLANAR_BASE_THICKNESS) \
        .box(width, height, board_thickness) \
        .edges("|Z") \
        .fillet(0.7)

    if side_tab_from_edge is not None:
        y_margin = 0.5
        result = result.cut(
            cq.Workplane("XY") \
                .workplane(offset=PLANAR_BASE_THICKNESS) \
                .moveTo(width / 2, height / 2 - side_tab_from_edge + y_margin) \
                .box(side_tab_margin * 2, board_thickness + y_margin * 2, board_thickness) \
                .mirror(mirrorPlane='XZ', union=True) \
                .mirror(mirrorPlane='YZ', union=True)
        )

    edge_width = 1.1
    result = result.cut(
        cq.Workplane("XY") \
            .workplane(offset=PLANAR_BASE_THICKNESS) \
            .box(width - 2 * edge_width, height - 2 * edge_width, board_thickness)
    )

    screw_hole_diameter = 0.3
    rivet_rear_diameter = 0.45

    for xy_diameter in [
        # Screw holes
        (0.5, 4.25, screw_hole_diameter),
        (0.5, height - 4.25, screw_hole_diameter),

        (width - 0.5, 4.25, screw_hole_diameter),
        (width - 0.5, height - 4.25, screw_hole_diameter),

        # Rivets
        (width / 2, 0.5, rivet_rear_diameter),

        (0.5, 1.4, rivet_rear_diameter),
        (0.5, 7.15, rivet_rear_diameter),
        (0.5, height - 7.15, rivet_rear_diameter),
        (0.5, height - 1.4, rivet_rear_diameter),

        (width - 0.5, 1.4, rivet_rear_diameter),
        (width - 0.5, 7.15, rivet_rear_diameter),
        (width - 0.5, height - 7.15, rivet_rear_diameter),
        (width - 0.5, height - 1.4, rivet_rear_diameter),
    ]:
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(xy_diameter[0] - width / 2, xy_diameter[1] - height / 2) \
                .circle(xy_diameter[2] / 2) \
                .extrude(1) \
                .mirror(mirrorPlane='XY', union=True)
        )

    # Cut slot for connector tabs
    connector_rectangle_y_offset = 0.95
    connector_tab_height = 0.9
    result = result.cut(
        cq.Workplane("XY") \
            .moveTo(0, height - connector_rectangle_y_offset - height / 2) \
            .rect(4.8, connector_tab_height) \
            .extrude(1) \
            .mirror(mirrorPlane='XY', union=True)
    )

    # boop
    if cut_top_bottom:
        result = result.intersect(
            cq.Workplane("XY") \
                .workplane(offset=PLANAR_BASE_THICKNESS) \
                .box(width, height - (side_tab_from_edge - 0.4) * 2, board_thickness)
        )

    return result.translate((0, 0, -PLANAR_BASE_THICKNESS / 2))


def sb65(extrude_screws=False):
    import cadquery as cq
    width = 6.4
    diameter = 8

    base_thickness = 0.3

    base_square = cq.Workplane("XY") \
        .rect(width, width) \
        .extrude(0.3)

    base_circle = cq.Workplane("XY") \
        .circle(diameter / 2) \
        .extrude(0.3)

    result = base_square.intersect(base_circle)

    # Screw holes
    for angle in [45, 135, 225, 315]:
        center = P(angle=angle, length=7.2 / 2)
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(center.x, center.y) \
                .circle(0.65 / 2) \
                .extrude(0.2)
        )
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(center.x, center.y) \
                .circle(0.33 / 2) \
                .extrude(base_thickness)
        )
        if extrude_screws:
            result = result.union(
                cq.Workplane("XY") \
                    .moveTo(center.x, center.y) \
                    .circle(0.33 / 2) \
                    .extrude(1.0) \
                    .mirror(mirrorPlane='XY', union=True)
            )

    surround = cq.Workplane("XY") \
        .circle(5.7 / 2) \
        .extrude(-0.25) \
        .cut(
        cq.Workplane("XY") \
            .circle(4.5 / 2) \
            .extrude(-0.25)
    )
    result = result.union(surround)

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=base_thickness) \
            .circle(6.0 / 2) \
            .workplane(offset=0.8) \
            .circle(5.5 / 2) \
            .loft(combine=True)
    )

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=base_thickness + 0.8) \
            .circle(5.5 / 2) \
            .workplane(offset=1.1) \
            .circle(4.6 / 2) \
            .loft(combine=True)
    )

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=3.27 + base_thickness) \
            .circle(3.8 / 2) \
            .extrude(-1.5) \
            .faces("+Z") \
            .fillet(0.3)
    )

    # Connectors
    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=0.5) \
            .rect(1.0, 5.7) \
            .extrude(2.5)
    )

    return result


CBT_PLANAR_BRACKET_BOARD_THICKNESS = 0.3
planar_bracket_edge_margin = 0.15
# planar_bracket_side_tab_from_edge = 1.9  # 0.4 board thickness
planar_bracket_side_tab_from_edge = 1.94  # 0.3 board thickness


def show():
    # p = planar(extCrude_screws=False)
    # show_object(p, name='planar', options={"color": (75, 75, 75)})
    # show_object(planar_bracket(), name='planar_bracket')
    # show_object(planar_bracket(board_thickness=0.3), name='planar_bracket')
    # pb = planar_bracket(CBT_PLANAR_BRACKET_BOARD_THICKNESS, cut_top_bottom=True,
    #                     side_tab_from_edge=planar_bracket_side_tab_from_edge,
    #                     side_tab_margin=planar_bracket_edge_margin)
    # show_object(pb, name='planar_bracket')
    sb = sb65()
    show_object(sb, name='sb65', options={"color": (125, 125, 125)})

    # obj = p \
    #     .union(planar(show_connectors=False, extrude_screws=False).translate((10, 11, 0)).rotateAboutCenter((1, 0, 0), -90)) \
    #     .union(pb.translate((10, 0, 0))) \
    #     .union(sb.translate((10, 17, 0))) \
    #     .union(sb.translate((-2, 17, 0)).rotateAboutCenter((0, 1, 0), -90).rotateAboutCenter((1, 0, 0), 90)) \
    #     .union(sb.translate((4, 17, 0)).rotateAboutCenter((0, 1, 0), -90))
    #
    # show_object(obj.rotateAboutCenter((0, 0, 1), 90))

    # import cadquery as cq
    # workplane = obj
    # workplane = obj.intersect(
    #     cq.Workplane("XY") \
    #         .box(30, 30, 0.01)
    # ).faces("+Z")

    # show_object(workplane)
    #
    # cq.exporters.export(workplane.section(), "obj.dxf")


def export(workplane, name):
    # Strip top layer
    workplane = workplane.faces("+Z")

    import cadquery as cq
    cq.exporters.export(workplane.section(), f"{name}.dxf")

# show()
#
# export(planar_bracket(), 'planar_bracket')
