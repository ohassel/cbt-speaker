import cadquery as cq

from geometry_2d import P as P


def calc_part(
        platform_height=2.5 + 0.18,
        platform_chamfer_height=1.9 + 0.18,
        platform_depth=0.4,

        bottom_extra_y=0.18,

        screw_depth=0.2,
        screw_diameter=0.4,
        screw_chamfer=True,

        inside_platform_width=None,
        inside_platform_height=None,
        inside_platform_depth=None,

        inside_platform_screw_diameter=None,
        inside_platform_screw_depth=None,

        rotate_sign=1,
):
    b = cq.Workplane("XZ")
    b = b.moveTo(0, 0)

    p_bot_left = P(x=0, y=0)

    p_top_left = P(angle=90 - 1, length=0.5)
    # p_top_left = P(x=0, y=0.5)

    p_bot_right = P(x=2.65, y=0)
    p_top_right = p_bot_right + P(x=-0.25, y=0.25)

    b = b.lineTo(p_top_left.x, p_top_left.y)
    b = b.lineTo(p_top_right.x, p_top_right.y)
    b = b.lineTo(p_bot_right.x, p_bot_right.y)
    b = b.lineTo(p_bot_left.x, p_bot_left.y)

    b = b.close()

    b = b.extrude(5.5)
    b = b.translate((0, 5.5 / 2, 0))

    boop = 1.5
    b = (
        cq.Workplane("XY")
        .rect(2.65, 5.5)

        .workplane(offset=0.5)
        .center(-boop/2 + 0.2, 0)
        .rect(boop, 3.5)
        .loft(combine=True)
        .translate((1.325, 0 ,0))
        # .translate((0, 2.65/2, platform_height / 2))
    )

    # bb = (
    #     cq.Workplane("XY")
    #         .lineTo(2.65, 0)
    #         .lineTo(2.65, 5.5 / 2 - 0.1)
    #         .lineTo(0, 5.5 / 2)
    #         .lineTo(0, 0)
    #         .close()
    #         .mirrorX()
    #         .extrude(5)
    # )
    #
    # b = b.intersect(bb)

    r = cq.Workplane("YZ")
    r = r.rect(5.5, platform_height)

    r = r.workplane(offset=platform_depth)
    r = r.center(0, -(platform_height - platform_chamfer_height) / 2 + 0.1)
    r = r.rect(3.5, platform_chamfer_height)
    r = r.loft(combine=True)
    r = r.translate((0, 0, platform_height / 2))

    # t_depth_margin = 0.25
    t_depth_margin = 0.01
    t_height = 0.35 + bottom_extra_y
    t = (
        cq.Workplane("YZ")
            .rect(0.5, t_height)
            .workplane(offset=0.25 + t_depth_margin)
            .rect(0.45, t_height)
            .loft(combine=True)
    )
    t = t.translate((0, 4.5 / 2, t_height / 2))
    t = t.rotate((0, 0, 0), (0, 0, 1), 180)
    t = t.mirror(mirrorPlane='XZ', union=True)
    t = t.translate((t_depth_margin, 0, 0))

    screw_chamfer_diameter = 0.9

    c0 = (
        cq.Workplane("XY")
            .circle(screw_diameter / 2)
            .workplane(offset=screw_depth)
            .circle(screw_diameter / 2)
            .loft(combine=True)
    )

    if screw_chamfer:
        c1 = (cq.Workplane("XY")
              .circle(screw_diameter / 2)
              .workplane(offset=screw_depth)
              .circle(screw_chamfer_diameter / 2)
              .loft(combine=True)
              .translate((0, 0, 0.2))
              )
        c = c0.union(c1)
    else:
        c = c0

    hole_y = 1.35 + bottom_extra_y
    c = c.rotate((0, 0, 0), (0, 1, 0), 90)
    c = c.translate((0, 0, hole_y))

    r = r.cut(c)

    inside_platform_screw_y = hole_y + 0.8

    if inside_platform_screw_diameter:
        ic = (
            cq.Workplane("XY")
                .moveTo(0, 0)
                .circle(inside_platform_screw_diameter / 2)
                .extrude(inside_platform_screw_depth)
                .rotate((0, 0, 0), (0, 1, 0), 90)
                .translate((0, 0, inside_platform_screw_y))
        )
        r = r.cut(ic)

    r = r.rotate((0, 0, 0), (0, 1, 0), 1 * rotate_sign)

    # r = r.faces('<X')
    # r = r.faces('>X')
    # r = r.fillet(0.3)
    #
    # r = r.edges('+X')
    # r = r.fillet(0.3)

    # b = b.faces('not<X')
    # b = b.fillet(0.1)

    b = b.union(t)
    b = b.union(r)

    if inside_platform_width:
        ip = (
            cq.Workplane("XY")
                .moveTo(0, 0)
                .rect(inside_platform_height, inside_platform_width)
                .extrude(inside_platform_depth)
                .rotate((0, 0, 0), (0, 1, 0), 90)
                .translate((0, 0, inside_platform_screw_y))
        )
        ip = ip.rotate((0, 0, 0), (0, 1, 0), 1 * rotate_sign)
        b = b.cut(ip)

    return b


b1 = calc_part()
b1 = b1.translate((1, 0, 0))

m4_threaded_insert_hole_diameter = 0.56
# m4_threaded_insert_hole_depth = 0.91
m4_threaded_insert_hole_depth = 0.51

inside_platform_screw_depth = 0.4 + 0.5

b2 = calc_part(
    platform_height=5.7,
    # platform_chamfer_height=4.1,
    platform_chamfer_height=3.9,
    # platform_depth=0.9 + m4_threaded_insert_hole_depth + 0.2,
    platform_depth=1.1,

    screw_depth=m4_threaded_insert_hole_depth + 0.4,
    screw_diameter=m4_threaded_insert_hole_diameter,
    screw_chamfer=False,

    inside_platform_width=3.5,
    inside_platform_height=5.0,
    inside_platform_depth=0.4,

    # inside_platform_screw_diameter=0.9 + 1.2,
    inside_platform_screw_diameter=0.9,
    inside_platform_screw_depth=0.4 + 0.5,

    rotate_sign=-1,
)
b2 = b2.rotate((0, 0, 0), (0, 0, 1), 180)
b2 = b2.translate((-1, 0, 0))

# show_object(t, name='t', options={})
# show_object(r, name='r1', options={})
show_object(b1, name='b1', options={})
show_object(b2, name='b2', options={})

cq.exporters.export(b1.union(b2), "kitchen-blocks.step")
