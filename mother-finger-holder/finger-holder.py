import cadquery as cq

from geometry_2d import P as P

d_margin = 0.5
thickness = 0.3

# w_id_h = [
#     (7.0, 11.0, 1.0),
#     (7.0, 10.0, 1.2),
#     (7.0, 9.5, 0.5),
# ]
#
# print(w_id_h)

# h0 = 1.0
# h1 = 1.2
# h2 = 0.5
#
# h0_id = 11
# h1_id = 10
# h2_id = 9.5

# h = 2.6


# print(h0)
h = 5.8


def calc_finger_boop(
        w0=0.0,
        id0=0.0,
        h0=0.0):
    # h0 = w_id_h[0]
    r = cq.Workplane("front")
    w = w0 + thickness
    id = id0 + 3.14 * thickness
    d = (id - w) / 2
    r = r.box(w, d, 0.1)
    r = r.faces(">Z")
    r = r.rect(w, d)
    # r = r.edges("|Z") \
    #     .fillet(d/2 - 0.01)

    r = r.workplane(offset=h0)
    # w = w0 - 0.5 + thickness
    w = w0 + thickness
    id = id0 - 1.0 + 3.14 * thickness
    d = (id - w) / 2
    r = r.rect(w, d, 0.25)
    r = r.loft(combine=True)

    r = r.edges("|Z") \
        .fillet(d / 2 - 0.01)

    return r


# id = 12
w = 3.95
id = 8.5

w = 9
id = 16
h = 8


r = calc_finger_boop(w + 2 * thickness, id + 3.14 * thickness, h)

r = r.cut(calc_finger_boop(w + 0.25, id, h))


show_object(r, name='finger-holder', options={})


cq.exporters.export(r, "finger-holder.step")
