from geometry_2d import P as P


class SB65:
    def __init__(self):
        self.width = 6.4
        self.diameter = 8

        self.base_height = 0.3

        self.surround_height = 0.25
        self.surround_outer_diameter = 5.7
        self.surround_inner_diameter = 4.5

        self.screw_holes = [
            P(angle=45, length=7.2 / 2),
            P(angle=135, length=7.2 / 2),
            P(angle=225, length=7.2 / 2),
            P(angle=315, length=7.2 / 2),
        ]
        self.screw_hole_inner_diameter = 0.33
        self.screw_hole_larger_diameter = 0.65


def sb65(extrude_screws=False):
    import cadquery as cq

    sb65 = SB65()

    base_square = cq.Workplane("XY") \
        .rect(sb65.width, sb65.width) \
        .extrude(sb65.base_height)

    base_circle = cq.Workplane("XY") \
        .circle(sb65.diameter / 2) \
        .extrude(sb65.base_height)

    result = base_square.intersect(base_circle)

    # Screw holes
    for hole in sb65.screw_holes:
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(hole.x, hole.y) \
                .circle(sb65.screw_hole_larger_diameter / 2) \
                .extrude(0.2)
        )
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(hole.x, hole.y) \
                .circle(sb65.screw_hole_inner_diameter / 2) \
                .extrude(sb65.base_height)
        )
        if extrude_screws:
            result = result.union(
                cq.Workplane("XY") \
                    .moveTo(hole.x, hole.y) \
                    .circle(sb65.screw_hole_inner_diameter / 2) \
                    .extrude(1.0) \
                    .mirror(mirrorPlane='XY', union=True)
            )

    surround = cq.Workplane("XY") \
        .circle(sb65.surround_outer_diameter / 2) \
        .extrude(-sb65.surround_height) \
        .cut(
        cq.Workplane("XY") \
            .circle(sb65.surround_inner_diameter / 2) \
            .extrude(-sb65.surround_height)
    )
    result = result.union(surround)

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=(sb65.base_height)) \
            .circle(6.0 / 2) \
            .workplane(offset=0.8) \
            .circle(5.5 / 2) \
            .loft(combine=True)
    )

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=sb65.base_height + 0.8) \
            .circle(5.5 / 2) \
            .workplane(offset=1.1) \
            .circle(4.6 / 2) \
            .loft(combine=True)
    )

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=3.27 + sb65.base_height) \
            .circle(3.8 / 2) \
            .extrude(-1.5) \
            .faces("+Z") \
            .fillet(0.3)
    )

    # Connectors
    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=0.5) \
            .rect(1.0, 5.7) \
            .extrude(2.5)
    )

    return result


def sb65_bracket(height=6.5, board_thickness=0.4, top_cut_width=7.4, top_cut_board_width=0.65,
                 max_width=SB65().width + 0.6):
    import cadquery as cq

    sb65 = SB65()

    result = cq.Workplane("XY").center(0, 0) \
        .lineTo(0, height / 2) \
        .lineTo(max_width / 2, height / 2) \
        .lineTo(sb65.width / 2, height / 4) \
        .lineTo(max_width / 2, 0) \
        .lineTo(0, 0) \
        .close() \
        .extrude(board_thickness) \
        .mirror(mirrorPlane='YZ', union=True) \
        .mirror(mirrorPlane='XZ', union=True)

    # Cut hole for surround
    result = result.cut(
        cq.Workplane("XY") \
            .circle(sb65.surround_outer_diameter / 2) \
            .extrude(board_thickness)
    )

    # Screw holes
    for hole in sb65.screw_holes:
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(hole.x, hole.y) \
                .circle(sb65.screw_hole_inner_diameter / 2) \
                .extrude(board_thickness)
        )

    if top_cut_width > max_width:
        result = result.union(
            cq.Workplane("XY").center(0, 0) \
                .moveTo(0, height / 2) \
                .lineTo(top_cut_width / 2, height / 2) \
                .lineTo(top_cut_width, height / 2 - top_cut_board_width / 2) \
                .lineTo(0, height / 2 - top_cut_board_width / 2) \
                .close() \
                .extrude(board_thickness) \
                .mirror(mirrorPlane='YZ', union=True) \
                .mirror(mirrorPlane='XZ', union=True)
        )

    # Top cuts
    result = result.cut(
        cq.Workplane("XY").center(0, 0) \
            .moveTo(top_cut_width / 2, height / 2) \
            .lineTo(top_cut_width / 2, height / 2 - top_cut_board_width / 2) \
            .lineTo(top_cut_width, height / 2 - top_cut_board_width / 2) \
            .lineTo(top_cut_width, height / 2) \
            .close() \
            .extrude(board_thickness) \
            .mirror(mirrorPlane='YZ', union=True) \
            .mirror(mirrorPlane='XZ', union=True)
    )

    return result


def raw_binding_post():
    import cadquery as cq

    post = cq.Workplane("XY").center(0, 0)

    for r in range(0, 360 + 60, 60):
        post = post.polarLineTo(distance=0.6351, angle=r)

    post = post.close() \
        .extrude(0.25)

    post = post.union(
        cq.Workplane("XY").center(0, 0) \
            .workplane(offset=0.25) \
            .circle(1.0 / 2) \
            .extrude(1.5)
    )

    post = post.union(
        cq.Workplane("XY").center(0, 0) \
            .workplane(offset=0.25 + 0.75) \
            .circle(1.5 / 2) \
            .extrude(0.75) \
            .faces("+Z") \
            .fillet(0.2)
    )

    post = post.union(
        cq.Workplane("XY").center(0, 0) \
            .circle(0.7 / 2) \
            .extrude(-0.8)
    )

    post = post.union(
        cq.Workplane("XY").center(0, 0) \
            .workplane(offset=-0.8) \
            .circle(0.6 / 2) \
            .extrude(-3.1)
    )

    return post. \
        translate((1.2, 0, 0)) \
        .mirror(mirrorPlane='ZY', union=True)


# Dayton Audio BPA-38G HD
def binding_post():
    import cadquery as cq

    base_width = 5.5
    base_height = 2.4
    base_depth = 0.6

    result = cq.Workplane("XY") \
        .workplane(offset=(base_depth - 0.15) / 2) \
        .box(base_width, base_height, base_depth - 0.15)

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=base_depth / 2 + 0.15) \
            .rect(base_width, base_height) \
            .workplane(offset=0.15) \
            .rect(base_width - 2 * 0.15, base_height - 2 * 0.15) \
            .loft(combine=True)
    )


    result = result.union(
        cq.Workplane("XY").center(0 / 2, 0) \
            .moveTo(1.9 / 2, 0) \
            .circle(1 / 2) \
            .extrude(base_depth + 2) \
            .mirror(mirrorPlane='ZY', union=True)
    )

    result = result.union(
        cq.Workplane("XY") \
            .center(1.9 / 2, 0) \
            .box(0.6, 0.2, 1.35) \
            .translate((0, 0, -1.35 / 2)) \
            .mirror(mirrorPlane='ZY', union=True)
    )

    result = result.cut(
        cq.Workplane("XY") \
            .center(4.5 / 2, 0)
            .circle(0.3 / 2) \
            .extrude(base_depth) \
            .mirror(mirrorPlane='ZY', union=True)
        )

    return result


def show():
    # show_object(sb65(), name='sb65', options={"color": (125, 125, 125)})
    # show_object(sb65_bracket().translate((0, 0, -0.4)), name='sb65_bracket')
    show_object(binding_post(), name='binding_post')
    show_object(raw_binding_post(), name='binding_post_raw')

# show()
