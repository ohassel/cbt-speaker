import geometry_2d as geom
from geometry_2d import P as P
import drivers as drivers
import math

CBT_INNER_BOARD_THICKNESS = 0.65
CBT_SPINE_BOARD_THICKNESS = CBT_INNER_BOARD_THICKNESS
CBT_BASE_BOARD_THICKNESS = 0.9
CBT_BASE_THICKNESS = 3.6

CBT_BASE_DEPTH = 75

CBT_BOTTOM_SUPPORT_DEPTH = 8

CBT_OUTER_RADIUS = 150
NUM_DRIVERS = 24

CBT_SPINE_OFFSET = 5.5
CBT_INNER_RIB_WIDTH = 5.5
CBT_INNER_RIB_DEPTH = 8.5
CBT_SPINE_RIB_OUTER_CUTOUT_DEPTH = CBT_INNER_RIB_DEPTH - CBT_SPINE_OFFSET

CBT_UPPER_DEPTH = 8
CBT_LOWER_DEPTH = 24

CBT_TOP_WIDTH = 12
CBT_BOTTOM_WIDTH = 30
CBT_BOTTOM_EDGE_OFFSET = 3
CBT_TOP_EDGE_OFFSET = 1.5

# RIB_SIDE_WIDTH = 3
RIB_SIDE_WIDTH_TOP = 1.33
RIB_SIDE_WIDTH_BOTTOM = 2

SB65_SQUARE_WIDTH = 6.5
SB65_CUTOUT_DIAMETER = 5.7
SB65_SCREW_HOLES_DIAMETER = 7.2
SB65_SCREW_HOLE_DIAMETER = 0.33

DEGREES_PER_DRIVER = geom.distance_across_circle(SB65_SQUARE_WIDTH, CBT_OUTER_RADIUS)
bottom_driver_r_offset = geom.distance_across_circle(CBT_INNER_BOARD_THICKNESS, CBT_OUTER_RADIUS) * 1.5
top_driver_r_offset = geom.distance_across_circle(CBT_INNER_BOARD_THICKNESS, CBT_OUTER_RADIUS) * 0.5
CBT_DEGREE_CUTOFF = DEGREES_PER_DRIVER * NUM_DRIVERS + bottom_driver_r_offset + top_driver_r_offset

SB65_BRACKET_THICKNESS = 0.3
CBT_RIB_TAB_THICKNESS = 0.3
SB65_BRACKET_TOP_CUT_WIDTH = 6.7 + 0.3
SB65_BRACKET_HEIGHT = (((CBT_OUTER_RADIUS) * math.pi) / 180 * DEGREES_PER_DRIVER) - 0.006

# Loop all drivers and calculate x and y coordinates
WOOFERS_R = []
for n in range(1, NUM_DRIVERS + 1):
    WOOFERS_R.append((n - 0.5) * DEGREES_PER_DRIVER + bottom_driver_r_offset)

RIBS_R = [
    WOOFERS_R[0] - DEGREES_PER_DRIVER / 2 - geom.distance_across_circle(CBT_INNER_BOARD_THICKNESS, CBT_OUTER_RADIUS),
    WOOFERS_R[0] - DEGREES_PER_DRIVER / 2,
]
for r in WOOFERS_R:
    RIBS_R.append(r + DEGREES_PER_DRIVER / 2)

RIBS_R.append(RIBS_R[-1])


def calculate_width(angle, lower_width=CBT_LOWER_DEPTH, upper_width=CBT_UPPER_DEPTH, angle_cutoff=CBT_DEGREE_CUTOFF):
    return lower_width - (angle / angle_cutoff) * (lower_width - upper_width)


def calculate_side_width(angle):
    return calculate_width(angle, RIB_SIDE_WIDTH_BOTTOM, RIB_SIDE_WIDTH_TOP)


CBT_BOTTOM_FRONT_XY = P(CBT_OUTER_RADIUS - CBT_BOTTOM_EDGE_OFFSET, 0)
CBT_BOTTOM_REAR_XY = CBT_BOTTOM_FRONT_XY - P(calculate_side_width(0), 0)
CBT_TOP_FRONT_XY = P(angle=CBT_DEGREE_CUTOFF, length=CBT_OUTER_RADIUS - CBT_TOP_EDGE_OFFSET)
CBT_TOP_REAR_XY = CBT_TOP_FRONT_XY - P(angle=CBT_DEGREE_CUTOFF, length=calculate_side_width(CBT_DEGREE_CUTOFF))

# side_angle_xz = P(CBT_BOTTOM_FRONT_XY.x - CBT_TOP_REAR_XY.x, (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) / 2)
side_angle_xz = P(
    CBT_BOTTOM_FRONT_XY.x - CBT_TOP_REAR_XY.x,
    (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) / 2
)


#######################################
#######################################
def calculate_spine(outer_offset=CBT_SPINE_OFFSET, cable_cutout_slot=False, inner=True, is_inner_rib=lambda i: False):
    import cadquery as cq

    spine = cq.Workplane("XY")
    spine = spine.center(-CBT_OUTER_RADIUS, 0)

    cutoff_angle = CBT_DEGREE_CUTOFF
    angles = list(range(0, int(cutoff_angle))) + [cutoff_angle]

    cbt_outer_spine = CBT_OUTER_RADIUS - CBT_SPINE_OFFSET
    spine = spine.spline(
        list(map(
            lambda angle: P(angle=angle, length=CBT_OUTER_RADIUS - outer_offset).values()
            , angles))
    )

    spine_rear = lambda r: Rib(r).bottom.y - CBT_SPINE_OFFSET

    spine = spine.polarLineTo(distance=cbt_outer_spine - spine_rear(cutoff_angle), angle=cutoff_angle)

    spine = spine.spline(
        list(map(
            lambda angle: P(angle=angle, length=cbt_outer_spine - spine_rear(angle)).values()
            , reversed(angles)))
    )
    result = spine.close().extrude(CBT_SPINE_BOARD_THICKNESS)

    for i in range(0, len(RIBS_R)):
        r = RIBS_R[i]
        if i == len(RIBS_R) - 1:
            continue
        rib = Rib(r, inner_rib=is_inner_rib(i))

        depth = rib.spine_inner_cutout_depth if inner else rib.spine_outer_cutout_depth
        if i == 0 and not inner:
            depth = 30

        rect = geom.get_rectangle_xy(
            P(length=CBT_OUTER_RADIUS, angle=r),
            P(length=CBT_SPINE_OFFSET + depth, angle=r + 180),
            CBT_INNER_BOARD_THICKNESS
        )
        result = result.cut(
            cq.Workplane("XY") \
                .center(-CBT_OUTER_RADIUS, 0) \
                .moveTo(rect[0].x, rect[0].y) \
                .lineTo(rect[1].x, rect[1].y) \
                .lineTo(rect[2].x, rect[2].y) \
                .lineTo(rect[3].x, rect[3].y) \
                .lineTo(rect[0].x, rect[0].y) \
                .close()
                .extrude(CBT_SPINE_BOARD_THICKNESS)
        )

    # Cut hole to route cables out the rear
    bottom_depth = spine_rear(0)
    cable_hole_rear_x_offset = 2
    result = result.cut(
        cq.Workplane("XY") \
            .center(0, 0) \
            .moveTo(-CBT_SPINE_OFFSET - bottom_depth + (1 if cable_cutout_slot else 1.3) + cable_hole_rear_x_offset,
                    0 + 2 * CBT_INNER_BOARD_THICKNESS) \
            .lineTo(-CBT_SPINE_OFFSET - bottom_depth + (1 if cable_cutout_slot else 1.3) + cable_hole_rear_x_offset,
                    1 + 2 * CBT_INNER_BOARD_THICKNESS) \
            .lineTo(-CBT_SPINE_OFFSET - bottom_depth + 0 + cable_hole_rear_x_offset, 1 + 2 * CBT_INNER_BOARD_THICKNESS) \
            .lineTo(-CBT_SPINE_OFFSET - bottom_depth + 0 + cable_hole_rear_x_offset, 0 + 2 * CBT_INNER_BOARD_THICKNESS) \
            .close()
            .extrude(CBT_SPINE_BOARD_THICKNESS)
    )

    if cable_cutout_slot:
        result = result.cut(
            cq.Workplane("XY") \
                .center(0, 0) \
                .moveTo(-CBT_SPINE_OFFSET - bottom_depth + cable_hole_rear_x_offset, 1 + 2 * CBT_INNER_BOARD_THICKNESS) \
                .lineTo(-CBT_SPINE_OFFSET - bottom_depth, CBT_INNER_BOARD_THICKNESS) \
                .lineTo(-CBT_SPINE_OFFSET - bottom_depth, 0) \
                .lineTo(-CBT_SPINE_OFFSET - bottom_depth + 1, 0) \
                .lineTo(-CBT_SPINE_OFFSET - bottom_depth + 1 + cable_hole_rear_x_offset,
                        0 + 2 * CBT_INNER_BOARD_THICKNESS) \
                .close()
                .extrude(CBT_SPINE_BOARD_THICKNESS)
        )

    for i in range(0, len(WOOFERS_R)):
        if (i % 3 == 1) or (22 <= i):
            r = WOOFERS_R[i]
            hole = P(angle=r, length=cbt_outer_spine - (spine_rear(r) / 2))
            result = result.cut(
                cq.Workplane("XY") \
                    .center(-CBT_OUTER_RADIUS, 0) \
                    .moveTo(hole.x, hole.y) \
                    .circle(0.8 / 2) \
                    .extrude(CBT_SPINE_BOARD_THICKNESS) \
                )

    return result


class Rib:
    def __init__(self, angle, inner_rib=False):
        self.inner_rib = inner_rib
        if inner_rib:
            angle = RIBS_R[-4]

        side = Side(angle=angle)

        self.outer_rib_offset = calculate_width(angle=angle, lower_width=CBT_BOTTOM_EDGE_OFFSET,
                                                upper_width=CBT_TOP_EDGE_OFFSET,
                                                angle_cutoff=CBT_DEGREE_CUTOFF)

        self.edge_of_woofer = P(SB65_SQUARE_WIDTH / 2, 0) - P(0.4, 0)

        self.top_side_edge = P(side.outer_cbt_width / 2, self.outer_rib_offset)
        front_arc_r = P(abs(self.edge_of_woofer.x - self.top_side_edge.x),
                        abs(self.edge_of_woofer.y - self.top_side_edge.y)).angle()
        front_arc_r = min(front_arc_r, 30)
        self.front_arc_point = self.top_side_edge - (P(0.1, 0).rotate(front_arc_r * 2))

        self.side_middle_vector = P(length=calculate_side_width(angle),
                                    x=0 - (side.outer_cbt_width - side.inner_cbt_width) / 2)

        # self.side_middle_vector = P(0, RIB_SIDE_WIDTH).rotate(side_angle_xz.angle())
        self.side_middle = self.top_side_edge + self.side_middle_vector

        bottom_spike = P(0, self.side_middle.y + P(angle=45, x=self.side_middle.x).y)
        self.bottom_board_width = bottom_spike - P(angle=-45,
                                                   length=-math.sqrt(
                                                       2 * math.pow(CBT_SPINE_BOARD_THICKNESS * 3 / 2, 2)))
        self.bottom = P(0, self.bottom_board_width.y)

        depth = self.bottom.y - CBT_SPINE_OFFSET
        inner_rib_depth = CBT_INNER_RIB_DEPTH - CBT_SPINE_OFFSET
        self.spine_outer_cutout_depth = inner_rib_depth if self.inner_rib else (0.5 * depth)
        self.spine_inner_cutout_depth = self.spine_outer_cutout_depth / 3

        self.bottom_screw_holes = {
            self.bottom + P(4, -8),
            self.top_side_edge + P(-4, 2),
            self.top_side_edge + P(-7.5, 5),
            self.bottom_board_width + P(2, -3),
            self.bottom_board_width + P(2 + 5, -3 - 5),
        }

        self.top_screw_holes = {
            0.3: [
                self.top_side_edge + P(-1.6, 1.6),
            ],
            0.4: [
                self.bottom - P(1.5 * CBT_INNER_BOARD_THICKNESS + 0.5, 1.6),
                self.top_side_edge + P(-1.4, 0.3),
            ]
        }

        self.rib_cutout_points = [
            self.top_side_edge + P(-3, 2.5),
            self.top_side_edge + P(-3, 1),
            P(SB65_BRACKET_TOP_CUT_WIDTH / 2 + 2.5, 2.5),
            P(self.bottom_board_width.x + 2, 7),
            P(self.bottom_board_width.x + 2, self.bottom_board_width.y - 4),
        ]


def calculate_rib(
        angle,
        index_text=None,
        driver_cutout=True,
        driver_cutout_bracket_tabs=True,
        rib_tabs=False,
        inner_rib=False,
        inner_spine_cutout=True,
        outer_spine_cutout=True,
        edge_cutout=True,
        board_thickness=CBT_INNER_BOARD_THICKNESS,
        bottom_screw_holes=False,
        top_screw_holes=False,
        ventilation_cutouts=False,
        velcro_margin=0,
):
    r = Rib(angle=angle, inner_rib=inner_rib)

    import cadquery as cq
    rib = cq.Workplane("XY").center(0, 0)

    if driver_cutout:
        rib = rib.lineTo(0, CBT_SPINE_OFFSET)
        rib = rib.lineTo(r.edge_of_woofer.x - 1, CBT_SPINE_OFFSET)
        rib = rib.lineTo(r.edge_of_woofer.x, CBT_SPINE_OFFSET - 1)
        rib = rib.lineTo(r.edge_of_woofer.x + 1, CBT_SPINE_OFFSET - 3)

        if driver_cutout_bracket_tabs:
            rib = rib.lineTo(r.edge_of_woofer.x + 1, r.edge_of_woofer.y + SB65_BRACKET_THICKNESS + 0.75 + 0.3)
            boop = r.edge_of_woofer.y + SB65_BRACKET_THICKNESS + max(0.3, CBT_RIB_TAB_THICKNESS if rib_tabs else 0)
            rib = rib.lineTo(r.edge_of_woofer.x, boop)

            if rib_tabs:
                rt = RibTab()
                rt_depth = r.edge_of_woofer.x + rt.length - P(y=CBT_INNER_BOARD_THICKNESS / 2,
                                                              angle=45).x + rt.depth - rt.tab_depth - 0.025
                rib = rib.lineTo(rt_depth, boop)
                rib = rib.lineTo(rt_depth, boop - CBT_RIB_TAB_THICKNESS)
                pass
            else:
                rib = rib.lineTo(r.edge_of_woofer.x, r.edge_of_woofer.y + SB65_BRACKET_THICKNESS)
            rib = rib.lineTo(SB65_BRACKET_TOP_CUT_WIDTH / 2, SB65_BRACKET_THICKNESS)
    else:
        rib = rib.moveTo(0, velcro_margin)

    rib = rib.lineTo(SB65_BRACKET_TOP_CUT_WIDTH / 2, velcro_margin)

    velcro_margin_point1 = P(angle=155, length=velcro_margin * math.sqrt(2))
    velcro_margin_point2 = P(angle=180, length=velcro_margin * 1.2)
    print(r.side_middle_vector.angle())
    rib = rib.threePointArc((r.front_arc_point + velcro_margin_point1).values(),
                            (r.top_side_edge + velcro_margin_point1).values())
    # rib = rib.lineTo(r.top_side_edge.x, r.top_side_edge.y)

    if edge_cutout:
        step = r.side_middle_vector / 5
        rib = rib.line(step.x, step.y)
        boop = P(length=CBT_INNER_BOARD_THICKNESS, angle=step.angle() - 90)
        rib = rib.line(boop.x, boop.y)
        rib = rib.line(step.x * 3, step.y * 3)
        rib = rib.line(-boop.x, -boop.y)

    rib = rib.lineTo(r.side_middle.x + velcro_margin_point2.x, r.side_middle.y + velcro_margin_point2.y)

    bottom_spine_x = r.bottom.x + r.bottom_board_width.x

    if velcro_margin > 0:
        rib = rib.line(-4, 4)
        rib = rib.lineTo(bottom_spine_x + 8.6, r.bottom.y - 8.6)

    rib = rib.lineTo(bottom_spine_x, r.bottom.y)
    if inner_spine_cutout and outer_spine_cutout:
        boop = min(r.bottom.y, CBT_SPINE_OFFSET + r.spine_outer_cutout_depth)
        if boop != r.bottom.y:
            rib = rib.lineTo(bottom_spine_x, boop)
        rib = rib.lineTo(bottom_spine_x - CBT_INNER_BOARD_THICKNESS, boop)
        rib = rib.lineTo(bottom_spine_x - CBT_INNER_BOARD_THICKNESS,
                         CBT_SPINE_OFFSET + r.spine_inner_cutout_depth)
        rib = rib.lineTo(0, CBT_SPINE_OFFSET + r.spine_inner_cutout_depth)
    elif inner_spine_cutout:
        rib = rib.lineTo(bottom_spine_x - CBT_INNER_BOARD_THICKNESS, r.bottom.y)
        rib = rib.lineTo(bottom_spine_x - CBT_INNER_BOARD_THICKNESS,
                         CBT_SPINE_OFFSET + r.spine_inner_cutout_depth)
        rib = rib.lineTo(0, CBT_SPINE_OFFSET + r.spine_inner_cutout_depth)
    else:
        rib = rib.lineTo(r.bottom.x, r.bottom.y)

    result = rib.close() \
        .extrude(board_thickness) \
        .mirror(mirrorPlane='ZY', union=True)

    if inner_rib:
        result = result.intersect(
            cq.Workplane("XY").center(0, 0) \
                .lineTo(CBT_INNER_RIB_WIDTH - 1, 0) \
                .lineTo(CBT_INNER_RIB_WIDTH, 1) \
                .lineTo(CBT_INNER_RIB_WIDTH, CBT_INNER_RIB_DEPTH - CBT_INNER_RIB_WIDTH + CBT_SPINE_BOARD_THICKNESS * 1.5) \
                .lineTo(CBT_SPINE_BOARD_THICKNESS * 1.5, CBT_INNER_RIB_DEPTH) \
                .lineTo(0, CBT_INNER_RIB_DEPTH) \
                .lineTo(0, 0) \
                .close() \
                .extrude(board_thickness) \
                .mirror(mirrorPlane='ZY', union=True)
        )


    if bottom_screw_holes:
        for hole in r.bottom_screw_holes:
            result = result.cut(
                cq.Workplane("XY") \
                    .moveTo(hole.x, hole.y) \
                    .circle(0.6 / 2) \
                    .extrude(board_thickness) \
                    .mirror(mirrorPlane='ZY', union=True)
            )

    if top_screw_holes:
        # for hole in r.top_screw_holes:
        for hole_diam, holes in r.top_screw_holes.items():
            for hole in holes:
                result = result.cut(
                    cq.Workplane("XY") \
                        .moveTo(hole.x, hole.y) \
                        .circle(hole_diam / 2) \
                        .extrude(board_thickness) \
                        .mirror(mirrorPlane='ZY', union=True)
                )

    if ventilation_cutouts:
        cut = cq.Workplane("XY").center(0, 0) \
            .moveTo(r.rib_cutout_points[0].x, r.rib_cutout_points[0].y)

        for p in r.rib_cutout_points[1:]:
            cut = cut.lineTo(p.x, p.y)

        result = result.cut(
            cut.close() \
                .extrude(board_thickness) \
                .mirror(mirrorPlane='ZY', union=True)
        )

    if index_text is not None and not inner_rib:
        result = result.cut(
            cq.Workplane("XY") \
                .text(txt=str(index_text), fontsize=0.5, distance=board_thickness, cut=True, clean=True) \
                .translate((1.5, (r.bottom.y - CBT_SPINE_OFFSET) / 2 + CBT_SPINE_OFFSET - 0.5, 0))
        )

    return result


def calculate_base(fillet=True, thickness=CBT_BASE_THICKNESS):
    r = Rib(angle=0)

    import cadquery as cq

    bottom_width = 10

    bottom_edge = (r.bottom + P(x=bottom_width / 2, y=CBT_BASE_DEPTH - r.bottom.y))
    bottom_inner_edge = P(5.5 / 2, bottom_edge.y - 4)

    # .threePointArc((bottom_inner_edge + P(x=0.1, y=0.1)).values(), bottom_inner_edge.values()) \

    # Distance margin from rear of speaker to the oak base. Must be enough for wool + fabric.
    margin = 0.5

    base = cq.Workplane("XY").center(0, 0) \
        .moveTo(r.bottom.x, r.bottom.y + margin) \
        .lineTo(r.bottom_board_width.x, r.bottom_board_width.y + margin) \
        .lineTo(r.side_middle.x, r.side_middle.y + margin) \
        .threePointArc((r.side_middle + P(x=0.1, y=1.2)).values(), bottom_edge.values())

    base = base.spline([
        (bottom_edge.x, bottom_edge.y),
        (bottom_edge.x - 0.25, bottom_edge.y - 0.1),
        (bottom_inner_edge.x, bottom_inner_edge.y),
        (bottom_inner_edge.x * 0.9, bottom_inner_edge.y),
        (bottom_inner_edge.x * 0.8, bottom_inner_edge.y),
        (bottom_inner_edge.x * 0.7, bottom_inner_edge.y),
        (bottom_inner_edge.x * 0.6, bottom_inner_edge.y),
        (bottom_inner_edge.x * 0.5, bottom_inner_edge.y),
        (bottom_inner_edge.x * 0.4, bottom_inner_edge.y),
        (bottom_inner_edge.x * 0.3, bottom_inner_edge.y),
        (bottom_inner_edge.x * 0.2, bottom_inner_edge.y),
        (bottom_inner_edge.x * 0.1, bottom_inner_edge.y),
        (0, bottom_inner_edge.y),
    ])

    base = base.close() \
        .extrude(thickness) \
        .mirror(mirrorPlane='ZY', union=True)

    if fillet:
        base = base.faces("+Z") \
            .fillet(0.9) \
            .faces("-Z") \
            .fillet(0.5)

    result = base

    return result


def calculate_bottom_cable_cover(board_thickness=CBT_BASE_BOARD_THICKNESS):
    r = Rib(angle=0)

    import cadquery as cq

    result = cq.Workplane("XY").center(0, 0) \
        .moveTo(0, r.bottom.y - 1) \
        .lineTo(2.5, r.bottom.y - 1) \
        .lineTo(2.5, r.side_middle.y + CBT_BASE_DEPTH - 11.5) \
        .lineTo(0, r.side_middle.y + CBT_BASE_DEPTH - 11.5) \
        .close() \
        .extrude(board_thickness) \
        .mirror(mirrorPlane='ZY', union=True)

    return result


def calculate_rear_base_bottom(bottom=True, board_thickness=CBT_BASE_BOARD_THICKNESS, cut_cable_slots=True,
                               router_skip_offset=0):
    r = Rib(angle=0)

    import cadquery as cq

    result = cq.Workplane("XY").center(0, 0) \
        .moveTo(0, r.bottom.y + router_skip_offset) \
        .lineTo(r.bottom_board_width.x, r.bottom_board_width.y + router_skip_offset) \
        .lineTo(r.side_middle.x - 5, r.side_middle.y + router_skip_offset + 5)

    result = result.lineTo(5, r.side_middle.y + CBT_BASE_DEPTH - 13.5)

    result = result.spline([
        (5, r.side_middle.y + CBT_BASE_DEPTH - 13.5),
        (4, r.side_middle.y + CBT_BASE_DEPTH - 12.0),
        (3, r.side_middle.y + CBT_BASE_DEPTH - 11.5),
    ])

    # result = result.lineTo(3, r.side_middle.y + CBT_BASE_DEPTH - 11.5)
    result = result.lineTo(0, r.side_middle.y + CBT_BASE_DEPTH - 11.5)

    result = result.close() \
        .extrude(board_thickness) \
        .mirror(mirrorPlane='ZY', union=True)

    if cut_cable_slots:
        bottom_hole = r.bottom.y + 0.75
        top_hole = r.side_middle.y + CBT_BASE_DEPTH - 15 - 1.75
        step = (top_hole - bottom_hole) / 3
        for hole in [
            P(1.75, bottom_hole),
            P(1.75, bottom_hole + 1 * step),
            P(1.75, bottom_hole + 2 * step),
            P(1.75, bottom_hole + 3 * step),
            P(1.75, bottom_hole + 4 * step),

            P(6, bottom_hole),
            P(5.5, bottom_hole + 0.5 * step),
            P(5, bottom_hole + 1 * step),
            P(4.75, bottom_hole + 1.5 * step),
            P(4.5, bottom_hole + 2 * step),
            P(4.25, bottom_hole + 2.5 * step),
            P(4, bottom_hole + 3 * step),
        ]:
            result = result.cut(
                cq.Workplane("XY") \
                    .center(0, 0) \
                    .moveTo(hole.x, hole.y) \
                    .circle(0.4 / 2) \
                    .extrude(board_thickness) \
                    .mirror(mirrorPlane='ZY', union=True)
            )
        if not bottom:
            bottom_cut = cq.Workplane("XY").center(0, 0) \
                .moveTo(r.bottom.x, r.bottom.y) \
                .lineTo(1, r.bottom.y) \
                .lineTo(1, r.side_middle.y + CBT_BASE_DEPTH - 17)

            bottom_cut = bottom_cut \
                .spline([
                (1, r.side_middle.y + CBT_BASE_DEPTH - 17),
                (1, r.side_middle.y + CBT_BASE_DEPTH - 16.8),
                (1, r.side_middle.y + CBT_BASE_DEPTH - 16.6),
                (1, r.side_middle.y + CBT_BASE_DEPTH - 16.4),
                (1, r.side_middle.y + CBT_BASE_DEPTH - 16.2),
                (1, r.side_middle.y + CBT_BASE_DEPTH - 16),
                (2.0, r.side_middle.y + CBT_BASE_DEPTH - 15.5),
                (3, r.side_middle.y + CBT_BASE_DEPTH - 15.0),
                (3, r.side_middle.y + CBT_BASE_DEPTH - 14.8),
                (3, r.side_middle.y + CBT_BASE_DEPTH - 14.6),
                (3, r.side_middle.y + CBT_BASE_DEPTH - 14.4),
                (3, r.side_middle.y + CBT_BASE_DEPTH - 14.2),
                (3, r.side_middle.y + CBT_BASE_DEPTH - 14.0),
            ])

            bottom_cut = bottom_cut \
                .lineTo(3, r.side_middle.y + CBT_BASE_DEPTH - 11) \
                .lineTo(0, r.side_middle.y + CBT_BASE_DEPTH - 11) \
                .close() \
                .extrude(
                board_thickness) \
                .mirror(mirrorPlane='ZY', union=True)
            result = result.cut(bottom_cut)

    if bottom:
        result = result.union(
            calculate_rib(angle=0,
                          driver_cutout=False,
                          driver_cutout_bracket_tabs=False,
                          inner_rib=False,
                          inner_spine_cutout=False,
                          outer_spine_cutout=False,
                          edge_cutout=False,
                          board_thickness=CBT_BASE_BOARD_THICKNESS,
                          bottom_screw_holes=True,
                          top_screw_holes=False,
                          ventilation_cutouts=False,
                          velcro_margin=1.5,
                          )
        )

    return result


def calculate_bottom_rear_rib_additions():
    r = Rib(angle=0)

    import cadquery as cq

    edge_offset = P(angle=90 + r.side_middle_vector.angle(), length=2)
    margin = 0.05
    result = cq.Workplane("XY").center(0, 0) \
        .moveTo(r.bottom_board_width.x + margin, r.bottom_board_width.y - margin) \
        .lineTo(r.side_middle.x - margin, r.side_middle.y + margin) \
        .lineTo(r.side_middle.x - edge_offset.x, r.side_middle.y - edge_offset.y) \
        .lineTo(r.bottom_board_width.x + margin, r.bottom_board_width.y - 2) \
        .close() \
        .extrude(CBT_BASE_BOARD_THICKNESS) \
        .mirror(mirrorPlane='ZY', union=True)

    for hole in r.bottom_screw_holes:
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(hole.x, hole.y) \
                .circle(0.6 / 2) \
                .extrude(CBT_BASE_BOARD_THICKNESS) \
                .mirror(mirrorPlane='ZY', union=True)
        )

    return result


class Side:
    def __init__(self, angle):
        self.outer_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_FRONT_XY.length(),
                                                              upper_width=CBT_TOP_FRONT_XY.length(),
                                                              angle_cutoff=CBT_DEGREE_CUTOFF))
        self.inner_xy = P(angle=angle, length=calculate_width(angle=angle, lower_width=CBT_BOTTOM_REAR_XY.length(),
                                                              upper_width=CBT_TOP_REAR_XY.length(),
                                                              angle_cutoff=CBT_DEGREE_CUTOFF))

        self.outer_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.outer_xy.x).y or 0

        self.inner_z = P(angle=side_angle_xz.angle(), x=CBT_BOTTOM_FRONT_XY.x - self.inner_xy.x).y or 0

        self.outer_z_ratio = 1 - (self.outer_z / side_angle_xz.y)
        self.inner_z_ratio = 1 - (self.inner_z / side_angle_xz.y)

        self.outer_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.outer_z_ratio + CBT_TOP_WIDTH
        self.inner_cbt_width = (CBT_BOTTOM_WIDTH - CBT_TOP_WIDTH) * self.inner_z_ratio + CBT_TOP_WIDTH


def calculate_edge(board_thickness=CBT_INNER_BOARD_THICKNESS):
    angles = list(range(0, int(CBT_DEGREE_CUTOFF))) + [CBT_DEGREE_CUTOFF, CBT_DEGREE_CUTOFF + 0.1]

    #     Lengthen x-axis since we have it at an angle
    ratio = side_angle_xz.length() / side_angle_xz.x

    move_to_center = lambda t: (t[0] - CBT_BOTTOM_FRONT_XY.x, t[1])

    transform_x = lambda t: (ratio * t[0], t[1])
    # transform_x = lambda t: t

    outer = list(map(
        lambda angle: transform_x(move_to_center((P(angle=angle, length=Side(angle).outer_xy.length())).values())),
        angles
    ))

    inner = list(map(
        lambda angle: transform_x(move_to_center((P(angle=angle, length=Side(angle).inner_xy.length())).values())),
        reversed(angles)
    ))

    import cadquery as cq

    spine = cq.Workplane("XY")
    # spine = spine.center(-CBT_OUTER_RADIUS + CBT_BOTTOM_EDGE_OFFSET, 0)
    spine = spine.center(0, 0)

    spine = spine.spline(outer)
    spine = spine.lineTo(inner[0][0], inner[0][1])
    spine = spine.spline(inner)

    return spine.close() \
        .extrude(board_thickness)


class RibTab:
    def __init__(self):
        self.length = 0.8
        self.depth = 0.8
        self.tab_depth = 0.2


def calculate_rib_tab(top_bottom=False):
    import cadquery as cq

    boop = RibTab().length
    depth = RibTab().depth
    tab_depth = RibTab().tab_depth

    result = cq.Workplane("XY").center(0, 0) \
        .moveTo(0, boop) \
        .lineTo(boop, 0) \
        .lineTo(CBT_INNER_BOARD_THICKNESS / 2, -depth) \
        .lineTo(CBT_INNER_BOARD_THICKNESS / 2, -depth + tab_depth) \
        .lineTo(0, -depth + tab_depth) \
        .close() \
        .extrude(CBT_RIB_TAB_THICKNESS) \
        .mirror(mirrorPlane='ZY', union=True)

    if top_bottom:
        result = result.cut(
            cq.Workplane("XY").center(0, 0) \
                .moveTo(CBT_INNER_BOARD_THICKNESS / 2, boop) \
                .lineTo(boop, boop) \
                .lineTo(boop, -depth) \
                .lineTo(CBT_INNER_BOARD_THICKNESS / 2, -depth) \
                .close() \
                .extrude(CBT_RIB_TAB_THICKNESS) \
            )

    return result


def calculate_square(length=10):
    import cadquery as cq
    return cq.Workplane("XY") \
        .rect(length, length) \
        .extrude(CBT_INNER_BOARD_THICKNESS)


def do_operations(shape, oprations, invert=False):
    out = shape

    for op in oprations if not invert else reversed(oprations):
        if op["type"] == "rotate":
            out = out.rotate((0, 0, 0), op["xyz"], op["angle"] if not invert else -op["angle"])
        elif op["type"] == "translate":
            out = out.translate(op["xyz"] if not invert else (-op["xyz"][0], -op["xyz"][1], -op["xyz"][2]))

    return out


def render_cbt(
        render_base=True,
        render_spine=True,
        render_edge=True,
        render_ribs=True,
        render_rib_tabs=True,
        render_ribs_index=lambda x: True,
        render_woofers=True,
        render_woofer_brackets=True,
        woofer_bracket_tabs=False,
        render_woofers_index=lambda x: True,
        render_2d_projection=False):

    shapes_2d = [(0, calculate_square(length=10))]  # (<rough width in cm>, <shape>)

    bases = []
    bases_ply = []
    binding_post = None
    if render_base:
        binding_post = drivers.raw_binding_post() \
            .rotate((0, 0, 0), (1, 0, 0), 90) \
            .rotate((0, 0, 0), (0, 0, 1), 180) \
            .translate((0, CBT_BASE_DEPTH - 4, 0)) \
            .translate((0, 0, 0.9))

        z_offset_if_2d = 0 if render_2d_projection else 1

        bases_ply = [
            calculate_rear_base_bottom(bottom=True) \
                .intersect(calculate_bottom_cable_cover()) \
                .translate((0, 0, -CBT_BASE_BOARD_THICKNESS * z_offset_if_2d)),
            calculate_rear_base_bottom(bottom=True) \
                .cut(calculate_bottom_cable_cover()) \
                .translate((0, 0, -CBT_BASE_BOARD_THICKNESS * z_offset_if_2d)),
            calculate_rear_base_bottom(bottom=False).translate((0, 0, 0 * CBT_BASE_BOARD_THICKNESS * z_offset_if_2d)),
            calculate_bottom_rear_rib_additions().translate((0, 0, 2 * CBT_INNER_BOARD_THICKNESS * z_offset_if_2d)),
        ]

        for bp in bases_ply:
            shapes_2d.append((CBT_BOTTOM_WIDTH + 5, bp))

        base = calculate_base(thickness=CBT_BASE_THICKNESS, fillet=not render_2d_projection)

        base = base.translate((0, 0, -CBT_BASE_BOARD_THICKNESS * z_offset_if_2d))

        if render_2d_projection:
            base = base.cut(
                calculate_rear_base_bottom(bottom=True, cut_cable_slots=False, board_thickness=CBT_BASE_THICKNESS,
                                           router_skip_offset=4)
            )
        else:
            base = base.cut(
                calculate_rear_base_bottom(bottom=True, cut_cable_slots=False).translate(
                    (0, 0, -CBT_BASE_BOARD_THICKNESS * z_offset_if_2d))) \
                .cut(calculate_rear_base_bottom(bottom=False, cut_cable_slots=False).translate(
                (0, 0, 0 * CBT_BASE_BOARD_THICKNESS)))

        shapes_2d.append((CBT_BOTTOM_WIDTH + 5, base))

        bases.append(base)

    is_inner_rib = lambda i: i % 3 != 1 and 1 < i < 24

    spines = []
    if render_spine:
        for s in [
            {
                "offset": 0 - CBT_SPINE_BOARD_THICKNESS,
                "outer_offset": CBT_SPINE_OFFSET + CBT_SPINE_BOARD_THICKNESS / 2,
                "cable_cutout_slot": False,
                "inner": False,
            }, {
                "offset": 0,
                "outer_offset": CBT_SPINE_OFFSET,
                "cable_cutout_slot": True,
                "inner": True,
            }, {
                "offset": 0 + CBT_SPINE_BOARD_THICKNESS,
                "outer_offset": CBT_SPINE_OFFSET + CBT_SPINE_BOARD_THICKNESS / 2,
                "cable_cutout_slot": False,
                "inner": False,
            },
        ]:

            spine = calculate_spine(outer_offset=s["outer_offset"],
                                    cable_cutout_slot=s["cable_cutout_slot"], inner=s["inner"],
                                    is_inner_rib=is_inner_rib)
            if render_2d_projection:
                shapes_2d.append((120, spine))

            spine = spine.translate((0, 0, -CBT_SPINE_BOARD_THICKNESS / 2))
            spine = spine.translate((0, 0, s["offset"]))
            spine = spine.rotate((0, 0, 0), (0, 0, 1), 90)
            spine = spine.rotate((0, 0, 0), (0, 1, 0), 90)
            spine = spine.rotate((0, 0, 0), (0, 0, 1), 180)

            spines.append(spine)

    # edge_thickness = CBT_INNER_BOARD_THICKNESS - (0.4 if render_2d_projection else 0)
    edge_thickness = CBT_INNER_BOARD_THICKNESS - 0.4
    edge_operations = [
        {"type": "translate", "xyz": (0, 0, -edge_thickness)},
        {"type": "rotate", "xyz": (1, 0, 0), "angle": 90},
        {"type": "rotate", "xyz": (0, 0, 1), "angle": -90},
        {"type": "translate", "xyz": (0, -0.15, 0)},
        {"type": "rotate", "xyz": (0, 0, CBT_BOTTOM_EDGE_OFFSET / 2), "angle": side_angle_xz.angle()},
        {"type": "translate", "xyz": (Rib(0).top_side_edge.x - CBT_INNER_BOARD_THICKNESS, Rib(0).top_side_edge.y, 0)},
    ]
    edge = None
    if render_edge:
        edge = calculate_edge(board_thickness=edge_thickness)
        edge = do_operations(edge, edge_operations)

    # if render_2d_projection:

    woofers = None
    if render_woofers:
        for r in WOOFERS_R:
            if render_woofers_index(r):
                xy = P(angle=r, length=CBT_OUTER_RADIUS)
                w = drivers.sb65()
                w = w.translate((0, 0, SB65_BRACKET_THICKNESS))
                w = w.rotate((0, 0, 0), (-1, 0, 0), 90)
                w = w.rotate((0, 0, 0), (-1, 0, 0), r)
                w = w.translate((0, CBT_OUTER_RADIUS - xy.x, xy.y))

                woofers = w if woofers is None else woofers.union(w)

    woofer_brackets = []
    if render_woofer_brackets:
        for r in WOOFERS_R:
            if render_woofers_index(r):
                xy = P(angle=r, length=CBT_OUTER_RADIUS)
                w = drivers.sb65_bracket(SB65_BRACKET_HEIGHT, SB65_BRACKET_THICKNESS,
                                         top_cut_width=SB65_BRACKET_TOP_CUT_WIDTH,
                                         top_cut_board_width=0.65,
                                         max_width=7.0)

                import cadquery as cq
                if r == WOOFERS_R[0]:
                    w = w.union(
                        cq.Workplane("XY") \
                            .workplane(offset=SB65_BRACKET_THICKNESS / 2) \
                            .center(0, SB65_BRACKET_HEIGHT / 2 + CBT_INNER_BOARD_THICKNESS / 4) \
                            .box(SB65_BRACKET_TOP_CUT_WIDTH, CBT_INNER_BOARD_THICKNESS / 2, SB65_BRACKET_THICKNESS)
                    )
                elif r == WOOFERS_R[-1]:
                    w = w.union(
                        cq.Workplane("XY") \
                            .workplane(offset=SB65_BRACKET_THICKNESS / 2) \
                            .center(0, -SB65_BRACKET_HEIGHT / 2 - CBT_INNER_BOARD_THICKNESS / 4) \
                            .box(SB65_BRACKET_TOP_CUT_WIDTH, CBT_INNER_BOARD_THICKNESS / 2, SB65_BRACKET_THICKNESS)
                    )
                if woofer_bracket_tabs:
                    bracket_tab_width = 0.6
                    bracket_tab_height = 0.4
                    x_offset = 1.6
                    for xy2 in [
                        (0 + x_offset, 0 + SB65_BRACKET_HEIGHT / 2, True),
                        (0 + x_offset + bracket_tab_width, 0 + SB65_BRACKET_HEIGHT / 2, False),
                        (0 - x_offset, 0 + SB65_BRACKET_HEIGHT / 2, True),
                        (0 - x_offset - bracket_tab_width, 0 + SB65_BRACKET_HEIGHT / 2, False),
                        (0 + x_offset, 0 - SB65_BRACKET_HEIGHT / 2, False),
                        (0 + x_offset + bracket_tab_width, 0 - SB65_BRACKET_HEIGHT / 2, True),
                        (0 - x_offset, 0 - SB65_BRACKET_HEIGHT / 2, False),
                        (0 - x_offset - bracket_tab_width, 0 - SB65_BRACKET_HEIGHT / 2, True),
                    ]:
                        ww = cq.Workplane("XY").workplane(offset=SB65_BRACKET_THICKNESS / 2).center(xy2[0],
                                                                                                    xy2[1]).box(
                            bracket_tab_width, bracket_tab_height, SB65_BRACKET_THICKNESS)
                        if xy2[2]:
                            w = w.union(ww)
                        else:
                            if (r == WOOFERS_R[0] and 0 < xy2[1]) or (r == WOOFERS_R[-1] and xy2[1] < 0):
                                pass
                            else:
                                w = w.cut(ww)

                if render_2d_projection:
                    shapes_2d.append((10, w))

                w = w.rotate((0, 0, 0), (-1, 0, 0), 90)
                w = w.rotate((0, 0, 0), (-1, 0, 0), r)
                w = w.translate((0, CBT_OUTER_RADIUS - xy.x, xy.y))

                woofer_brackets.append(w)

    if render_rib_tabs and render_2d_projection:
        shapes_2d.append((15, calculate_rib_tab()))
        shapes_2d.append((5, calculate_rib_tab(top_bottom=True)))

    ribs = []
    rib_tabs = []
    if render_ribs:
        for i in range(0, len(RIBS_R)):
            last = len(RIBS_R) - 1
            r = RIBS_R[i] if not (i == last) else RIBS_R[i - 1]
            if render_ribs_index(r):
                outer_radius = P(length=CBT_OUTER_RADIUS, angle=r)
                inner_rib = is_inner_rib(i)
                last = last
                rib = calculate_rib(angle=RIBS_R[i],
                                    index_text=i if not (i in [last - 1, last]) else None,
                                    driver_cutout=i not in [0, last],
                                    driver_cutout_bracket_tabs=i not in [0, last],
                                    rib_tabs=render_rib_tabs,
                                    inner_rib=inner_rib,
                                    inner_spine_cutout=i not in [last],
                                    outer_spine_cutout=i not in [0, last],
                                    edge_cutout=i not in [0, last],
                                    board_thickness=CBT_INNER_BOARD_THICKNESS,
                                    bottom_screw_holes=i < 2,
                                    top_screw_holes=i in [last - 1, last],
                                    ventilation_cutouts=not inner_rib and 2 <= i < 19
                                    )

                if render_2d_projection:
                    shapes_2d.append((33, rib))

                if i == last:
                    rib = rib.translate((0, 0, CBT_INNER_BOARD_THICKNESS))

                rtt = []
                if render_rib_tabs and i not in (0, last):
                    for boop in [True, False]:
                        d = 3.3
                        rt = calculate_rib_tab()
                        rt = rt.translate((0, 0, SB65_BRACKET_THICKNESS - CBT_RIB_TAB_THICKNESS))
                        rt = rt.rotate((0, 0, 0), (0, 0, 1), 90 if boop else -90)
                        rt = rt.rotate((0, 0, 0), (1, 0, 0), 90)
                        rt = rt.translate((0, 2 * SB65_BRACKET_THICKNESS, 0))
                        rt = rt.translate((0, 0, SB65_BRACKET_THICKNESS / 2))
                        rt = rt.translate((d if boop else -d, 0, 0))

                        rt = rt.translate((0, 0, -SB65_BRACKET_THICKNESS / 2))
                        rt = rt.rotate((0, 0, 0), (1, 0, 0), -r)
                        rt = rt.translate((0, CBT_OUTER_RADIUS - outer_radius.x, outer_radius.y))
                        rtt.append(rt)

                rib = rib.translate((0, 0, -CBT_INNER_BOARD_THICKNESS / 2))
                rib = rib.rotate((0, 0, 0), (1, 0, 0), -r)
                rib = rib.translate((0, CBT_OUTER_RADIUS - outer_radius.x, outer_radius.y))

                if render_edge:
                    edge = edge.cut(rib)

                for t in rtt:
                    # rib = rib.cut(t)
                    rib_tabs.append(t)

                ribs.append(rib)

    if render_edge and render_2d_projection:
        shapes_2d.append((50, do_operations(edge, edge_operations, invert=True)))

    options = {}
    # options = {"alpha":0.5, "color": (64, 164, 223)}

    if render_2d_projection:
        x_offset = 0
        sum = None
        for shape in shapes_2d:
            x_offset += shape[0]
            s = shape[1]
            s = s.translate((x_offset, 0, 0))
            sum = s if sum is None else sum.union(s)

        sum = sum.faces("-Z")
        show_object(sum)
        import cadquery as cq
        cq.exporters.export(sum.section(), f"fullrange-cbt.dxf")
    else:

        if binding_post:
            show_object(binding_post, name='binding_post', options={"color": (125, 125, 125)})

        for i in range(0, len(bases)):
            show_object(bases[i], name=f'base{i}', options={"color": (101, 67, 33)})

        for i in range(0, len(bases_ply)):
            show_object(bases_ply[i], name=f'bases_ply{i}', options=options)

        for i in range(0, len(spines)):
            show_object(spines[i], name=f'spine{i}', options=options)
        if render_edge:
            show_object(edge.mirror(mirrorPlane='YZ', union=True), name='inner_edge', options=options)

        for i in range(0, len(ribs)):
            show_object(ribs[i], name=f"rib{i}", options=options)

        if woofers is not None:
            show_object(woofers, name='woofers', options={"color": (125, 125, 125)})

        for i in range(0, len(woofer_brackets)):
            show_object(woofer_brackets[i], name=f'woofer_brackets{i}', options=options)

        for i in range(0, len(rib_tabs)):
            show_object(rib_tabs[i], name=f'rib_tabs{i}', options=options)

    # Strip top layer
    # result = result.faces("+Z")

    # r.exportSvg('test.svg', view_vector = (-1.75,1.1,5))
    # cq.exporters.export(result.section(),'result.dxf')
    # import cadquery as cq
    # cq.exporters.export(spine, 'result.dxf')


def export(workplane, name):
    # Strip top layer
    workplane = workplane.faces("-Z")

    import cadquery as cq
    cq.exporters.export(workplane.section(), f"{name}.dxf")

    # In inkscape:
    # 1. scale to correct size
    # 2. CTRL+A all lines (select and transform objects)
    # 3. CTRL+A all lines (edit paths by nodes) and select "Join selected nodes"


# render_cbt()

# RIBS_R = RIBS_R[0:6]
# WOOFERS_R = WOOFERS_R[0:4]

render_cbt(
    render_base=True,
    render_spine=True,
    render_ribs=True,
    render_rib_tabs=True,
    # render_ribs_index=lambda x: 0 <= RIBS_R.index(x) < 2,
    render_edge=True,
    render_woofers=False,
    render_woofer_brackets=True,
    woofer_bracket_tabs=True,
    # render_woofers_index=lambda x: 0 <= WOOFERS_R.index(x) < 2,
    render_2d_projection=False
)

# spine = calculate_spine()
# rib = calculate_rib(
#     angle=WOOFERS_R[0]
# )
# rib = rib.rotate((0, 0, 0), (0, 0, 1), angleDegrees=45)
# rib = rib.translate((15.5, -15.5, 0))
#
# edge = calculate_edge(layers=1)


# Side(0)
# Side(15)
# Side(50)
# Rib(0)
# Rib(50)
