import drivers as drivers
import cadquery as cq

board_thickness = 0.65

vesa_mount_width = 14

vesa_mount = cq.Workplane("XY") \
    .workplane(offset=board_thickness / 2) \
    .box(vesa_mount_width, vesa_mount_width, board_thickness)

for d in [10, 7.5]:
    vesa_mount = vesa_mount.cut(
        cq.Workplane("XY") \
            .moveTo(d / 2, d / 2) \
            .circle(0.5 / 2) \
            .extrude(board_thickness) \
            .mirror(mirrorPlane='YZ', union=True) \
            .mirror(mirrorPlane='XZ', union=True)
    )

vesa_mount = vesa_mount.rotate((0, 0, 0), (0, -1, 0), 90) \
    .rotate((0, 0, 0), (0, 0, 1), 90)

dock = cq.Workplane("XY") \
    .workplane(offset=5.5 / 2) \
    .box(14.5, 14.5, 5.5) \
    .edges("|Z") \
    .fillet(1)

laptop = cq.Workplane("XY") \
    .workplane(offset=0) \
    .box(35.5, 23.5, 1.25) \
    .edges("|Z") \
    .fillet(0.5) \
    .rotate((0, 0, 0), (1, 0, 0), 45)

laptop = laptop.union(
    cq.Workplane("XY") \
        .workplane(offset=0) \
        .box(35.5, 23.5, 0.5) \
        .edges("|Z") \
        .fillet(0.5) \
        .rotate((0, 0, 0), (1, 0, 0), 90) \
        .translate((0, 8.1, 20.5))
)


# laptop_base = cq.Workplane("XY") \
#     .workplane(offset=board_thickness/2) \
#     .box(38, board_thickness, 3)

# laptop_base = cq.Workplane("XY").center(0, 0) \
#     .lineTo(0, 23.5) \
#     .lineTo(3, 23.5) \
#     .lineTo(3, 26.5) \
#     .lineTo(-3, 26.5) \
#     .lineTo(-3, 0) \
#     .close() \
#     .extrude(board_thickness) \
#     .translate((0, 0, -board_thickness / 2)) \
#     .rotate((0, 0, 0), (1, 0, 0), 180) \
#     .rotate((0, 0, 0), (0, -1, 0), 90)

def fan_120mm():
    return fan(width_height=12.0, screw_hole_width=10.5)


def fan_140mm():
    return fan(width_height=14.0, screw_hole_width=12.45)


def fan(width_height, screw_hole_width, depth=2.5, screw_hole_diameter=0.33):
    r = cq.Workplane("XY").workplane(offset=depth / 2).box(width_height, width_height, depth)

    r = r.cut(
        cq.Workplane("XY") \
            .center(0, 0) \
            .circle(screw_hole_width / 2) \
            .extrude(depth)
    )

    r = r.cut(
        cq.Workplane("XY") \
            .moveTo(screw_hole_width / 2, screw_hole_width / 2) \
            .circle(screw_hole_diameter / 2) \
            .extrude(depth) \
            .mirror(mirrorPlane='YZ', union=True) \
            .mirror(mirrorPlane='XZ', union=True)
    )
    return r


offset = 6


def laptop_rib():
    return cq.Workplane("XY").center(0, 0) \
        .lineTo(0, vesa_mount_width / 2) \
        .lineTo(offset, vesa_mount_width / 2) \
        .polarLine(distance=23.5, angle=-45) \
        .polarLine(distance=3, angle=-45 - 270) \
        .polarLine(distance=4, angle=-45) \
        .polarLine(distance=7, angle=-90 - 45) \
        .polarLine(distance=11.5, angle=-180 - 45) \
        .lineTo(offset, -vesa_mount_width / 2) \
        .lineTo(0, -vesa_mount_width / 2) \
        .close() \
        .extrude(board_thickness) \
        .translate((0, 0, -board_thickness / 2)) \
        .rotate((0, 0, 0), (0, 0, 1), 90) \
        .rotate((0, 0, 0), (1, 0, 0), 180) \
        .rotate((0, 0, 0), (0, 1, 0), 90)


laptop_base = laptop_rib()

for x in [16, 16 - board_thickness]:
    laptop_base = laptop_base.union(
        laptop_rib() \
            .translate((x, 0, 0))
    )

laptop_base = laptop_base.mirror(mirrorPlane='YZ', union=True)

# .translate((vesa_mount_width / 2 - board_thickness / 2, 0, 0))

# laptop_base = cq.Workplane("XY") \
#     .workplane(offset=board_thickness/2) \
#     .box(38, board_thickness, 3)


# vesa_mount = cq.Workplane("XY").center(0, 0) \
#     .lineTo(6, 0) \
#     .lineTo(6, 6) \
#     .lineTo(0, 6) \
#     .lineTo(0, 0) \
#     .close() \
#     .extrude(board_thickness) \
#     .mirror(mirrorPlane='YZ', union=True) \
#     .mirror(mirrorPlane='XZ', union=True)

# for hole in d.screw_holes:
#     result = result.cut(
#         cq.Workplane("XY") \
#             .moveTo(hole.x, hole.y) \
#             .circle(d.screw_hole_diameter / 2) \
#             .extrude(d.base_depth)
#     )
#     result = result.cut(
#         cq.Workplane("XY") \
#             .workplane(offset=d.base_depth - d.base_inner_loft_depth) \
#             .moveTo(hole.x, hole.y) \
#             .circle(d.screw_hole_pocket_diameter / 2) \
#             .extrude(1)
#     )

dock = dock.translate((0, -8, -15))
laptop = laptop.translate((0, -8.75 - offset, -0.75))

fan0 = fan_120mm()
fan0 = fan0.translate((7.5, -7.5, -7))

show_object(laptop_base, name='laptop_base')

show_object(vesa_mount, name='vesa_mount')
show_object(fan0, name='fan0', options={"color": (125, 125, 125)})
show_object(dock, name='dock', options={"color": (75, 75, 75)})
show_object(laptop, name='laptop', options={"color": (75, 75, 75)})
