import math
from sympy import symbols, Eq, solve


def is_in_radius(x, y, x0, y0, x1, y1, xP, yP):
    # Find the xy where the distance to (x0, y0), (x1, y1), (xP, yP) is equal


    r_xy = get_xy_equal_distance_from(x0, y0, x1, y1, xP, yP)
    rx = r_xy.get("x")
    ry = r_xy.get("y")

    # print(r_xy)
    # print(f"distance({x0}, {y0}, {rx}, {ry})")
    radius = distance(x0, y0, rx, ry)*2

    return is_in_hole(x, y, rx, ry, radius)

get_xy_equal_distance_from_cache = {}

def get_xy_equal_distance_from(x0, y0, x1, y1, x2, y2):
    cache_string = f"{x0}-{y0}-{x1}-{y1}-{x2,}-{y2}"

    if cache_string in get_xy_equal_distance_from_cache:
        return get_xy_equal_distance_from_cache[cache_string]

    x, y = symbols('x y')

    eq1 = Eq(((x-x0)**2 + (y-y0)**2) - ((x-x1)**2 + (y-y1)**2), 0)
    eq2 = Eq(((x-x0)**2 + (y-y0)**2) - ((x-x2)**2 + (y-y2)**2), 0)
    eq3 = Eq(((x-x1)**2 + (y-y1)**2) - ((x-x2)**2 + (y-y2)**2), 0)

    # print(f"eq1: {eq1}")
    # print(f"eq2: {eq2}")
    # print(f"eq3: {eq3}")

    solution = solve((eq1,eq2, eq3), (x, y))

    # print(solution)

    result = {"x": solution.get(x), "y": solution.get(y)}

    get_xy_equal_distance_from_cache[cache_string] = result

    return result

def distance(x0, y0, x1, y1):
    return math.sqrt(math.pow(x0 - x1, 2) + math.pow(y0 - y1, 2))


def is_in_triangle(x, y, x1, y1, x2, y2, x3, y3):
    c1 = (x2 - x1) * (y - y1) - (y2 - y1) * (x - x1)
    c2 = (x3 - x2) * (y - y2) - (y3 - y2) * (x - x2)
    c3 = (x1 - x3) * (y - y3) - (y1 - y3) * (x - x3)
    if (c1 <= 0 and c2 <= 0 and c3 <= 0) or (c1 >= 0 and c2 >= 0 and c3 >= 0):
        return True
    else:
        return False


def is_in_hole(x, y, hole_x, hole_y, hole_diameter):
    hole_x_diff = abs(hole_x - x)
    hole_y_diff = abs(hole_y - y)

    hole_c_diff = math.sqrt(math.pow(hole_x_diff, 2) + math.pow(hole_y_diff, 2))

    if hole_c_diff < hole_diameter / 2.0:
        return True


def distance_across_circle(distance_across_circle, c):
    return math.degrees(math.acos(1 - ((distance_across_circle * distance_across_circle) / (2 * c * c))))


import unittest
tc = unittest.TestCase('__init__')


def distance_along_line(x0, y0, x_target, y_target, dist):
    x_normalized = x_target - x0
    y_normalized = y_target - y0

    target_length = distance(0, 0, x_normalized, y_normalized)

    ratio = dist / target_length

    return {"x": x_normalized * ratio + x0, "y": y_normalized * ratio + y0}

def middle(x0, y0, x1, y1):
    return distance_along_line(x0, y0, x1, y1, distance(x0, y0, x1, y1)/2)


def testPoint(actual, expected):
    tc.assertAlmostEqual(actual["x"], expected["x"])
    tc.assertAlmostEqual(actual["y"], expected["y"])

testPoint(distance_along_line(0, 0, 10, 0, 2), {"x": 2, "y": 0})
testPoint(distance_along_line(0, 0, 10, 10, math.sqrt(2)), {"x": 1, "y": 1})
