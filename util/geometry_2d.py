import math

import unittest

tc = unittest.TestCase('__init__')


class P:
    def __init__(self, x=None, y=None, length=None, angle=None):
        # if angle is not None and x is not None:
        #     self.x = x
        #     self.y = math.sin()
        if length is not None and angle is not None:
            self.x = math.cos(math.radians(angle)) * length
            self.y = math.sin(math.radians(angle)) * length
        else:
            self.x = x
            self.y = y

    def __str__(self):
        return f"x: {self.x}, y: {self.y}"

    def __add__(self, other):
        return P(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return P(self.x - other.x, self.y - other.y)

    def __mul__(self, scalar):
        return P(self.x * scalar, self.y * scalar)

    def __truediv__(self, scalar):
        return P(self.x / scalar, self.y / scalar)

    def distance(self, other):
        return math.sqrt(math.pow(self.x - other.x, 2) + math.pow(self.y - other.y, 2))

    def angle(self):
        if self.x == 0.0:
            return 90 if self.y > 0 else -90
        return math.degrees(math.atan(self.y / self.x))

    def length(self):
        return self.distance(P(0, 0))

    def abs(self):
        return P(abs(self.x), abs(self.y))

    def rotate(self, degrees):
        d = P(0, 0).distance(self)
        angle = self.angle()
        new_x = d * math.cos(math.radians(angle + degrees))
        new_y = d * math.sin(math.radians(angle + degrees))
        return P(new_x, new_y)

    def middle(self, other):
        return middle(self, other)


def polarCoordinate(length, angle):
    return P(math.cos(math.degrees(angle)) * length, math.sin(math.degrees(angle)) * length)


def testPoint(actual, expected):
    tc.assertAlmostEqual(actual.x, expected.x)
    tc.assertAlmostEqual(actual.y, expected.y)


# testPoint(P(length=1, angle=0), P(1, 0))
testPoint(P(length=1, angle=90), P(0, 1))
testPoint(P(length=1, angle=180), P(-1, 0))
testPoint(P(length=1, angle=270), P(0, -1))

testPoint(P(0, 0) + P(2, 0), P(2, 0))
testPoint(P(1, 0).rotate(90), P(0, 1))
testPoint(P(1, 0).rotate(-90), P(0, -1))
testPoint(P(1, 0).rotate(0), P(1, 0))

testPoint(P(2, 1).rotate(0), P(2, 1))
testPoint(P(math.sqrt(2) / 2, math.sqrt(2) / 2).rotate(45), P(0, 1))
testPoint(P(math.sqrt(2) / 2, math.sqrt(2) / 2).rotate(-45), P(1, 0))

testPoint(P(math.sqrt(2) / 2, -math.sqrt(2) / 2).rotate(-45), P(0, -1))

testPoint(P(0.000000001, 6).rotate(0), P(0, 6))
testPoint(P(0, 6).rotate(0), P(0, 6))


# testPoint(distance_along_line(Point(0, 0), Point(10, 10), math.sqrt(2)), Point(1, 1))

def is_in_radius(xy, xy0, xy1, xyP):
    # Find the xy where the distance to (x0, y0), (x1, y1), (xP, yP) is equal

    r_xy = get_xy_equal_distance_from(xy0, xy1, xyP)

    # print(r_xy)
    # print(f"distance({x0}, {y0}, {rx}, {ry})")
    radius = xy0.distance(r_xy) * 2

    return is_in_hole(xy, r_xy, radius)


get_xy_equal_distance_from_cache = {}

from sympy import symbols, Eq, solve


def get_xy_equal_distance_from(xy0, xy1, xy2):
    x0 = xy0.x
    x1 = xy1.x
    x2 = xy2.x

    y0 = xy0.y
    y1 = xy1.y
    y2 = xy2.y
    # cache_string = f"{xy0.x}-{xy0.y}-{xy1.x}-{xy1.y}-{xy2.x,}-{xy2.y}"
    cache_string = f"{x0}-{y0}-{x1}-{y1}-{x2,}-{y2}"

    if cache_string in get_xy_equal_distance_from_cache:
        return get_xy_equal_distance_from_cache[cache_string]

    x, y = symbols('x y', real=True)

    eq1 = Eq(((x - x0) ** 2 + (y - y0) ** 2) - ((x - x1) ** 2 + (y - y1) ** 2), 0)
    eq2 = Eq(((x - x0) ** 2 + (y - y0) ** 2) - ((x - x2) ** 2 + (y - y2) ** 2), 0)
    eq3 = Eq(((x - x1) ** 2 + (y - y1) ** 2) - ((x - x2) ** 2 + (y - y2) ** 2), 0)

    # print(f"eq1: {eq1}")
    # print(f"eq2: {eq2}")
    # print(f"eq3: {eq3}")

    solution = solve((eq1, eq2, eq3), (x, y))

    # print(solution)

    result = P(solution.get(x), solution.get(y))

    get_xy_equal_distance_from_cache[cache_string] = result

    return result


testPoint(get_xy_equal_distance_from(P(0, 1), P(1, 0), P(math.sqrt(2) / 2, math.sqrt(2) / 2)), P(0, 0))


# testPoint(get_xy_equal_distance_from(
#     P(13.851949702904731, 5.77163859753386),
#     P(10.58257906224982, 10.664597538627362),
#     P(12.46058189419337, 8.380697631574165)),
#     P(0, 0))


def is_in_triangle(xy, xy1, xy2, xy3):
    x = xy.x
    x1 = xy1.x
    x2 = xy2.x
    x3 = xy3.x

    y = xy.y
    y1 = xy1.y
    y2 = xy2.y
    y3 = xy3.y

    c1 = (x2 - x1) * (y - y1) - (y2 - y1) * (x - x1)
    c2 = (x3 - x2) * (y - y2) - (y3 - y2) * (x - x2)
    c3 = (x1 - x3) * (y - y3) - (y1 - y3) * (x - x3)
    if (c1 <= 0 and c2 <= 0 and c3 <= 0) or (c1 >= 0 and c2 >= 0 and c3 >= 0):
        return True
    else:
        return False


def is_in_hole(xy, hole_xy, hole_diameter):
    hole_x_diff = abs(hole_xy.x - xy.x)
    hole_y_diff = abs(hole_xy.y - xy.y)

    hole_c_diff = math.sqrt(math.pow(hole_x_diff, 2) + math.pow(hole_y_diff, 2))

    if hole_c_diff < hole_diameter / 2.0:
        return True


def distance_across_circle(distance_across_circle, c):
    return math.degrees(math.acos(1 - ((distance_across_circle * distance_across_circle) / (2 * c * c))))


def distance_along_line(xy0, target_xy, dist):
    normalized = target_xy - xy0

    target_length = xy0.distance(target_xy)

    ratio = dist / target_length

    return xy0 + (normalized * ratio)


def middle(xy0, xy1):
    return distance_along_line(xy0, xy1, xy0.distance(xy1) / 2)


testPoint(distance_along_line(P(0, 0), P(10, 0), 2), P(2, 0))
testPoint(distance_along_line(P(0, 0), P(10, 10), math.sqrt(2)), P(1, 1))


def is_in_rectangle(xy, xy_start, rectangle_vector, rectangle_width):
    angle = rectangle_vector.angle()
    half_width = rectangle_width / 2

    top_left = xy_start + P(length=half_width, angle=angle + 90)
    bottom_left = xy_start + P(length=half_width, angle=angle - 90)

    top_right = xy_start + rectangle_vector + P(length=half_width, angle=angle + 90)
    bottom_right = xy_start + rectangle_vector + P(length=half_width, angle=angle - 90)

    return is_in_triangle(xy, top_left, bottom_right, bottom_left) or \
           is_in_triangle(xy, top_left, bottom_right, top_right)
