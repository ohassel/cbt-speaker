import cadquery as cq

from geometry_2d import P as P
import geometry_2d


def monitor(screen_diagonal=49.0, aspect_ratio_x=32 / 9, radius=1800):
    screen_diagonal_mm = screen_diagonal * 25.6

    angle = P(aspect_ratio_x, 1).angle()

    p_diag = P(angle=angle, length=screen_diagonal_mm)

    screen_x_r = geometry_2d.distance_across_circle(p_diag.x, radius)

    print(f"""
    screen_diagonal_mm: {screen_diagonal_mm}
    angle: {angle}
    p_diag: {p_diag}
    screen_x_r: {screen_x_r}
""")

    outer_radius = radius + 25
    b = (
        cq.Workplane("XY")
            .circle(outer_radius)
            .extrude(p_diag.y)
    )

    b = b.cut((
        cq.Workplane("XY")
            .circle(radius)
            .extrude(p_diag.y)
    ))

    slice = (
        cq.Workplane("XY")
            .polarLineTo(distance=2 * outer_radius, angle=-screen_x_r / 2)
            .polarLineTo(distance=2 * outer_radius, angle=screen_x_r / 2)
            .lineTo(0, 0)
            .close()
            .extrude(p_diag.y)
    )

    b = b.intersect(slice)

    b = b.translate((-radius, 0, 0))

    return b


crg90 = monitor(screen_diagonal=48.8, aspect_ratio_x=32 / 9, radius=1800)
g95nc = monitor(screen_diagonal=57.1, aspect_ratio_x=32 / 9, radius=1000)
ark = monitor(screen_diagonal=55, aspect_ratio_x=16 / 9, radius=1000)
ideal = monitor(screen_diagonal=57, aspect_ratio_x=21 / 9, radius=1000)

ruler = (
    cq.Workplane("XY")
        .box(20, 1170, 20)
        .translate((-120, 0, 200))
)

# show_object(t, name='t', options={})
# show_object(r, name='r1', options={})
show_object(crg90, name='crg90', options={"color": (125, 45, 45)})
show_object(g95nc, name='g95nc', options={"color": (125, 125, 45)})
show_object(ark, name='ark', options={"color": (45, 125, 45)})
show_object(ideal, name='ideal', options={})
show_object(ruler, name='ruler', options={"color": (125, 125, 125)})
# show_object(b2, name='b2', options={})

# cq.exporters.export(b1.union(b2), "kitchen-blocks.step")
