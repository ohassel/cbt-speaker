import cadquery as cq

base_thickness = 5

b = (
    cq.Workplane("XY")
        .lineTo(0, 1.25)
        # .lineTo(2.5, 3.5)
        # .lineTo(5, 4)
        .threePointArc((2.5, 3), (12, 6))
        # .lineTo(10, 6)
        # .lineTo(15, 5)
        .threePointArc((25, 7.25), (40-5, 10))
        # .lineTo(20, 6)
        # .lineTo(25, 7)
        # .lineTo(30, 8)
        # .lineTo(40, 10)
        .lineTo(45-5, 10)
        .lineTo(47.5-5, 10)
        .threePointArc((49.5-5, 5), (50-5, 0))
        # .lineTo(49.5, 5)
        # .lineTo(50, 0)
        .lineTo(0, 0)
        .close()
        .extrude(base_thickness)
        .mirror(mirrorPlane='XZ', union=True)
        .edges("|Z")
        .fillet(2)
        .edges("<Z or >Z")
        .fillet(0.75)
)

p_x_offset = 1.5
p_x = 23
p_t = 2.2
p_y = 8

p = (
    cq.Workplane("XY")
        .moveTo(p_x_offset, 0)
        .lineTo(p_x_offset, p_t / 2)

        .lineTo(p_x_offset + p_x / 2 - p_t / 2, p_t / 2)
        .lineTo(p_x_offset + p_x / 2 - p_t / 2, p_y / 2)
        .lineTo(p_x_offset + p_x / 2 + p_t / 2, p_y / 2)
        .lineTo(p_x_offset + p_x / 2 + p_t / 2, p_t / 2)

        .lineTo(p_x_offset + p_x, p_t / 2)
        .lineTo(p_x_offset + p_x, 0)
        .close()
        .extrude(base_thickness+10)
        .mirror(mirrorPlane='XZ', union=True)
        .edges(">Z")
        .chamfer(0.5)
        .edges("|Z")
        .fillet(0.25)
)

b = b.union(p)

c_radius = 6.4 / 2

c = (
    cq.Workplane("XY")
        .moveTo(37.5 - c_radius, 0)
        .circle(c_radius)
        .extrude(base_thickness+7)
        .edges(">Z")
        .chamfer(0.5)
)

c_cut = (
        cq.Workplane("XY")
            .moveTo(37.5 - c_radius, 0)
            .circle(3/2)
            .extrude(base_thickness+7)
            .translate((0, 0, 2))
    )

b = b.union(c)

b = b.cut(c_cut)


show_object(b, name=f'b', options={"color": (125, 45, 45)})

# show_object(s, name=f'stadium')


cq.exporters.export(b, "parents_curtain_sides.step")