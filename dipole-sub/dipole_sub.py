from geometry_2d import P as P
import drivers as drivers

import cadquery as cq

print('BEGIN')


class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


num_woofers = 4
woofers_list = range(0, num_woofers)

# index_woofer_is_inverted = lambda i: False
index_slot_is_inverted = lambda i: i in [1, 3]
index_woofer_is_inverted = lambda i: i in [2, 3]

middle_board_thickness = 1.2
side_board_thickness = 1.2
sbt = side_board_thickness
base_thickness = 2 * 1.8

sbt_plug = 0.6
edge_screws_diameter_mm = 0.8
edge_screw_depth = 6
bottom_screw_diameter = 0.8

up_down_board_thickness = 1.2

double_woofer_rear_side_boards = False

woofer_distance = 32
depth = 33
# slot_width = 10.5
slot_width = 8 * side_board_thickness
# slot_width = 6.5
# slot_width = 8.5

height = num_woofers * woofer_distance
width = 2 * side_board_thickness + 2 * slot_width + 2 * middle_board_thickness

# woofer_to_middle_offset = 0.5 * middle_board_thickness
woofer_to_middle_offset = 0 + 1.0 * middle_board_thickness

# print(f"""
# height:'{height}'
# width:'{width}'
# """)

all_shapes = []
all_shapes_map = {}


class Operation:
    def __init__(self, xyz, angle=None):
        self.xyz = xyz
        self.angle = angle

    def __str__(self):
        return f"xyz: {self.xyz}, angle: {self.angle}"


class Shape:
    def __init__(self, name, create_shape, show_options=None, **kwargs):
        self.name = name
        self.show_options = show_options
        self.create_shape = create_shape
        self.shape = None
        self.kwargs = kwargs
        self.operations = []
        self.visible = False
        all_shapes.append(self)
        all_shapes_map[name] = self

    def set_visible(self, visible: bool):
        self.visible = visible
        return self

    def add_operation(self, op: Operation):
        self.operations.append(op)
        return self

    def rotate(self, xyz, angle):
        self.add_operation(Operation(xyz=xyz, angle=angle))
        return self

    def translate(self, xyz):
        self.add_operation(Operation(xyz=xyz))
        return self

    def get_or_create(self):
        if self.shape is None:
            print(f"create_shape '{self.name}', args='{self.kwargs.items()}'")
            self.shape = self.create_shape(**self.kwargs)
        return self.shape

    def cut(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().cut(other.get_or_create())
        return self

    def union(self, other):
        if not self.visible or not other.visible:
            return
        else:
            self.shape = self.get_or_create().union(other.get_or_create())
        return self

    def show(self):
        print(f"show: '{self.name}', visible: '{self.visible}'")
        if self.visible:
            show_object(self.get_or_create(), name=self.name, options=self.show_options or {})

    def apply_operations(self, invert=False):
        if self.visible:

            for op in self.operations if not invert else reversed(self.operations):
                # print(op)
                if op.angle is not None:
                    self.shape = self.get_or_create().rotate((0, 0, 0), op.xyz, op.angle if not invert else -op.angle)
                else:
                    self.shape = self.get_or_create().translate(
                        op.xyz if not invert else (-op.xyz[0], -op.xyz[1], -op.xyz[2]))



def calc_y_middle(
        inner=True,
        solid=False,
        screw_edge_x=5.75 * side_board_thickness,
        screw_tab_y=3 * side_board_thickness,
        bottom=False,
        bottom_screw_cuts=False,
):
    union_margin = 1
    middle_width = width / 2 - 2 * side_board_thickness if not solid else width / 2 + union_margin

    r = cq.Workplane("XY").center(0, 0) \
        .moveTo(0, middle_width)

    if not solid:
        tab_width = width / 2 - side_board_thickness
        tab_x = side_board_thickness / 2
        if inner:
            pass
            # for i in range(0, 2):
            #     d_off = (depth / 5) * (i + 1)
            #     r = r.lineTo(d_off - tab_x, middle_width) \
            #         .lineTo(d_off - tab_x, tab_width) \
            #         .lineTo(d_off + tab_x, tab_width) \
            #         .lineTo(d_off + tab_x, middle_width)

        else:
            for i in range(0, 3):
                d_off = (depth / 5) * (i + 0.5)
                r = r.lineTo(d_off - tab_x, middle_width) \
                    .lineTo(d_off - tab_x, tab_width) \
                    .lineTo(d_off + tab_x, tab_width) \
                    .lineTo(d_off + tab_x, middle_width)

    r = r.lineTo(depth / 2, middle_width) \
        .lineTo(depth / 2, 0) \
        .lineTo(0, 0) \
        .close() \
        .extrude(side_board_thickness)

    screw_tab_width = (2.25 if inner else 3.0) * side_board_thickness
    r = r.cut(
        cq.Workplane("XY").center(0, 0) \
            .moveTo(screw_edge_x, screw_tab_y) \
            .line(+screw_tab_width / 2, 0) \
            .line(0, +side_board_thickness) \
            .line(-screw_tab_width, 0) \
            .line(0, -side_board_thickness) \
            .line(-screw_tab_width / 2, 0) \
            .close() \
            .extrude(side_board_thickness)
    )
    if inner:
        r = r.cut(
            cq.Workplane("XY").center(0, 0) \
                .moveTo(screw_edge_x, 0) \
                .line(+side_board_thickness / 2, 0) \
                .line(0, + (edge_screw_depth + 1)) \
                .line(-side_board_thickness, 0) \
                .line(0, - (edge_screw_depth + 1)) \
                .line(-side_board_thickness / 2, 0) \
                .close() \
                .extrude(side_board_thickness)
        )

    tt = cq.Workplane("XY").center(0, 0) \
        .moveTo(0 - 0.0001, 0)

    # tabz_vals = [3, screw_edge_x - 2 * sbt, 2 * sbt, 4] if not inner else [0, 1]
    tabz_vals = [0.00001, 2.25 * sbt, 1.25 * sbt, 2.5 * sbt, 1.25 * sbt, 7] if not inner else [0, 20]

    ii = 1
    for tabz in tabz_vals:
        if tabz != 0:
            tt = tt.line(tabz * side_board_thickness, 0)
        tt = tt.line(0, ii * side_board_thickness)
        ii = -ii

    r = r.cut(
        tt.close() \
            .extrude(side_board_thickness)
    )

    if inner:
        r = r.union(
            cq.Workplane("XY") \
                .circle(edge_screws_diameter_mm / 2) \
                .extrude(edge_screw_depth) \
                .rotate((0, 0, 0), (1, 0, 0), 90) \
                .translate((screw_edge_x, edge_screw_depth, sbt / 2))
        )

    for xy in [(10, 6)]:
        r = r.cut(
            cq.Workplane("XY") \
                .center(0, 0) \
                .moveTo(xy[0], xy[1]) \
                .circle(sbt_plug / 2) \
                .extrude(sbt)
        )

    if bottom:
        for xy in [(3.5, 5), (12, 5)]:
            r = r.cut(
                cq.Workplane("XY") \
                    .center(0, 0) \
                    .moveTo(xy[0], xy[1]) \
                    .circle(bottom_screw_diameter / 2) \
                    .extrude(sbt)
            )
            if bottom_screw_cuts:
                print('baoooo')
                r = r.union(
                    cq.Workplane("XY") \
                        .center(0, 0) \
                        .moveTo(xy[0], xy[1]) \
                        .circle(bottom_screw_diameter / 2) \
                        .extrude(-(sbt + base_thickness))
                )

    r = r.translate((-depth / 2, -width / 2, 0))
    r = r.mirror(mirrorPlane='YZ', union=True)
    r = r.mirror(mirrorPlane='XZ', union=True)

    return r


for i in range(0, num_woofers + 1):
    for x in range(0, 3):
        s = Shape(
            name=f"y_middle_{i}{x}",
            create_shape=calc_y_middle,
            inner=x in [1],
            solid=(i == 0 and x == 0) or (i == num_woofers and x == 2),
            bottom=i == 0,
            bottom_screw_cuts=i == 0 and x == 0,
        )
        # s.rotate((0, 0, 1), 90)
        s.translate((0, 0, i * woofer_distance - side_board_thickness / 2))
        s.translate((0, 0, (x - 1) * side_board_thickness))


def calc_middle(
        front_woofer_cutout=[],
        rear_woofer_cutout=[],
        front_woofer_cutout_diameter=27,
        rear_woofer_cutout_diameter=18,
        woofer_screw_holes=[],
        woofer_screw_unions=[],
        rear_woofer_edge_cutout=False,
        rear_woofer_edge_cutout_left=False,
):
    r = cq.Workplane("XY") \
        .moveTo(-depth / 2, 0 - side_board_thickness / 2) \
        .lineTo(-depth / 2, height + side_board_thickness / 2) \
        .lineTo(depth / 2, height + side_board_thickness / 2) \
        .lineTo(depth / 2, 0 - side_board_thickness / 2) \
        .close() \
        .extrude(middle_board_thickness)

    w_driver = drivers.L26R04Y()

    for woofer in woofers_list:
        woofer_y = (woofer + 0.5) * woofer_distance

        if front_woofer_cutout[woofer]:
            r = r.cut(
                cq.Workplane("XY") \
                    .moveTo(0, woofer_y) \
                    .circle(front_woofer_cutout_diameter / 2) \
                    .extrude(middle_board_thickness)
            )

        if rear_woofer_cutout[woofer]:
            r = r.cut(
                cq.Workplane("XY") \
                    .moveTo(0, woofer_y) \
                    .circle((rear_woofer_cutout_diameter) / 2) \
                    .extrude(middle_board_thickness)
            )

        if woofer_screw_unions[woofer]:
            for hole in w_driver.screw_holes_rotated:
                r = r.union(
                    cq.Workplane("XY") \
                        .moveTo(hole.x, hole.y + woofer_y) \
                        .circle(1.5) \
                        .extrude(middle_board_thickness)
                )

        if not rear_woofer_cutout[woofer] or woofer_screw_unions[woofer]:
            r = r.cut(
                cq.Workplane("XY") \
                    .moveTo(0, woofer_y) \
                    .circle(23.6 / 2) \
                    .extrude(middle_board_thickness)
            )

        if woofer_screw_holes[woofer]:
            for hole in w_driver.screw_holes_rotated:
                r = r.cut(
                    cq.Workplane("XY") \
                        .moveTo(hole.x, hole.y + woofer_y) \
                        .circle((0.6 if rear_woofer_cutout else 0.5) / 2) \
                        .extrude(middle_board_thickness)
                )

        if rear_woofer_edge_cutout:
            sign = -1 if rear_woofer_edge_cutout_left ^ index_woofer_is_inverted(woofer) else 1
            remove = cq.Workplane("XY") \
                .moveTo(0, woofer_y) \
                .line(sign * 6.2, 11) \
                .line(sign * 12, 3) \
                .line(0, -14) \
                .line(0, -14) \
                .line(-sign * 12, 3) \
                .close() \
                .extrude(middle_board_thickness)

            r = r.cut(remove)

    return r


class Middle:
    def __init__(self, offset, inner, left):
        self.offset = offset
        self.inner = inner
        self.left = left

    def __str__(self):
        return str(
            {
                "offset": self.offset,
                "inner": self.inner,
                "left": self.left,
            }
        )


middle = [
    Middle(
        offset=-1.5 * middle_board_thickness,
        inner=False,
        left=True,
    ),
    Middle(
        offset=-0.5 * middle_board_thickness,
        inner=True,
        left=True,
    ),
    Middle(
        offset=0.5 * middle_board_thickness,
        inner=True,
        left=False,
    ),
    Middle(
        offset=1.5 * middle_board_thickness,
        inner=False,
        left=False,
    ),
]
for i, m in enumerate(middle):
    print(f"i={i}, m={m}")

    s = Shape(
        name=f"mmiddle_{i}",
        create_shape=calc_middle,
        front_woofer_cutout=list(map(
            lambda w: i == 0 if index_woofer_is_inverted(w) else i == len(middle) - 1
            , woofers_list)),
        rear_woofer_cutout=list(map(
            lambda w: m.left if not index_woofer_is_inverted(w) else not m.left
            , woofers_list)),
        front_woofer_cutout_diameter=27,
        rear_woofer_cutout_diameter=23.6 + (0.0 if m.inner else 1.5) * middle_board_thickness,
        woofer_screw_unions=list(map(
            lambda w: m.left if not index_woofer_is_inverted(w) else not m.left
            , woofers_list)),
        woofer_screw_holes=list(map(
            lambda w: True
            , woofers_list)),
        rear_woofer_edge_cutout=i in [0, 3],
        rear_woofer_edge_cutout_left=i == 0,
    )
    s.translate((0, 0, -1.5 * middle_board_thickness))
    s.translate((0, 0, m.offset))
    s.translate((0, 0, middle_board_thickness))
    s.rotate((1, 0, 0), 90)


# side_veneer = []
# def calc_side(thickness):
#     return cq.Workplane("XY") \
#         .moveTo(-depth / 2, 0) \
#         .lineTo(-depth / 2, height) \
#         .lineTo(depth / 2, height) \
#         .lineTo(depth / 2, 0) \
#         .close() \
#         .extrude(thickness)


def calc_edge(edge_width=10, edge_height=10, inner=False):
    def calc_edge_inner():
        r = cq.Workplane("XY") \
            .moveTo(0, 0) \
            .lineTo(0, edge_height)

        ew_steps = 1
        for i in range(0, ew_steps):
            step = (edge_width - 3 * side_board_thickness) / ew_steps
            print(f"step: {step}")

            r = r.lineTo((i + (1 / 4)) * step + side_board_thickness / 2, edge_height)
            r = r.lineTo((i + (1 / 4)) * step + side_board_thickness / 2, edge_height - side_board_thickness)

            r = r.lineTo((i + (3 / 4)) * step + side_board_thickness / 2, edge_height - side_board_thickness)
            r = r.lineTo((i + (3 / 4)) * step + side_board_thickness / 2, edge_height)

        # if i_from_out != 0:
        r = r.lineTo(edge_width - 2 * side_board_thickness, edge_height)
        r = r.lineTo(edge_width - 2 * side_board_thickness, edge_height - side_board_thickness)

        # r = r.lineTo(edge_width - side_board_thickness, edge_height)
        r = r.lineTo(edge_width - side_board_thickness, edge_height - side_board_thickness)

        r = r.lineTo(edge_width - side_board_thickness, edge_height - 2 * side_board_thickness)
        r = r.lineTo(edge_width, edge_height - 2 * side_board_thickness)
        # r = r.lineTo(edge_width, edge_height)

        eh_steps = 2
        offset = 2.5 * side_board_thickness
        for i in range(0, eh_steps):
            step = (edge_height - offset) / eh_steps
            print(f"step: {step}")

            r = r.lineTo(
                edge_width,
                edge_height - offset - (i + (1 / 4)) * step
            )
            r = r.lineTo(
                edge_width - side_board_thickness,
                edge_height - offset - (i + (1 / 4)) * step
            )
            r = r.lineTo(
                edge_width - side_board_thickness,
                edge_height - offset - (i + (3 / 4)) * step
            )
            r = r.lineTo(
                edge_width,
                edge_height - offset - (i + (3 / 4)) * step
            )

        r = r.lineTo(edge_width, 0) \
            .close() \
            .extrude(side_board_thickness)

        if inner:
            r = r.cut(
                cq.Workplane("XY") \
                    .moveTo(0, 0) \
                    .lineTo(0, edge_height) \
                    .lineTo(sbt, edge_height) \
                    .lineTo(sbt, 0) \
                    .close() \
                    .extrude(side_board_thickness)
            )

        for xy in [(6, 12)]:
            r = r.cut(
                cq.Workplane("XY") \
                    .center(0, 0) \
                    .moveTo(xy[0], xy[1]) \
                    .circle(sbt_plug / 2) \
                    .extrude(sbt)
            )

        r = r.mirror(mirrorPlane='XZ', union=True)
        return r

    a = calc_edge_inner()

    if not inner:
        a = a.cut(
            calc_edge_inner()
                .translate((-edge_width + sbt, 0, 0))
        )

    return a


for woofer in woofers_list:

    for i in range(0, 2):
        edge_width = width / 2 + side_board_thickness - i * side_board_thickness
        edge_height = (woofer_distance + 3 * side_board_thickness) / 2 - i * side_board_thickness
        for rear in [False, True]:
            s = Shape(
                name=f"edge_{'r' if rear else 'f'}_{woofer}{i}",
                create_shape=calc_edge,
                edge_width=edge_width,
                edge_height=edge_height,
                inner=0 < i
            )
            s.rotate((1, 0, 0), -90)
            s.rotate((0, 0, 1), 90)

            s.translate(((i + 1) * side_board_thickness, 0, 0))
            s.translate((0, -(i - 1) * side_board_thickness, 0))
            s.translate((0, -edge_width, 0))

            if index_slot_is_inverted(woofer):
                s.rotate((1, 0, 0), 180)

            s.translate((-depth / 2, 0, 0))
            s.translate((0, 0, + woofer_distance / 2))

            if rear:
                s.rotate((0, 0, 1), 180)

            s.translate((0, 0, woofer * woofer_distance))

for side in ['left', 'right']:
    def get_side(woofer_magnet_cuts=[]):
        r = cq.Workplane("XY").center(0, 0) \
            .moveTo(0, 0) \
            .lineTo(depth, 0) \
            .lineTo(depth, height + 3 * side_board_thickness) \
            .lineTo(0, height + 3 * side_board_thickness) \
            .close() \
            .extrude(side_board_thickness)

        r = r.translate((-depth / 2, 0, 0))

        for woofer in woofer_magnet_cuts:
            woofer_y = (woofer + 0.5) * woofer_distance + 1.5 * side_board_thickness
            r = r.cut(
                cq.Workplane("XY") \
                    .moveTo(0, woofer_y) \
                    .circle(18 / 2) \
                    .extrude(side_board_thickness)
            )

        return r


    s = Shape(
        name=f"sside_{side}",
        create_shape=get_side,
        woofer_magnet_cuts=list(
            filter(lambda w: index_woofer_is_inverted(w) ^ (1 if side != 'left' else 0), woofers_list)),
    )

    s.translate((0, 0, -0.5 * side_board_thickness))
    s.rotate((1, 0, 0), 90)
    s.translate((0, 0, -1.5 * side_board_thickness))
    s.translate((0, (-1 if side == 'left' else 1) * (width / 2 - side_board_thickness / 2), 0))

for woofer in woofers_list:

    def get_woofer(woofer):
        print(f"get_woofer({woofer})")

        woofer_y = (woofer + 0.5) * woofer_distance

        d = drivers.l26r04y()

        d = d.rotate((0, 0, 0), (0, 0, 1), 22.5)

        d = d.translate((0, 0, 0 + woofer_to_middle_offset))

        if index_woofer_is_inverted(woofer):
            d = d.rotate((0, 0, 0), (0, 1, 0), 180)

        d = d.translate((0, woofer_y, 0))
        d = d.rotate((0, 0, 0), (1, 0, 0), 90)

        return d


    Shape(
        name=f"l26r04y_{woofer}",
        show_options={"color": (125, 125, 125)},
        create_shape=get_woofer,
        woofer=woofer,
    )


def calc_base():
    # Assume bauhaus massive oak shelf
    b_depth = 59
    b_width = 59
    # print(f"base_width: '{b_width}', base_depth: '{b_depth}'")
    return cq.Workplane("XY") \
        .workplane(offset=base_thickness / 2) \
        .box(b_depth, b_width, base_thickness)


base = Shape(name='base', show_options={"color": (101, 67, 33)}, create_shape=calc_base)
base.translate((0, 0, -1.5 * side_board_thickness - base_thickness))
base.translate((0, 0, -sbt))


def calc_base_cover_hider():
    return cq.Workplane("XY") \
        .workplane(offset=sbt / 2) \
        .box(depth - 3 * sbt, width - 3 * sbt, sbt)


base_cover_hider = Shape(name='base_cover_hider', create_shape=calc_base_cover_hider)
base_cover_hider.translate((0, 0, sbt / 2 - base_thickness))

# plate_amp = drivers.FA252() \
#     .rotate((0, 0, 0), (0, -1, 0), 90) \
#     .rotate((0, 0, 0), (1, 0, 0), 90) \
#     .translate((0, 0, 13.5 / 2)) \
#     .translate((-depth / 2 - 14, 0, 0)) \
#     .translate((0, 0, -1.2 - 2 * 0.9)) \
#     .translate((0, 0, -1.2)) \
#     .translate((0, base_y_offset, 0))

show_lambdas = [
    lambda name: 'l26r04y' in name,
    lambda name: 'y_middle' in name,
    lambda name: 'mmiddle' in name,
    lambda name: 'edge' in name,
    lambda name: 'sside' in name,
    lambda name: name in ['base_cover_hider'],
    lambda name: name in ['base'],
    # lambda name: name in ['y_middle_00', 'y_middle_01', 'y_middle_02'],
    # lambda name: name in ['y_middle_10', 'y_middle_11', 'y_middle_12'],
]

for name, shape in all_shapes_map.items():
    for l in show_lambdas:
        if l(name):
            shape.set_visible(True)

for s in all_shapes:
    s.apply_operations()


def shapes_cut(s1, s2):
    for mm in list(filter(lambda s: s1 in s.name, all_shapes)):
        for yy in list(filter(lambda s: s2 in s.name, all_shapes)):
            mm.cut(yy)


shapes_cut('mmiddle', 'y_middle')
shapes_cut('y_middle', 'edge')
shapes_cut('mmiddle', 'edge')
shapes_cut('sside', 'y_middle')
shapes_cut('sside', 'edge')
shapes_cut('base_cover_hider', 'y_middle')
shapes_cut('base', 'y_middle')

print(all_shapes_map)
if num_woofers == 4:
    for fr in ['f', 'r']:
        for t in [('00', ['10', '20', '30']), ('11', ['21'])]:
            a = all_shapes_map[f"edge_{fr}_{t[0]}"]
            for n in t[1]:
                b = all_shapes_map[f"edge_{fr}_{n}"]
                a.union(b)
                b.set_visible(False)

for s in all_shapes:
    s.show()

# show_object(plate_amp, name='plate_amp', options={"color": (125, 125, 125)})
