from geometry_2d import P as P


class L26R04Y:
    def __init__(self):
        self.base_diameter = 26.9
        self.base_inner_loft_diameter = self.base_diameter - 2 * 0.85
        self.base_inner_loft_depth = 0.28
        self.base_depth = 1.05

        self.basket_upper_diameter = 23.6
        self.basket_middle_diameter = 20.6
        self.basket_lower_diameter = 15.6

        self.basket_upper_middle_depth = 4.5
        self.basket_middle_lower_depth = 3.2

        self.magnet_diameter = 18
        self.magnet_depth = 6.2

        self.surround_outer_diameter = self.basket_upper_diameter
        self.surround_depth = 1.4
        self.surround_inner_diameter = self.surround_outer_diameter - self.surround_depth * 4

        self.screw_holes_diameter = 25.6
        self.screw_holes = []
        self.screw_holes_rotated = []

        for r in range(0, 360 + 45, 45):
            self.screw_holes.append(P(angle=r, length=self.screw_holes_diameter / 2))
        for r in range(0, 360 + 45, 45):
            self.screw_holes_rotated.append(P(angle=r + 22.5, length=self.screw_holes_diameter / 2))

        self.screw_hole_diameter = 0.55
        self.screw_hole_pocket_diameter = 0.95


def l26r04y():
    import cadquery as cq

    d = L26R04Y()

    result = cq.Workplane("XY") \
        .circle(d.base_diameter / 2) \
        .extrude(d.base_depth - d.base_inner_loft_depth)

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=d.base_depth - d.base_inner_loft_depth) \
            .circle(d.base_diameter / 2) \
            .workplane(offset=d.base_inner_loft_depth) \
            .circle(d.base_inner_loft_diameter / 2) \
            .loft(combine=True)
    )

    # Surround
    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=d.base_depth) \
            .circle(d.surround_outer_diameter / 2) \
            .extrude(d.surround_depth) \
            .cut(
            cq.Workplane("XY") \
                .workplane(offset=d.base_depth) \
                .circle(d.surround_inner_diameter / 2) \
                .extrude(d.surround_depth)
        ) \
            .faces("+Z") \
            .fillet(d.surround_depth * 0.75)
    )

    # Basket
    result = result.union(
        cq.Workplane("XY").workplane(offset=0) \
            .circle(d.basket_upper_diameter / 2) \
            .workplane(offset=-d.basket_upper_middle_depth) \
            .circle(d.basket_middle_diameter / 2) \
            .loft(combine=True)
    )

    result = result.union(
        cq.Workplane("XY").workplane(offset=-d.basket_upper_middle_depth) \
            .circle(d.basket_middle_diameter / 2) \
            .workplane(offset=-d.basket_middle_lower_depth) \
            .circle(d.basket_lower_diameter / 2) \
            .loft(combine=True)
    )

    # Magnet
    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=-d.basket_upper_middle_depth - d.basket_middle_lower_depth) \
            .circle((d.magnet_diameter - 0.5 * 2) / 2) \
            .extrude(-d.magnet_depth)
    )
    magnet_edge_platform_depth = 0.8
    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=-d.basket_upper_middle_depth - d.basket_middle_lower_depth - magnet_edge_platform_depth) \
            .circle(d.magnet_diameter / 2) \
            .extrude(-d.magnet_depth + magnet_edge_platform_depth * 2)
    )

    # Screw holes
    for hole in d.screw_holes:
        result = result.cut(
            cq.Workplane("XY") \
                .moveTo(hole.x, hole.y) \
                .circle(d.screw_hole_diameter / 2) \
                .extrude(d.base_depth)
        )
        result = result.cut(
            cq.Workplane("XY") \
                .workplane(offset=d.base_depth - d.base_inner_loft_depth) \
                .moveTo(hole.x, hole.y) \
                .circle(d.screw_hole_pocket_diameter / 2) \
                .extrude(1)
        )

    return result


def FA252():
    import cadquery as cq

    result = cq.Workplane("XY") \
        .workplane(offset=0.15) \
        .box(31.5, 13.5, 0.3)

    result = result.union(
        cq.Workplane("XY") \
            .workplane(offset=-5.5 / 2) \
            .box(29.1, 11.1, 5.5)
    )

    return result


def show():
    # show_object(FA252(), name='FA252', options={"color": (125, 125, 125)})
    show_object(l26r04y().rotate((0, 0, 0), (0, 0, 1), 22.5), name='l26r04y')

# show()
