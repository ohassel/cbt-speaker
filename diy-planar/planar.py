import math

import cadquery as cq

import geometry_2d
import wg_functions
from geometry_2d import P
from wg_classes import Screw

print()

printer_wall_thickness = 0.05
printer_wall_thickness_gasket = 0.05

num_magnets_lengthwise = 1
# num_magnets_lengthwise = 3

num_magnets_in_length = 1
magnets_support_gap = printer_wall_thickness

planar_front_rear_steel_thickness = 0.15
# steel_plate_magnet_gap_fillet = 0.2
steel_plate_magnet_gap_fillet = 0.15
edge_screw_diameter_multi = 2.0

RENDER_STEEL_PLATES = True
RENDER_STEEL_PLATE_SUPPORTS = False
RENDER_MAGNETS = True
RENDER_EDGE = True
RENDER_GASKET_SOLID_TOP = True
RENDER_GASKET_SOLID_BOT = True
RENDER_MEMBRANE = True
RENDER_MIDDLE_MEMBRANE_XMAX_COMPARISON = True

EDGE_TOP_BOT_SPLIT = True
RENDER_EDGE_EXTRA_WITH_FILLET = True
RENDER_STEEL_EDGE_SUPPORT = False
RENDER_STEEL_MAGNET_SUPPORTS = False
RENDER_MIDDLE_MOUNT_SCREW = False
RENDER_THREADED_INSERT = False
RENDER_OUTER_SCREWS_AND_NUTS = False
RENDER_HORIZONTAL_GASKET_TABS = True
RENDER_VERTICAL_GASKET_TABS = False
RENDER_EDGE_STEEL_INDEX_PEGS = True

EXPORT_GASKET = False
EXPORT_EDGE = False
EXPORT_STEEL_PLATE = False

horizontal_magnets = False

radiating_area_width = 4.0
planar_width = 7.3
planar_extra_edge_width = 8.0

# 2x 12x5x3
num_magnets_rows = 5
num_magnets_in_length = 2
actual_magnet_length = 1.2
magnet_width = 0.5
magnet_height = 0.3
steel_plate_magnet_support_width = 0.2

# 2x 12x4x3
# num_magnets_rows = 5
# num_magnets_in_length = 2
# actual_magnet_length = 1.2
# magnet_width = 0.4
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.2

# 2x 12x4x3
# num_magnets_rows = 7
# num_magnets_in_length = 2
# actual_magnet_length = 1.2
# magnet_width = 0.4
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.2
# radiating_area_width = 5.2
# planar_width = 8.3
# planar_extra_edge_width = 9.0

# 50x3x3x
# num_magnets_rows = 7
# actual_magnet_length = 5
# magnet_width = 0.3
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.4
# steel_plate_magnet_gap_fillet = 0.125

# 48x2.6x2.6x
# num_magnets_rows = 9
# actual_magnet_length = 4.8
# magnet_width = 0.261
# magnet_height = 0.261
# steel_plate_magnet_support_width = 0.4
# steel_plate_magnet_gap_fillet = 0.1
# edge_screw_diameter_multi = 1.5

# 40x5x5x
# num_magnets_lengthwise = 1
# num_magnets_rows = 3
# actual_magnet_length = 4
# magnet_width = 0.5
# magnet_height = 0.5
# steel_plate_magnet_support_width = 0.4
# planar_front_rear_steel_thickness = 0.2

# 40x5x5x
# num_magnets_lengthwise = 1
# num_magnets_rows = 5
# actual_magnet_length = 4
# magnet_width = 0.5
# magnet_height = 0.5
# steel_plate_magnet_support_width = 0.4
# planar_front_rear_steel_thickness = 0.2

# 50x3x3x horizontal
# num_magnets_lengthwise = 5
# num_magnets_rows = 1
# actual_magnet_length = 5
# magnet_width = 0.3
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.4
# steel_plate_magnet_gap_fillet = 0.125
# horizontal_magnets = True
# RENDER_EDGE_STEEL_INDEX_PEGS = False

magnet_length = actual_magnet_length * num_magnets_in_length + (1 + num_magnets_in_length) * magnets_support_gap
magnet_length_gap_margin = 0.025

magnet_size_str = f"{int(magnet_length * 10)}x{int(magnet_width * 10)}x{int(magnet_height * 10)}"
steel_plate_size_str = f"w{round(planar_width, 2)}_{num_magnets_rows}rows_{magnet_size_str}"

num_screws_per_magnet = 3


magnet_z_offset = 0.15

cbt_radius = 75.0
# cbt_radius = 1000.0

middle_cut_margin = 0.01

if horizontal_magnets:
    radiating_area_width = magnet_length
    radiating_area_width_including_edge_magnets = magnet_length
    gap_width_between_magnets = magnet_width * 1.25
    magnet_ctc_length_degrees = geometry_2d.degrees_across_circle(magnet_width + gap_width_between_magnets, cbt_radius)
    num_screws_per_magnet = 1
else:
    radiating_area_width_including_edge_magnets = radiating_area_width + 2 * magnet_width
    gap_width_between_magnets = (radiating_area_width - (num_magnets_rows - 2) * magnet_width) / (num_magnets_rows - 1)
    magnet_ctc_length_degrees = geometry_2d.degrees_across_circle(magnet_length + magnet_length_gap_margin,
                                                                  cbt_radius)

planar_height = num_magnets_lengthwise * magnet_length
planar_height_degrees = num_magnets_lengthwise * magnet_ctc_length_degrees
planar_extra_edge_fillet = (planar_extra_edge_width - planar_width) * 0.45

MICRON_TO_CM = 0.0001

membrane_xmax_one_way = 0.04

membrane_corrugation_length = 0.5 / 4
membrane_corrugation_fillet_radius = 0.03
membrane_corrugation_depth_peak_to_peak = 2 * membrane_xmax_one_way * 1.2
membrane_thickness = MICRON_TO_CM * 42
middle_membrane_xmax_comparison_height = membrane_xmax_one_way * 2

membrane_corrugation_peaks_per_squiggly = 3
gasket_squiggly_at_peak = False

membrane_gasket_edge_margin = 0.05
membrane_gasket_overlap_total = 0.6

planar_edge_inner_cavity_edge_height = 0.10

gasket_margin = 0.005

gasket_magnet_membrane_thickness_intersect = magnet_z_offset * 2
# gasket_magnet_membrane_thickness_intersect = membrane_corrugation_depth_peak_to_peak + 0.06

gasket_edge_margin = printer_wall_thickness
# gasket_tabs_radius = magnet_height - 2 * gasket_margin
gasket_tabs_radius = planar_edge_inner_cavity_edge_height * 1.25

print(f"gasket_tabs_radius={gasket_tabs_radius}")

gasket_solid_to_top_steel = True

edge_screw_diameter = Screw('m3').screw_diameter_tight

# membrane_gasket_magnet_overlap = magnet_width / 2
membrane_gasket_magnet_overlap = 0
membrane_gasket_overlap = membrane_gasket_overlap_total - membrane_gasket_magnet_overlap

edge_screw_margin = 2 * printer_wall_thickness

gasket_width = planar_width - 2 * (edge_screw_diameter + 1.5 * edge_screw_margin)


planar_thickness_inner = (magnet_z_offset + magnet_height) * 2
planar_thickness = planar_thickness_inner + 2 * planar_front_rear_steel_thickness

# planar_edge_inner_cavity_height = (magnet_z_offset + magnet_height / 2) * 2
planar_edge_inner_cavity_height = planar_thickness_inner


# cavity_width = membrane_width + 2 * membrane_gasket_edge_margin + 2 * gasket_edge_margin
cavity_width = gasket_width
gasket_width = gasket_width - 2 * gasket_edge_margin
gasket_actual_width = cavity_width - 2 * gasket_margin

print(f"cavity_width={round(cavity_width, 2)}")
print(f"gasket_width={round(gasket_width, 2)}")
print(f"gasket_actual_width={round(gasket_actual_width, 2)}")
# membrane_width = radiating_area_width + 2 * magnet_width + 2 * membrane_gasket_overlap
# membrane_width = gasket_width - 2 * membrane_gasket_edge_margin - 2 * gasket_edge_margin
membrane_width = gasket_actual_width - 2 * membrane_gasket_edge_margin

membrane_gasket_overlap_mm = membrane_width - radiating_area_width_including_edge_magnets
print(f"membrane_width={round(membrane_width, 2)}")
print(f"membrane_gasket_overlap_mm={round(membrane_gasket_overlap_mm / 2, 2)}")

# edge_screw_width = cavity_width + edge_screw_diameter + 3 * printer_wall_thickness
edge_screw_width = planar_width - 1.0 * (edge_screw_diameter + 2 * edge_screw_margin)

edge_index_peg_diameter = planar_width - cavity_width - edge_screw_diameter * edge_screw_diameter_multi - printer_wall_thickness * 2
edge_index_peg_diameter_margin = 0.02

print(f"planar_edge_inner_cavity_height={planar_edge_inner_cavity_height}")
print(f"edge_index_peg_diameter={edge_index_peg_diameter}")

screw_offset_from_edge = (planar_width - edge_screw_width) * (2 / 3)
index_peg_step = (magnet_length - 2 * screw_offset_from_edge) / max((num_screws_per_magnet - 1), 1)
# screw_offset_from_edge = (planar_width - edge_screw_width) * (1/2)


print(f"planar_thickness={planar_thickness}")

num_magnet_gaps = num_magnets_rows - 1

opening_area = num_magnet_gaps * gap_width_between_magnets * (magnet_length - 3 * steel_plate_magnet_support_width)
cavity_volume = opening_area * (magnet_height + planar_front_rear_steel_thickness) + cavity_width * magnet_z_offset
length_of_opening = magnet_height + planar_front_rear_steel_thickness

print(f"cavity_volume={round(cavity_volume, 2)}cm3")
print(f"opening_area={round(opening_area, 2)}cm2")
print(f"length_of_opening={round(length_of_opening, 2)}cm")


def calc_magnets_row(outer=True):
    magnet = (
        cq.Workplane("XY")
            .rect(actual_magnet_length, magnet_width)
            .extrude(magnet_height if outer else -magnet_height)
    )

    if num_magnets_in_length == 2:
        magnet = (
            magnet
                .translate(((actual_magnet_length + magnets_support_gap) / 2, 0, 0))
                .mirror(mirrorPlane='YZ', union=True)
        )

    if horizontal_magnets:
        magnet_row = (
            magnet
                .rotate((0, 0, 0), (0, 0, 1), 90)
                .translate((0, 0, magnet_z_offset if outer else -magnet_z_offset))
        )
    else:
        magnet_row_list = []

        for i in range(0, math.ceil(num_magnets_rows / 2)):
            magnet_row_list.append((
                magnet
                    .translate((0, i * (gap_width_between_magnets + magnet_width), 0))
                    .translate((0, 0, magnet_z_offset if outer else -magnet_z_offset))
            ))

        magnet_row = wg_functions.union_shapes_list(magnet_row_list)

    magnet_row = magnet_row.mirror(mirrorPlane='XZ', union=True)
    return magnet_row


def calc_magnets(outer=True):
    return mirror_over_each_magnet(calc_magnets_row(outer=outer))


def calc_membrane_corrugation_gear(radius, width):
    outer_r = radius + (membrane_corrugation_depth_peak_to_peak / 2)
    inner_r = radius - (membrane_corrugation_depth_peak_to_peak / 2)

    pc = P(-cbt_radius, 0)

    degrees_per_corrugation = geometry_2d.degrees_across_circle(membrane_corrugation_length, radius)
    num_corugations_half = (planar_height_degrees / 2) / degrees_per_corrugation
    # print(f"num_corugations_half={num_corugations_half}")

    p0 = pc + P(outer_r, 0).rotate(0)

    b = (
        cq.Workplane("XY")
            # .lineTo(pc.x, pc.y)
            .lineTo(p0.x, p0.y)
    )

    for i in range(1, math.ceil(num_corugations_half) + 1):
        odd = (i % 2) == 1
        corr_radius = inner_r if odd else outer_r
        corr_r = i * degrees_per_corrugation
        pn = pc + P(corr_radius, 0).rotate(corr_r)
        b = (
            b
                .lineTo(pn.x, pn.y)
        )

    b = (
        b
            .lineTo(pc.x, pc.y)
            .close()
            .extrude(width)
            .translate((0, 0, -width / 2))
            .mirror(mirrorPlane='XZ', union=True)
            .edges("|Z")
            .fillet(membrane_corrugation_fillet_radius)
            .rotate((0, 0, 0), (0, 1, 0), -90)
            .rotate((0, 0, 0), (0, 0, 1), -90)
    )

    return b


def calc_edge_part(radius, width, degrees=planar_height_degrees):
    pc = P(-cbt_radius, 0)

    p0 = pc + P(radius, 0).rotate(-degrees / 2)
    p1 = pc + P(radius, 0).rotate(0)
    p2 = pc + P(radius, 0).rotate(degrees / 2)

    b = (
        cq.Workplane("XY")
            .moveTo(p0.x, p0.y)
            .threePointArc((p1.x, p1.y), (p2.x, p2.y))
            .lineTo(pc.x, pc.y)
            .close()
            .extrude(width)
            .translate((0, 0, -width / 2))
            # .mirror(mirrorPlane='XZ', union=True)
            .rotate((0, 0, 0), (0, 1, 0), -90)
            .rotate((0, 0, 0), (0, 0, 1), -90)
    )

    return b


def calc_edge_part_square(radius, width, degrees=magnet_ctc_length_degrees):
    pc = P(-cbt_radius, 0)

    p0 = pc + P(x=radius, angle=-degrees / 2)
    p2 = pc + P(x=radius, angle=degrees / 2)

    b = (
        cq.Workplane("XY")
            .moveTo(p0.x, p0.y)
            .lineTo(p2.x, p2.y)
            .lineTo(pc.x, pc.y)
            .close()
            .extrude(width)
            .translate((0, 0, -width / 2))
            # .mirror(mirrorPlane='XZ', union=True)
            .rotate((0, 0, 0), (0, 1, 0), -90)
            .rotate((0, 0, 0), (0, 0, 1), -90)
    )

    return mirror_over_each_magnet(b)


def calc_gasket_tab(radius, width, z_offset, magnet_ctc_length_degrees_multi=1.5):
    b = (
        cq.Workplane("XY")
            .circle(radius / 2)
            .extrude(width)
            .translate((0, 0, -width / 2))
            .rotate((0, 0, 0), (0, 1, 0), -90)
            .rotate((0, 0, 0), (0, 0, 1), -90)
            .translate((0, 0, z_offset))
            .translate((0, 0, cbt_radius))
            .rotate((0, 0, 0), (0, 1, 0),
                    magnet_ctc_length_degrees_multi * magnet_ctc_length_degrees)
            .translate((0, 0, -cbt_radius))
    )

    return b


def calc_vertical_gasket_tabs(radius):

    return None


def calc_gasket_tabs(radius, width, top=None):
    gasket_tabs = []

    def append_gasket(multi, z=0.0):
        for i in [1] if top else [-1]:
            gasket_tabs.append(
                calc_gasket_tab(radius=radius, width=width, z_offset=i * (planar_edge_inner_cavity_height / 2 - z),
                                magnet_ctc_length_degrees_multi=multi)
            )

    for i in range(0, math.ceil(num_magnets_lengthwise / 2)):
        append_gasket((0.5 if top else 0.0) + i, z=planar_edge_inner_cavity_edge_height)

    return wg_functions.union_shapes_list(gasket_tabs).mirror(mirrorPlane='YZ', union=True)


def gasket_top_cavity_solid(width=cavity_width):
    b = (
        calc_edge_part_square(radius=cbt_radius + planar_thickness_inner / 2, width=width)
            .cut(calc_edge_part_square(radius=cbt_radius + planar_thickness_inner / 2,
                                       width=radiating_area_width_including_edge_magnets))
            .cut(calc_edge_part_square(radius=cbt_radius, width=width))
    )

    return b


def calc_membrane_only(width, thickness):
    membrane, ignored, ignored = calc_membrane(width, thickness)
    return membrane


def calc_membrane(width, thickness):
    membrane_corrugation_outer = calc_membrane_corrugation_gear(radius=cbt_radius + thickness / 2, width=width)
    membrane_corrugation_inner = calc_membrane_corrugation_gear(radius=cbt_radius - thickness / 2, width=width)
    membrane = membrane_corrugation_outer.cut(membrane_corrugation_inner)

    return membrane, membrane_corrugation_outer, membrane_corrugation_inner


def mirror_over_each_magnet(shape):
    shapes = []
    for i in range(0, math.ceil(num_magnets_lengthwise / 2)):
        shapes.append(
            shape
                .translate((0, 0, cbt_radius))
                .rotate((0, 0, 0), (0, 1, 0),
                        i * magnet_ctc_length_degrees)
                .translate((0, 0, -cbt_radius))
        )
    shapes = wg_functions.union_shapes_list(shapes)
    shapes = shapes.mirror(mirrorPlane='YZ', union=True)

    return shapes


def calc_edge_screw_holes(screw_diameter=edge_screw_diameter):
    screw_holes_for_plate = calc_edge_screw_holes_for_plate(screw_diameter=screw_diameter)
    return mirror_over_each_magnet(screw_holes_for_plate)


def calc_edge_index_pegs(screw_diameter=edge_index_peg_diameter, width=planar_width / 2, squared=True, chamfer=False):
    peg = (
        cq.Workplane("XY")
            .circle(screw_diameter / 2)
            .extrude(planar_thickness)
            .translate((0, width, 0))
    )

    if squared:
        peg = (
            peg
                .intersect((
                cq.Workplane("XY")
                    .rect(screw_diameter, width * 2)
                    .extrude(planar_thickness)
            ))
        )

    peg = (
        peg
            .translate((0, 0, -planar_thickness / 2))
            .translate((index_peg_step / 2, 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
            .mirror(mirrorPlane='XZ', union=True)
    )

    # pegs = calc_edge_screw_holes_for_plate(screw_diameter=screw_diameter, screws_per_magnet=1, screw_height=planar_thickness)
    if chamfer:
        peg = peg.chamfer(planar_front_rear_steel_thickness / 4)

    return mirror_over_each_magnet(peg)


def calc_edge_screw_holes_for_plate(screw_diameter=edge_screw_diameter, screws_per_magnet=num_screws_per_magnet,
                                    screw_height=None):
    if screw_height is None:
        screw_height = planar_thickness + 0.5

    screw = (
        cq.Workplane("XY")
            .circle(screw_diameter / 2)
            .extrude(screw_height)
            .translate((0, 0, -screw_height / 2))
    )

    if screws_per_magnet == 1:
        return (
            screw
                .translate((0, edge_screw_width / 2, 0))
                .mirror(mirrorPlane='XZ', union=True)
        )

    screw_step = (magnet_length - 2 * screw_offset_from_edge) / (screws_per_magnet - 1)

    screw_holes = []

    for i in range(0, screws_per_magnet):
        screw_holes.append(
            screw
                .translate((0, edge_screw_width / 2, 0))
                .translate((-magnet_length / 2, 0, 0))
                .translate((screw_offset_from_edge, 0, 0))
                .translate((i * screw_step, 0, 0))
        )

    screw_holes = wg_functions.union_shapes_list(screw_holes)

    # screw_holes = screw_holes.mirror(mirrorPlane='YZ', union=True)
    screw_holes = screw_holes.mirror(mirrorPlane='XZ', union=True)

    return screw_holes


def calc_magnet_supports_for_plate(top=True):
    b0 = (
        cq.Workplane("XY")
            .rect(steel_plate_magnet_support_width, radiating_area_width_including_edge_magnets)
            .extrude(magnet_height)
            .edges(">Z and |Y")
            .chamfer(steel_plate_magnet_support_width * 0.5 - magnets_support_gap / 2)
    )

    b1 = (
        cq.Workplane("XY")
            .rect(steel_plate_magnet_support_width, radiating_area_width_including_edge_magnets)
            .extrude(magnet_height)
            .edges(">Z and |Y and <X")
            .chamfer(steel_plate_magnet_support_width - magnets_support_gap)
            .translate((((magnet_length - steel_plate_magnet_support_width) / 2), 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
    )

    b = b0.union(b1)

    z = magnet_z_offset + magnet_height
    b = b.translate((0, 0, -z))

    b = b.cut(calc_magnets_row(outer=False))

    if not top:
        b = b.rotate((0, 0, 0), (0, 1, 0), 180)

    return b


def calc_steel_plate_curved(top=True, screw_diameter=edge_screw_diameter):
    t = planar_front_rear_steel_thickness

    z = magnet_z_offset + magnet_height + t / 2
    if not top:
        z = -z

    r = cbt_radius + z

    plate_l = geometry_2d.distance_across_circle(magnet_ctc_length_degrees * num_magnets_lengthwise, r)
    screw_ctc_l = geometry_2d.distance_across_circle(magnet_ctc_length_degrees, r)

    b = (
        cq.Workplane("XY")
            .rect(plate_l, planar_width - 2 * edge_index_peg_diameter_margin)
            .extrude(t)
            .translate((0, 0, -t/2))
    )

    #
    screw_h = 10
    screw = (
        cq.Workplane("XY")
            .circle(screw_diameter / 2)
            .extrude(screw_h)
            .translate((0, 0, -screw_h/2))
            .translate((0, edge_screw_width / 2, 0))
            .mirror(mirrorPlane='XZ', union=True)
    )

    magnet_gap = (
        cq.Workplane("XY")
            .rect(gap_width_between_magnets + magnet_width * 1.1, actual_magnet_length)
            .extrude(t)
            .cut((
            cq.Workplane("XY")
                .rect(magnet_width, actual_magnet_length)
                .extrude(t)
            ))
            .translate((0, 0, -t/2))
    )

    screw_holes = []
    magnet_holes = []

    for i in range(0, math.ceil(num_magnets_lengthwise / 2)):
        for sign in [-1, 1]:
            if i == 0 and sign == -1:
                continue
            screw_holes.append((
                screw
                .translate((sign * i * screw_ctc_l, 0, 0))
            ))
            magnet_holes.append((
                magnet_gap
                    .translate((sign * i * screw_ctc_l, 0, 0))
            ))
    screw_holes = wg_functions.union_shapes_list(screw_holes)
    magnet_holes = wg_functions.union_shapes_list(magnet_holes)

    b = b.cut(magnet_holes)

    b = (
        b
            .edges("|Z")
            .fillet(steel_plate_magnet_gap_fillet * 0.5)
    )

    b = b.cut(screw_holes)

    b = (
        b
        .translate((0, 0, z))
    )

    return b


def calc_steel_plate_flat(top=True, screw_diameter=edge_screw_diameter):
    t = planar_front_rear_steel_thickness
    if not top:
        t = -t

    b = (
        cq.Workplane("XY")
            .rect(magnet_length, planar_width - 2 * edge_index_peg_diameter_margin)
            .extrude(t)
    )

    gap = (
        cq.Workplane("XY")
            .rect(magnet_length - 2 * steel_plate_magnet_support_width, gap_width_between_magnets)
            .extrude(t)
            .cut((
            cq.Workplane("XY")
                .rect(steel_plate_magnet_support_width, gap_width_between_magnets)
                .extrude(t)
        ))
            .edges("|Z")
            .fillet(steel_plate_magnet_gap_fillet)
    )

    gaps = []
    for i in range(0, math.floor(num_magnets_rows / 2)):
        gaps.append(
            gap.translate((0, (0.5 + i) * magnet_width + (0.5 + i) * gap_width_between_magnets, 0))
        )

    gaps = wg_functions.union_shapes_list(gaps)

    gaps = gaps.mirror(mirrorPlane='XZ', union=True)

    b = b.cut(gaps)

    b = b.cut(calc_edge_screw_holes_for_plate(screw_diameter=screw_diameter))
    if RENDER_EDGE_STEEL_INDEX_PEGS:
        b = b.cut(calc_edge_index_pegs())

    z = magnet_z_offset + magnet_height
    b = b.translate((0, 0, z if top else -z))

    return b


def calc_steel_edge_filament_glue_pegs():
    w = planar_width + 2 * planar_front_rear_steel_thickness
    peg_depth = planar_front_rear_steel_thickness + edge_screw_diameter

    filament_glue_pegs = (
        cq.Workplane("XY")
            .circle(0.175 / 2)
            .extrude(peg_depth)
            .translate((0, 0, w / 2 - peg_depth))
            .translate((index_peg_step / 2, 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .mirror(mirrorPlane='XZ', union=True)
    )

    return mirror_over_each_magnet(filament_glue_pegs)


def calc_steel_edge_curved_support():
    delta = planar_thickness_inner / 2 + planar_front_rear_steel_thickness
    w = planar_width + 2 * planar_front_rear_steel_thickness
    b = (
        calc_edge_part_square(radius=cbt_radius + delta, width=w)
            .cut(
            calc_edge_part_square(radius=cbt_radius - delta, width=w)
        )
            .cut(
            calc_edge_part_square(radius=cbt_radius + delta, width=planar_width)
        )
    )

    if RENDER_STEEL_EDGE_SUPPORT:
        b = b.cut(calc_steel_edge_filament_glue_pegs())

    return b


def calc_threaded_insert():
    screw = (
        cq.Workplane("XY")
            .circle(Screw('m3').heatsert_diameter / 2)
            .extrude(-Screw('m3').heatsert_short_depth)
            .translate((0, 0, planar_thickness / 2 - planar_front_rear_steel_thickness))
            .translate((0, edge_screw_width / 2, 0))
    )

    return screw


def calc_middle_screw():
    screw = (
        cq.Workplane("XY")
            .circle(Screw('m3').screw_diameter_tight * 0.5)
            .extrude(Screw('m3').heatsert_long_depth)
            .translate((0, 0, -planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    head = (
        cq.Workplane("XY")
            .circle(Screw('m3').screw_diameter_tight * 1.0)
            .extrude(-Screw('m3').screw_head_thickness)
            .translate((0, 0, -planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    screw = screw.union(head)

    return screw


def calc_outer_screws_and_nuts():
    screw = (
        cq.Workplane("XY")
            .circle(Screw('m3').screw_diameter_tight * 0.5)
            .extrude(-planar_thickness - 0.4)
            .translate((0, 0, planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    head = (
        cq.Workplane("XY")
            .circle(Screw('m3').screw_diameter_tight * 1.0)
            .extrude(Screw('m3').screw_head_thickness)
            .translate((0, 0, planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    nut_l = Screw('m3').screw_diameter_tight * 1.0

    p0 = P(length=nut_l, angle=0)
    p1 = P(length=nut_l, angle=60)
    p2 = P(length=nut_l, angle=120)
    p3 = P(length=nut_l, angle=180)
    p4 = P(length=nut_l, angle=240)
    p5 = P(length=nut_l, angle=300)

    nut = (
        cq.Workplane("XY")
            .moveTo(p0.x, p0.y)
            .lineTo(p1.x, p1.y)
            .lineTo(p2.x, p2.y)
            .lineTo(p3.x, p3.y)
            .lineTo(p4.x, p4.y)
            .lineTo(p5.x, p5.y)
            .close()
            .extrude(-Screw('m3').screw_head_thickness)
            .translate((0, 0, -planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    screw_step = (magnet_length - 2 * screw_offset_from_edge) / (num_screws_per_magnet - 1)

    screw = (
        screw
            .union(head)
            .union(nut)
            .translate((screw_step, 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
    )

    return screw


def calc_gasked_solid_squiggly(t, outer_r_curved):
    suspension_big_r = 10 * t

    s2_r = membrane_corrugation_length * 2 - t

    x_offset = 0
    if membrane_corrugation_peaks_per_squiggly == 2:
        x = 1.1
    elif membrane_corrugation_peaks_per_squiggly == 3:
        x = 1.70
    y = 0.8
    s0 = True

    # x_offset = membrane_corrugation_length
    # x = 1.1
    # y = 1.2
    # s0 = False

    if s0:
        s0 = (
            cq.Workplane("XY")
                .rect(2 * t, 3.5 * t)
                .extrude(gasket_actual_width)
                .translate((0, 1.25 * t, 0))
                .translate((0, 0, -gasket_actual_width / 2))
        )

    s2 = (
        cq.Workplane("XY")
            .ellipse(s2_r * x + t / 2, s2_r * y + t / 2, 0)
            .ellipse(s2_r * x - t / 2, s2_r * y - t / 2, 0)
            # .circle(s2_r + t / 2)
            # .circle(s2_r - t / 2)
            .extrude(gasket_actual_width)
            .intersect((
            cq.Workplane("XY")
                .polarLineTo(suspension_big_r, 5)
                .polarLineTo(suspension_big_r, 0)
                .polarLineTo(suspension_big_r, -45)
                .polarLineTo(suspension_big_r, -90)
                .polarLineTo(suspension_big_r, -135)
                .polarLineTo(suspension_big_r, -180)
                .polarLineTo(suspension_big_r, -185)
                .close()
                .extrude(gasket_actual_width)
        ))
            .translate((x_offset, 0, 0))
            # .translate((-suspension_small_r, 0, 0))
            .translate((0, outer_r_curved, 0))
            .translate((0, 0, -gasket_actual_width/2))
    )

    if s0:
        s = s2.union(s0)
    else:
        s = s2

    s = (
        s
            .rotate((0, 0, 0), (1, 0, 0), 90)
    )

    return s


def calc_gasket_solid(top=True):
    top_bot_walls = 1

    t = printer_wall_thickness_gasket

    outer_r_curved = planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height - t * top_bot_walls

    inner_r_curved = planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height - t * top_bot_walls

    b = (
        calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height, width=gasket_actual_width)
            .cut(
            calc_edge_part(radius=cbt_radius + outer_r_curved
                           , width=gasket_actual_width))
    )

    b_down = (
        calc_edge_part(radius=cbt_radius - inner_r_curved
                       , width=gasket_actual_width)
        .cut(
            calc_edge_part(radius=cbt_radius - planar_edge_inner_cavity_height / 2 + planar_edge_inner_cavity_edge_height, width=gasket_actual_width)
        )
    )

    b = b.union(b_down)

    membrane_shell = (
        calc_membrane_only(
            width=gasket_actual_width,
            # thickness=membrane_thickness + 2 * gasket_margin + 2 * t
            thickness=3 * t
        )
    )

    top_bot_z_offset = (-1 if top else 1) * (membrane_xmax_one_way - membrane_thickness)
    membrane_shell = membrane_shell.translate((0, 0, top_bot_z_offset))

    b = b.union(membrane_shell)

    s = calc_gasked_solid_squiggly(t, outer_r_curved)
    s_down = (
        calc_gasked_solid_squiggly(t, outer_r_curved)
            .mirror(mirrorPlane='XY', union=False)
    )

    squigglys = []

    degrees_per_corrugation = geometry_2d.degrees_across_circle(membrane_corrugation_length * 2, cbt_radius)
    num_corugations_half = (planar_height_degrees / 2) / degrees_per_corrugation

    for i in range(0, math.ceil(num_corugations_half) + 1):
        if i % membrane_corrugation_peaks_per_squiggly != 0:
            continue
        for sign in [-1, 1]:
            if i == 0 and sign == -1:
                continue
            if membrane_corrugation_peaks_per_squiggly == 2:
                offset_top = 0.5 if gasket_squiggly_at_peak else 0.25
                offset_bot = 0 if gasket_squiggly_at_peak else 0.25
            if membrane_corrugation_peaks_per_squiggly == 3:
                offset_top = 0 if gasket_squiggly_at_peak else 0.25
                offset_bot = 0.5 if gasket_squiggly_at_peak else 0.25
            offset = offset_top if top else offset_bot
            offset = 0
            squigglys.append((
                (s if top else s_down)
                    .translate((0, 0, cbt_radius))
                    .rotate((0, 0, 0), (0, 1, 0), (offset + sign * i) * degrees_per_corrugation)
                    .translate((0, 0, -cbt_radius))
            ))
            # squigglys.append((
            #     s_down
            #         .translate((0, 0, cbt_radius))
            #         .rotate((0, 0, 0), (0, 1, 0), (0.5 + sign * i) * degrees_per_corrugation)
            #         .translate((0, 0, -cbt_radius))
            # ))

    b = b.union(wg_functions.union_shapes_list(squigglys))

    b = (
        b.cut(calc_edge_part(radius=cbt_radius + planar_thickness, width=radiating_area_width_including_edge_magnets + 2 * printer_wall_thickness))
        #     .cut(
        #     membrane_inner
        # )
            .intersect(calc_edge_part(radius=cbt_radius + planar_thickness / 2, width=gasket_actual_width))
    )

    bot_solid = (
        calc_membrane_corrugation_gear(radius=cbt_radius - 0 / 2, width=gasket_actual_width)
        .translate((0, 0, top_bot_z_offset))
    )
    if top:
        b = b.cut(bot_solid)
    else:
        b = b.intersect(bot_solid)

    return b


if RENDER_MEMBRANE:
    membrane, membrane_outer, membrane_inner = calc_membrane(width=membrane_width, thickness=membrane_thickness)
    membrane = membrane.intersect(calc_edge_part(radius=cbt_radius + planar_thickness / 2, width=planar_width))

magnets_top = calc_magnets(outer=True) if RENDER_MAGNETS else None
magnets_bot = calc_magnets(outer=False) if RENDER_MAGNETS else None

edge_cavity = (
    calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2, width=cavity_width)
        .cut(calc_edge_part(radius=cbt_radius - planar_edge_inner_cavity_height / 2, width=cavity_width))
)

edge = None
edge_top = None
edge_bot = None

if RENDER_EDGE:
    edge = (
        calc_edge_part_square(radius=cbt_radius + planar_thickness_inner / 2, width=planar_width)
            .cut(calc_edge_part_square(radius=cbt_radius - planar_thickness_inner / 2, width=planar_width))
            .cut(calc_edge_part(radius=cbt_radius + planar_thickness,
                                width=radiating_area_width_including_edge_magnets + 2 * printer_wall_thickness))
            .cut(edge_cavity)
    )

    screw_holes = calc_edge_screw_holes()
    if RENDER_EDGE_STEEL_INDEX_PEGS:
        edge_screw_index_pegs = calc_edge_index_pegs(screw_diameter=edge_index_peg_diameter - edge_index_peg_diameter_margin)
        edge = edge.union(edge_screw_index_pegs)
    edge = edge.cut(screw_holes)

if RENDER_EDGE_EXTRA_WITH_FILLET and RENDER_EDGE:
    edge_extra = (
        calc_edge_part_square(radius=cbt_radius + planar_thickness / 2, width=planar_extra_edge_width)
            .cut(calc_edge_part_square(radius=cbt_radius - planar_thickness / 2, width=planar_extra_edge_width))
            .cut(calc_edge_part_square(radius=cbt_radius + planar_thickness / 2, width=planar_width))
            .edges(">Y and |X or <Y and |X")
            .fillet(planar_extra_edge_fillet)
    )
    edge = edge.union(edge_extra)


edge_cavity_gasket_margin = (
    calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2 - gasket_margin,
                   width=gasket_actual_width)
        .cut(
        calc_edge_part(radius=cbt_radius - planar_edge_inner_cavity_height / 2 + gasket_margin,
                       width=gasket_actual_width))
)

edge_cavity_gasket_margin = edge_cavity_gasket_margin.union(
    gasket_top_cavity_solid(width=gasket_actual_width))

if RENDER_EDGE and gasket_solid_to_top_steel:
    edge = edge.cut(gasket_top_cavity_solid(width=cavity_width))

gasket_solid_top = calc_gasket_solid(top=True) if RENDER_GASKET_SOLID_TOP else None
gasket_solid_bot = calc_gasket_solid(top=False) if RENDER_GASKET_SOLID_BOT else None

if RENDER_GASKET_SOLID_TOP or RENDER_GASKET_SOLID_BOT:
    tabs_intersect = (
        calc_edge_part_square(radius=cbt_radius + planar_edge_inner_cavity_height / 2, width=gasket_actual_width)
            .cut(
            calc_edge_part_square(radius=cbt_radius - planar_edge_inner_cavity_height / 2, width=gasket_actual_width)
        )
        .cut(
            calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height * 1.5, width=gasket_actual_width)
                .cut(
                calc_edge_part(radius=cbt_radius - planar_edge_inner_cavity_height / 2 + planar_edge_inner_cavity_edge_height * 1.5, width=gasket_actual_width)
            )
        )
        .cut(
            calc_edge_part_square(radius=cbt_radius + planar_thickness / 2, width=radiating_area_width_including_edge_magnets + 2 * printer_wall_thickness)
        )
    )
    if RENDER_GASKET_SOLID_TOP:
        gasket_solid_top = gasket_solid_top.union(
            calc_gasket_tabs(radius=gasket_tabs_radius, width=gasket_actual_width, top=True).intersect(tabs_intersect)
        )
    if RENDER_GASKET_SOLID_BOT:
        gasket_solid_bot = gasket_solid_bot.union(
            calc_gasket_tabs(radius=gasket_tabs_radius, width=gasket_actual_width, top=False).intersect(tabs_intersect)
        )

if RENDER_EDGE and edge:
    ttop = (
        calc_edge_part_square(radius=cbt_radius + planar_edge_inner_cavity_height / 2, width=cavity_width)
            .cut(
            calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height, width=cavity_width)
        )
    )
    bbot = (
        calc_edge_part(radius=cbt_radius - planar_edge_inner_cavity_height / 2 + planar_edge_inner_cavity_edge_height, width=cavity_width)
            .cut(
            calc_edge_part_square(radius=cbt_radius - planar_edge_inner_cavity_height / 2, width=cavity_width)
        )
    )
    mmid = (
        calc_edge_part_square(radius=cbt_radius + planar_edge_inner_cavity_height / 2, width=radiating_area_width_including_edge_magnets + 2 * printer_wall_thickness)
    )
    tb = (
        ttop
            .union(bbot)
            .cut(mmid)
    )
    edge = edge.union(tb)

    chamfer_r = 2 * printer_wall_thickness
    # chamfer_cut_thickness = membrane_thickness + 2 * 1.5 * membrane_xmax_one_way + 2 * printer_wall_thickness + 2 * chamfer_r
    # chamfer_cut_thickness = planar_edge_inner_cavity_height - 2 * (planar_edge_inner_cavity_edge_height + printer_wall_thickness_gasket)
    chamfer_cut_thickness = planar_edge_inner_cavity_height - 2 * planar_edge_inner_cavity_edge_height

    mmembrane_chamfer_cut = (
        calc_edge_part(radius=cbt_radius + chamfer_cut_thickness / 2, width=cavity_width + 2 * printer_wall_thickness, degrees=180)
            .cut(
            calc_edge_part(radius=cbt_radius - chamfer_cut_thickness / 2, width=cavity_width + 2 * printer_wall_thickness, degrees=180)
        )
        .edges(">Y or <Y")
        .chamfer(chamfer_r)
    )

    # mmembrane = calc_membrane_only(width=cavity_width + 2 * printer_wall_thickness, thickness=membrane_thickness + 2 * 1.5 * membrane_xmax_one_way + 2 * printer_wall_thickness)

    # mmembrane = mmembrane.chamfer(membrane_xmax_one_way)
    edge = edge.cut(mmembrane_chamfer_cut)

    edge_cavity_maybe_tabs = None

    if RENDER_HORIZONTAL_GASKET_TABS:
        edge_cavity_maybe_tabs = (
            calc_gasket_tabs(radius=gasket_tabs_radius+ 2 * gasket_margin, width=cavity_width, top=True)
            .union(
                calc_gasket_tabs(radius=gasket_tabs_radius+ 2 * gasket_margin, width=cavity_width, top=False)
            )
        )
    if RENDER_VERTICAL_GASKET_TABS:
        edge_cavity_maybe_tabs = calc_edge_index_pegs(screw_diameter=gasket_tabs_radius + 2 * gasket_margin, width=cavity_width/2, squared=False)


    edge = edge.cut(edge_cavity_maybe_tabs)



if RENDER_STEEL_EDGE_SUPPORT:
    edge = edge.cut(calc_steel_edge_filament_glue_pegs())
# edge = edge.union(edge_screw_index_pegs)

steel_plates_single = None
steel_plates_top = None
steel_plates_bot = None

if RENDER_STEEL_PLATES:
    if horizontal_magnets:
        steel_plates_top = calc_steel_plate_curved(top=True)
        steel_plates_bot = calc_steel_plate_curved(top=False)
    else:
        steel_plates_single = calc_steel_plate_flat(top=True)
        steel_plates_top = mirror_over_each_magnet(calc_steel_plate_flat(top=True))
        steel_plates_bot = mirror_over_each_magnet(calc_steel_plate_flat(top=False))

steel_edge_curved_support = None
if RENDER_STEEL_PLATE_SUPPORTS:
    steel_plate_support_top = mirror_over_each_magnet(
        calc_magnet_supports_for_plate(top=True)) if RENDER_STEEL_MAGNET_SUPPORTS else None
    steel_plate_support_bot = mirror_over_each_magnet(
        calc_magnet_supports_for_plate(top=False)) if RENDER_STEEL_MAGNET_SUPPORTS else None
    steel_edge_curved_support = calc_steel_edge_curved_support() if RENDER_STEEL_EDGE_SUPPORT else None

# middle_cut = (
#     calc_edge_part(radius=cbt_radius + middle_cut_margin / 2, width=planar_width)
#         .cut(calc_edge_part(radius=cbt_radius - middle_cut_margin / 2, width=planar_width))
# )

if RENDER_EDGE and EDGE_TOP_BOT_SPLIT:
    mmembrane, mm_outer, mm_inner = calc_membrane(width=planar_width, thickness=middle_cut_margin)
    edge = edge.cut(mmembrane)

    sq_inner_cut = (
        calc_edge_part_square(radius=cbt_radius + middle_cut_margin / 2, width=planar_width * 2)
        .cut(
            calc_edge_part_square(radius=cbt_radius - planar_thickness, width=planar_width * 2)
        )
            .cut(
            calc_edge_part_square(radius=cbt_radius + planar_thickness, width=planar_width)
        )
    )
    sq_inner_union = (
        calc_edge_part_square(radius=cbt_radius - middle_cut_margin / 2, width=planar_width * 2)
            .cut(
            calc_edge_part_square(radius=cbt_radius - planar_thickness, width=planar_width * 2)
        )
            .cut(
            calc_edge_part_square(radius=cbt_radius + planar_thickness, width=planar_width)
        )
    )

    sq_transition_margin_cut_top = (
        calc_edge_part_square(radius=cbt_radius + middle_cut_margin / 2, width=planar_width)
            .cut(
            calc_edge_part_square(radius=cbt_radius + planar_thickness, width=planar_width - printer_wall_thickness / 2)
        )
    )
    sq_transition_margin_cut_bot = (
        calc_edge_part_square(radius=cbt_radius + planar_thickness, width=planar_width)
        .cut(
            calc_edge_part_square(radius=cbt_radius - middle_cut_margin / 2, width=planar_width)
        )
        .cut(
            calc_edge_part_square(radius=cbt_radius + planar_thickness, width=planar_width - printer_wall_thickness / 2)
        )
    )

    edge_top = edge.cut(mm_inner).cut(sq_inner_cut).cut(sq_transition_margin_cut_top)
    # edge_bot = edge.cut(mm_outer).cut(sq_inner_union)
    edge_bot = edge.intersect(mm_outer.union(sq_inner_union)).cut(sq_transition_margin_cut_bot)
    edge = None


if RENDER_MIDDLE_MEMBRANE_XMAX_COMPARISON:
    middle_membrane_xmax_comparison_squiggly = (
        calc_membrane_only(width=planar_width, thickness=middle_membrane_xmax_comparison_height)
        .intersect(calc_edge_part(radius=cbt_radius + planar_thickness / 2, width=planar_width))
    )
    middle_membrane_xmax_comparison_flat = (
        calc_edge_part(radius=cbt_radius + membrane_xmax_one_way, width=membrane_width)
            .cut(calc_edge_part(radius=cbt_radius - membrane_xmax_one_way, width=membrane_width))
    )

threaded_insert = mirror_over_each_magnet(calc_threaded_insert()) if RENDER_THREADED_INSERT else None

middle_screw = mirror_over_each_magnet(calc_middle_screw()) if RENDER_MIDDLE_MOUNT_SCREW else None
outer_screws_and_nuts = mirror_over_each_magnet(calc_outer_screws_and_nuts()) if RENDER_OUTER_SCREWS_AND_NUTS else None

# show_object(membrane_corrugation_outer, name=f'membrane_corrugation_outer', options={"color": (125, 45, 45)})
# show_object(membrane_corrugation_inner, name=f'membrane_corrugation_inner', options={"color": (125, 45, 45)})
# show_object(gasket_tabs, name=f'gasket_tabs', options={"color": (45, 45, 125)})
if RENDER_MIDDLE_MEMBRANE_XMAX_COMPARISON:
    show_object(middle_membrane_xmax_comparison_squiggly, name=f'middle_membrane_xmax_comparison_squiggly', options={"color": (125, 45, 45), "alpha": 0.5})
    show_object(middle_membrane_xmax_comparison_flat, name=f'middle_membrane_xmax_comparison_flat', options={"color": (125, 45, 45), "alpha": 0.5})
if RENDER_MEMBRANE:
    show_object(membrane, name=f'membrane', options={"color": (125, 45, 45)})
# show_object(screw_holes, name=f'screw_holes', options={"color": (120, 120, 120)})
if RENDER_MAGNETS:
    show_object(magnets_top, name=f'magnets_top', options={"color": (40, 40, 40)})
    show_object(magnets_bot, name=f'magnets_bot', options={"color": (40, 40, 40)})
# show_object(magnet_plate_gaps(), name=f'magnet_plate_gaps', options={"color": (40, 40, 40)})

if gasket_solid_top:
    show_object(gasket_solid_top, name=f'gasket_solid_top', options={"color": (200, 60, 0)})
if gasket_solid_bot:
    show_object(gasket_solid_bot, name=f'gasket_solid_bot', options={"color": (200, 60, 0)})
# show_object(steel_plates, name=f'steel_plates', options={"color": (15, 15, 15)})

# show_object(edge_screw_index_pegs, name=f'edge_screw_index_pegs', options={"color": (125, 90, 0)})

if threaded_insert:
    show_object(threaded_insert, name=f'threaded_insert', options={"color": (200, 90, 0)})
if RENDER_EDGE:
    if edge:
        show_object(edge, name=f'edge', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge_top:
        show_object(edge_top, name=f'edge_top', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge_bot:
        show_object(edge_bot, name=f'edge_bot', options={"color": (125, 90, 0), "alpha": 0.5})
# show_object(edge_top, name=f'edge_top', options={"color": (125, 90, 0)})
# show_object(edge_bot, name=f'edge_bot', options={"color": (125, 90, 0)})
if RENDER_STEEL_PLATE_SUPPORTS:
    if steel_plate_support_top:
        show_object(steel_plate_support_top, name=f'steel_plate_support_top', options={"color": (125, 90, 0)})
    if steel_plate_support_bot:
        show_object(steel_plate_support_bot, name=f'steel_plate_support_bot', options={"color": (125, 90, 0)})
# show_object(gasket_top, name=f'gasket_top', options={"color": (15, 15, 15)})
# show_object(gasket_bot, name=f'gasket_bot', options={"color": (15, 15, 15)})
if RENDER_STEEL_PLATES:
    if steel_plates_top:
        show_object(steel_plates_top, name=f'steel_plates_top', options={"color": (120, 120, 120)})
    if steel_plates_bot:
        show_object(steel_plates_bot, name=f'steel_plates_bot', options={"color": (120, 120, 120)})
if RENDER_STEEL_PLATE_SUPPORTS:
    if steel_edge_curved_support:
        show_object(steel_edge_curved_support, name=f'steel_edge_curved_support', options={"color": (120, 120, 120), "alpha": 0.5})
# show_object(steel_plates_single, name=f'steel_plates_single', options={"color": (120, 120, 120)})

if middle_screw:
    show_object(middle_screw, name=f'middle_screw', options={"color": (0, 0, 120)})
if outer_screws_and_nuts:
    show_object(outer_screws_and_nuts, name=f'outer_screws_and_nuts', options={"color": (120, 0, 0)})

if EXPORT_GASKET:
    if RENDER_GASKET_SOLID_TOP and gasket_solid_top:
        cq.exporters.export(wg_functions.scale_cm_to_mm(gasket_solid_top), f"gasket_solid_top_{magnet_size_str}.step")
    if RENDER_GASKET_SOLID_BOT and gasket_solid_bot:
        cq.exporters.export(wg_functions.scale_cm_to_mm(gasket_solid_bot), f"gasket_solid_bot_{magnet_size_str}.step")

if EXPORT_EDGE:
    if edge:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge), f"edge_{magnet_size_str}.step")
    if edge_top:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_top), f"edge_top_{magnet_size_str}.step")
    if edge_bot:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_bot), f"edge_bot_{magnet_size_str}.step")

if EXPORT_STEEL_PLATE:
    if steel_plates_single:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plates_single), f"steel_plates_single_{steel_plate_size_str}.step")
    if horizontal_magnets and steel_plates_top:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plates_top), f"steel_plates_top_curved_{steel_plate_size_str}.step")
    if horizontal_magnets and steel_plates_bot:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plates_bot), f"steel_plates_bot_curved_{steel_plate_size_str}.step")
    if steel_edge_curved_support:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_edge_curved_support),
                            f"steel_edge_curved_support_{num_magnets_lengthwise}_lengthwise.step")
