import cadquery as cq
from cq_gears import SpurGear
from wg_classes import Screw
import wg_functions

# uchida corrugator
uchida_gear_outer_diameter = 22.0
uchida_gear_inner_diameter = 16.5
uchida_gear_teeth_height = (uchida_gear_outer_diameter - uchida_gear_inner_diameter) / 2
uchida_gear_ctc = 2 * uchida_gear_inner_diameter + 1

ball_bearing_outer_diameter = 32.0
ball_bearing_inner_diameter = 20.0
ball_bearing_depth = 7.0
ball_bearing_outer_ring_thickness = 1.3
ball_bearing_inner_ring_thickness = 1.7
ball_bearing_inner_depth = 6.0

# gear_steel_rod_diameter = None
gear_steel_bore_rod_diameter = 12

# gear_teeth_depth = 2.8
# gear_module = 1.22
# gear_num_teeth = 35
# gear_ctc_margin = 0.75

gear_teeth_depth = 1.5
gear_module = 0.70
gear_num_teeth = 61
gear_ctc_margin = 0.5

gears_ctc = 43.0
gear_target_diameter = gears_ctc + gear_teeth_depth - gear_ctc_margin
gears_center_z = gear_target_diameter + 6.0

edge_width = 80.0
edge_height = 2 * gears_center_z
edge_depth = 12
edge_screw_ctc = edge_width - 1.5 * edge_depth

gear_depth = 85.0 + 2 * edge_depth
gear_top_depth = 10.0
gear_bot_depth = gear_depth - gear_top_depth

gear_to_side_margin = 1.5

base_width = edge_width * 2
base_height = gear_depth * 2
base_offset = edge_width / 3

handle_inner_tabs_width = ball_bearing_inner_diameter / 2
handle_shaft_tab_depth = 5
handle_shaft_length = gears_ctc + ball_bearing_inner_diameter
handle_length = 80
handle_depth = edge_depth + handle_shaft_tab_depth
gear_to_handle_base_depth = 25
screw_length = 30

printer_margin = 0.1


funnel_gap_z_max = 4
funnel_gap_z = 2

funnel_wall_thickness = 5 + funnel_gap_z_max - funnel_gap_z
funnel_length = 150
funnel_edge_fillet_inner_diameter = 4.1
# funnel_edge_fillet_diameter = 15
funnel_edge_fillet_diameter = funnel_edge_fillet_inner_diameter + 2 * funnel_wall_thickness

funnel_edge_support_length = funnel_length / 2

cavity_width = 40

funnel_screw_support_diameter = funnel_edge_fillet_diameter * 1.0
funnel_screw_y_offset = funnel_gap_z_max + funnel_screw_support_diameter / 2

funnel_edge_screw_x = [
    edge_width / 2,
    edge_width / 2 + funnel_edge_support_length - funnel_edge_fillet_diameter,
]


CHEAP = False

heatsert_diameter = Screw('m4').heatsert_diameter * 10
heatsert_long_depth = Screw('m4').heatsert_long_depth * 10
screw_head_diameter = Screw('m4').screw_head_diameter * 10
screw_head_thickness = Screw('m4').screw_head_thickness * 10
screw_diameter_tight = Screw('m4').screw_diameter_tight * 10
screw_diameter_loose = Screw('m4').screw_diameter_loose * 10

handle_heatsert_diameter = Screw('m6').heatsert_diameter * 10


def calc_ball_bearing():
    b = (
        cq.Workplane("XY")
            .circle(ball_bearing_outer_diameter / 2)
            .circle(ball_bearing_inner_diameter / 2)
            .extrude(ball_bearing_inner_depth)
            .translate((0, 0, (ball_bearing_depth - ball_bearing_inner_depth) / 2))
    )
    b = b.union((
        cq.Workplane("XY")
            .circle(ball_bearing_outer_diameter / 2)
            .circle(ball_bearing_outer_diameter / 2 - ball_bearing_outer_ring_thickness)
            .extrude(ball_bearing_depth)
    ))
    b = b.union((
        cq.Workplane("XY")
            .circle(ball_bearing_inner_diameter / 2 + ball_bearing_inner_ring_thickness)
            .circle(ball_bearing_inner_diameter / 2)
            .extrude(ball_bearing_depth)
    ))
    return b


def calc_teeth_depth_comparison():
    b = (
        cq.Workplane("XY")
            .circle(gear_target_diameter / 2)
            .circle((gear_target_diameter - 2 * gear_teeth_depth) / 2)
            .extrude(gear_depth)
            .translate((0, 0, -gear_depth))
    )
    return b


def calc_gear(depth):
    bore_d = gear_steel_bore_rod_diameter if gear_steel_bore_rod_diameter else 10
    if CHEAP:
        return (
            cq.Workplane("XY")
                .circle(gears_ctc / 2)
                .circle(bore_d / 2)
                .extrude(depth)
        )

    # Create a gear object with the SpurGear class
    spur_gear = (
        SpurGear(
            module=gear_module,
            pressure_angle=30,
            teeth_number=gear_num_teeth,
            width=depth,
            bore_d=bore_d,
            backlash=0.0,
            clearance=0.0,
        )
    )
    # print(f"spur_gear.r0={spur_gear.r0}")
    # print(f"spur_gear.ra={spur_gear.ra}")
    # print(f"spur_gear.rd={spur_gear.rd}")
    gear = cq.Workplane('XY').gear(spur_gear)

    if gear_steel_bore_rod_diameter is None:
        gear = gear.union((
            cq.Workplane("XY")
                .circle(bore_d / 2)
                .extrude(depth)
        ))

    return gear


# Build this gear using the gear function from cq.Workplane
gear = (
    calc_gear(depth=-gear_depth + gear_to_side_margin * 2)
        .translate((0, 0, - gear_to_side_margin))
        .union((
        cq.Workplane("XY")
            .circle(ball_bearing_inner_diameter / 2)
            .circle(gear_steel_bore_rod_diameter / 2)
            .extrude(gear_depth + (edge_depth) * 2)
            .translate((0, 0, - gear_depth - edge_depth))
    ))
        .union((
        cq.Workplane("XY")
            .circle(ball_bearing_inner_diameter / 2 + ball_bearing_inner_ring_thickness * 1.5)
            .circle(gear_steel_bore_rod_diameter / 2)
            .extrude(-gear_to_side_margin)
    ))
        .rotate((0, 0, 0), (1, 0, 0), 90)
)
bb = (
    calc_ball_bearing()
        .rotate((0, 0, 0), (1, 0, 0), 90)
)

teeth_comparison = (
    calc_teeth_depth_comparison()
        .rotate((0, 0, 0), (1, 0, 0), 90)
)

teeth_comparison = (
    (
        teeth_comparison.translate((0, 0, edge_height / 2 + gears_ctc / 2))
    )
        .union((
        teeth_comparison.translate((0, 1, edge_height / 2 - gears_ctc / 2))
    ))
)

def calc_heatsert_with_screw_through(length=100):
    heatsert_with_screw_through = (
        (
            cq.Workplane("XY")
                .circle(screw_diameter_tight / 2)
                .extrude(length)
        )
            .union((
            cq.Workplane("XY")
                .circle(screw_diameter_tight / 2)
                .extrude(-edge_depth)
        ))
            .union((
            cq.Workplane("XY")
                .circle(screw_head_diameter / 2)
                .extrude(screw_head_thickness)
                .translate((0, 0, -edge_depth))
        ))
            .union((
            cq.Workplane("XY")
                .circle(screw_head_diameter / 2)
                .workplane(offset=(screw_head_diameter - screw_diameter_tight) / 2)
                .circle(screw_diameter_tight / 2)
                .loft(combine=True)
                .translate((0, 0, -edge_depth + screw_head_thickness))
        ))
            .union((
            cq.Workplane("XY")
                .circle(heatsert_diameter / 2)
                .extrude(heatsert_long_depth)
        ))
    )
    return heatsert_with_screw_through


heatsert_with_screw_through = calc_heatsert_with_screw_through()

edge_screws_base = (
    (
        heatsert_with_screw_through
            .translate((edge_screw_ctc / 2, -edge_depth / 2, 0))
    )
        .union((
        heatsert_with_screw_through
            .translate((-edge_screw_ctc / 2, -edge_depth / 2, 0))
    ))
        .translate((0, 0, -edge_height / 2))
        .mirror(mirrorPlane='XY', union=True)
        .translate((0, 0, edge_height / 2))
)

edge_funnel_screws = []
for x in funnel_edge_screw_x:
    for sign in [-1, 1]:
        edge_funnel_screws.append((
            heatsert_with_screw_through
                .rotate((0, 0, 0), (1, 0, 0), -90)
                .translate((x, 0, edge_height / 2 + sign * funnel_screw_y_offset))
        ))

edge_funnel_screws = wg_functions.union_shapes_list(edge_funnel_screws)

edge_screws = (
    edge_screws_base
        .union((
        edge_screws_base.translate((0, gear_depth + edge_depth))
    ))
        .union((
        (
            calc_heatsert_with_screw_through(length=20)
                .rotate((0, 0, 0), (0, 1, 0), 180)
                .translate((0, 0, gear_to_handle_base_depth + handle_shaft_tab_depth))
        )
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .translate((0, 0, gears_ctc / 2 + edge_height / 2))
    ))
        .union(edge_funnel_screws)
    # .union((
    #     heatsert_with_screw_through
    #     .translate((0, 0, gear_to_handle_base_depth + handle_depth))
    #     .translate((0, -handle_shaft_length + ball_bearing_inner_diameter, 0))
    #     .rotate((0, 0, 0), (1, 0, 0), 90)
    #     .translate((0, 0, edge_height / 2 + gears_ctc / 2))
    # ))
)

edge = (
    (
        cq.Workplane("XY")
            .rect(edge_width, edge_height)
            .pushPoints([(0, gears_ctc / 2), (0, -gears_ctc / 2)])
            .circle(ball_bearing_outer_diameter / 2)
            .extrude(ball_bearing_depth)
    )
        .union((
        cq.Workplane("XY")
            .rect(edge_width, edge_height)
            .pushPoints([(0, gears_ctc / 2), (0, -gears_ctc / 2)])
            .circle(ball_bearing_outer_diameter / 2 - ball_bearing_outer_ring_thickness * 1.5)
            .extrude(edge_depth - ball_bearing_depth)
            .translate((0, 0, ball_bearing_depth))
    ))
    .union((
        cq.Workplane("XY")
            .center(edge_width / 2, 0)
            .moveTo(0, edge_height / 3)
            .lineTo(funnel_edge_support_length, (funnel_screw_y_offset + funnel_screw_support_diameter / 2))
            .lineTo(funnel_edge_support_length, -(funnel_screw_y_offset + funnel_screw_support_diameter / 2))
            .lineTo(0, -edge_height / 3)
            .close()
            .extrude(edge_depth)
    ))
        .rotate((0, 0, 0), (1, 0, 0), 90)
        .translate((0, 0, gears_center_z))
        .cut(edge_screws)
)

edge_top = (
    (
        cq.Workplane("XY")
            .rect(edge_width, gear_depth + 2 * edge_depth)
            .extrude(edge_depth)
            .translate((0, gear_depth / 2, 0))
            .translate((0, 0, edge_height))
    )
        .cut(edge_screws)
)

hole_x = base_width / 2 - 15
hole_y = base_height / 2 - 15
edge_bot = (
    (
        cq.Workplane("XY")
            .rect(base_width, base_height)
            .pushPoints([
            (hole_x, hole_y),
            (hole_x, -hole_y),
            (-hole_x, hole_y),
            (-hole_x, -hole_y),
        ])
            .circle(screw_diameter_loose / 2)
            .extrude(-edge_depth)
            .translate((base_offset, 0, 0))
            .translate((0, gear_depth / 2, 0))
    )
        .cut(edge_screws)
)

edge_left = edge
edge_right = (
    edge
        .mirror(mirrorPlane='XZ', union=False)
        .translate((0, gear_depth, 0))
)

gear_top = (
    gear
        .union((
        cq.Workplane("XY")
            .circle(ball_bearing_inner_diameter / 2)
            .extrude(gear_to_handle_base_depth + handle_shaft_tab_depth - edge_depth)
            .translate((0, 0, edge_depth))
            .rotate((0, 0, 0), (1, 0, 0), 90)
    ))
        .cut((
        cq.Workplane("XY")
            .circle(ball_bearing_inner_diameter / 2)
            .extrude(handle_shaft_tab_depth)
            .cut((
            cq.Workplane("XY")
                .rect(handle_inner_tabs_width, ball_bearing_inner_diameter)
                .extrude(handle_shaft_tab_depth)
        ))
            .translate((0, 0, gear_to_handle_base_depth))
            .rotate((0, 0, 0), (1, 0, 0), 90)
    ))
        .translate((0, 0, gears_ctc / 2))
        .translate((0, 0, gears_center_z))
        .cut(edge_screws)
)
gear_bot = (
    gear
        .rotate((0, 0, 0), (0, 1, 0), 180 / gear_num_teeth)
        .translate((0, 0, -gears_ctc / 2))
        .translate((0, 0, gears_center_z))
)

handle_shaft = (
    (
        cq.Workplane("XY")
            .rect(ball_bearing_inner_diameter, handle_shaft_length)
            .pushPoints([(0, -handle_shaft_length / 2 + ball_bearing_inner_diameter / 2)])
            .circle(handle_heatsert_diameter / 2)
            .extrude(handle_depth)
            .edges("|Z")
            .fillet(ball_bearing_inner_diameter * 0.45)
            # .cut((
            #     cq.Workplane("XY")
            #         .circle(screw_head_diameter / 2)
            #         .extrude(handle_depth)
            #         .translate((0, 0, -handle_depth / 2))
            #         .translate((0, -handle_shaft_length / 2 + ball_bearing_inner_diameter / 2, 0))
            # ))
            .cut((
            cq.Workplane("XY")
                .rect(handle_inner_tabs_width + 2 * printer_margin, ball_bearing_inner_diameter + 2 * printer_margin)
                .extrude(handle_shaft_tab_depth)
                .translate((0, +handle_shaft_length / 2 - ball_bearing_inner_diameter / 2, 0))
        ))
            .translate((0, -handle_shaft_length / 2 + ball_bearing_inner_diameter / 2, 0))
            .translate((0, 0, gear_to_handle_base_depth))
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .translate((0, 0, edge_height / 2 + gears_ctc / 2))
            .cut(edge_screws)
    )
)

# handle = (
#     (
#         cq.Workplane("XY")
#             .circle(ball_bearing_inner_diameter * 0.75 / 2)
#             .extrude(handle_length)
#             .translate((0, 0, gear_to_handle_base_depth + handle_depth))
#             .translate((0, -handle_shaft_length + ball_bearing_inner_diameter, 0))
#             .rotate((0, 0, 0), (1, 0, 0), 90)
#             .translate((0, 0, edge_height / 2 + gears_ctc / 2))
#             .cut(edge_screws)
#     )
# )

funnel_edge_vert_fillet = (
    cq.Workplane("XY")
        .circle(funnel_edge_fillet_diameter / 2)
        .extrude(funnel_gap_z + funnel_edge_fillet_diameter)
        .cut((
            cq.Workplane("XY")
                .rect(funnel_edge_fillet_diameter, funnel_edge_fillet_diameter)
                .extrude(funnel_gap_z + funnel_edge_fillet_diameter)
                .translate((funnel_edge_fillet_diameter / 2, 0, 0))
        ))
        .translate((0, 0, -(funnel_gap_z + funnel_edge_fillet_diameter) / 2))
        .rotate((0, 0, 0), (0, 0, 1), -90)
        .rotate((0, 0, 0), (1, 0, 0), 90)
        .translate((funnel_length / 2, 0, 0))
        .translate((0, 0, edge_depth - funnel_edge_fillet_diameter / 2))
)

funnel_edge_solid = (
    (
        cq.Workplane("XY")
            .rect(funnel_length, funnel_gap_z + 2 * funnel_edge_fillet_diameter)
            .extrude(edge_depth)
    )
    .union((
        cq.Workplane("XY")
            .rect(funnel_length + funnel_edge_fillet_diameter, funnel_gap_z + 1 * funnel_edge_fillet_diameter)
            .extrude(edge_depth - funnel_edge_fillet_diameter / 2)
    ))
        .union((
        cq.Workplane("XY")
            .circle(funnel_edge_fillet_diameter * 0.5)
            .extrude(edge_depth)
            .translate((funnel_length / 2, 0, 0))
            .translate((0, funnel_gap_z / 2 + funnel_edge_fillet_diameter / 2, 0))
            .mirror(mirrorPlane='XZ', union=True)
    ))
)

funnel_screw_support = (
    cq.Workplane("XY")
        .circle(funnel_screw_support_diameter / 2)
        .extrude(cavity_width)
        .edges(">Z")
        .fillet(funnel_wall_thickness * 0.45)
)

funnel = (
    (
        cq.Workplane("XY")
            .rect(funnel_length, funnel_wall_thickness)
            .extrude(cavity_width)
            .edges(">Z")
            .fillet(funnel_wall_thickness * 0.45)
    )
        .union((
        cq.Workplane("XY")
            .circle(funnel_edge_fillet_diameter / 2)
            .extrude(cavity_width)
            .edges(">Z")
            .fillet(funnel_wall_thickness * 0.45)
            .translate((funnel_length / 2, - funnel_edge_fillet_diameter / 2 + funnel_wall_thickness / 2, 0))
    ))
        .translate((0, - funnel_gap_z / 2 - funnel_wall_thickness / 2, 0))

        .union((
            funnel_screw_support
                .translate((funnel_edge_screw_x[0] - funnel_length/2, - funnel_screw_y_offset, 0))
        ))
        .union((
            funnel_screw_support
                # .translate((funnel_edge_support_length / 2 - funnel_edge_fillet_diameter, - funnel_edge_fillet_diameter / 2 + funnel_wall_thickness / 2, 0))
                .translate((funnel_edge_screw_x[1] - funnel_length/2, - funnel_screw_y_offset, 0))
        ))

        .mirror(mirrorPlane='XZ', union=True)
        .union(funnel_edge_solid)
        .union(funnel_edge_vert_fillet)
        .translate((0, 0, -cavity_width / 2))
        .rotate((0, 0, 0), (1, 0, 0), 90)
        .translate((0, cavity_width / 2, 0))
        .translate((funnel_length / 2, 0, 0))
        .translate((0, 0, edge_height / 2))
)

for sign in [-1, 1]:
    funnel = funnel.cut((
        cq.Workplane("XY")
            .circle(gear_target_diameter / 2 + 2)
            .extrude(gear_depth)
            .rotate((0, 0, 0), (1, 0, 0), -90)
            .translate((0, 0, edge_height / 2 + sign * gears_ctc / 2))
    ))

funnel_left = (
    funnel
        .mirror(mirrorPlane='XZ', union=False)
        .translate((0, cavity_width, 0))
        .cut(edge_funnel_screws)
)

funnel_right = (
    funnel_left
        .mirror(mirrorPlane='XZ', union=False)
        .translate((0, gear_depth, 0))
)

bbs = (
    (
        bb.translate((0, 0, gears_ctc / 2))
    )
        .union((
        bb.translate((0, gear_depth + ball_bearing_depth, gears_ctc / 2))
    ))
        .union((
        bb.translate((0, 0, -gears_ctc / 2))
    ))
        .union((
        bb.translate((0, gear_depth + ball_bearing_depth, -gears_ctc / 2))
    ))
        .translate((0, 0, gears_center_z))
)

# show_object(gear, name=f'gear')
# show_object(edge, name=f'edge')
show_object(edge_left, name=f'edge_left')
show_object(edge_right, name=f'edge_right')
show_object(edge_top, name=f'edge_top')
show_object(edge_bot, name=f'edge_bot')

# show_object(funnel, name=f'funnel', options={"color": (165, 40, 40)})
show_object(funnel_left, name=f'funnel_left', options={"color": (165, 40, 40)})
show_object(funnel_right, name=f'funnel_right', options={"color": (165, 40, 40)})

show_object(handle_shaft, name=f'handle_shaft')
# show_object(handle, name=f'handle')

# show_object(edge_funnel_screws, name=f'edge_funnel_screws', options={"color": (40, 165, 40)})
show_object(edge_screws, name=f'edge_screws', options={"color": (40, 165, 40)})

# show_object(gear, name=f'gear')
show_object(gear_top, name=f'gear_top')
show_object(gear_bot, name=f'gear_bot')
show_object(teeth_comparison, name=f'teeth_comparison', options={"color": (40, 40, 165), "alpha": 0.5})
# show_object(bb, name=f'bb', options={"color": (40, 40, 40)})
show_object(bbs, name=f'bbs', options={"color": (40, 40, 40)})
