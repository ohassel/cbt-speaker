import cadquery as cq
import math
import wg_functions

num_magnets_rows = 5
num_magnets_lengthwise = 17
magnet_length = 1.2
magnet_width = 0.5
magnet_height = 0.3

plate_top_bottom_margin = 0.2

# planar_height = 20
planar_height = num_magnets_lengthwise * magnet_length
planar_width = 7
planar_thickness = 0.12

radiating_area_width = 4
gap_width_between_magnets = (radiating_area_width - (num_magnets_rows - 2) * magnet_width) / (num_magnets_rows - 1)

magnet = (
    cq.Workplane("XY")
        .rect(magnet_length, magnet_width)
        .extrude(magnet_height)
)

base = (
    cq.Workplane("XY")
        .rect(planar_height, radiating_area_width)
        .extrude(0.1)
)

magnets = []

for i in range(0, math.ceil(num_magnets_rows / 2)):
    magnets.append(magnet.translate((0, i * (gap_width_between_magnets + magnet_width), 0)))

plate = (
    cq.Workplane("XY")
        .rect(planar_height, planar_width)
        .extrude(planar_thickness)
)

mmagnets = wg_functions.union_shapes_list(magnets)
mmagnets = mmagnets.mirror(mirrorPlane='XZ', union=True)

magnets_cut = (
    cq.Workplane("XY")
        .rect(magnet_length - plate_top_bottom_margin, radiating_area_width)
        .extrude(planar_thickness)
        # .cut((
        # cq.Workplane("XY")
        #     .rect(plate_top_bottom_margin, radiating_area_width)
        #     .extrude(planar_thickness)
        # ))
        .cut(mmagnets)
)

magnets_cuts = []
# magnets_cut = magnets_cut.union(magnets_cut.translate((magnet_length, 0, 0)))
# magnets_cut = magnets_cut.union(magnets_cut.translate((-magnet_length, 0, 0)))
for i in range(0, math.ceil(num_magnets_lengthwise / 2)):
    # print(i)
    magnets_cuts.append(magnets_cut.translate((i * magnet_length, 0, 0)))

magnets_cut = wg_functions.union_shapes_list(magnets_cuts)


magnets_cut = magnets_cut.intersect((
    cq.Workplane("XY")
        .rect(planar_height - plate_top_bottom_margin * 1.5, planar_width)
        .extrude(planar_thickness)
))

magnets_cut = (magnets_cut
               .edges("|Z")
               .fillet(0.2)
               )

plate = plate.edges("|Z")
plate = plate.fillet(0.2)

screw_hole = (
    cq.Workplane("XY")
        .circle(0.3 / 2)
        .extrude(planar_thickness)
)

screw_holes = []

screw_edge_offset = 1.0

# for i in [0, 1, 2]:
for i in range(0, math.ceil(num_magnets_lengthwise / 2)):
    x = magnet_length * (0.0 + i)
    y = (planar_width - screw_edge_offset) / 2
    screw_holes.append(
        screw_hole.translate((x, y, 0))
    )

screw_holes = wg_functions.union_shapes_list(screw_holes)

screw_holes = screw_holes.mirror(mirrorPlane='XZ', union=True)
screw_holes = screw_holes.mirror(mirrorPlane='YZ', union=True)

magnets_cut = magnets_cut.mirror(mirrorPlane='YZ', union=True)

plate = plate.cut(magnets_cut)
plate = plate.cut(screw_holes)

plate = wg_functions.scale_cm_to_mm(plate)
mmagnets = wg_functions.scale_cm_to_mm(mmagnets)

# show_object(base, name=f'base', options={"color": (125, 45, 45)})
show_object(mmagnets, name=f'mmagnets', options={"color": (120, 120, 120)})
# show_object(magnets_cut, name=f'magnets_cut', options={"color": (120, 120, 120)})

show_object(plate, name=f'plate', options={"color": (120, 120, 120)})

cq.exporters.export(
    plate,
    'diy_planar_front_rear_plate.step'
)

cq.exporters.export(
    plate,
    'diy_planar_front_rear_plate.dxf'
)
