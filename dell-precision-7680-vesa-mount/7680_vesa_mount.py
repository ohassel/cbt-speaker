import cadquery as cq

from geometry_2d import P as P
import geometry_2d


def wd19dc_dock():
    base = (
        cq.Workplane("XY")
            .box(20.5, 9, 3)
    )
    power_cable = (
        cq.Workplane("XY")
            .box(5, 3, 2)
            .translate((2.5 + 20.5 / 2, 0, -1 / 2))
    )
    front_cables = (
        cq.Workplane("XY")
            .box(6, 3, 2)
            .translate((0, 4.5 + 1.5 / 2))
    )
    rear_cables = (
        cq.Workplane("XY")
            .box(18, 3, 2)
            .translate((0, - 4.5 - 1.5 / 2))
    )
    return (
        base
            .union(power_cable)
            .union(front_cables)
            .union(rear_cables)
    )


def tb16_dock():
    base = (
        cq.Workplane("XY")
            .box(14.5, 14.5, 5.5)
    )
    power_cable = (
        cq.Workplane("XY")
            .box(10, 3, 2)
            .translate((14.5 / 2, 0, -1.25))
    )
    rear_cables = (
        cq.Workplane("XY")
            .box(12.5, 6, 4)
            .translate((0, -14.5 / 2, 0))
    )
    return (
        base
            .union(power_cable)
            .union(rear_cables)
    )


def dell_precision_7680(screen_rotation_angle=0):
    base = (
        cq.Workplane("XY")
            .box(36, 26, 2.1)
    )
    left_cables = (
        cq.Workplane("XY")
            .box(10, 13, 1.75)
            .translate((36 / 2, -13 / 2, 0))
    )
    rear_vents = (
        cq.Workplane("XY")
            .box(32, 7, 3)
            .translate((0, -5.75, -2))
    )
    screen_y = 25.75
    min_angle = -180
    max_angle = 10
    if screen_rotation_angle < min_angle or max_angle < screen_rotation_angle:
        raise Exception(f"screen_rotation_angle cannot be angle {screen_rotation_angle}. Must be within {min_angle} < r < {max_angle}")
    screen = (
        cq.Workplane("XY")
            .box(36, screen_y, 1)
            .translate((0, -screen_y / 2, 0))
            .rotate((0, 0, 0), (1, 0, 0), screen_rotation_angle)
            .translate((0, -screen_y / 2, 1))
    )
    return (
        base
            .union(left_cables)
            .cut(rear_vents)
            .union(screen)
    )


wd19dc = wd19dc_dock()
tb16 = tb16_dock().translate((0, 20, 0))
laptop = dell_precision_7680(screen_rotation_angle=10).translate((0, 75, 0))

show_object(wd19dc, name='wd19dc', options={"color": (25, 25, 25)})
show_object(tb16, name='tb16', options={"color": (25, 25, 25)})
show_object(laptop, name='laptop', options={"color": (25, 25, 25)})
